<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {    
	redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$allLessons = Lesson::getAllLessons();

?>
<!DOCTYPE html>
<html lang="el">
<head>	
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Διαχείριση Μαθημάτων</title>
	<?php initHelper::getAnimationCss(); ?>	
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css")?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>
<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Διαχείριση Μαθημάτων
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Διαχείριση Μαθημάτων',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<div class="card card-default" id="animoSort">
							<div class="card-header border-bottom">
								<div class="d-flex align-items-center">
									<div class="card-title my-auto">
										<h4 class="mb-0">Προβολή Καταχωρημένων Μαθημάτων</h4>
										<small class="" id="lessonsCount"></small>
									</div>
									<span class="float-right ml-auto">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#createLessonModal">
											<i class="fas fa-plus mr-2"></i>Προσθήκη Μαθήματος
										</button>
									</span>
								</div>
							</div>
							<div class="card-body border-top">
								<table class="table table-striped table-hover my-1 w-100" id="lessonsTbl">
									<thead>
										<tr>
											<th style="max-width: 35px;">ID</th>
											<th>Όνομα Μαθήματος</th>
											<th>Κωδικός Μαθήματος</th>
											<th>Δημιουργήθηκε</th>
											<th>Τροποποιήθηκε</th>
											<th style="max-width: 140px; min-width: 140px;" class="text-center">Ενέργειες</th>
										</tr>
									</thead>
									<tbody>																			
									</tbody>
								</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include('_parts/footer.php'); ?>
</div>
<?php include('_parts/scripts.php'); ?>
<script src=<?php initHelper::getWebAdminPath("/js/manage-lessons.js") ?>></script>
<?php
//create lesson modal
$modal = new Modal([
    'modalId' => 'createLessonModal',
    'modalSize' => '',
    'modalBgHeaderColor' => 'bg-primary',
    'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge5"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Δημιουργία Νέου Μαθήματος</h4>',
    'modalBody'=> '
	<div class="col-md-12">
	<div class="modal-contents">
	<div class="row">
	<div class="col-md-12 mb-3">
	<label for="addLessonName">Όνομα Μαθήματος <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" name="addLessonName" id="addLessonName" required placeholder="Όνομα Μαθήματος..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-md-12 mb-3">
	<label for="addLessonDesc">Κωδικός Μαθήματος <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" id="addLessonDesc" name="addLessonDesc" required placeholder="Κωδικός Μαθήματος..">
	<div class="invalid-feedback"></div>
	</div>
	</div>
	<div class="row">
	<div class="col-md-12">
	<p class="mb-0" style="font-size: 12px; text-align: justify">
	<i class="fas fa-exclamation-circle mr-1 text-primary fa-120x"></i>
	<b>Προσοχή: </b> Ο κωδικός μαθήματος πρέπει να είναι
	μοναδικός για κάθε μάθημα και αποτελεί τον τρόπο με τον οποίο
	θα φαίνονται τα μαθήματα στην καρτέλα του μαθητή.
	</p>
	</div>
	</div>
	<input type="hidden" name="csrf_token" id="csrf_token" value="'.$_SESSION['csrf_token'].'">
	</div>
	</div>
	',
    'modalFooter' => '<button class="btn btn-success btnw-3" id="addLessonBtn" type="button">Save</button>',
    'animoId' => ''
]);
echo $modal->createModal();

//edit lesson modal
$modal = new Modal([
    'modalId' => 'editLessonModal',
    'modalSize' => '',
    'modalBgHeaderColor' => 'bg-info',
    'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge6"><em class="fas fa-pencil-alt float-left fa-150x mr-2"></em>Επεξεργασία Μαθήματος</h4>',
    'modalBody'=> '
	<div class="col-md-12">
	<div class="modal-contents">
	<input type="text" id="lessonEditId" name="lessonEditId" style="display: none;">
	<div class="row">
	<div class="col-md-12 mb-3">
	<label for="updateLessonName">Όνομα Μαθήματος <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" name="updateLessonName" id="updateLessonName" required placeholder="Όνομα Μαθήματος..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-md-12 mb-3">
	<label for="updateLessonDesc">Κωδικός Μαθήματος <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" id="updateLessonDesc" name="updateLessonDesc" required placeholder="Κωδικός Μαθήματος..">
	<div class="invalid-feedback"></div>
	</div>
	</div>
	<div class="row">
	<div class="col-md-12">
	<p class="mb-0" style="font-size: 12px; text-align: justify">
	<i class="fas fa-exclamation-circle mr-1 text-primary fa-120x"></i>
	<b>Προσοχή: </b> Ο κωδικός μαθήματος πρέπει να είναι
	μοναδικός για κάθε μάθημα και αποτελεί τον τρόπο με τον οποίο
	θα φαίνονται τα μαθήματα στην καρτέλα του μαθητή.
	</p>
	</div>
	</div>
	<input type="hidden" name="csrf_token_edit_lesson" id="csrf_token_edit_lesson" value="'.$_SESSION['csrf_token'].'">
	</div>
	</div>
	',
    'modalFooter' => '<button class="btn btn-success btnw-3" id="saveEditLessonBtn" data-id="" type="button">Save</button>',
    'animoId' => 'editLessonModalAnimo'
]);
echo $modal->createModal();
//delete lesson modal
$modal = new Modal([
    'modalId' => 'deleteLessonModal',
    'modalSize' => '',
    'modalBgHeaderColor' => 'bg-danger',
    'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Μαθήματος</h4>',
    'modalBody'=>'
	<input type="hidden" id="lessonDeleteId" name="lessonDeleteId">
	<input type="hidden" name="delete_csrf_token" id="delete_csrf_token" value="'.$_SESSION['csrf_token'].'">
	<p class="pt-3" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε αυτή τη συντόμευση;</p>
	<p class="pb-3" style="font-size: 90%;"><i class="fas fa-info-circle mr-1 text-info"></i> Σιγουρευτείτε ότι δεν υπάρχει καμία αναφορά σε αυτό το μάθημα (Βαθμολογία, Ανάθεση σε καθηγητή, Μαθητής που να το παρακολουθεί), διαφορετικά η διαγραφή δεν θα πραγματοποιηθεί.</p>
	',
    'modalFooter' => '<button class="btn btn-success btnw-3" data-id="" id="btnConfirmDelete" type="button">Submit</button>',
    'animoId' => ''
]);
echo $modal->createModal();
?>
</body>
</html>