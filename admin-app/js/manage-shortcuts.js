$(document).ready(function () {

	var shortcutsMng = {
		$elements: {
			$createShortcutElements: {
				addShortcutName: $('#addShortcutName'),
				createShortcutModal: $('#createSortcutModal'),
				addShortcutBtn: $('#addShortcutBtn'),
			},
			$editShortcutElements: {
				saveEditShortcutBtn: $('#saveEditShortcutBtn'),
				clearShortcutEditFormBtn: $('#clearShortcutEditFormBtn'),
				editShortcutName: $('#editShortcutName'),
				editSortcutModal: $('#editSortcutModal'),
			},
			$deleteShortcutElements: {
				saveDeleteShortcutBtn: $('#saveDeleteShortcutBtn'),
				deleteSortcutModal: $('#deleteSortcutModal'),
			},
			editLessonsCheckboxes: $('#editLessonBox'),
			createShortcutBtn: $('#createShortcutBtn'),
			shortcutsTbl: $('#shortcutsTbl'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'ManageShortcutsController.php']
		},
		init: function () {
			jQuery.ajax({
				url: shortcutsMng.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('animoSort');
				},
				complete: function (data) {
					removeStandardWhirlAnime('animoSort');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						var tbody = shortcutsMng.$elements.shortcutsTbl.find('tbody');
						var tbodyData = '';
						$.each(response.data, function (key, shortcut) {
							tbodyData += `
									<tr id="${shortcut.id}">
									<td><b>${shortcut.id}</b></td>
									<td>${shortcut.name}</td>
									<td>${shortcut.lessonsAsString}</td>
									<td data-sort="${shortcut.created_at}">${shortcut.created_at ? Helpers.dateConverter(shortcut.created_at) : ''}</td>
									<td data-sort="${shortcut.updated_at}">${shortcut.updated_at ? Helpers.dateConverter(shortcut.updated_at) : ''}</td>
									<td class="text-center">
										<button 
										class="btn btn-sm btn-info mr-1 command-edit editShortcutBtn"
										title="Επεξεργασία Μαθήματος ${shortcut.name}"
										data-toggle="modal" 
										data-target="#editSortcutModal"
										data-id="${shortcut.id}">
										<em class="fas fa-pencil-alt fa-fw"></em>
										</button>
										<button
											data-id="${shortcut.id}"
											data-toggle="modal" 
											data-target="#deleteSortcutModal"
											class="btn btn-sm btn-danger mr-1 command-edit deleteShortcutBtn"
											title="Διαγραφή μαθήματος ${shortcut.name}">
											<em class="fas fa-trash fa-fw"></em>
										</button>
									</td>
									</tr>
								`;
						});
						$(tbody).empty().append(tbodyData);
						$('#shortcutsCount').text('Καταχωρήσεις: ' + response.data.length);
						shortcutsMng.handleEditShortcut();
						$('.deleteShortcutBtn').on('click', function () {
							shortcutsMng.$elements.$deleteShortcutElements.saveDeleteShortcutBtn.data('id', $(this).data('id'));
						});
					} else {
						alert(response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						shortcutsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						shortcutsMng.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			shortcutsMng.$elements.alertBox.removeClass();
			shortcutsMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			shortcutsMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			shortcutsMng.$elements.alertBox.find('#alertMessage').html(message);
			shortcutsMng.$elements.alertBox.show();
		},
		handleEditShortcut: function () {
			$(".editShortcutBtn").click(function () {
				shortcutsMng.$elements.$editShortcutElements.saveEditShortcutBtn.data('id', $(this).data('id'));
				jQuery.ajax({
					url: shortcutsMng.getEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_SINGLE_DATA",
						"Method": "GET",
					},
					data: {
						shortcutId: shortcutsMng.$elements.$editShortcutElements.saveEditShortcutBtn.data('id'),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('editSortcutModalAnimo');
					},
					complete: function (data) {
						removeStandardWhirlAnime('editSortcutModalAnimo');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							shortcutsMng.$elements.$editShortcutElements.editShortcutName.val(response.data.name);
							shortcutsMng.unselectModalCheckboxes();
							$(response.data.lessons).each(function () {
								$('#e_' + $(this).attr('id')).prop("checked", true);
							});
						} else {
							shortcutsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							shortcutsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							shortcutsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});

			});
		},
		handleSaveEditShortcut: function () {
			shortcutsMng.$elements.$editShortcutElements.clearShortcutEditFormBtn.on('click', function () {
				shortcutsMng.unselectModalCheckboxes();
			});

			shortcutsMng.$elements.$editShortcutElements.saveEditShortcutBtn.on('click', function () {
				var btnDiv = shortcutsMng.$elements.$editShortcutElements.saveEditShortcutBtn.parent();
				var lessonsToAdd = [];
				$("input[name='shortcut_lessons_edit[]']:checked").each(function () {
					lessonsToAdd.push(this.value);
				});
				jQuery.ajax({
					url: shortcutsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						shortcutEditId: shortcutsMng.$elements.$editShortcutElements.saveEditShortcutBtn.data('id'),
						editShortcutName: shortcutsMng.$elements.$editShortcutElements.editShortcutName.val(),
						shortcut_lessons_edit: lessonsToAdd,
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div class="loaderDiv"><div style='float:left; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait </div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.loaderDiv').remove();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						shortcutsMng.$elements.$editShortcutElements.editSortcutModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							shortcutsMng.manageAlertBox('success', response.data);
							shortcutsMng.init();
						} else {
							shortcutsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							shortcutsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							shortcutsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleDeleteShortcut: function () {
			shortcutsMng.$elements.$deleteShortcutElements.saveDeleteShortcutBtn.on('click', function () {
				var btnDiv = shortcutsMng.$elements.$deleteShortcutElements.saveDeleteShortcutBtn.parent();
				jQuery.ajax({
					url: shortcutsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						shortcutDeleteId: shortcutsMng.$elements.$deleteShortcutElements.saveDeleteShortcutBtn.data('id'),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						shortcutsMng.$elements.$deleteShortcutElements.deleteSortcutModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							shortcutsMng.manageAlertBox('success', response.data);
							shortcutsMng.init();
						} else {
							shortcutsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							shortcutsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							shortcutsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleCreateShortcut: function () {
			var btnDiv = shortcutsMng.$elements.$createShortcutElements.addShortcutBtn.parent();
			shortcutsMng.$elements.$createShortcutElements.addShortcutBtn.on('click', function () {
				var lessonsToAdd = [];
				$("input[name='shortcut_lessons[]']:checked").each(function () {
					lessonsToAdd.push(this.value);
				});
				jQuery.ajax({
					url: shortcutsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						shortcut_lessons: lessonsToAdd,
						addShortcutName: shortcutsMng.$elements.$createShortcutElements.addShortcutName.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						shortcutsMng.$elements.$createShortcutElements.createShortcutModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							shortcutsMng.manageAlertBox('success', response.data);
							shortcutsMng.init();
						} else {
							shortcutsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							shortcutsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							shortcutsMng.manageAlertBox('danger', errorThrown);
						}
						shortcutsMng.$elements.$createShortcutElements.createShortcutModal.modal('hide');
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		bindButtonActions: function () {
			shortcutsMng.handleCreateShortcut();
			shortcutsMng.handleSaveEditShortcut();
			shortcutsMng.handleDeleteShortcut();
		},
		unselectModalCheckboxes() {
			shortcutsMng.$elements.editLessonsCheckboxes.find(':input').each(function () {
				$(this).prop("checked", false);
			})
		},
	}
	//called on first load
	shortcutsMng.init();
	shortcutsMng.bindButtonActions();
});