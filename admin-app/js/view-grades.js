$(document).ready(function () {

    var viewGradesJs = {
        $elements: {
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                lessons: $('#searchLessons'),
                lessonIds: $('#searchLessonIds'),
                studentId: $('#searchStudent'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
                searchLoader: $('#searchingLoader'),
                lessonModal: $('#lessonsModal'),
                removeLessonsBtn: $('.removeLessonsBtn'),
            },
            modals: {
                gradeLessonsPicker: {
                    theModal: $('#lessonsModal'),
                    saveLessons: $('#saveLessons'),
                },
                searchModal: {
                    theModal: $('#filterGradesModal'),
                },
            },
            viewGradesCard: $('#viewGradesCard'),
            viewGradesTbl: $('#view_grades_tbl'),
            gradeStatsTbl: $('#stats_grades_tbl'),
            assignmentsBox: $('#assignmentsBox'),
            alertBox: $('#alertBox'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
            url: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
            statsEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php'],
        },
        resetSearchFields: function () {
            Helpers.emptyFields(viewGradesJs.$elements.searchElements);
            unselectAll();
        },
        getEndPoint: function () {
            return viewGradesJs.$elements.url.join('/');
        },
        getSearchEndPoint: function () {
            return viewGradesJs.$elements.searchEndpointUrl.join('/');
        },
        getStatsEndPoint: function () {
            return viewGradesJs.$elements.statsEndpointUrl.join('/');
        },
        init: function () {
            viewGradesJs.initViewGradesTable();
            viewGradesJs.bindSaveLessons();
            viewGradesJs.bindRemoveLessons();
            viewGradesJs.bindSearchBtn();
            viewGradesJs.bindResetSearchBtn();
            viewGradesJs.initStatsTable();
        },
        bindSaveLessons: function () {
            viewGradesJs.$elements.modals.gradeLessonsPicker.saveLessons.on('click', function () {
                var lessons = '';
                var ids = '';
                $('#assignmentsBox').find(':input').each(function () {
                    if ($(this).prop("checked")) {
                        lessons += $(this).attr("name") + ", ";
                        ids += $(this).attr("id") + ",";
                    }
                });
                viewGradesJs.$elements.searchElements.lessons.val(lessons.substring(0, lessons.length - 2));
                viewGradesJs.$elements.searchElements.lessonIds.val(ids.substring(0, ids.length - 1));
                viewGradesJs.$elements.modals.gradeLessonsPicker.theModal.modal('hide');
            });
        },
        bindRemoveLessons: function () {
            viewGradesJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
                Helpers.emptyField(viewGradesJs.$elements.searchElements.lessons);
                Helpers.emptyField(viewGradesJs.$elements.searchElements.lessonIds);
                unselectAll();
            });
        },
        bindSearchBtn: function () {
            viewGradesJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!viewGradesJs.isTelestisAccepted(viewGradesJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterGradesModal').show();
                var searchData = {
                    user_id: viewGradesJs.$elements.searchElements.teacher.val(),
                    exam_kind: viewGradesJs.$elements.searchElements.examKind.val(),
                    absence: viewGradesJs.$elements.searchElements.absence.val(),
                    grade: viewGradesJs.$elements.searchElements.telestis.val() + ":" + viewGradesJs.$elements.searchElements.grade.val(),
                    date_from: viewGradesJs.$elements.searchElements.examDateFrom.val(),
                    date_to: viewGradesJs.$elements.searchElements.examDateTo.val(),
                    lesson_id: viewGradesJs.$elements.searchElements.lessonIds.val(),
                    student_id: viewGradesJs.$elements.searchElements.studentId.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                        console.log(jqXHR);
                    },
                });
            });
        },
        bindResetSearchBtn: function () {
            $('#removeFilterGradesModal').on("click", function () {
                $('#removeFilterGradesModal').hide();
                viewGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            viewGradesJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterGradesModal').hide();
                viewGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        initViewGradesTable: function () {
            jQuery.ajax({
                url: viewGradesJs.getEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_ALL_DATA",
                    "Method": "GET",
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSort');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSort');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewGradesJs.renderGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        viewGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        viewGradesJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderGradesTableData: function (the_data) {
            destroyDatatable('view_grades_tbl');
            if (the_data == 1) {
                viewGradesJs.$elements.viewGradesTbl.find('tbody').remove();
            } else {
                var cntExam = 0;
                var cntTest = 0;
                var cntErgasia = 0;
                var cntParon = 0;
                var cntApon = 0;
                var tbody = '';
                $.each(the_data, function (key, grade) {
                    grade.absence == "ΠΑΡΩΝ" ? cntParon++ : cntApon++;
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                        cntExam++;
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                        cntErgasia++;
                    } else {
                        cntTest++;
                    }
                    tbody += `
                <tr>
                <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                <td>${grade.teacherLastname} ${grade.teacherFirstname}</td>
                <td><a href="view-student?sid=${Helpers.utf8_to_b64(grade.student_id)}">${grade.studentLastname} ${grade.studentFirstname}</a></td>
                <td><a href="view-lesson.php?lesid=${Helpers.utf8_to_b64(grade.lesson_id)}&lesname=${Helpers.utf8_to_b64(grade.lessonName)}">${grade.lessonName}</a></td>
                <td>${grade.tmimaName}</td>
                <td><span class="badge ${badgeColor}">${grade.exam_kind}</span></td>
                <td><span class="badge ${(grade.absence == "ΠΑΡΩΝ") ? 'badge-success' : 'badge-danger'}">${grade.absence}</span></td>
                <td><b>${grade.grade}</b></td>
                <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                </tr>
                `;
                });
                viewGradesJs.$elements.viewGradesTbl.find('tbody').empty().append(tbody);
                var tfooter = `
                <div class="d-flex justify-content-between py-1">
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-purple">ΔΙΑΓΩΝΙΣΜΑ</span> : ${cntExam}
                        </div>
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-primary">TEST</span> : ${cntTest}
                        </div>
                        <div>
                            <span class="badge badge-green">ΕΡΓΑΣΙΑ</span> : ${cntErgasia}
                        </div>                    
                    </div>
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-success">ΠΑΡΩΝ</span> : ${cntParon}
                        </div>
                        <div>
                            <span class="badge badge-danger">AΠΩΝ</span> : ${cntApon}
                        </div>
                    </div>
                </div>
                `;
                viewGradesJs.$elements.viewGradesCard.find('div.card-footer').empty().append(tfooter);
            }
            viewGradesJs.initViewGradeDatatable();
        },
        initStatsTable: function () {
            jQuery.ajax({
                url: viewGradesJs.getStatsEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'all_grades_stats'
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('gradeStatsAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('gradeStatsAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewGradesJs.renderStatsGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        viewGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        viewGradesJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderStatsGradesTableData: function (the_data) {
            destroyDatatable('stats_grades_tbl');
            if (the_data == 1) {
                viewGradesJs.$elements.gradeStatsTbl.find('tbody').remove();
            } else {
                var tbody = '';
                $.each(the_data, function (key, stat) {
                    tbody += `
                    <tr>             
                        <td>
                            <div class="${stat.avgGrades < 50 ? 'text-danger' : 'text-green'}">
                            ${stat.avgGrades < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                            </div>
                        </td>
                        <td>
                            <a href="view-lesson?lesid=${Helpers.utf8_to_b64(stat.lesson_id)}&lesname=${Helpers.utf8_to_b64(stat.lessonName)}">${stat.lessonName}</a>
                        </td>
                        <td>${stat.countApon}</td>
                        <td>${stat.countGrades}</td>
                        <td>${parseFloat(stat.avgGrades).toFixed(0)}%</td>
                        <td>${((parseFloat(stat.avgGrades)*20)/100).toFixed(1)}</td>
                        <td>${stat.maxGrade}%</td>
                        <td>${stat.minGrade}%</td>
                        <td data-sort="${stat.maxCreatedAt}">${Helpers.dateConverter(stat.maxCreatedAt)}</td>
                        <td data-sort="${stat.maxUpdatedAt}">${stat.maxUpdatedAt?Helpers.dateConverter(stat.maxUpdatedAt):''}</td>
                    `;
                });
                viewGradesJs.$elements.gradeStatsTbl.find('tbody').empty().append(tbody);
            }
            viewGradesJs.initGradeStatsDatatable();
            viewGradesJs.initPieChart("chartjs-piechart");
            viewGradesJs.initBarChart('chartjs-barchart');
        },
        initPieChart: function (id) {
            statsData = [];
            labelsData = [];
            var count = 0;
            $('#stats_grades_tbl > tbody > tr').each(function (index, value) {
                labelsData.push($('td:eq(1)', this).text());
                statsData.push($('td:eq(3)', this).text());
                count += parseInt($('td:eq(3)', this).text()) + parseInt($('td:eq(2)', this).text());
            });
            $('.count-card').html(`Συνολικές Καταχωρήσεις: <b>${count}</b>`);
            var pieData = {
                labels: labelsData,
                datasets: [{
                    data: statsData,
                    backgroundColor: ["rgba(128, 0, 0, 0.6)", "rgba(170, 110, 40, 0.6)", "rgba(128, 128, 0, 0.6)", "rgba(0, 128, 128, 0.6)", "rgba(230, 25, 75, 0.6)", "rgba(245, 130, 48, 0.6)",
                        "rgba(255, 225, 25, 0.6)", "rgba(210, 245, 60, 0.6)", "rgba(60, 180, 75, 0.6)", "rgba(0, 130, 200, 0.6)", "rgba(145, 30, 180, 0.6)", "rgba(240, 50, 230, 0.6)",
                        "rgba(128, 128, 128, 0.6)", "rgba(255, 215, 180, 0.6)", "rgba(170, 255, 195, 0.6)", "rgba(230, 190, 255, 0.6)", "rgba(51,51,51, 0.6)", "rgba(229,30,50, 0.6)", "rgba(181,182,30, 0.6)"
                        , "rgba(0,100,0, 0.6)", "rgba(0,0,139, 0.6)", "rgba(0,0,0, 0.6)", "rgba(0,255,255, 0.6)"],
                    borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
                        "rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
                        "rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)", "rgba(51,51,51, 1)", "rgba(229,30,50, 1)", "rgba(181,182,30, 1)"
                        , "rgba(0,100,0, 1)", "rgba(0,0,139, 1)", "rgba(0,0,0, 1)", "rgba(0,255,255, 1)"]
                }]
            };

            var pieOptions = {
                legend: {
                    display: false
                }
            };
            var piectx = document.getElementById(id).getContext('2d');
            var pieChart = new Chart(piectx, {
                data: pieData,
                type: 'pie',
                options: pieOptions
            });
        },
        initBarChart: function (id) {
            statsData = [];
            labelsData = [];
            barColor = [];
            barBorder = [];
            var count = 0;
            var sum = 0;
            var i = 0;
            $('#stats_grades_tbl > tbody > tr').each(function (index, value) {
                labelsData.push($('td:eq(1)', this).find('a').text());
                statsData.push($('td:eq(4)', this).text().slice(0, -1).split(',').join('.'));
                count += 1;
                sum += parseInt($('td:eq(4)', this).text());
                switch (i) {
                    case 0:
                        barColor.push("rgb(255, 224, 230)");
                        barBorder.push("rgb(255,99,132)");
                        i++;
                        break;
                    case 1:
                        barColor.push("rgb(255,245,221)");
                        barBorder.push("rgb(255,205,86)");
                        i++;
                        break;
                    case 2:
                        barColor.push("rgb(215,236,251)");
                        barBorder.push("rgb(54,162,235)");
                        i++;
                        break;
                    case 3:
                        barColor.push("rgb(198,221,221)");
                        barBorder.push("rgb(75,192,192)");
                        i++;
                        break;
                    case 4:
                        barColor.push("rgb(235,224,255)");
                        barBorder.push("rgb(153,102,255)");
                        i++;
                        break;
                    default:
                        barColor.push("rgb(255,236,217)");
                        barBorder.push("rgb(255,159,64)");
                        i = 0;
                        break;
                }
            });
            let avg = (sum / count).toFixed(1);
            $('.avg-card').html(`Συνολικός Μέσος Όρος: <b>${parseFloat(avg).toFixed(0) + '%'} (${(avg * 20 / 100).toFixed(1)}</b>)`);
            var barData = {
                labels: labelsData,
                datasets: [{
                    backgroundColor: barColor,
                    borderColor: barBorder,
                    borderWidth: 1,
                    data: statsData
                }]
            };

            var barOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 100,
                            min: 0,
                            stepSize: 20
                        }
                    }],
                },
                legend: {
                    display: false
                }
            };
            var barctx = document.getElementById(id).getContext('2d');
            var barChart = new Chart(barctx, {
                data: barData,
                type: 'bar',
                options: barOptions
            });
        },
        initViewGradeDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [0, 'desc'],
                    id: 'view_grades_tbl',
                    scrollx: true,
                    scrolly: "70vh",
                    downloadTitle: "Πίνακας Βαθμολογιών Πολύτροπο",
                    buttons: true,
                }

            );
        },
        initGradeStatsDatatable: function () {
            initDatatable(
                {
                    pages: [20, 50, 100, -1],
                    order: [1, 'asc'],
                    id: 'stats_grades_tbl',
                    columnDefs: [{
                        targets: 0,
                        orderable: false
                    }],
                    scrollx: true,
                    scrolly: "70vh",
                    downloadTitle: "Στατιστικά ανά Μάθημα",
                    buttons: true,
                }

            );
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || viewGradesJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    }

    viewGradesJs.init();
});
function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}