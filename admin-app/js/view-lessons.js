$(document).ready(function () {
    var viewLessonJs = {
        $elements: {
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                student: $('#searchStudent'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
            },
            modals: {
                searchModal: {
                    theModal: $('#filterLessonGradesModal'),
                },
            },
            assignmentsBox: $('#assignmentsBox'),
            endpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
            gradesEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
            lessonId: null,
            lessonName: null,
            lessonNameHeader: $('#lessonNameHeader'),
            lessonGradesCard: $('#lessonGradesCard'),
            studentsTable: $('#student_tbl'),
            gradesTable: $('#lessonGradesTbl'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
        },
        getEndPoint: function () {
            return viewLessonJs.$elements.endpointUrl.join('/');
        },
        getGradesEndpointUrl: function () {
            return viewLessonJs.$elements.gradesEndpointUrl.join('/');
        },
        getSearchEndPoint: function () {
            return viewLessonJs.$elements.searchEndpointUrl.join('/');
        },
        resetSearchFields: function () {
            Helpers.emptyFields(viewLessonJs.$elements.searchElements);
        },
        init: function () {
            viewLessonJs.$elements.lessonId = Helpers.b64_to_utf8(Helpers.urlParam('lesid'));
            viewLessonJs.$elements.lessonName = Helpers.b64_to_utf8(Helpers.urlParam('lesname'));
            viewLessonJs.$elements.lessonNameHeader.text(viewLessonJs.$elements.lessonName);
            viewLessonJs.initStudentTable();
            viewLessonJs.initLessonGradesData();
            viewLessonJs.bindSearchBtn();
            viewLessonJs.bindResetSearchBtn();
        },
        initStudentTable: function () {
            jQuery.ajax({
                url: viewLessonJs.getEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_SINGLE_DATA",
                    "Method": "GET",
                },
                data: {
                    lessonId: viewLessonJs.$elements.lessonId,
                    fireAction: "students_by_lesson_id"
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('viewLessonStudentAnimate');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('viewLessonStudentAnimate');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewLessonJs.renderStudentsTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    var tbody = `
                    <tr>
                        <td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Συνέβη Σφάλμα:</td>
                    </tr>
                    <tr>
                        <td class="text-center text-bold" style="font-size: 120%" colspan="8">${errorThrown}</td>
                    </tr>`;
                    viewLessonJs.$elements.studentsTable.find('tbody').empty().append(tbody);
                },
            });
        },
        renderStudentsTableData: function (the_data) {
            destroyDatatable('student_tbl');
            if (the_data == 1) {
                viewLessonJs.$elements.studentsTable.find('tbody').remove();
            } else {
                var tbody = '';
                $.each(the_data, function (key, student) {
                    tbody += `
                    <tr>
					<td>${student.id}</td>
					<td>${student.lastname}</td>
					<td>${student.firstname}</td>
					<td>${student.phone}</td>
					<td>${student.mobile}</td>
					<td>
						<a href="mailto:${student.email}">
							${student.email}
						</a>
					</td>
					<td>${student.tmima.name}</td>
					<td>${student.fullAddress}</td>
					<td class="d-none">${student.fphone}</td>
					<td class="d-none">${student.mphone}</td>
					<td class="d-none">${student.class.list_entry}</td>
					<td class="d-none">${student.mname}</td>
					<td class="d-none">${student.faname}</td>
					<td class="d-none">${student.kid_name}</td>
					<td class="d-none">${student.email_kidemona}</td>
					<td class="d-none">${student.kid_phone}</td>
					<td class="d-none">${student.fee}</td>
					<td class="d-none">${student.periodos}</td>
					<td class="d-none">${student.sx_etos}</td>
					<td class="d-none">${Helpers.dateOnlyConverter(student.date_eggrafis)}</td>
					<td class="d-none">${student.informed_by?student.informed_by.list_entry:''}</td>
					<td class="d-none">${student.lessonsAsString}</td>											
					<td class="text-center">
						<a title="Πλήρης Προβολή: ${student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
							<em class="fas fa-external-link-alt fa-fw"></em>
						</a>
						<a href="edit-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-info command-edit text-white" title="Επεξεργασία: ${student.fullName}">
							<em class="fas fa-edit fa-fw"></em>
						</a>
					</td>
				</tr>`
                });
                viewLessonJs.$elements.studentsTable.find('tbody').empty().append(tbody);
                viewLessonJs.initStudentsDatatable();
            }
        },
        initLessonGradesData: function () {
            jQuery.ajax({
                url: viewLessonJs.getGradesEndpointUrl(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    lessonId: viewLessonJs.$elements.lessonId,
                    fireAction: 'grades_by_lesson_id',
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('viewLessonGradesAnimate');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('viewLessonGradesAnimate');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewLessonJs.renderGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    var tbody = `
                    <tr>
                        <td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Συνέβη Σφάλμα:</td>
                    </tr>
                    <tr>
                        <td class="text-center text-bold" style="font-size: 120%" colspan="8">${errorThrown}</td>
                    </tr>`;
                    viewLessonJs.$elements.gradesTable.find('tbody').empty().append(tbody);
                },
            });
        },
        renderGradesTableData: function (the_data) {
            destroyDatatable('lessonGradesTbl');
            var cntExam = 0;
            var cntTest = 0;
            var cntErgasia = 0;
            var cntParon = 0;
            var cntApon = 0;
            if (the_data == 1) {
                viewLessonJs.$elements.gradesTable.find('tbody').remove();
            } else {
                var tbody = '';
                $.each(the_data, function (key, grade) {
                    grade.absence == "ΠΑΡΩΝ" ? cntParon++ : cntApon++;
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                        cntExam++;
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                        cntErgasia++;
                    } else {
                        cntTest++;
                    }
                    tbody += `
                    <tr>                    
                        <td>
                            <div class="${grade.grade < 50 ? 'text-danger' : 'text-green'}">
                            ${grade.grade < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                            </div>
                        </td>
                        <td>${grade.id}</td>
                        <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                        <td>${grade.fullUser.profile.fullname}</td>                        
                        <td>
                            <a href="view-student?sid=${Helpers.utf8_to_b64(grade.fullStudent.id)}">${grade.fullStudent.fullName}</a>
                        </td>
                        <td>
                            <span class="badge ${badgeColor}">${grade.exam_kind}</span>
                        </td>
                        <td>
                            <span class="badge ${grade.absence == "ΠΑΡΩΝ" ? 'badge-success' : 'badge-danger'}">${grade.absence}</span>
                        </td>
                        <td><strong>${grade.grade}</strong></td>
                        <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                        <td>${!Helpers.isEmpty(grade.updated_at) ? grade.updated_at : "Καμία"}</td>
                    </tr>									
                    `
                });
                viewLessonJs.$elements.gradesTable.find('tbody').empty().append(tbody);
            }
            viewLessonJs.initLessonGradesDatatable();
            var tfooter = `
                <div class="d-flex justify-content-between py-1">
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-purple">ΔΙΑΓΩΝΙΣΜΑ</span> : ${cntExam}
                        </div>
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-primary">TEST</span> : ${cntTest}
                        </div>
                        <div>
                            <span class="badge badge-green">ΕΡΓΑΣΙΑ</span> : ${cntErgasia}
                        </div>                    
                    </div>
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-success">ΠΑΡΩΝ</span> : ${cntParon}
                        </div>
                        <div>
                            <span class="badge badge-danger">AΠΩΝ</span> : ${cntApon}
                        </div>
                    </div>
                </div>
                `;
            viewLessonJs.$elements.lessonGradesCard.find('div.card-footer').empty().append(tfooter);
        },
        bindResetSearchBtn: function () {
            $('#removeFilterLessonGradesModal').on("click", function () {
                $('#removeFilterLessonGradesModal').hide();
                viewLessonJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewLessonJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        lessonId: viewLessonJs.$elements.lessonId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewLessonJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewLessonJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            viewLessonJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterLessonGradesModal').hide();
                viewLessonJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewLessonJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        lessonId: viewLessonJs.$elements.lessonId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewLessonJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        bindSearchBtn: function () {
            viewLessonJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!viewLessonJs.isTelestisAccepted(viewLessonJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterLessonGradesModal').show();
                var searchData = {
                    user_id: viewLessonJs.$elements.searchElements.teacher.val(),
                    exam_kind: viewLessonJs.$elements.searchElements.examKind.val(),
                    absence: viewLessonJs.$elements.searchElements.absence.val(),
                    grade: viewLessonJs.$elements.searchElements.telestis.val() + ":" + viewLessonJs.$elements.searchElements.grade.val(),
                    date_from: viewLessonJs.$elements.searchElements.examDateFrom.val(),
                    date_to: viewLessonJs.$elements.searchElements.examDateTo.val(),
                    student_id: viewLessonJs.$elements.searchElements.student.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewLessonJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        lessonId: viewLessonJs.$elements.lessonId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('viewLessonGradesAnimate');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewLessonJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewLessonJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        initStudentsDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [1, 'asc'],
                    id: "student_tbl",
                    columnDefs: [{
                        targets: 22,
                        orderable: false
                    }],
                    scrollx: true,
                    scrolly: "60vh",
                    downloadTitle: "Πίνακας Μαθητών - " + viewLessonJs.$elements.lessonName,
                    buttons: true,
                }
            );
        },
        initLessonGradesDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [2, 'desc'],
                    columnDefs: [{
                        targets: 0,
                        orderable: false
                    }],
                    id: "lessonGradesTbl",
                    scrollx: true,
                    scrolly: "60vh",
                    downloadTitle: "Πίνακας Βαθμολογιών - " + viewLessonJs.$elements.lessonName,
                    buttons: true,
                }
            );
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || viewLessonJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    };
    viewLessonJs.init();
});