$(document).ready(function () {
	var manageClassJs = {
		$elements: {
			modals: {
				editTmimaModal: {
					theModal: $('#editTmimaModal'),
					submitSaveEditClassBtn: $('#submitSaveEditClass'),
					editTmima: $('#editTmima'),
					editTaksi: $('#editTaksi'),
					editComments: $('#editComments'),
				},
				deleteTmimaModal: {
					theModal: $('#deleteTmimaModal'),
					submitDeleteTmimaBtn: $('#submitDeleteTmimaBtn'),
				}
			},
			editTmimaId: null,
			editTmimaBtn: null,
			deleteTmimaBtn: null,
			tmimataTbl: $('#tmimataTbl'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'ClassController.php']
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			manageClassJs.$elements.alertBox.removeClass();
			manageClassJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			manageClassJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			manageClassJs.$elements.alertBox.find('#alertMessage').html(message);
			manageClassJs.$elements.alertBox.show();
		},
		getEndPoint: function () {
			return manageClassJs.$elements.endpointUrl.join('/');
		},
		init: function () {
			manageClassJs.renderTmimataData();
			manageClassJs.sendUpdateTmimaData();
			manageClassJs.sendDeleteTmimaData();
		},
		renderTmimataData: function () {
			jQuery.ajax({
				url: manageClassJs.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('tmimataTblAnimo');
				},
				complete: function (data) {
					removeStandardWhirlAnime('tmimataTblAnimo');
				},
				success: function (response, textStatus, jQxhr) {
					destroyDatatable('tmimataTbl');
					if (response.code === 200) {
						var tbody = "";
						if (response.data == 1) {
							manageClassJs.$elements.tmimataTbl.find('tbody').remove();
						} else {
							$.each(response.data, function (key, tmima) {
								tbody += `
							<tr>
								<td>${tmima.id}</td>
								<td>${tmima.name}</td>
								<td>${tmima.class.list_entry}</td>
								<td>${tmima.comments}</td>
								<td data-sort="${tmima.created_at}">${Helpers.dateConverter(tmima.created_at)}</td>
								<td data-sort="${tmima.updated_at ? tmima.updated_at : ''}">${tmima.updated_at ? Helpers.dateConverter(tmima.updated_at) : ''}</td>
								<td class="text-center">
									<button class="btn btn-sm btn-info command-edit editTmimaBtn" type="button" data-toggle="modal" data-target="#editTmimaModal" data-id="${tmima.id}" title="Επεξεργασία τμήματος ${tmima.name}">
										<em class="fa fa-edit fa-fw"></em>
									</button>
									<button class="btn btn-sm btn-danger command-edit deleteTmimaBtn" type="button" data-toggle="modal" data-target="#deleteTmimaModal" data-id="${tmima.id}" title="Διαγραφή τμήματος ${tmima.name}">
										<em class="fa fa-trash fa-fw"></em>
									</button>
								</td>
							</tr>
							`;
							});
							manageClassJs.$elements.tmimataTbl.find("tbody").empty().append(tbody);
							manageClassJs.prepareEditTmimata();
							manageClassJs.initTmimataDatatable();
						}

					}
				},
				error: function (jqXHR, error, errorThrown) {
					console.log(errorThrown);
					console.log(jqXHR);
					if (jqXHR.responseJSON) {
						manageClassJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						manageClassJs.manageAlertBox('danger', errorThrown);
					}
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		prepareEditTmimata: function () {
			manageClassJs.$elements.editTmimaBtn = $('.editTmimaBtn');
			manageClassJs.$elements.editTmimaBtn.on("click", function () {
				manageClassJs.$elements.editTmimaId = $(this).data('id');
				manageClassJs.renderEditTmimataModalData();
			});
			manageClassJs.$elements.deleteTmimaBtn = $('.deleteTmimaBtn');
			manageClassJs.$elements.deleteTmimaBtn.on("click", function () {
				manageClassJs.$elements.editTmimaId = $(this).data('id');
			});
		},
		renderEditTmimataModalData: function () {
			jQuery.ajax({
				url: manageClassJs.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_SINGLE_DATA",
					"Method": "GET",
				},
				data: {
					tmimaId: manageClassJs.$elements.editTmimaId
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('editTmimaModal');
				},
				complete: function (data) {
					removeStandardWhirlAnime('editTmimaModal');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						manageClassJs.$elements.modals.editTmimaModal.editTmima.val(response.data.name);
						manageClassJs.$elements.modals.editTmimaModal.editTaksi.val(response.data.class);
						manageClassJs.$elements.modals.editTmimaModal.editComments.val(response.data.comments);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					console.log(errorThrown);
					console.log(jqXHR);
					if (jqXHR.responseJSON) {
						manageClassJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						manageClassJs.manageAlertBox('danger', errorThrown);
					}
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		sendUpdateTmimaData: function () {
			manageClassJs.$elements.modals.editTmimaModal.submitSaveEditClassBtn.on("click", function () {
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				jQuery.ajax({
					url: manageClassJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						classId: manageClassJs.$elements.editTmimaId,
						editTmima: manageClassJs.$elements.modals.editTmimaModal.editTmima.val(),
						editTaksi: manageClassJs.$elements.modals.editTmimaModal.editTaksi.val(),
						comments: manageClassJs.$elements.modals.editTmimaModal.editComments.val()
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageClassJs.$elements.modals.editTmimaModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							manageClassJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
							setTimeout(function () {
								location.reload();
							}, 3000);
						} else {
							manageClassJs.manageAlertBox('danger', response.errorMessage);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
					error: function (jqXHR, error, errorThrown) {
						console.log(errorThrown);
						console.log(jqXHR);
						if (jqXHR.responseJSON) {
							manageClassJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							manageClassJs.manageAlertBox('danger', errorThrown);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		sendDeleteTmimaData: function () {
			manageClassJs.$elements.modals.deleteTmimaModal.submitDeleteTmimaBtn.on('click', function () {
				var btnDiv = $(this).parent();
				jQuery.ajax({
					url: manageClassJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						tmimaDeleteId: manageClassJs.$elements.editTmimaId
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageClassJs.$elements.modals.deleteTmimaModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							manageClassJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
						} else {
							manageClassJs.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
						setTimeout(function () {
							location.reload();
						}, 3000);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							manageClassJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							manageClassJs.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		initTmimataDatatable: function () {
			initDatatable(
				{
					pages: [10, 20, 50, 100, -1],
					order: [1, 'asc'],
					columnDefs: [{
						targets: 6,
						orderable: false
					}],
					id: "tmimataTbl",
					scrollx: true,
					scrolly: "60vh",
					downloadTitle: "Πίνακας Τμημάτων Πολύτροπο",
					buttons: true,
				}
			);
		},
	}
	manageClassJs.init();
});