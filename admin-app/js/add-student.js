$(document).ready(function () {
	var addStudentJs = {
		$elements: {
			form: {
				sub_date: $('#sub_date'),
				periodos: $('#periodos'),
				lessons: $('#lessons'),
				lesson_ids: $('#lesson_ids'),
				saveLessonsBtn: $('#saveLessons'),
				removeLessonsBtn: $('.removeLessonsBtn'),
				addStudetForm: $('#addStudentForm'),
			},
			modals: {
				lessonModal: $('#lessonModal'),
				submitSaveStudentModal: $('#submitSaveStudentModal'),
			},
			submitBtn: $('#submitSaveStudent'),
			todayDate: null,
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php']
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			addStudentJs.$elements.alertBox.removeClass();
			addStudentJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			addStudentJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			addStudentJs.$elements.alertBox.find('#alertMessage').html(message);
			addStudentJs.$elements.alertBox.show();
		},
		getEndPoint: function () {
			return addStudentJs.$elements.endpointUrl.join('/');
		},
		init: function () {
			addStudentJs.$elements.todayDate = new Date();
			addStudentJs.setPeriodosValue();
			addStudentJs.setSubDateValue();
			addStudentJs.bindSaveLessons();
			addStudentJs.bindRemoveLessons();
			addStudentJs.bindSendRequest();
		},
		setPeriodosValue: function () {
			var day = addStudentJs.$elements.todayDate.getDate();
			var month = addStudentJs.$elements.todayDate.getMonth() + 1;
			if (parseInt(day) < 10)
				day = "0" + day;
			if (parseInt(month) < 10)
				month = "0" + month;
			if (month >= 5 && month <= 7) {
				addStudentJs.$elements.form.periodos.val('Θερινή');
			} else {
				addStudentJs.$elements.form.periodos.val('Χειμερινή');
			}
		},
		setSubDateValue: function () {
			var yyyy = addStudentJs.$elements.todayDate.getFullYear();
			var day = addStudentJs.$elements.todayDate.getDate();
			var month = addStudentJs.$elements.todayDate.getMonth() + 1;
			if (parseInt(day) < 10) {
				day = "0" + day;
			}
			if (parseInt(month) < 10) {
				month = "0" + month;
			}
			addStudentJs.$elements.form.sub_date.val(yyyy + "-" + month + "-" + day);
		},
		bindSaveLessons: function () {
			addStudentJs.$elements.form.saveLessonsBtn.on('click', function () {
				var lessons = '';
				var ids = '';
				$('#assignmentsBox').find(':input').each(function () {
					if ($(this).prop("checked")) {
						lessons += $(this).attr("name") + ", ";
						ids += $(this).attr("id") + ",";
					}
				});
				addStudentJs.$elements.form.lessons.val(lessons.substring(0, lessons.length - 2));
				addStudentJs.$elements.form.lesson_ids.val(ids.substring(0, ids.length - 1));
				addStudentJs.$elements.modals.lessonModal.modal('hide');
			});
		},
		bindRemoveLessons: function () {
			addStudentJs.$elements.form.removeLessonsBtn.on('click', function () {
				Helpers.emptyField(addStudentJs.$elements.form.lessons);
				Helpers.emptyField(addStudentJs.$elements.form.lesson_ids);
				unselectAll();
			});
		},
		bindSendRequest: function () {
			addStudentJs.$elements.submitBtn.on('click', function () {
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				var the_data = addStudentJs.$elements.form.addStudetForm.serialize();
				jQuery.ajax({
					url: addStudentJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: the_data,
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						addStudentJs.$elements.modals.submitSaveStudentModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							addStudentJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
							setTimeout(function () {
								location.reload();
							}, 3000);
						} else {
							addStudentJs.manageAlertBox('danger', response.errorMessage);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
					error: function (jqXHR, error, errorThrown) {
						console.log(errorThrown);
						console.log(jqXHR);
						if (jqXHR.responseJSON) {
							addStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							addStudentJs.manageAlertBox('danger', errorThrown);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
	};
	addStudentJs.init();
});

function selectCheckbox(args) {
	unselectAll();
	let dataArr = args.split(',');
	for (var i = 0; i < dataArr.length; i++) {
		document.getElementById(dataArr[i]).checked = true;
	}
}

function unselectAll() {
	$('#assignmentsBox').find(':input').each(function () {
		$(this).prop("checked", false);
	})
}