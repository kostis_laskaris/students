$(document).ready(function () {
	var lessonsMng = {
		$elements: {
			$createLessonElements: {
				addLessonName: $('#addLessonName'),
				addLessonDesc: $('#addLessonDesc'),
				createLessonModal: $('#createLessonModal'),
			},
			$editLessonElements: {
				updateLessonName: $('#updateLessonName'),
				updateLessonDesc: $('#updateLessonDesc'),
				editLessonModal: $('#editLessonModal'),
				saveEditLessonBtn: $('#saveEditLessonBtn'),
			},
			$deleteLessonElements: {
				deleteLessonBtn: $('#btnConfirmDelete'),
				deleteLessonModal: $('#deleteLessonModal'),
			},
			createLessonBtn: $('#addLessonBtn'),
			lessonsTable: $('#lessonsTbl'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'ManageLessonsController.php']
		},
		init: function () {
			jQuery.ajax({
				url: lessonsMng.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('animoSort');
				},
				complete: function (data) {
					removeStandardWhirlAnime('animoSort');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						var tbody = lessonsMng.$elements.lessonsTable.find('tbody');
						var tbodyData = '';
						$.each(response.data, function (key, lesson) {
							tbodyData += `
									<tr id="${lesson.id}">
									<td>${lesson.id}</td>
									<td>${lesson.name}</td>
									<td>${lesson.description}</td>
									<td data-sort="${lesson.created_at}">${lesson.created_at ? Helpers.dateConverter(lesson.created_at) : ''}</td>
									<td data-sort="${lesson.updated_at}">${lesson.updated_at ? Helpers.dateConverter(lesson.updated_at) : ''}</td>
									<td class="text-center">
										<button 
										class="btn btn-sm btn-info mr-1 command-edit editLessonBtn"
										title="Επεξεργασία Μαθήματος ${lesson.name}"
										data-toggle="modal" 
										data-target="#editLessonModal"
										data-id="${lesson.id}">
										<em class="fas fa-pencil-alt fa-fw"></em>
										</button>
										<button
											data-id="${lesson.id}"
											data-toggle="modal" 
											data-target="#deleteLessonModal"
											class="btn btn-sm btn-danger mr-1 command-edit deleteLessonBtn"
											title="Διαγραφή μαθήματος ${lesson.name}">
											<em class="fas fa-trash fa-fw"></em>
										</button>
									</td>
									</tr>
								`;
						});
						$(tbody).empty().append(tbodyData);
						$('#lessonsCount').text('Καταχωρήσεις: ' + response.data.length);
						lessonsMng.handleEditLesson();
						$('.deleteLessonBtn').on('click', function () {
							lessonsMng.$elements.$deleteLessonElements.deleteLessonBtn.data('id', $(this).data('id'));
						});
					} else {
						alert(response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						lessonsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						lessonsMng.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			lessonsMng.$elements.alertBox.removeClass();
			lessonsMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			lessonsMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			lessonsMng.$elements.alertBox.find('#alertMessage').html(message);
			lessonsMng.$elements.alertBox.show();
		},
		handleEditLesson: function () {
			$('.editLessonBtn').click(function () {
				var lessonBtnId = $(this).data('id');
				lessonsMng.$elements.$editLessonElements.saveEditLessonBtn.data('id', lessonBtnId);
				jQuery.ajax({
					url: lessonsMng.getEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_SINGLE_DATA",
						"Method": "GET",
					},
					data: {
						editLessonId: lessonsMng.$elements.$editLessonElements.saveEditLessonBtn.data('id'),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('editLessonModal');
					},
					complete: function (data) {
						removeStandardWhirlAnime('editLessonModal');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonsMng.$elements.$editLessonElements.updateLessonName.val(response.data.name);
							lessonsMng.$elements.$editLessonElements.updateLessonDesc.val(response.data.description);
						} else {
							lessonsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleSaveEditLesson: function () {
			lessonsMng.$elements.$editLessonElements.saveEditLessonBtn.on('click', function () {
				var btnDiv = lessonsMng.$elements.$editLessonElements.saveEditLessonBtn.parent();
				jQuery.ajax({
					url: lessonsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						lessonEditId: lessonsMng.$elements.$editLessonElements.saveEditLessonBtn.data('id'),
						updateLessonName: lessonsMng.$elements.$editLessonElements.updateLessonName.val(),
						updateLessonDesc: lessonsMng.$elements.$editLessonElements.updateLessonDesc.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						lessonsMng.$elements.$editLessonElements.editLessonModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonsMng.manageAlertBox('success', response.data);
							lessonsMng.init();
						} else {
							lessonsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleDeleteLesson: function () {
			lessonsMng.$elements.$deleteLessonElements.deleteLessonBtn.on('click', function () {
				var btnDiv = lessonsMng.$elements.$deleteLessonElements.deleteLessonBtn.parent();
				jQuery.ajax({
					url: lessonsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						lessonDeleteId: lessonsMng.$elements.$deleteLessonElements.deleteLessonBtn.data('id'),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						lessonsMng.$elements.$deleteLessonElements.deleteLessonModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonsMng.manageAlertBox('success', response.data);
							lessonsMng.init();
						} else {
							lessonsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonsMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleCreateLesson: function () {
			lessonsMng.$elements.createLessonBtn.on('click', function () {
				var btnDiv = lessonsMng.$elements.createLessonBtn.parent();
				jQuery.ajax({
					url: lessonsMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						addLessonName: lessonsMng.$elements.$createLessonElements.addLessonName.val(),
						addLessonDesc: lessonsMng.$elements.$createLessonElements.addLessonDesc.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						lessonsMng.$elements.$createLessonElements.createLessonModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonsMng.manageAlertBox('success', response.data);
							lessonsMng.init();
						} else {
							lessonsMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonsMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonsMng.manageAlertBox('danger', errorThrown);
						}
						lessonsMng.$elements.$createLessonElements.createLessonModal.modal('hide');
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		bindButtonActions: function () {
			lessonsMng.handleCreateLesson();
			lessonsMng.handleSaveEditLesson();
			lessonsMng.handleDeleteLesson();
		},
	}
	lessonsMng.init();
	lessonsMng.bindButtonActions();

});