$(document).ready(function () {
	var addClassJs = {
		$elements: {
			modals: {
				confirmationModal: {
					theModal: $('#submitSaveClassModal'),
					submitSaveClassBtn: $('#submitSaveClass')
				},
			},
			theForm: $('#addClassForm'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'ClassController.php']
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			addClassJs.$elements.alertBox.removeClass();
			addClassJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			addClassJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			addClassJs.$elements.alertBox.find('#alertMessage').html(message);
			addClassJs.$elements.alertBox.show();
		},
		getEndPoint: function () {
			return addClassJs.$elements.endpointUrl.join('/');
		},
		init: function () {
			addClassJs.bindSaveClass();
		},
		bindSaveClass: function () {
			addClassJs.$elements.modals.confirmationModal.submitSaveClassBtn.on("click", function () {
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				var the_data = addClassJs.$elements.theForm.serialize();
				jQuery.ajax({
					url: addClassJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: the_data,
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						addClassJs.$elements.modals.confirmationModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							addClassJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
							setTimeout(function () {
								location.reload();
							}, 3000);
						} else {
							addClassJs.manageAlertBox('danger', response.errorMessage);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
					error: function (jqXHR, error, errorThrown) {
						console.log(errorThrown);
						console.log(jqXHR);
						if (jqXHR.responseJSON) {
							addClassJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							addClassJs.manageAlertBox('danger', errorThrown);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
	}
	addClassJs.init();
});