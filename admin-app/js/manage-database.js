$(document).ready(function () {
    var testJs = {
        $elements: {
            yearFrom: $('#from_sx_year'),
            yearTo: $('#to_sx_year'),
            periodosFrom: $('#from_periodos'),
            periodosTo: $('#to_periodos'),
            saveBtn: $('#saveNewDb'),
            alertBox: $('#alertBox'),
            endpointUrl: ['..', 'php_actions', 'Controllers', 'CreateDatabaseController.php'],
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            testJs.$elements.alertBox.removeClass();
            testJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            testJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            testJs.$elements.alertBox.find('#alertMessage').html(message);
            testJs.$elements.alertBox.show();
        },
        getEndPoint: function () {
            return testJs.$elements.endpointUrl.join('/');
        },
        init: function () {
            testJs.bindSaveNewDbTable();
        },
        bindSaveNewDbTable: function () {
            testJs.$elements.saveBtn.on("click", function () {
                var theData = testJs.getParsedSxYears();
                if (!theData) {
                    return false;
                }
                jQuery.ajax({
                    url: testJs.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "INSERT_DATA",
                        "Method": "POST",
                    },
                    data: {
                        fromValue: theData.fromValue,
                        toValue: theData.toValue,
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('theAnimo');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('theAnimo');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            testJs.manageAlertBox('success', response.data);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        console.log(errorThrown);
                        console.log(jqXHR);
                        if (jqXHR.responseJSON.errorMessage) {
                            testJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            testJs.manageAlertBox('danger', errorThrown);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        getParsedSxYears: function () {
            if ( Helpers.isEmpty(testJs.$elements.periodosTo.val() ) || Helpers.isEmpty(testJs.$elements.periodosFrom.val()) ) {
                alert('Παρακαλώ, επιλέξτε Περίοδο Μαθημάτων.');
                return false;
            }
            if ( Helpers.isEmpty(testJs.$elements.yearFrom.val()) || Helpers.isEmpty(testJs.$elements.yearTo.val()) ) {
                alert('Παρακαλώ, επιλέξτε Σχολικό Έτος.');
                return false;
            }
            var from = testJs.$elements.yearFrom.val().split('-');
            from = from[0].substr(from[0].length - 2) + "_" + from[1].substr(from[1].length - 2) + "_" + testJs.$elements.periodosFrom.val();

            var to = testJs.$elements.yearTo.val().split('-');
            to = to[0].substr(to[0].length - 2) + "_" + to[1].substr(to[1].length - 2) + "_" + testJs.$elements.periodosTo.val();
            return ({
                fromValue: from,
                toValue: to,
            });
        },
    }
    testJs.init();
});