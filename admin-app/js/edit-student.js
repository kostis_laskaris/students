$(document).ready(function () {
	var editStudentJs = {
		$elements: {
			studentElements: {
				student_id: $('#student_id'),
				firstname: $('#firstname'),
				lastname: $('#lastname'),
				address: $('#address'),
				perioxi: $('#perioxi'),
				zip: $('#zip'),
				mobile: $('#mobile'),
				phone: $('#phone'),
				email: $('#email'),
				date_eggrafis: $('#date_eggrafis'),
				mname: $('#mname'),
				faname: $('#faname'),
				mphone: $('#mphone'),
				fphone: $('#fphone'),
				email_kidemona: $('#email_kidemona'),
				kid_name: $('#kid_name'),
				kid_phone: $('#kid_phone'),
				fee: $('#fee'),
				tmima: $('#tmima'),
				sx_etos: $('#sx_etos'),
				periodos: $('#periodos'),
				class: $('#class'),
				informed_by: $('#informed_by'),
				lessons: $('#lessons'),
				lesson_ids: $('#lesson_ids'),
			},
			modal: {
				theModal: $('#updateStudentModal'),
				saveUpdateStudentBtn: $('#saveUpdateStudentBtn'),
				studentIdField: $('#updateStudentId'),
			},
			studentForm: $('#updateStudentForm'),
			lessonsModal: $('#lessonModal'),
			saveLessonsBtn: $('#saveLessons'),
			editLessonsBtn: $('.editLessonsBtn'),
			studentNameSpan: $('#student-name-span'),
			studentId: null,
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			editStudentJs.$elements.alertBox.removeClass();
			editStudentJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			editStudentJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			editStudentJs.$elements.alertBox.find('#alertMessage').html(message);
			editStudentJs.$elements.alertBox.show();
		},
		getEndPoint: function () {
			return editStudentJs.$elements.endpointUrl.join('/');
		},
		init: function () {
			editStudentJs.$elements.studentId = Helpers.b64_to_utf8(Helpers.urlParam('sid'));
			editStudentJs.initStudentData();
			editStudentJs.bindEditLessons();
			editStudentJs.bindSaveLessons();
			editStudentJs.sendStudentData();
		},
		initStudentData: function () {
			jQuery.ajax({
				url: editStudentJs.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_SINGLE_DATA",
					"Method": "GET",
				},
				data: { 
					studentId: editStudentJs.$elements.studentId,					
                    fireAction: "student_by_id"
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('editStudentAnimo');
				},
				complete: function (data) {
					removeStandardWhirlAnime('editStudentAnimo');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						editStudentJs.$elements.studentNameSpan.text(response.data.fullName);
						editStudentJs.$elements.studentElements.student_id.val(editStudentJs.$elements.studentId);
						editStudentJs.$elements.studentElements.firstname.val(response.data.firstname);
						editStudentJs.$elements.studentElements.lastname.val(response.data.lastname);
						editStudentJs.$elements.studentElements.address.val(response.data.address);
						editStudentJs.$elements.studentElements.perioxi.val(response.data.perioxi);
						editStudentJs.$elements.studentElements.zip.val(response.data.zip);
						editStudentJs.$elements.studentElements.mobile.val(response.data.mobile);
						editStudentJs.$elements.studentElements.phone.val(response.data.phone);
						editStudentJs.$elements.studentElements.email.val(response.data.email);
						editStudentJs.$elements.studentElements.date_eggrafis.val(response.data.date_eggrafis);

						editStudentJs.$elements.studentElements.mname.val(response.data.mname);
						editStudentJs.$elements.studentElements.faname.val(response.data.faname);
						editStudentJs.$elements.studentElements.mphone.val(response.data.mphone);
						editStudentJs.$elements.studentElements.fphone.val(response.data.fphone);
						editStudentJs.$elements.studentElements.email_kidemona.val(response.data.email_kidemona);
						editStudentJs.$elements.studentElements.kid_name.val(response.data.kid_name);
						editStudentJs.$elements.studentElements.kid_phone.val(response.data.kid_phone);

						editStudentJs.$elements.studentElements.fee.val(response.data.fee);
						editStudentJs.$elements.studentElements.periodos.val(response.data.periodos);
						editStudentJs.$elements.studentElements.class.val(response.data.class.id);
						editStudentJs.$elements.studentElements.informed_by.val(response.data.informed_by?response.data.informed_by.id:'');
						editStudentJs.$elements.studentElements.tmima.val(response.data.tmima.id);
						editStudentJs.$elements.studentElements.sx_etos.val(response.data.sx_etos);

						editStudentJs.$elements.studentElements.lesson_ids.val(response.data.lessonsIdsAsString);
						editStudentJs.$elements.studentElements.lessons.val(response.data.lessonsAsString);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					console.log(errorThrown);
					console.log(jqXHR);
					if (jqXHR.responseJSON.errorMessage) {
						editStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						editStudentJs.manageAlertBox('danger', errorThrown);
					}
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		bindEditLessons: function () {
			editStudentJs.$elements.editLessonsBtn.on('click', function () {
				selectCheckbox($('#lesson_ids').val());
			});
		},
		bindSaveLessons: function () {
			editStudentJs.$elements.saveLessonsBtn.on('click', function () {
				var txt = '';
				var ids = '';
				$('#assignmentsBox').find(':input').each(function () {
					if ($(this).prop("checked") == true) {
						txt += $(this).parent().text().trim() + ", ";
						ids += $(this).attr('id') + ",";
					}
				});
				editStudentJs.$elements.studentElements.lessons.val(txt.substring(0, txt.length - 2));
				editStudentJs.$elements.studentElements.lesson_ids.val(ids.substring(0, ids.length - 1));
				editStudentJs.$elements.lessonsModal.modal('hide');
			});
		},
		sendStudentData: function () {
			editStudentJs.$elements.modal.saveUpdateStudentBtn.on('click', function () {
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				var the_data = editStudentJs.$elements.studentForm.serialize();
				jQuery.ajax({
					url: editStudentJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: the_data,
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						editStudentJs.$elements.modal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							editStudentJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
							setTimeout(function () {
								location.reload();
							}, 3000);
						} else {
							editStudentJs.manageAlertBox('danger', response.errorMessage);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
					error: function (jqXHR, error, errorThrown) {
						console.log(errorThrown);
						console.log(jqXHR);
						if (jqXHR.responseJSON.errorMessage) {
							editStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							editStudentJs.manageAlertBox('danger', errorThrown);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
	};
	editStudentJs.init();
});

function selectCheckbox(args) {
	unselectAll();
	if (args) {
		let dataArr = args.split(',');
		for (var i = 0; i < dataArr.length; i++) {
			document.getElementById(dataArr[i]).checked = true;
		}
	}
}

function unselectAll() {
	$('#assignmentsBox').find(':input').each(function () {
		$(this).prop("checked", false);
	})
}