$(document).ready(function () {
    var dashboardJs = {
        $elements: {
            teacherNameSpan: $('#teacherNameSpan'),
            renderLineChartBtn: $('#renderLineChartBtn'),
            teacherDropdown: $('#searchUser'),
            gradeStatsPerMonthPerUserTable: $('#gradeStatsPerMonthPerUserTable'),
            teacherGradesAverageCard: $('#teacherGradesAverageCard'),
            teacherGradesCard: $('#teacherGradesCard'),
            studentPerClassCard: $('#studentPerClassCard'),
            studentEndpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
            gradeEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            gradeStatsEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php'],
            latestStudentsTable: $('#latestStudentsTable'),
            latestGradesTable: $('#latestGradesTable'),
            gradesPerUserTable: $('#gradesPerUserTable'),
            theMonths: ['', 'Ιανουάριος', 'Φεβρουάριος', 'Μάρτιος', 'Απρίλιος', 'Μάιος', 'Ιούνιος', 'Ιούλιος', 'Αύγουστος', 'Σεπτέμβριος', 'Οκτώβριος', 'Νοέμβριος', 'Δεκέμβριος'],
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            dashboardJs.$elements.alertBox.removeClass();
            dashboardJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            dashboardJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            dashboardJs.$elements.alertBox.find('#alertMessage').html(message);
            dashboardJs.$elements.alertBox.show();
        },
        getStudentEndPoint: function () {
            return dashboardJs.$elements.studentEndpointUrl.join("/");
        },
        getGradeEndPoint: function () {
            return dashboardJs.$elements.gradeEndpointUrl.join("/");
        },
        getGradeStatsEndPoint: function () {
            return dashboardJs.$elements.gradeStatsEndpointUrl.join("/");
        },
        init: function () {
            dashboardJs.initAllTables();
            dashboardJs.initAllCharts();
        },
        initAllTables: function () {
            dashboardJs.initLatestStudentsTable();
            dashboardJs.initLatestGradesTable();
            dashboardJs.initGradesPerUserTable();
            dashboardJs.initUserGradesPerMonthTable();
            dashboardJs.$elements.teacherDropdown.prop("selectedIndex", 1).change();
        },
        initLatestStudentsTable: function () {
            jQuery.ajax({
                url: dashboardJs.getStudentEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'latest_students',
                    studentCount: 7
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('latestStudentsAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('latestStudentsAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        if (response.data == 1) {
                            dashboardJs.$elements.latestStudentsTable.empty().append('<span class="text-center text-danger pt-2 pb-2">Δεν έχουν εγγραφεί μαθητές.</span>');
                        } else {
                            var item = '';
                            $.each(response.data, function (key, student) {
                                item += `
                                <tr>
                                <td class="py-2 pr-0 align-middle">
                                    <span class="fa-stack">
                                        <em class="icon-graduation fa-stack-2x text-green"></em>
                                    </span>
                                </td>
                                <td class="py-2 pl-1">
                                    <div>${student.fullName}</div>
                                    <div>
                                        <a class="small" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}">Προβολή Μαθητή</a>
                                    </div>
                                </td>
                                <td class="py-2 small align-middle text-right">${Helpers.getDateDiff(student.date_eggrafis)} ημέρες πριν</td>
                                </tr>
                                `;
                            });
                            dashboardJs.$elements.latestStudentsTable.find('tbody').empty().append(item);
                        }
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        initLatestGradesTable: function () {
            jQuery.ajax({
                url: dashboardJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'last_n_grades',
                    gradeCount: 10
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('latestGradesAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('latestGradesAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        if (response.data == 1) {
                            dashboardJs.$elements.latestGradesTable.empty().append('<span class="text-center text-danger pt-2 pb-2">Δεν έχουν εγγραφεί μαθητές.</span>');
                        } else {
                            var item = '';
                            $.each(response.data, function (key, grade) {
                                item += `
                                <tr>
                                <td class="py-2 pr-0 align-middle">
                                    <span class="fa-stack">
                                        <em class="icon-star fa-stack-2x text-purple"></em>
                                    </span>
                                </td>
                                <td class="py-2 pl-1">
                                    <div>
                                        <a class="text-bold" title="Προβολή μαθητή" href="view-student?sid=${Helpers.utf8_to_b64(grade.student_id)}">
                                            ${grade.studentLastname + " " + grade.studentFirstname}
                                        </a>
                                    </div>
                                    <div>
                                        <small class="mr-1 br pr-1">${Helpers.dateOnlyConverter(grade.exam_date)}</small>
                                        <a class="small" href="view-lesson?lesid=${Helpers.utf8_to_b64(grade.lesson_id)}&lesname=${Helpers.utf8_to_b64(grade.lessonName)}">${grade.lessonName}</a>
                                    </div>
                                </td>
                                <td class="py-2 text-center align-middle text-right text-bold">
                                    <div class="${parseInt(grade.grade) > 50 ? 'text-green' : 'text-danger'}">
                                        ${grade.grade}%
                                    </div>
                                    <div class="mt-1 badge ${grade.absence == 'ΠΑΡΩΝ' ? 'badge-success' : 'badge-danger'}">
                                        ${grade.absence}
                                    </div>
                                </td>
                                </tr>
                                `;
                            });
                            dashboardJs.$elements.latestGradesTable.find('tbody').empty().append(item);
                        }
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        initGradesPerUserTable: function () {
            jQuery.ajax({
                url: dashboardJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'grades_per_user_and_lesson'
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('gradesPerUserAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('gradesPerUserAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        if (response.data == 1) {
                            dashboardJs.$elements.gradesPerUserTable.find('tbody').remove();
                        } else {
                            var item = '';
                            $.each(response.data, function (key, stat) {
                                item += `
                                <tr>
                                    <td>${stat.teacherLastname + " " + stat.teacherFirstname}</td>
                                    <td>
                                        <a href="view-lesson?lesid=${Helpers.utf8_to_b64(stat.lesson_id)}&lesname=${Helpers.utf8_to_b64(stat.lessonName)}">${stat.lessonName}</a>
                                    </td>
                                    <td>${stat.countGrades}</td>
                                    <td>${stat.gradeAvg}%</td>
                                    <td data-sort="${stat.latestDate}">${Helpers.dateConverter(stat.latestDate)}</td>
                                </tr>
                                `;
                            });
                            dashboardJs.$elements.gradesPerUserTable.find('tbody').empty().append(item);
                        }
                        dashboardJs.initGradesPerUserDatatable();
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        initUserGradesPerMonthTable: function () {
            dashboardJs.$elements.teacherDropdown.on("change", function () {
                var theTeacher = dashboardJs.$elements.teacherDropdown.find("option:selected").text();
                jQuery.ajax({
                    url: dashboardJs.getGradeStatsEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    data: {
                        fireAction: 'grade_stats_per_month',
                        user_id: $(this).val()
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('gradeStatsPerMonthPerUser');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('gradeStatsPerMonthPerUser');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            destroyDatatable('gradeStatsPerMonthPerUserTable');
                            if (response.data == 1) {
                                dashboardJs.$elements.renderLineChartBtn.addClass('disabled');
                                dashboardJs.$elements.gradeStatsPerMonthPerUserTable.find('tbody').empty().append(`
                                <tr>
                                <td colspan="6" class="text-danger text-bold text-center">Δεν βρέθηκαν βαθμολογίες</td>
                                </tr>
                                `);
                            } else {
                                response.data.teacherName = theTeacher;
                                dashboardJs.$elements.renderLineChartBtn.removeClass('disabled');
                                dashboardJs.renderUserGradesPerMonthChart(response.data);
                                var tbody = '';
                                $.each(response.data, function (key, stat) {
                                    tbody += `
                                    <tr>
                                    <td>${theTeacher}</td>
                                    <td data-sort="${stat.theMonth}">${dashboardJs.$elements.theMonths[stat.theMonth]}</td>
                                    <td class="text-center">${stat.gradeAvg}%</td>
                                    <td class="text-center">${stat.countGrades}</td>
                                    <td class="text-center">${stat.maxGrade}%</td>
                                    <td class="text-center">${stat.minGrade}%</td>
                                    </tr>
                                    `;
                                });
                                dashboardJs.$elements.gradeStatsPerMonthPerUserTable.find('tbody').empty().append(tbody);
                            }
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            dashboardJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        initAllCharts: function () {
            dashboardJs.initGradesPerTeacherChart();
            dashboardJs.initStudentPerClassChart();
        },
        initGradesPerTeacherChart: function () {
            jQuery.ajax({
                url: dashboardJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'count_grades_per_user',
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('teacherGradesChartAnimo');
                    addStandardWhirlAnime('teacherGradesAverageChartAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('teacherGradesChartAnimo');
                    removeStandardWhirlAnime('teacherGradesAverageChartAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        dashboardJs.renderTeacherGradesAverageChart(response.data);
                        var labels = [];
                        var data = [];
                        $.each(response.data, function (key, stat) {
                            labels.push(stat.teacherLastname + " " + stat.teacherFirstname);
                            data.push(parseInt(stat.countGrades));
                        });
                        var hBarData = {
                            labels: labels,
                            datasets: [{
                                data: data,
                                backgroundColor: ["rgba(128, 0, 0, 0.6)", "rgba(170, 110, 40, 0.6)", "rgba(128, 128, 0, 0.6)", "rgba(0, 128, 128, 0.6)", "rgba(230, 25, 75, 0.6)", "rgba(245, 130, 48, 0.6)",
                                    "rgba(255, 225, 25, 0.6)", "rgba(210, 245, 60, 0.6)", "rgba(60, 180, 75, 0.6)", "rgba(0, 130, 200, 0.6)", "rgba(145, 30, 180, 0.6)", "rgba(240, 50, 230, 0.6)",
                                    "rgba(128, 128, 128, 0.6)", "rgba(255, 215, 180, 0.6)", "rgba(170, 255, 195, 0.6)", "rgba(230, 190, 255, 0.6)", "rgba(51,51,51, 0.6)", "rgba(229,30,50, 0.6)", "rgba(181,182,30, 0.6)"
                                    , "rgba(0,100,0, 0.6)", "rgba(0,0,139, 0.6)", "rgba(0,0,0, 0.6)", "rgba(0,255,255, 0.6)"],
                                borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
                                    "rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
                                    "rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)", "rgba(51,51,51, 1)", "rgba(229,30,50, 1)", "rgba(181,182,30, 1)"
                                    , "rgba(0,100,0, 1)", "rgba(0,0,139, 1)", "rgba(0,0,0, 1)", "rgba(0,255,255, 1)"]
                            }]
                        };

                        var hBarOptions = {
                            legend: {
                                display: false
                            },
                        };
                        var piectx = document.getElementById('teacherGradesBarchart').getContext('2d');
                        var hBarChart = new Chart(piectx, {
                            data: hBarData,
                            type: 'horizontalBar',
                            options: hBarOptions
                        });
                        dashboardJs.$elements.teacherGradesCard.find('div.card-footer').empty().append(`
                        <a class="btn btn-primary btn-oval" href="view-grades.php">
                            Προβολή Όλων
                        </a>
                        `);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        initStudentPerClassChart: function () {
            jQuery.ajax({
                url: dashboardJs.getStudentEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'student_per_class',
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('studentPerClassChartAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('studentPerClassChartAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        labels = [];
                        datas = [];
                        $.each(response.data, function (key, value) {
                            labels.push(value.class_name);
                            datas.push(value.total);
                        });
                        var pieData = {
                            labels: labels,
                            datasets: [{
                                data: datas,
                                backgroundColor: ["rgba(128, 0, 0, 0.5)", "rgba(170, 110, 40, 0.5)", "rgba(128, 128, 0, 0.5)", "rgba(0, 128, 128, 0.5)", "rgba(230, 25, 75, 0.5)", "rgba(245, 130, 48, 0.5)",
                                    "rgba(255, 225, 25, 0.5)", "rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)", "rgba(0, 130, 200, 0.5)", "rgba(145, 30, 180, 0.5)", "rgba(240, 50, 230, 0.5)",
                                    "rgba(128, 128, 128, 0.5)", "rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
                                borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
                                    "rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
                                    "rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
                            }]
                        };

                        var pieOptions = {
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    fontSize: 11
                                }
                            }
                        };
                        var piectx = document.getElementById('studentPerClassPiechart').getContext('2d');
                        var pieChart = new Chart(piectx, {
                            data: pieData,
                            type: 'pie',
                            options: pieOptions
                        });
                        dashboardJs.$elements.studentPerClassCard.find('div.card-footer').empty().append(`
                        <a class="btn btn-primary btn-oval" href="manage-students.php">
                            Προβολή Όλων
                        </a>
                        `);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderUserGradesPerMonthChart: function (theData) {
            var labels = [];
            var charData = [];
            var arrSum = 0;
            $.each(theData, function (key, value) {
                labels.push(dashboardJs.$elements.theMonths[value.theMonth]);
                charData.push(value.gradeAvg);
                arrSum += parseFloat(value.gradeAvg);
            });
            var lineData = {
                labels: labels,
                datasets: [{
                    backgroundColor: 'rgba(114,102,186,0.2)',
                    borderColor: 'rgba(114,102,186,1)',
                    pointBorderColor: '#fff',
                    data: charData,
                    pointRadius: 5,
                    pointBackgroundColor: '#443d71',
                }]
            };

            var lineOptions = {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                            stepSize: 20,
                        }
                    }]
                }
            };
            $('#userGradesPerMonthLineChart').empty().append(`<canvas ></canvas>`);
            dashboardJs.$elements.teacherNameSpan.empty().text(theData.teacherName);
            $('#finalAverage').html(`Συνολικό Μέσο Όρο: <span class="text-bold">` + (arrSum / charData.length).toFixed(1) + '% (' + ((arrSum / charData.length) * 20 / 100).toFixed(1) + `)</span>`);
            var linectx = document.getElementById('userGradesPerMonthLineChart').getElementsByTagName('canvas')[0].getContext('2d');
            var lineChart = new Chart(linectx, {
                data: lineData,
                type: 'line',
                options: lineOptions
            });
        },
        renderTeacherGradesAverageChart: function (theData) {
            var labels = [];
            var data = [];
            $.each(theData, function (key, stat) {
                labels.push(stat.teacherLastname + " " + stat.teacherFirstname);
                data.push(parseInt(stat.gradeAvg));
            });
            var hBarData = {
                labels: labels,
                datasets: [{
                    data: data,
                    backgroundColor: ["rgba(128, 0, 0, 0.6)", "rgba(170, 110, 40, 0.6)", "rgba(128, 128, 0, 0.6)", "rgba(0, 128, 128, 0.6)", "rgba(230, 25, 75, 0.6)", "rgba(245, 130, 48, 0.6)",
                        "rgba(255, 225, 25, 0.6)", "rgba(210, 245, 60, 0.6)", "rgba(60, 180, 75, 0.6)", "rgba(0, 130, 200, 0.6)", "rgba(145, 30, 180, 0.6)", "rgba(240, 50, 230, 0.6)",
                        "rgba(128, 128, 128, 0.6)", "rgba(255, 215, 180, 0.6)", "rgba(170, 255, 195, 0.6)", "rgba(230, 190, 255, 0.6)", "rgba(51,51,51, 0.6)", "rgba(229,30,50, 0.6)", "rgba(181,182,30, 0.6)"
                        , "rgba(0,100,0, 0.6)", "rgba(0,0,139, 0.6)", "rgba(0,0,0, 0.6)", "rgba(0,255,255, 0.6)"],
                    borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
                        "rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
                        "rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)", "rgba(51,51,51, 1)", "rgba(229,30,50, 1)", "rgba(181,182,30, 1)"
                        , "rgba(0,100,0, 1)", "rgba(0,0,139, 1)", "rgba(0,0,0, 1)", "rgba(0,255,255, 1)"]
                }]
            };

            var hBarOptions = {
                legend: {
                    display: false
                },
				maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                            stepSize: 20,
                        }
                    }]
                }
            };
            var piectx = document.getElementById('teacherGradesAverageBarchart').getContext('2d');
            var hBarChart = new Chart(piectx, {
                data: hBarData,
                type: 'bar',
                options: hBarOptions
            });
            dashboardJs.$elements.teacherGradesAverageCard.find('div.card-footer').empty().append(`
                        <a class="btn btn-primary btn-oval" href="view-grades.php">
                            Προβολή Όλων
                        </a>
                        `);
        },
        initGradesPerUserDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [0, 'asc'],
                    id: 'gradesPerUserTable',
                    scrollx: true,
                    scrolly: "35vh",
                    buttons: false,
                }
            );
        },
    }
    dashboardJs.init();
});