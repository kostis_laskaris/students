$(document).ready(function () {
    var addGradesJs = {
        $elements: {
            gradeData: {
                studentDropdown: $('#student_name'),
                studentClass: $('#class'),
                studentClassId: $('#class_id'),
                studentTmima: $('#tmima'),
                studentTmimaId: $('#tmima_id'),
                studentLesson: $('#lesson'),
                studentAbsence: $('#absence'),
                studentGrade: $('#grade'),
                studentGradeOnTwenty: $('#onTwenty'),
                studentExamKind: $('#exam_kind'),
                studentExamDate: $('#exam_date'),
                studentComments: $('#comments'),
                submitStudentGradeBtn: $('#submitBtn'),
            },
            modals: {
                submitGradeModal: {
                    theModal: $('#submitSaveGradeModal'),
                    submitSaveGrade: $('#submitSaveGrade'),
                },
            },
            theForm: $('#addGradeForm'),
            letterCounter: $('#letter-count'),
            latestGradesTbl: $('#latestGradesTbl'),
            studentId: null,
            alertBox: $('#alertBox'),
            endpointUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            studentEndpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            addGradesJs.$elements.alertBox.removeClass();
            addGradesJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            addGradesJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            addGradesJs.$elements.alertBox.find('#alertMessage').html(message);
            addGradesJs.$elements.alertBox.show();
        },
        getEndPoint: function () {
            return addGradesJs.$elements.endpointUrl.join('/');
        },
        getStudentEndPoint: function () {
            return addGradesJs.$elements.studentEndpointUrl.join('/');
        },
        init: function () {
            addGradesJs.bindStudentData();
            addGradesJs.bindLatestGrades();
            addGradesJs.bindAbsenceDropdownAction();
            addGradesJs.bindGradeInputAction();
            addGradesJs.bindCommentsInputAction();
            addGradesJs.sendNewGradeData();
        },
        bindStudentData: function () {
            addGradesJs.$elements.gradeData.studentDropdown.on("change", function () {
                addGradesJs.$elements.studentId = $(this).val();
                jQuery.ajax({
                    url: addGradesJs.getStudentEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_SINGLE_DATA",
                        "Method": "GET",
                    },
                    data: {
                        fireAction: 'student_by_id',
                        studentId: addGradesJs.$elements.studentId,
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('addGradeAnimo');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('addGradeAnimo');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            if (response.data == 1) {
                                return false;
                            }
                            addGradesJs.$elements.gradeData.studentClass.val(response.data.class.list_entry);
                            addGradesJs.$elements.gradeData.studentClassId.val(response.data.class.id)
                            addGradesJs.$elements.gradeData.studentTmima.val(response.data.tmima.name);
                            addGradesJs.$elements.gradeData.studentTmimaId.val(response.data.tmima.id);
                            var options = '<option value=""></option>';
                            $.each(response.data.lessonsByUserId, function (key, lesson) {
                                options += `<option value="${lesson.id}" ${(key == 0) ? 'selected' : ''}>${lesson.name}</option>`;
                            });
                            addGradesJs.$elements.gradeData.studentLesson.empty().append(options);
                            addGradesJs.enableFields();
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            addGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            addGradesJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        bindLatestGrades: function () {
            jQuery.ajax({
                url: addGradesJs.getEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'last_n_grades_by_user'
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('latestGradesAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('latestGradesAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        if (response.data == 1) {
                            addGradesJs.$elements.latestGradesTbl.find('tbody').remove();
                        } else {
                            var tbody = '';
                            $.each(response.data, function (key, grade) {
                                var badgeColor = 'badge-primary';
                                if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                                    badgeColor = 'badge-purple';
                                } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                                    badgeColor = 'badge-green';
                                }
                                tbody += `
                                <tr>             
                                    <td>
                                        <div class="${grade.grade < 50 ? 'text-danger' : 'text-green'}">
                                        ${grade.grade < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                                        </div>
                                    </td>
                                    <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                                    <td>
                                        <a title="Πλήρης Προβολή: ${grade.fullStudent.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(grade.fullStudent.id)}">
                                        ${grade.fullStudent.fullName}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="view-lesson?lesid=${Helpers.utf8_to_b64(grade.lesson_id)}&lesname=${Helpers.utf8_to_b64(grade.fullLesson.name)}">${grade.fullLesson.name}</a>
                                    </td>
                                    <td>${grade.fullTmima.name}</td>
                                    <td>
                                        <span class="badge ${badgeColor}">${grade.exam_kind}</span>
                                    </td>
                                    <td>
                                        <span class="badge ${grade.absence == "ΠΑΡΩΝ" ? 'badge-success' : 'badge-danger'}">${grade.absence}</span>
                                    </td>
                                    <td><strong>${grade.grade}</strong></td>
                                    <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                                </tr>`;
                            });
                            addGradesJs.$elements.latestGradesTbl.find('tbody').empty().append(tbody);
                        }
                        addGradesJs.initLatestGradesDatatable();
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        addGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        addGradesJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        bindAbsenceDropdownAction: function () {
            addGradesJs.$elements.gradeData.studentAbsence.on("change", function () {
                if ($(this).val() == 'ΑΠΩΝ') {
                    addGradesJs.$elements.gradeData.studentGrade.val('0');
                    addGradesJs.$elements.gradeData.studentGrade.attr("readonly", true);
                } else {
                    addGradesJs.$elements.gradeData.studentGrade.val('');
                    addGradesJs.$elements.gradeData.studentGrade.attr("readonly", false);
                }
            });
        },
        bindGradeInputAction: function () {
            addGradesJs.$elements.gradeData.studentGrade.on("keyup change", function () {
                let grade = $(this).val();
                if (grade >= 0 && grade < 101) {
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.text((20 * grade) / 100 + " / 20");
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.removeClass('d-none');
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.addClass('d-flex');
                } else {
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.text('/20');
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.removeClass('d-flex');
                    addGradesJs.$elements.gradeData.studentGradeOnTwenty.addClass('d-none');
                }
            });
        },
        bindCommentsInputAction: function () {
            addGradesJs.$elements.gradeData.studentComments.on("keyup", function () {
                if ($(this).val().length > 300) {
                    $(this).val($(this).val().substring(0, 300));
                } else {
                    addGradesJs.$elements.letterCounter.text($(this).val().length + "/300 Χαρακτήρες");
                }
            });
        },
        initLatestGradesDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [1, 'desc'],
                    id: 'latestGradesTbl',
                    scrollx: true,
                    scrolly: "70vh",
                    buttons: false,
                }
            );
        },
        sendNewGradeData: function () {
            addGradesJs.$elements.modals.submitGradeModal.submitSaveGrade.on('click', function () {
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                var the_data = addGradesJs.$elements.theForm.serialize();
                jQuery.ajax({
                    url: addGradesJs.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "INSERT_DATA",
                        "Method": "POST",
                    },
                    data: the_data,
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        addGradesJs.$elements.modals.submitGradeModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            console.log(response)
                            addGradesJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            addGradesJs.manageAlertBox('danger', response.errorMessage);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                    error: function (jqXHR, error, errorThrown) {
                        console.log(errorThrown);
                        console.log(jqXHR);
                        if (jqXHR.responseJSON) {
                            addGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            addGradesJs.manageAlertBox('danger', errorThrown);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        enableFields: function () {
            addGradesJs.$elements.gradeData.studentLesson.attr("disabled", false);
            addGradesJs.$elements.gradeData.studentAbsence.attr("disabled", false);
            addGradesJs.$elements.gradeData.studentGrade.attr("readonly", false);
            addGradesJs.$elements.gradeData.studentExamKind.attr("disabled", false);
            addGradesJs.$elements.gradeData.studentExamDate.attr("readonly", false);
            addGradesJs.$elements.gradeData.studentComments.attr("readonly", false);
        },
        disabledFields: function () {
            addGradesJs.$elements.gradeData.studentLesson.attr("disabled", true);
            addGradesJs.$elements.gradeData.studentAbsence.attr("disabled", true);
            addGradesJs.$elements.gradeData.studentGrade.attr("readonly", true);
            addGradesJs.$elements.gradeData.studentExamKind.attr("disabled", true);
            addGradesJs.$elements.gradeData.studentExamDate.attr("readonly", true);
            addGradesJs.$elements.gradeData.studentComments.attr("readonly", true);
        },
    };

    addGradesJs.init();
});