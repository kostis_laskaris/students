$(document).ready(function () {

    var userProfile = {
        $elements: {
            firstname: $('#firstname'),
            lastname: $('#lastname'),
            email: $('#email'),
            phone: $('#phone'),
            mobile: $('#mobile'),
            address: $('#address'),
            zip: $('#zip'),
            city: $('#city'),
            alertBox: $('#alertBox'),
            saveBtn: $('#saveProfileBtn'),
            endpointUrl: ['..', 'php_actions', 'Controllers', 'UserProfileController.php'],
        },
        init: function () {
            jQuery.ajax({
				url: userProfile.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_SINGLE_DATA",
					"Method": "GET",
				},
                data: {
                    userId: userProfile.$elements.saveBtn.data('id')
                },
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('userProfileForm');
				},
				complete: function (data) {
					removeStandardWhirlAnime('userProfileForm');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
                        userProfile.$elements.firstname.val(response.data.profile.firstname);
                        userProfile.$elements.lastname.val(response.data.profile.lastname);
                        userProfile.$elements.email.val(response.data.email);
                        userProfile.$elements.phone.val(response.data.profile.phone);
                        userProfile.$elements.mobile.val(response.data.profile.mobile_phone);
                        userProfile.$elements.address.val(response.data.profile.address);
                        userProfile.$elements.zip.val(response.data.profile.zip);
                        userProfile.$elements.city.val(response.data.profile.city);
					} else {
						alert(response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						userProfile.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						userProfile.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
        },
        getEndPoint: function () {
            return this.$elements.endpointUrl.join('/');
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            userProfile.$elements.alertBox.removeClass();
            userProfile.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            userProfile.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            userProfile.$elements.alertBox.find('#alertMessage').html(message);
            userProfile.$elements.alertBox.show();
        },
        handleEditUserProfile: function () {
            userProfile.$elements.saveBtn.click(function () {
                var userBtnId = userProfile.$elements.saveBtn.data('id');
				var btnDiv = userProfile.$elements.saveBtn.parent();
                $('#editUserBtn').data('id', userBtnId);
                jQuery.ajax({
                    url: userProfile.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "UPDATE_DATA",
                        "Method": "PATCH",
                    },
                    data: {
                        userId: userBtnId,
                        email: userProfile.$elements.email.val(),
                        firstname: userProfile.$elements.firstname.val(),
                        lastname: userProfile.$elements.lastname.val(),
                        phone: userProfile.$elements.phone.val(),
                        mobile: userProfile.$elements.mobile.val(),
                        address: userProfile.$elements.address.val(),
                        zip: userProfile.$elements.zip.val(),
                        city: userProfile.$elements.city.val(),
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
							userProfile.manageAlertBox('success', response.data);
						} else {
							userProfile.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON.errorMessage) {
							userProfile.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							userProfile.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
                    },
                });
            });
        },
        bindButtonActions: function () {
            userProfile.handleEditUserProfile();
        },
    }
    //called on first load
    userProfile.init();
    userProfile.bindButtonActions();
});