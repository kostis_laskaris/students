$(document).ready(function () {
	var manageStudentsJs = {
		$elements: {
			modals: {
				deleteStudentModal: {
					theModal: $('#deleteStudentModal'),
					studentDeleteId: $('#studentDeleteId'),
					fillStudentItem: $('#fillStudentItem'),
					saveDeleteStudentBtn: $('#saveDeleteStudentBtn'),
				},
				studentPerClassModal: {
					theModal: $('#classStudentModal'),
					sumStudentSpan: $('#StudentClassSum'),
					modalHeader: $('#students-selected-title'),
				},
				studentPerPerioxiModal: {
					theModal: $('#perioxiStudentModal'),
					sumStudentSpan: $('#StudentPerioxiSum'),
					modalHeader: $('#perioxi-selected-title'),
				},
				studentPerLessonModal: {
					theModal: $('#lessonStudentModal'),
					sumStudentSpan: $('#StudentLessonSum'),
					modalHeader: $('#lesson-selected-title'),
				},
				studentlessonsPicker: {
					theModal: $('#lessonModal'),
					saveLessons: $('#saveLessons'),
				},
				searchModal: {
					theModal: $('#filterStudentsModal'),
				}
			},
			searchElements: {
				searchFirstname: $('#searchFirstname'),
				searchLastname: $('#searchLastname'),
				searchTmima: $('#searchTmima'),
				searchTaksi: $('#searchTaksi'),
				searchEmail: $('#searchEmail'),
				searchLessons: $('#searchLessons'),
				searchLessonIds: $('#searchLessonIds'),
				initSearchBtn: $('#initSearchBtn'),
				removeLessonsBtn: $('.removeLessonsBtn'),
				clearSearchBtn: $('#clearSearchBtn'),
			},
			popupStudentPerClassModalLink: null,
			popupStudentPerPerioxiModalLink: null,
			popupStudentPerLessonModalLink: null,
			deleteStudentBtn: null,
			studentPerClassTable: $('#studentsPerClassTbl'),
			studentsPerPerioxiTable: $('#studentsPerPerioxiTbl'),
			studentsPerLessonTable: $('#studentsPerLessonTbl'),
			studentsTable: $('#studentsTbl'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
			searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchStudentController.php'],
			the_data: null,
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			manageStudentsJs.$elements.alertBox.removeClass();
			manageStudentsJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			manageStudentsJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			manageStudentsJs.$elements.alertBox.find('#alertMessage').html(message);
			manageStudentsJs.$elements.alertBox.show();
		},
		getEndPoint: function () {
			return manageStudentsJs.$elements.endpointUrl.join('/');
		},
		getSearchEndPoint: function () {
			return manageStudentsJs.$elements.searchEndpointUrl.join('/');
		},
		init: function () {
			manageStudentsJs.initAllData();
			manageStudentsJs.handleDeleteStudent();
			manageStudentsJs.bindSearchBtn();
			manageStudentsJs.bindSaveLessons();
			manageStudentsJs.bindRemoveLessons();
			manageStudentsJs.bindResetSearchBtn();
		},
		initAllData: function () {
			jQuery.ajax({
				url: manageStudentsJs.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_CUSTOM_DATA",
					"Method": "GET",
				},
				data: {
					fireAction: 'manage_students_data'
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('theAnimo');
				},
				complete: function (data) {
					removeStandardWhirlAnime('theAnimo');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						manageStudentsJs.$elements.the_data = response.data;

						if (manageStudentsJs.$elements.the_data.allStudents == 1) {
							$(`<p class="text-danger py-3 text-bold text-center">Δεν υπάρχουν δεδομένα μαθητών.</p>`).insertAfter("table");
							return false;
						}
						manageStudentsJs.initAllTables();
						manageStudentsJs.initAllCharts();
						manageStudentsJs.prepareDeleteStudent();
						manageStudentsJs.handleStudentPerClassModal();
						manageStudentsJs.handleStudentPerPerioxiModal();
						manageStudentsJs.handleStudentPerLessonModal();
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						manageStudentsJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						manageStudentsJs.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		initAllTables: function () {
			manageStudentsJs.initStudentsTable();
			manageStudentsJs.initStudentPerClassTable();
			manageStudentsJs.initStudentPerPerioxiTable();
			manageStudentsJs.initStudentPerLessonTable();
		},
		initAllCharts: function () {
			manageStudentsJs.initStudentPerClassChart();
			manageStudentsJs.initStudentPerPerioxiChart();
			manageStudentsJs.initStudentPerLessonChart();
		},
		initStudentsTable: function () {
			destroyDatatable('studentsTbl');
			var list;
			for (let student of manageStudentsJs.$elements.the_data.allStudents) {
				list += `
				<tr class="student-card">
					<td>${student.id}</td>
					<td>${student.lastname}</td>
					<td>${student.firstname}</td>
					<td>${student.phone}</td>
					<td>${student.mobile}</td>
					<td>
						<a href="mailto:${student.email}">
							${student.email}
						</a>
					</td>
					<td>${student.tmima}</td>
					<td>${student.fullAddress}</td>
					<td class="d-none">${student.fphone}</td>
					<td class="d-none">${student.mphone}</td>
					<td class="d-none">${student.class.list_entry}</td>
					<td class="d-none">${student.mname}</td>
					<td class="d-none">${student.faname}</td>
					<td class="d-none">${student.kid_name}</td>
					<td class="d-none">${student.email_kidemona}</td>
					<td class="d-none">${student.kid_phone}</td>
					<td class="d-none">${student.fee}</td>
					<td class="d-none">${student.periodos}</td>
					<td class="d-none">${student.sx_etos}</td>
					<td class="d-none">${Helpers.dateOnlyConverter(student.date_eggrafis)}</td>
					<td class="d-none">${student.informed_by ? student.informed_by.list_entry : ''}</td>
					<td class="d-none">${student.lessonsAsString}</td>											
					<td class="text-center">
						<a title="Πλήρης Προβολή: ${student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
							<em class="fas fa-external-link-alt fa-fw"></em>
						</a>
						<a href="edit-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${student.fullName}">
							<em class="fas fa-edit fa-fw"></em>
						</a>
						<button title="Διαγραφή: ${student.fullName}" data-id="${student.id}" data-studentname="${student.fullName}" type="button" data-toggle="modal" data-target="#deleteStudentModal" class="btn btn-sm btn-danger command-edit mr-1 deleteStudentBtn">
							<em class="fas fa-trash fa-fw"></em>
						</button>
					</td>
				</tr>
				`;
			}
			manageStudentsJs.$elements.studentsTable.find('tbody').empty().append(list);
			manageStudentsJs.$elements.deleteStudentBtn = $('button.deleteStudentBtn');
			manageStudentsJs.initStudentsDatatable();
		},
		initStudentPerClassTable: function () {
			destroyDatatable('studentsPerClassTbl');
			var list;
			var size = manageStudentsJs.$elements.the_data.allStudents.length;
			for (let i of manageStudentsJs.$elements.the_data.studentsPerClass) {
				list += `
				<tr>
				<td>${i.class_name}</td>
				<td class="text-center">${i.total}</td>
				<td class="text-center">${((i.total / size) * 100).toFixed(2).replace('.', ',')}%</td>
				<td class="text-center">
					<span class="text-primary student-per-class-event like-a" data-toggle="modal" data-target="#classStudentModal" data-id="${i.class_id}" data-classname="${i.class_name}">
						<i class="fas fa-external-link-alt mr-2"></i>Προβολή
					</span>
				</td>
				</tr>
				`;
			}
			manageStudentsJs.$elements.studentPerClassTable.find('tbody').empty().append(list);
			manageStudentsJs.$elements.popupStudentPerClassModalLink = $('.student-per-class-event');
			manageStudentsJs.initStudentsPerClassDatatable();
		},
		initStudentPerPerioxiTable: function () {
			destroyDatatable('studentsPerPerioxiTbl');
			var list;
			var size = manageStudentsJs.$elements.the_data.allStudents.length;
			for (let i of manageStudentsJs.$elements.the_data.studentsPerPerioxi) {
				list += `
				<tr>
				<td>${i.perioxi_name}</td>
				<td class="text-center">${i.total}</td>
				<td class="text-center">${((i.total / size) * 100).toFixed(2).replace('.', ',')}%</td>
				<td class="text-center">
					<span class="text-primary student-per-perioxi-event like-a" data-toggle="modal" data-target="#perioxiStudentModal" data-perioxiname="${i.perioxi_name}">
						<i class="fas fa-external-link-alt mr-2"></i>Προβολή
					</span>
				</td>
				</tr>
				`;
			}
			manageStudentsJs.$elements.studentsPerPerioxiTable.find('tbody').empty().append(list);
			manageStudentsJs.$elements.popupStudentPerPerioxiModalLink = $('.student-per-perioxi-event');
			manageStudentsJs.initStudentsPerPerioxiDatatable();
		},
		initStudentPerLessonTable: function () {
			destroyDatatable('studentsPerLessonTbl');
			var list;
			var size = manageStudentsJs.$elements.the_data.allStudents.length;
			$.each(manageStudentsJs.$elements.the_data.studentsPerLesson, function (key, the_lesson) {
				list += `
				<tr>
				<td>
					<a href="view-lesson.php?lesid=${Helpers.utf8_to_b64(the_lesson.lesson_id)}&lesname=${Helpers.utf8_to_b64(the_lesson.lesson_name)}" title="Προβολή">${the_lesson.lesson_name}</a>
				</td>
				<td>${the_lesson.lesson_code}</td>
				<td class="text-center">${the_lesson.total}</td>
				<td class="text-center">${((the_lesson.total / size) * 100).toFixed(2).replace('.', ',')}%</td>
				<td>${Helpers.dateOnlyConverter(the_lesson.created_at)}</td>
				<td class="text-center">
					<span class="text-primary student-per-lesson-event like-a" data-toggle="modal" data-target="#lessonStudentModal" data-lessonname="${the_lesson.lesson_name}">
						<i class="fas fa-external-link-alt mr-2"></i>Προβολή
					</span>
				</td>
				</tr>
				`;
			});
			manageStudentsJs.$elements.studentsPerLessonTable.find('tbody').empty().append(list);
			manageStudentsJs.$elements.popupStudentPerLessonModalLink = $('.student-per-lesson-event');
			manageStudentsJs.initStudentsPerLessonDatatable();
		},
		initStudentPerClassChart: function () {
			classNames = new Array();
			dataSet = new Array();
			$.each(manageStudentsJs.$elements.the_data.studentsPerClass, function (key, value) {
				classNames.push(value.class_name);
				dataSet.push(value.total);
			});
			var pieData = {
				labels: classNames,
				datasets: [{
					data: dataSet,
					backgroundColor: ["rgba(128, 0, 0, 0.5)", "rgba(170, 110, 40, 0.5)", "rgba(128, 128, 0, 0.5)", "rgba(0, 128, 128, 0.5)", "rgba(230, 25, 75, 0.5)", "rgba(245, 130, 48, 0.5)",
						"rgba(255, 225, 25, 0.5)", "rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)", "rgba(0, 130, 200, 0.5)", "rgba(145, 30, 180, 0.5)", "rgba(240, 50, 230, 0.5)",
						"rgba(128, 128, 128, 0.5)", "rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
					borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
						"rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
						"rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
				}]
			};

			var pieOptions = {
				legend: {
					display: false
				}
			};
			var piectx = document.getElementById('studentClassesChart').getContext('2d');
			var pieChart = new Chart(piectx, {
				data: pieData,
				type: 'pie',
				options: pieOptions
			});
		},
		initStudentPerPerioxiChart: function () {
			perioxiNames = new Array();
			dataSet = new Array();
			$.each(manageStudentsJs.$elements.the_data.studentsPerPerioxi, function (key, value) {
				perioxiNames.push(value.perioxi_name);
				dataSet.push(value.total);
			});
			var pieData = {
				labels: perioxiNames,
				datasets: [{
					data: dataSet,
					backgroundColor: ["rgba(128, 0, 0, 0.5)", "rgba(170, 110, 40, 0.5)", "rgba(128, 128, 0, 0.5)", "rgba(0, 128, 128, 0.5)", "rgba(230, 25, 75, 0.5)", "rgba(245, 130, 48, 0.5)",
						"rgba(255, 225, 25, 0.5)", "rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)", "rgba(0, 130, 200, 0.5)", "rgba(145, 30, 180, 0.5)", "rgba(240, 50, 230, 0.5)",
						"rgba(128, 128, 128, 0.5)", "rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
					borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
						"rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
						"rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
				}]
			};

			var pieOptions = {
				legend: {
					display: false
				}
			};
			var piectx = document.getElementById('studentPerioxesChart').getContext('2d');
			var pieChart = new Chart(piectx, {
				data: pieData,
				type: 'pie',
				options: pieOptions
			});
		},
		initStudentPerLessonChart: function () {
			lessonNames = new Array();
			dataSet = new Array();
			$.each(manageStudentsJs.$elements.the_data.studentsPerLesson, function (key, value) {
				lessonNames.push(value.lesson_name);
				dataSet.push(value.total);
			});
			var barData = {
				labels: lessonNames,
				datasets: [{
					data: dataSet,
					backgroundColor: ["rgba(128, 0, 0, 0.5)", "rgba(170, 110, 40, 0.5)", "rgba(128, 128, 0, 0.5)", "rgba(0, 128, 128, 0.5)", "rgba(230, 25, 75, 0.5)", "rgba(245, 130, 48, 0.5)",
						"rgba(255, 225, 25, 0.5)", "rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)", "rgba(0, 130, 200, 0.5)", "rgba(145, 30, 180, 0.5)", "rgba(240, 50, 230, 0.5)",
						"rgba(128, 128, 128, 0.5)", "rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)", "rgba(51,51,51, 0.5)", "rgba(229,30,50, 0.5)", "rgba(181,182,30, 0.5)"
						, "rgba(0,100,0, 0.5)", "rgba(0,0,139, 0.5)", "rgba(0,0,0, 0.5)", "rgba(0,255,255, 0.5)"],
					borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
						"rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
						"rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)", "rgba(51,51,51, 1)", "rgba(229,30,50, 1)", "rgba(181,182,30, 1)"
						, "rgba(0,100,0, 1)", "rgba(0,0,139, 1)", "rgba(0,0,0, 1)", "rgba(0,255,255, 1)"]
				}]
			};
			var barOptions = {
				legend: {
					display: false
				},
				maintainAspectRatio: false
			};
			var barctx = document.getElementById('studentLessonsChart').getContext('2d');
			var barChart = new Chart(barctx, {
				data: barData,
				type: 'bar',
				options: barOptions
			});
		},
		prepareDeleteStudent: function () {
			manageStudentsJs.$elements.deleteStudentBtn.on("click", function () {
				manageStudentsJs.$elements.modals.deleteStudentModal.studentDeleteId.val($(this).data('id'));
				manageStudentsJs.$elements.modals.deleteStudentModal.fillStudentItem.text($(this).data('studentname'));
			})
		},
		handleDeleteStudent: function () {
			manageStudentsJs.$elements.modals.deleteStudentModal.saveDeleteStudentBtn.on('click', function () {
				var btnDiv = $(this).parent();
				jQuery.ajax({
					url: manageStudentsJs.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						studentDeleteId: manageStudentsJs.$elements.modals.deleteStudentModal.studentDeleteId.val()
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageStudentsJs.$elements.modals.deleteStudentModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							manageStudentsJs.manageAlertBox('success', response.data);
							manageStudentsJs.init();
						} else {
							manageStudentsJs.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							manageStudentsJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							manageStudentsJs.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleStudentPerClassModal: function () {
			manageStudentsJs.$elements.popupStudentPerClassModalLink.on("click", function () {
				addStandardWhirlAnime('classModalAnimo');
				var list = '';
				var the_class = $(this).data('id');
				var cnt = 0;
				manageStudentsJs.$elements.modals.studentPerClassModal.modalHeader.html("Προβολή Μαθητών " + the_class);
				$.each(manageStudentsJs.$elements.the_data.allStudents, function (key, the_student) {
					if (the_class == the_student.class.id) {
						list +=
							`<tr>
								<td>${the_student.lastname}</td>
								<td>${the_student.firstname}</td>
								<td>${the_student.tmima}</td>
								<td>${Helpers.dateOnlyConverter(the_student.date_eggrafis)}</td>
								<td>
									<a title="Πλήρης Προβολή: ${the_student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
										<em class="fas fa-external-link-alt fa-fw"></em>
									</a>
									<a href="edit-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${the_student.fullName}">
										<em class="fas fa-edit fa-fw"></em>
									</a>
								</td>
							</tr>`;
						cnt++;
					}
				});
				manageStudentsJs.$elements.modals.studentPerClassModal.theModal.find('tbody').empty().append(list);
				manageStudentsJs.$elements.modals.studentPerClassModal.sumStudentSpan.text(cnt);
				removeStandardWhirlAnime('classModalAnimo');
			});
		},
		handleStudentPerPerioxiModal: function () {
			manageStudentsJs.$elements.popupStudentPerPerioxiModalLink.on("click", function () {
				addStandardWhirlAnime('perioxiModalAnimo');
				var list = '';
				var the_perioxi = $(this).data('perioxiname');
				var cnt = 0;
				manageStudentsJs.$elements.modals.studentPerPerioxiModal.modalHeader.html("Προβολή Μαθητών " + the_perioxi);
				$.each(manageStudentsJs.$elements.the_data.allStudents, function (key, the_student) {
					if (the_perioxi == the_student.perioxi) {
						list +=
							`<tr>
								<td>${the_student.lastname}</td>
								<td>${the_student.firstname}</td>
								<td>${the_student.tmima}</td>
								<td>${Helpers.dateOnlyConverter(the_student.date_eggrafis)}</td>
								<td>
									<a title="Πλήρης Προβολή: ${the_student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
										<em class="fas fa-external-link-alt fa-fw"></em>
									</a>
									<a href="edit-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${the_student.fullName}">
										<em class="fas fa-edit fa-fw"></em>
									</a>
								</td>
							</tr>`;
						cnt++;
					}
				});
				manageStudentsJs.$elements.modals.studentPerPerioxiModal.theModal.find('tbody').empty().append(list);
				manageStudentsJs.$elements.modals.studentPerPerioxiModal.sumStudentSpan.text(cnt);
				removeStandardWhirlAnime('perioxiModalAnimo');
			});
		},
		handleStudentPerLessonModal: function () {
			manageStudentsJs.$elements.popupStudentPerLessonModalLink.on("click", function () {
				addStandardWhirlAnime('lessonModalAnimo');
				var list = '';
				var the_lesson = $(this).data('lessonname');
				var cnt = 0;
				manageStudentsJs.$elements.modals.studentPerLessonModal.modalHeader.html("Προβολή Μαθητών " + the_lesson);
				$.each(manageStudentsJs.$elements.the_data.allStudents, function (key, the_student) {
					$.each(the_student.lessons, function (key, lesson) {
						if (the_lesson == lesson.name) {
							list +=
								`<tr>
									<td>${the_student.lastname}</td>
									<td>${the_student.firstname}</td>
									<td>${the_student.tmima}</td>
									<td>${Helpers.dateOnlyConverter(the_student.date_eggrafis)}</td>
									<td>
										<a title="Πλήρης Προβολή: ${the_student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
											<em class="fas fa-external-link-alt fa-fw"></em>
										</a>
										<a href="edit-student?sid=${Helpers.utf8_to_b64(the_student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${the_student.fullName}">
											<em class="fas fa-edit fa-fw"></em>
										</a>
									</td>
								</tr>`;
							cnt++;
						}
					});
				});
				manageStudentsJs.$elements.modals.studentPerLessonModal.theModal.find('tbody').empty().append(list);
				manageStudentsJs.$elements.modals.studentPerLessonModal.sumStudentSpan.text(cnt);
				removeStandardWhirlAnime('lessonModalAnimo');
			});
		},
		bindSaveLessons: function () {
			manageStudentsJs.$elements.modals.studentlessonsPicker.saveLessons.on('click', function () {
				var lessons = '';
				var ids = '';
				$('#assignmentsBox').find(':input').each(function () {
					if ($(this).prop("checked")) {
						lessons += $(this).attr("name") + ", ";
						ids += $(this).attr("id") + ",";
					}
				});
				manageStudentsJs.$elements.searchElements.searchLessons.val(lessons.substring(0, lessons.length - 2));
				manageStudentsJs.$elements.searchElements.searchLessonIds.val(ids.substring(0, ids.length - 1));
				manageStudentsJs.$elements.modals.studentlessonsPicker.theModal.modal('hide');
			});
		},
		resetSearchFields: function () {
			Helpers.emptyFields(manageStudentsJs.$elements.searchElements);
			unselectAll();
		},
		bindRemoveLessons: function () {
			manageStudentsJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
				Helpers.emptyField(manageStudentsJs.$elements.searchElements.searchLessons);
				Helpers.emptyField(manageStudentsJs.$elements.searchElements.searchLessonIds);
				unselectAll();
			});
		},
		bindResetSearchBtn: function () {
			$('#removeFilterStudentsModal').on("click", function () {
				$('#removeFilterStudentsModal').hide();
				manageStudentsJs.resetSearchFields();
				var searchData = {
					reset: true
				};
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				jQuery.ajax({
					url: manageStudentsJs.getSearchEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_CUSTOM_DATA",
						"Method": "GET",
					},
					dataType: 'json',
					data: { attribs: searchData },
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						removeStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageStudentsJs.$elements.modals.searchModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							destroyDatatable('studentsTbl');
							var tbody = '';
							if (response.data == 1) {
								tbody += `
							<tr>
							<td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Κανένα Αποτέλεσμα</td>
							</tr>`;
							} else {
								$.each(response.data, function (key, student) {
									tbody += `
									<tr class="student-card">
										<td>${student.id}</td>
										<td>${student.lastname}</td>
										<td>${student.firstname}</td>
										<td>${student.phone}</td>
										<td>${student.mobile}</td>
										<td>
											<a href="mailto:${student.email}">
												${student.email}
											</a>
										</td>
										<td>${student.tmima}</td>
										<td>${student.fullAddress}</td>
										<td class="d-none">${student.fphone}</td>
										<td class="d-none">${student.mphone}</td>
										<td class="d-none">${student.class}</td>
										<td class="d-none">${student.mname}</td>
										<td class="d-none">${student.faname}</td>
										<td class="d-none">${student.kid_name}</td>
										<td class="d-none">${student.email_kidemona}</td>
										<td class="d-none">${student.kid_phone}</td>
										<td class="d-none">${student.fee}</td>
										<td class="d-none">${student.periodos}</td>
										<td class="d-none">${student.sx_etos}</td>
										<td class="d-none">${Helpers.dateOnlyConverter(student.date_eggrafis)}</td>
										<td class="d-none">${student.informed_by}</td>
										<td class="d-none">${student.lessonsAsString}</td>											
										<td class="text-center">
											<a title="Πλήρης Προβολή: ${student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
												<em class="fas fa-external-link-alt fa-fw"></em>
											</a>
											<a href="edit-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${student.fullName}">
												<em class="fas fa-edit fa-fw"></em>
											</a>
											<button title="Διαγραφή: ${student.fullName}" data-id="${student.id}" data-studentname="${student.fullName}" type="button" data-toggle="modal" data-target="#deleteStudentModal" class="btn btn-sm btn-danger command-edit mr-1 deleteStudentBtn">
												<em class="fas fa-trash fa-fw"></em>
											</button>
										</td>
									</tr>									
									`
								});
							}
							manageStudentsJs.$elements.studentsTable.find('tbody').empty().append(tbody);
							manageStudentsJs.$elements.deleteStudentBtn = $('button.deleteStudentBtn');
							initDatatable(
								{
									pages: [10, 20, 50, 100, -1],
									order: [1, 'asc'],
									id: 'studentsTbl',
									columnDefs: [
										{
											"targets": 8,
											"orderable": false
										}
									],
									scrollx: true,
									scrolly: "70vh",
									downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
									buttons: true,
								},
							);
						} else {
							alert(response.errorMessage);
							console.log(response);
						}
					},
					error: function (jqXHR, error, errorThrown) {
						alert('Η αναζήτηση απέτυχε. Error code: ' + error);
						console.log(errorThrown);
					},
				});
			});
			manageStudentsJs.$elements.searchElements.clearSearchBtn.on("click", function () {
				$('#removeFilterStudentsModal').hide();
				manageStudentsJs.resetSearchFields();
				var searchData = {
					reset: true
				};
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				jQuery.ajax({
					url: manageStudentsJs.getSearchEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_CUSTOM_DATA",
						"Method": "GET",
					},
					dataType: 'json',
					data: { attribs: searchData },
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						removeStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageStudentsJs.$elements.searchElements.searchLessons.prop('readonly', true);
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							destroyDatatable('studentsTbl');
							var tbody = '';
							if (response.data == 1) {
								tbody += `
							<tr>
							<td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Κανένα Αποτέλεσμα</td>
							</tr>`;
							} else {
								$.each(response.data, function (key, student) {
									tbody += `
									<tr class="student-card">
										<td>${student.id}</td>
										<td>${student.lastname}</td>
										<td>${student.firstname}</td>
										<td>${student.phone}</td>
										<td>${student.mobile}</td>
										<td>
											<a href="mailto:${student.email}">
												${student.email}
											</a>
										</td>
										<td>${student.tmima}</td>
										<td>${student.fullAddress}</td>
										<td class="d-none">${student.fphone}</td>
										<td class="d-none">${student.mphone}</td>
										<td class="d-none">${student.class}</td>
										<td class="d-none">${student.mname}</td>
										<td class="d-none">${student.faname}</td>
										<td class="d-none">${student.kid_name}</td>
										<td class="d-none">${student.email_kidemona}</td>
										<td class="d-none">${student.kid_phone}</td>
										<td class="d-none">${student.fee}</td>
										<td class="d-none">${student.periodos}</td>
										<td class="d-none">${student.sx_etos}</td>
										<td class="d-none">${Helpers.dateOnlyConverter(student.date_eggrafis)}</td>
										<td class="d-none">${student.informed_by}</td>
										<td class="d-none">${student.lessonsAsString}</td>											
										<td class="text-center">
											<a title="Πλήρης Προβολή: ${student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
												<em class="fas fa-external-link-alt fa-fw"></em>
											</a>
											<a href="edit-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${student.fullName}">
												<em class="fas fa-edit fa-fw"></em>
											</a>
											<button title="Διαγραφή: ${student.fullName}" data-id="${student.id}" data-studentname="${student.fullName}" type="button" data-toggle="modal" data-target="#deleteStudentModal" class="btn btn-sm btn-danger command-edit mr-1 deleteStudentBtn">
												<em class="fas fa-trash fa-fw"></em>
											</button>
										</td>
									</tr>									
									`
								});
							}
							manageStudentsJs.$elements.studentsTable.find('tbody').empty().append(tbody);
							manageStudentsJs.$elements.deleteStudentBtn = $('button.deleteStudentBtn');
							initDatatable(
								{
									pages: [10, 20, 50, 100, -1],
									order: [1, 'asc'],
									id: 'studentsTbl',
									columnDefs: [
										{
											"targets": 8,
											"orderable": false
										}
									],
									scrollx: true,
									scrolly: "70vh",
									downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
									buttons: true,
								},
							);
						} else {
							alert(response.errorMessage);
							console.log(response);
						}
					},
					error: function (jqXHR, error, errorThrown) {
						alert('Η αναζήτηση απέτυχε. Error code: ' + error);
						console.log(errorThrown);
					},
				});
			});
		},
		bindSearchBtn: function () {
			manageStudentsJs.$elements.searchElements.initSearchBtn.on("click", function () {
				$('#removeFilterStudentsModal').show();
				var searchData = {
					firstname: manageStudentsJs.$elements.searchElements.searchFirstname.val(),
					lastname: manageStudentsJs.$elements.searchElements.searchLastname.val(),
					tmima: manageStudentsJs.$elements.searchElements.searchTmima.val(),
					class: manageStudentsJs.$elements.searchElements.searchTaksi.val(),
					email: manageStudentsJs.$elements.searchElements.searchEmail.val(),
					lesson_id: manageStudentsJs.$elements.searchElements.searchLessonIds.val(),
				};
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
				jQuery.ajax({
					url: manageStudentsJs.getSearchEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_CUSTOM_DATA",
						"Method": "GET",
					},
					dataType: 'json',
					data: { attribs: searchData },
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						removeStandardWhirlAnime('addStudentAnimate');
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						manageStudentsJs.$elements.modals.searchModal.theModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							destroyDatatable('studentsTbl');
							var tbody = '';
							if (response.data == 1) {
								tbody += `
							<tr>
							<td class="text-center text-danger text-bold" style="font-size: 120%" colspan="9">Κανένα Αποτέλεσμα</td>
							</tr>`;
							} else {
								$.each(response.data, function (key, student) {
									tbody += `
									<tr class="student-card">
										<td>${student.id}</td>
										<td>${student.lastname}</td>
										<td>${student.firstname}</td>
										<td>${student.phone}</td>
										<td>${student.mobile}</td>
										<td>
											<a href="mailto:${student.email}">
												${student.email}
											</a>
										</td>
										<td>${student.tmima}</td>
										<td>${student.fullAddress}</td>
										<td class="d-none">${student.fphone}</td>
										<td class="d-none">${student.mphone}</td>
										<td class="d-none">${student.class}</td>
										<td class="d-none">${student.mname}</td>
										<td class="d-none">${student.faname}</td>
										<td class="d-none">${student.kid_name}</td>
										<td class="d-none">${student.email_kidemona}</td>
										<td class="d-none">${student.kid_phone}</td>
										<td class="d-none">${student.fee}</td>
										<td class="d-none">${student.periodos}</td>
										<td class="d-none">${student.sx_etos}</td>
										<td class="d-none">${Helpers.dateOnlyConverter(student.date_eggrafis)}</td>
										<td class="d-none">${student.informed_by}</td>
										<td class="d-none">${student.lessonsAsString}</td>											
										<td class="text-center">
											<a title="Πλήρης Προβολή: ${student.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
												<em class="fas fa-external-link-alt fa-fw"></em>
											</a>
											<a href="edit-student?sid=${Helpers.utf8_to_b64(student.id)}" class="btn btn-sm btn-info mr-1 command-edit text-white" title="Επεξεργασία: ${student.fullName}">
												<em class="fas fa-edit fa-fw"></em>
											</a>
											<button title="Διαγραφή: ${student.fullName}" data-id="${student.id}" data-studentname="${student.fullName}" type="button" data-toggle="modal" data-target="#deleteStudentModal" class="btn btn-sm btn-danger command-edit mr-1 deleteStudentBtn">
												<em class="fas fa-trash fa-fw"></em>
											</button>
										</td>
									</tr>									
									`
								});
							}
							if (response.data == 1) {
								manageStudentsJs.$elements.studentsTable.find('tbody').remove();
							} else {
								manageStudentsJs.$elements.studentsTable.find('tbody').empty().append(tbody);
								manageStudentsJs.$elements.deleteStudentBtn = $('button.deleteStudentBtn');
							}
							manageStudentsJs.initStudentsDatatable();
						} else {
							alert(response.errorMessage);
							console.log(response);
						}
					},
					error: function (jqXHR, error, errorThrown) {
						alert('Η αναζήτηση απέτυχε. Error code: ' + error);
						console.log(errorThrown);
					},
				});
			});
		},
		initStudentsDatatable: function () {
			initDatatable(
				{
					pages: [10, 20, 50, 100, -1],
					order: [1, 'asc'],
					id: 'studentsTbl',
					columnDefs: [
						{
							"targets": 22,
							"orderable": false
						}
					],
					scrollx: true,
					scrolly: "70vh",
					downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
					buttons: true,
				}
			);
		},
		initStudentsPerClassDatatable: function () {
			initDatatable(
				{
					pages: [5, - 1],
					order: [1, 'desc'],
					id: 'studentsPerClassTbl',
					scrollx: true,
					downloadTitle: "Πίνακας Μαθητών ανά Τάξη",
					buttons: true,
				}
			);
		},
		initStudentsPerPerioxiDatatable: function () {
			initDatatable(
				{
					pages: [5, - 1],
					order: [1, 'desc'],
					id: 'studentsPerPerioxiTbl',
					scrollx: true,
					downloadTitle: "Πίνακας Μαθητών ανά Περιοχή Διαμονής",
					buttons: true,
				}
			);
		},
		initStudentsPerLessonDatatable: function () {
			initDatatable(
				{
					pages: [10, - 1],
					order: [2, 'desc'],
					id: 'studentsPerLessonTbl',
					scrollx: true,
					downloadTitle: "Πίνακας Μαθητών ανά Μάθημα",
					buttons: true,
				}
			);
		},
	}
	manageStudentsJs.init();
});

function selectCheckbox(args) {
	unselectAll();
	let dataArr = args.split(',');
	for (var i = 0; i < dataArr.length; i++) {
		document.getElementById(dataArr[i]).checked = true;
	}
}

function unselectAll() {
	$('#assignmentsBox').find(':input').each(function () {
		$(this).prop("checked", false);
	})
}