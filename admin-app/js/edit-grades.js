$(document).ready(function () {

    var editGradesJs = {
        $elements: {
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                lessons: $('#searchLessons'),
                lessonIds: $('#searchLessonIds'),
                studentId: $('#searchStudent'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
                searchLoader: $('#searchingLoader'),
                lessonModal: $('#lessonsModal'),
                removeLessonsBtn: $('.removeLessonsBtn'),
            },
            gradeData: {
                gradeStudentDropdown: $('#student_name'),
                gradeClass: $('#class'),
                gradeClassId: $('#class_id'),
                gradeTmima: $('#tmima'),
                gradeTmimaId: $('#tmima_id'),
                gradeLesson: $('#lesson'),
                gradeAbsence: $('#absence'),
                gradeGrade: $('#grade'),
                gradeOnTwenty: $('#onTwenty'),
                gradeExamKind: $('#exam_kind'),
                gradeExamDate: $('#exam_date'),
                gradeComments: $('#comments'),
                gradeId: $('#editGradeId'),
            },
            modals: {
                gradeLessonsPicker: {
                    theModal: $('#lessonsModal'),
                    saveLessons: $('#saveLessons'),
                },
                searchModal: {
                    theModal: $('#filterGradesModal'),
                },
                editGradeModal: {
                    theModal: $('#editGradeModal'),
                    saveEditGradeBtn: $('#saveEditGradeBtn'),
                    theForm: $('#editGradeForm'),
                },
                deleteGradeModal: {
                    theModal: $('#deleteGradeModal'),
                    saveDeleteGradeBtn: $("#saveDeleteGradeBtn"),
                    deleteGradeId: $('#gradeDeleteId'),
                },
            },
            editGradeBtn: null,
            deleteGradeBtn: null,
            editGradeTbl: $('#edit_grades_tbl'),
            letterCounter: $('#letter-count'),
            theGradeValue: null,
            assignmentsBox: $('#assignmentsBox'),
            alertBox: $('#alertBox'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
            url: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            editGradesJs.$elements.alertBox.removeClass();
            editGradesJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            editGradesJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            editGradesJs.$elements.alertBox.find('#alertMessage').html(message);
            editGradesJs.$elements.alertBox.show();
        },
        getEndPoint: function () {
            return editGradesJs.$elements.url.join('/');
        },
        getSearchEndPoint: function () {
            return editGradesJs.$elements.searchEndpointUrl.join('/');
        },
        resetSearchFields: function () {
            Helpers.emptyFields(editGradesJs.$elements.searchElements);
            unselectAll();
        },
        init: function () {
            editGradesJs.initEditGradesTable();
            editGradesJs.bindSaveLessons();
            editGradesJs.bindRemoveLessons();
            editGradesJs.bindCommentsInputAction();
            editGradesJs.bindAbsenceDropdownAction();
            editGradesJs.bindSearchBtn();
            editGradesJs.bindResetSearchBtn();
            editGradesJs.sendEditGradeData();
            editGradesJs.sendDeleteGradeData();
        },
        bindSaveLessons: function () {
            editGradesJs.$elements.modals.gradeLessonsPicker.saveLessons.on('click', function () {
                var lessons = '';
                var ids = '';
                $('#assignmentsBox').find(':input').each(function () {
                    if ($(this).prop("checked")) {
                        lessons += $(this).attr("name") + ", ";
                        ids += $(this).attr("id") + ",";
                    }
                });
                editGradesJs.$elements.searchElements.lessons.val(lessons.substring(0, lessons.length - 2));
                editGradesJs.$elements.searchElements.lessonIds.val(ids.substring(0, ids.length - 1));
                editGradesJs.$elements.modals.gradeLessonsPicker.theModal.modal('hide');
            });
        },
        bindRemoveLessons: function () {
            editGradesJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
                Helpers.emptyField(editGradesJs.$elements.searchElements.lessons);
                Helpers.emptyField(editGradesJs.$elements.searchElements.lessonIds);
                unselectAll();
            });
        },
        bindCommentsInputAction: function () {
            editGradesJs.$elements.gradeData.gradeComments.on("keyup", function () {
                if ($(this).val().length > 300) {
                    $(this).val($(this).val().substring(0, 300));
                } else {
                    editGradesJs.$elements.letterCounter.text($(this).val().length + "/300 Χαρακτήρες");
                }
            });
        },
        bindAbsenceDropdownAction: function () {
            editGradesJs.$elements.gradeData.gradeAbsence.on("change", function () {
                if ($(this).val() == 'ΑΠΩΝ') {
                    editGradesJs.$elements.gradeData.gradeGrade.val('0');
                    editGradesJs.$elements.gradeData.gradeGrade.attr("readonly", true);
                } else {
                    editGradesJs.$elements.gradeData.gradeGrade.val(editGradesJs.$elements.theGradeValue ? editGradesJs.$elements.theGradeValue : '');
                    editGradesJs.$elements.gradeData.gradeGrade.attr("readonly", false);
                }
            });
        },
        initEditGradesTable: function () {
            jQuery.ajax({
                url: editGradesJs.getEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_ALL_DATA",
                    "Method": "GET",
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSort');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSort');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        editGradesJs.renderGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        editGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        editGradesJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        bindSearchBtn: function () {
            editGradesJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!editGradesJs.isTelestisAccepted(editGradesJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterGradesModal').show();
                var searchData = {
                    user_id: editGradesJs.$elements.searchElements.teacher.val(),
                    exam_kind: editGradesJs.$elements.searchElements.examKind.val(),
                    absence: editGradesJs.$elements.searchElements.absence.val(),
                    grade: editGradesJs.$elements.searchElements.telestis.val() + ":" + editGradesJs.$elements.searchElements.grade.val(),
                    date_from: editGradesJs.$elements.searchElements.examDateFrom.val(),
                    date_to: editGradesJs.$elements.searchElements.examDateTo.val(),
                    lesson_id: editGradesJs.$elements.searchElements.lessonIds.val(),
                    student_id: editGradesJs.$elements.searchElements.studentId.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: editGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        editGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            // console.log(response)
                            editGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                        console.log(jqXHR);
                    },
                });
            });
        },
        bindResetSearchBtn: function () {
            $('#removeFilterGradesModal').on("click", function () {
                $('#removeFilterGradesModal').hide();
                editGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: editGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        editGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            editGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            editGradesJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterGradesModal').hide();
                editGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: editGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            editGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        renderGradesTableData: function (the_data) {
            destroyDatatable('edit_grades_tbl');
            if (the_data == 1) {
                editGradesJs.$elements.editGradeTbl.find('tbody').remove();
            } else {
                var tbody = '';
                $.each(the_data, function (key, grade) {
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                    }
                    tbody += `
                <tr>
                <td data-sort=${grade.exam_date}>${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                <td>${grade.teacherLastname} ${grade.teacherFirstname}</td>
                <td><a href="view-student?sid=${Helpers.utf8_to_b64(grade.student_id)}">${grade.studentLastname} ${grade.studentFirstname}</a></td>
                <td><a href="view-lesson.php?lesid=${Helpers.utf8_to_b64(grade.lesson_id)}&lesname=${Helpers.utf8_to_b64(grade.lessonName)}">${grade.lessonName}</a></td>
                <td>${grade.tmimaName}</td>
                <td><span class="badge ${badgeColor}">${grade.exam_kind}</span></td>
                <td><span class="badge ${(grade.absence == "ΠΑΡΩΝ") ? 'badge-success' : 'badge-danger'}">${grade.absence}</span></td>
                <td><b>${grade.grade}</b></td>
                <td>
                    <button class="btn btn-sm btn-info mr-1 command-edit editGradeBtn" type="button" data-target="#editGradeModal" data-toggle="modal" data-id="${grade.id}" title="Επεξεργασία">
                        <em class="fas fa-pencil-alt fa-fw"></em>
                    </button>
                    <button class="btn btn-sm btn-danger command-edit deleteGradeBtn" type="button" data-toggle="modal" data-target="#deleteGradeModal" data-id="${grade.id}" title="Διαγραφή">
                        <em class="fas fa-trash fa-fw"></em>
                    </button>
                </td>
                </tr>
                `;
                });
                editGradesJs.$elements.editGradeTbl.find('tbody').empty().append(tbody);
                editGradesJs.$elements.editGradeBtn = $('.editGradeBtn');
                editGradesJs.$elements.deleteGradeBtn = $('.deleteGradeBtn');
                editGradesJs.bindEditGradeAction();
                editGradesJs.bindDeleteGradeAction();
            }
            editGradesJs.initEditGradeDatatable();
        },
        bindEditGradeAction: function () {
            editGradesJs.$elements.editGradeBtn.on("click", function () {
                var grade_id = $(this).data('id');
                jQuery.ajax({
                    url: editGradesJs.getEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_SINGLE_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        gradeId: grade_id,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            editGradesJs.$elements.gradeData.gradeId.val(response.data.id);
                            editGradesJs.$elements.gradeData.gradeLesson.val(response.data.fullLesson.id);
                            editGradesJs.$elements.gradeData.gradeExamDate.val(response.data.exam_date);
                            editGradesJs.$elements.gradeData.gradeExamKind.val(response.data.exam_kind);
                            editGradesJs.$elements.gradeData.gradeAbsence.val(response.data.absence);
                            editGradesJs.$elements.gradeData.gradeGrade.val(response.data.grade);
                            editGradesJs.$elements.gradeData.gradeComments.val(response.data.comments);
                            editGradesJs.$elements.theGradeValue = response.data.grade;
                            editGradesJs.$elements.gradeData.gradeGrade.attr('readonly', false);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            editGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            editGradesJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        bindDeleteGradeAction: function () {
            editGradesJs.$elements.deleteGradeBtn.on("click", function () {
                editGradesJs.$elements.modals.deleteGradeModal.deleteGradeId.val($(this).data('id'));
            });
        },
        sendEditGradeData: function () {
            editGradesJs.$elements.modals.editGradeModal.saveEditGradeBtn.on('click', function () {
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                var the_data = editGradesJs.$elements.modals.editGradeModal.theForm.serialize();
                jQuery.ajax({
                    url: editGradesJs.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "UPDATE_DATA",
                        "Method": "PATCH",
                    },
                    data: the_data,
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        editGradesJs.$elements.modals.editGradeModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            editGradesJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            editGradesJs.manageAlertBox('danger', response.errorMessage);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                    error: function (jqXHR, error, errorThrown) {
                        console.log(errorThrown);
                        console.log(jqXHR);
                        if (jqXHR.responseJSON) {
                            editGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            editGradesJs.manageAlertBox('danger', errorThrown);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        sendDeleteGradeData: function () {
            editGradesJs.$elements.modals.deleteGradeModal.saveDeleteGradeBtn.on('click', function () {
                var btnDiv = $(this).parent();
                jQuery.ajax({
                    url: editGradesJs.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "DELETE_DATA",
                        "Method": "DELETE",
                    },
                    data: {
                        gradeDeleteId: editGradesJs.$elements.modals.deleteGradeModal.deleteGradeId.val()
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        editGradesJs.$elements.modals.deleteGradeModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            editGradesJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            editGradesJs.manageAlertBox('danger', response.errorMessage);
                        }
                        $(window).scrollTop(0);
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            editGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            editGradesJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        initEditGradeDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [0, 'desc'],
                    id: 'edit_grades_tbl',
                    columnDefs: [
                        {
                            "targets": 8,
                            "orderable": false
                        }
                    ],
                    scrollx: true,
                    scrolly: "70vh",
                    buttons: false,
                }

            );
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || editGradesJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    }
    editGradesJs.init();
});

function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}