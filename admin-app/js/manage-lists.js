$(document).ready(function () {
	var listMng = {
		$elements: {
			$createListItemsElements: {
				saveListBtn: $('#addListEntryBtn'),
				saveToList: $('#listModalDropdown'),
				createListEntryName: $('#listEntryName'),
				listEntryAddAppearanceName: $('#listEntryAddAppearanceName'),
				createListItemModal: $('#addListEntryModal'),
			},
			$editListItemElements: {
				editListBtn: null,
				editListModal: $('#editListModal'),
				editListItemField: $('#updateListItemName'),
				editListNameSpan: $('#fillEditListItem'),
				editListItemId: $('#listItemEditId'),
				saveEditListItemBtn: $('#saveEditListBtn'),
			},
			$deleteListItemElements: {
				deleteListBtn: null,
				savDeleteListItemBtn: $('#saveDeleteListBtn'),
				deleteListModal: $('#deleteListModal'),
				listItemDeleteId: $('#listItemDeleteId'),
			},
			usersTable: $('#usersTbl'),
			listDropdown: $('#listDropdown'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'ManageListsController.php']
		},
		init: function () {
			listMng.renderPageContent();
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			listMng.$elements.alertBox.removeClass();
			listMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			listMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			listMng.$elements.alertBox.find('#alertMessage').html(message);
			listMng.$elements.alertBox.show();
		},
		handleDeleteListItem: function () {
			listMng.$elements.$deleteListItemElements.deleteListBtn.click(function () {
				listMng.$elements.$deleteListItemElements.listItemDeleteId.val($(this).data('id'));
			});
		},
		handleSaveDeleteEditListItem: function () {
			listMng.$elements.$deleteListItemElements.savDeleteListItemBtn.on('click', function () {
				var btnDiv = listMng.$elements.$deleteListItemElements.savDeleteListItemBtn.parent();
				jQuery.ajax({
					url: listMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						listItemDeleteId: listMng.$elements.$deleteListItemElements.listItemDeleteId.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						listMng.$elements.$deleteListItemElements.deleteListModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							listMng.manageAlertBox('success', response.data);
							listMng.init();
						} else {
							listMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							listMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							listMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleEditListItem: function () {
			listMng.$elements.$editListItemElements.editListBtn.click(function () {
				listMng.$elements.$editListItemElements.editListNameSpan.text($(this).data('listitemappearance'));
				listMng.$elements.$editListItemElements.editListItemField.val($(this).data('listitem'));
				listMng.$elements.$editListItemElements.editListItemId.val($(this).data('id'));
			});
		},
		handleSaveEditListItem: function () {
			listMng.$elements.$editListItemElements.saveEditListItemBtn.on('click', function () {
				var btnDiv = listMng.$elements.$editListItemElements.saveEditListItemBtn.parent();
				jQuery.ajax({
					url: listMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						listItemEditId: listMng.$elements.$editListItemElements.editListItemId.val(),
						updateListItemName: listMng.$elements.$editListItemElements.editListItemField.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						listMng.$elements.$editListItemElements.editListModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							listMng.manageAlertBox('success', response.data);
							listMng.init();
						} else {
							listMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							listMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							listMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});

		},
		handleCreateListItem: function () {
			var btnDiv = listMng.$elements.$createListItemsElements.saveListBtn.parent();
			listMng.$elements.$createListItemsElements.saveListBtn.on('click', function () {
				jQuery.ajax({
					url: listMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						listModalDropdown: listMng.$elements.$createListItemsElements.saveToList.val(),
						listEntryName: listMng.$elements.$createListItemsElements.createListEntryName.val(),
						listEntryAddAppearanceName: listMng.$elements.$createListItemsElements.listEntryAddAppearanceName.val()
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						listMng.$elements.$createListItemsElements.createListItemModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							listMng.manageAlertBox('success', response.data);
							listMng.init();
						} else {
							listMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							listMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							listMng.manageAlertBox('danger', errorThrown);
						}
						listMng.$elements.$createListItemsElements.createListItemModal.modal('hide');
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		renderPageContent: function () {
			Helpers.emptyField(listMng.$elements.listDropdown);
			$('#list-table-body').empty();
			listMng.$elements.listDropdown.change(function () {
				var listName = $(this).val();
				jQuery.ajax({
					url: listMng.getEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_SINGLE_DATA",
						"Method": "GET",
					},
					data: {
						listName: listName,
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('animoSpin');
					},
					complete: function (data) {
						removeStandardWhirlAnime('animoSpin');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							var content = '';
							$(response.data).each(function () {
								content += `
					<tr id="${$(this).attr('id')}">
					<td><strong>${$(this).attr('id')}</strong></td>
					<td><span class="badge badge-primary">${$(this).attr('list_entry')}</span></td>
					<td>${Helpers.dateConverter($(this).attr('created_at'))}</td>
					<td>${Helpers.dateConverter($(this).attr('updated_at'))}</td>
					<td class="text-center">
					<button 
					class="btn btn-sm btn-info command-edit editListBtn mr-1"
					type="button"
					data-toggle="modal" 
					data-target="#editListModal"
					data-id="${$(this).attr('id')}"
					data-listitem = "${$(this).attr('list_entry')}"
					data-listitemappearance = "${$(this).attr('appearance_name')}"
					title="Επεξεργασία Λίστας ${$(this).attr('list_name')}">
					<em class="fas fa-pencil-alt fa-fw"></em>
					</button>
					<button 
					class="btn btn-sm btn-danger command-edit deleteListItemBtn"
					type="button"
					data-toggle="modal" 
					data-target="#deleteListModal"
					data-id="${$(this).attr('id')}"
					data-listitem = "${$(this).attr('list_entry')}"
					title="Διαγραφή Στοιχείου Λίστας ${$(this).attr('list_name')}">
					<em class="fa fa-trash fa-fw"></em>
					</button>
					</td>
					</tr>
					`;
							});
							$('#list-table-body').empty().append(content);
							//set editListBtn and deleteListBtn when it's been created!
							listMng.$elements.$editListItemElements.editListBtn = $('.editListBtn');
							listMng.$elements.$deleteListItemElements.deleteListBtn = $('.deleteListItemBtn');
						} else {
							listMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
						listMng.handleEditListItem();
						listMng.handleDeleteListItem();
					},
					error: function (jqXHR, error, errorThrown) {
						console.log(errorThrown);
						console.log(jqXHR);
						if (jqXHR.responseJSON) {
							listMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							listMng.manageAlertBox('danger', errorThrown);
						}
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
			$('#listModalDropdown').change(function () {
				$('#listEntryAddAppearanceName').val($('#listModalDropdown option:selected').text());
			});

			$(".addListBtn").click(function () {
				$('#listEntryAddName').val($('#listDropdown').val());
			});
		},
		bindButtonActions: function () {
			listMng.handleCreateListItem();
			listMng.handleSaveEditListItem();
			listMng.handleSaveDeleteEditListItem();
		},
	}
	//called on first load
	listMng.init();
	listMng.bindButtonActions();
});
