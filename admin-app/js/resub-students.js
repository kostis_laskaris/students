$(document).ready(function () {
    var resubStudentsJs = {
        $elements: {
            studentOptionModal: $('#resubStudentOptionModal'),
            studentOptionsTable: null,
            resubStudentOptionContainer: $('#resubStudentOptionContainer'),
            openOptionsPopupBtn: $('#viewOptionModal'),
            sendStudentResubDataBtn: $('#saveResubData'),
            sxEtosDropdown: $('#sx_etos'),
            studentsTable: $('#studentsTbl'),
            alertBox: $('#alertBox'),
            endpointUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchStudentController.php'],
            the_data: null,
            the_sx_etos: null,
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            resubStudentsJs.$elements.alertBox.removeClass();
            resubStudentsJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            resubStudentsJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            resubStudentsJs.$elements.alertBox.find('#alertMessage').html(message);
            resubStudentsJs.$elements.alertBox.show();
        },
        getEndPoint: function () {
            return resubStudentsJs.$elements.endpointUrl.join('/');
        },
        getSearchEndPoint: function () {
            return resubStudentsJs.$elements.searchEndpointUrl.join('/');
        },
        init: function () {
            resubStudentsJs.initAllData();
            resubStudentsJs.bindSxEtosDropdown();
            resubStudentsJs.sendResubStudentData();
            resubStudentsJs.bindOptionPopup();
        },
        initAllData: function () {
            resubStudentsJs.initStudentsTable();
        },
        initSelectDataColor: function () {
            $("[name='check-student']").change(function () {
                $(this).closest('tr').toggleClass('colorTr');
                $('#count-selected').text($('input:checkbox[name=check-student]:checked').length)
            });
        },
        initStudentsTable: function () {
            var list = `
                <tr class="student-card">
                    <td colspan="9" class="text-center text-bold text-danger">Επιλέξτε Σχολικό Έτος</td>				
				</tr>
				`;
            resubStudentsJs.$elements.studentsTable.find('tbody').empty().append(list);
            resubStudentsJs.initSelectDataColor();
        },
        refreshDatatables: function () {
            setTimeout(function () {
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
            }, 300);
        },
        bindSxEtosDropdown: function () {
            resubStudentsJs.$elements.sxEtosDropdown.on("change", function () {
                resubStudentsJs.$elements.the_sx_etos = $(this).val()
                var searchData = {
                    from_sx_etos: resubStudentsJs.$elements.the_sx_etos
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: resubStudentsJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: { attribs: searchData },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('resubStudentAnimo');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('resubStudentAnimo');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            var tbody = '';
                            if (response.data == 1) {
                                tbody += `
							<tr>
							<td class="text-center text-danger text-bold" style="font-size: 120%" colspan="9">Κανένα Αποτέλεσμα</td>
							</tr>`;
                            } else {
                                resubStudentsJs.$elements.the_data = response.data;
                                $.each(response.data, function (key, student) {
                                    tbody += `
                                    <tr class="student-card">
                                    <td>
                                        <div class="checkbox c-checkbox">
                                            <label class="lessonModalLbl">
                                            <input type="checkbox" class="check-student" name="check-student" data-id="${student.id}" data-lesson-ids="${student.lessonsIdsAsString}">
                                            <span class="fa fa-check"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>${student.id}</td>
                                    <td>${student.lastname}</td>
                                    <td>${student.firstname}</td>
                                    <td>${student.phone}</td>
                                    <td>${student.mobile}</td>
                                    <td>${student.email}</td>
                                    <td>${student.tmima}</td>
                                    <td>${student.fullAddress}</td>					
                                    </tr>									
									`
                                });
                            }
                            resubStudentsJs.$elements.studentsTable.find('tbody').empty().append(tbody);
                            resubStudentsJs.initSelectDataColor();
                            // resubStudentsJs.initDataTables();
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        bindOptionPopup: function () {
            resubStudentsJs.$elements.openOptionsPopupBtn.on("click", function () {
                var content = `<table class="table table-striped" id="studentOptionsTbl">
                                <thead>
                                <tr>
                                <th>Α/Α</th>
                                <th>Όνοματεπώνυμο</th>
                                <th>Τάξη</th>
                                <th>Τμήμα</th>
                                </tr>
                                </thead>
                                <tbody>`;
                var count_students = 0;
                $.each(resubStudentsJs.$elements.the_data, function (key, student) {
                    $("input:checkbox[name=check-student]:checked").each(function () {
                        if ($(this).data('id') == student.id) {
                            count_students++;
                            content += `
                            <tr data-student-id="${student.id}" data-lesson-ids="${student.lessonsIdsAsString}">
                            <td><b>${count_students}.</b></td>
                            <td>${student.fullName}</td>
                            <td>${model.taksi}</td>
                            <td>${model.tmima}</td>
                            </tr>
                            `;
                        }
                    });
                });
                content += `</tbody></table>`;
                resubStudentsJs.$elements.resubStudentOptionContainer.empty().append(content);
                resubStudentsJs.$elements.studentOptionsTable = $('#studentOptionsTbl');
            });
        },
        sendResubStudentData: function () {
            resubStudentsJs.$elements.sendStudentResubDataBtn.on("click", function () {
                var student_data = [];
                resubStudentsJs.$elements.studentOptionsTable.find('> tbody > tr').each(function (key, value) {
                    student_data.push({
                        'student_id': $(this).data('student-id'),
                        'lessons_ids': $(this).data('lesson-ids'),
                        'taksi': $(this).find('select#new-taksi-dropdown').val(),
                        'tmima': $(this).find('select#new-tmima-dropdown').val(),
                    });
                });
                var sendData = {
                    student_data: student_data,
                    from_sx_etos: resubStudentsJs.$elements.the_sx_etos,
                }
				var saveBtn = $(this);
				var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: resubStudentsJs.getEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "CUSTOM_INSERT_DATA",
                        "Method": "POST",
                    },
                    data: { attribs: sendData },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
                        resubStudentsJs.$elements.studentOptionModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            resubStudentsJs.manageAlertBox('success', response.data + "<p>Η σελίδα θα <b>ανανεωθεί σε 3 δευτερόλεπτα</b></p>");
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            resubStudentsJs.manageAlertBox('danger', response.errorMessage);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON.errorMessage) {
                            resubStudentsJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            resubStudentsJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        initDataTables: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    id: 'studentsTbl',
                    columnDefs: [
                        {
                            "targets": 0,
                            "orderable": false
                        }
                    ],
                    scrollx: true,
                    scrolly: "70vh",
                    downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
                    buttons: false,
                },
            );
        },
    }
    resubStudentsJs.init();
});

function selectCheckbox(args) {
    unselectAll();
    let dataArr = args.split(',');
    for (var i = 0; i < dataArr.length; i++) {
        document.getElementById(dataArr[i]).checked = true;
    }
}

function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}