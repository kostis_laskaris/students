$(document).ready(function () {
	var userMng = {
		$elements: {
			$createUserElements: {
				addFirstname: $('#addFirstname'),
				addLastname: $('#addLastname'),
				addEmail: $('#addEmail'),
				addUsername: $('#addUsername'),
				addPermission: $('#addPermission'),
				addStatus: $('#addStatus'),
				addPassword: $('#addPassword'),
				addRePass: $('#addRePass'),
				createUserModal: $('#createUserModal'),
			},
			$editPassElements: {
				passUserId: $('#passUserId'),
				passUsername: $('#passUsername'),
				newPass: $('#newPass'),
				reNewPass: $('#reNewPass'),
				editPassModal: $('#passModal'),
			},
			$editUserElements: {
				editUserFirstname: $('#firstname'),
				editUserLastname: $('#lastname'),
				editUserEmail: $('#email'),
				editUserUsername: $('#username'),
				editUserPermission: $('#permission'),
				editUserStatus: $('#status'),
				editUserModal: $('#editUserModal'),
				saveEditUserBtn: $('#editUserBtn'),
			},
			usersTable: $('#usersTbl'),
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'UserController.php']
		},
		init: function () {
			jQuery.ajax({
				url: userMng.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('animoSort');
				},
				complete: function (data) {
					removeStandardWhirlAnime('animoSort');
				},
				success: function (response, textStatus, jQxhr) {
					destroyDatatable('usersTbl');
					if (response.code === 200) {
						var tbody = userMng.$elements.usersTable.find('tbody');
						var tbodyData = '';
						$.each(response.data, function (key, user) {

							let kindBadge = statusColor = statusText = icon = '';

							if (user.permission == "Administrator") {
								kindBadge = "badge-primary";
							} else {
								kindBadge = "badge-green";
							}

							if (user.status == 1) {
								statusColor = 'badge-success';
								statusText = 'Ενεργός';
								icon = '<em class="fas fa-user-check fa-120x text-muted"></em>';
							} else {
								statusColor = "badge-danger";
								statusText = "Ανενεργός";
								icon = '<em class="fas fa-user-times fa-120x text-danger"></em>';
							}

							tbodyData += `
								<tr id="${user.id}">
								<td>${icon}</td>
								<td>${user.id}</td>
								<td>${user.profile.firstname}</td>
								<td>${user.profile.lastname}</td>
								<td>${user.username}</td>
								<td>
									<a href="mailto:${user.email}">
										${user.email}														
									</a>
								</td>
								<td>
									<span class="badge ${statusColor}">
									${statusText}															
									</span>
								</td>
								<td class="text-center">
									<span class="badge ${kindBadge}">
									${user.permission}														
									</span>
								</td>
								<td data-sort="${user.last_activity_date}">
									${user.last_activity_date ? Helpers.dateConverter(user.last_activity_date) : ''}
								</td>
								<td data-sort="${user.date_submitted}">
									${Helpers.dateOnlyConverter(user.date_submitted)}
								</td>
								<td class="text-center">
									<button 
									class="btn btn-sm btn-purple mr-1 command-edit"
									title="Αλλαγή Κωδικού ${user.username}"
									data-toggle="modal" 
									data-target="#passModal"
									onclick="document.getElementById('passUserId').value = '${user.id}';document.getElementById('passUsername').value = '${user.username}';">
									<em class="fas fa-key fa-fw"></em>
									</button>
									<button
										data-id="${user.id}"
										data-toggle="modal" 
										data-target="#editUserModal"
										class="btn btn-sm btn-info mr-1 command-edit editUserBtn"
										title="Επεξεργασία Χρήστη ${user.username}">
										<em class="fas fa-user-edit fa-fw"></em>
									</button>
								</td>
								</tr>
							`;
						});
						$(tbody).empty().append(tbodyData);
						userMng.initTables();
						userMng.refreshDatatables();
						userMng.handleEditUser();
					} else {
						alert(response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						userMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						userMng.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		initTables: function () {
			initDatatable(
				{
					pages: [15, - 1],
					order: [1, 'asc'],
					id: 'usersTbl',
					columnDefs: [{
						"targets": 0,
						"orderable": false
					}, {
						"targets": 10,
						"orderable": false
					}],
					downloadTitle: "Πίνακας Χρηστών Εφαρμογής",
                    buttons: true,
				}
			);
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		refreshDatatables: function () {
			setTimeout(function () {
				$($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
			}, 300);
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			userMng.$elements.alertBox.removeClass();
			userMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			userMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			userMng.$elements.alertBox.find('#alertMessage').html(message);
			userMng.$elements.alertBox.show();
		},
		handleEditPassword: function () {
			$('.editPassBtn').on('click', function () {
				var editBtn = $(this);
				var btnDiv = editBtn.parent();
				jQuery.ajax({
					url: userMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "CUSTOM_UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						newPass: userMng.$elements.$editPassElements.newPass.val(),
						reNewPass: userMng.$elements.$editPassElements.reNewPass.val(),
						passUserId: userMng.$elements.$editPassElements.passUserId.val(),
						passUsername: userMng.$elements.$editPassElements.passUsername.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						userMng.$elements.$editPassElements.editPassModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							userMng.manageAlertBox('success', response.data);
							userMng.init();
							userMng.refreshDatatables();
						} else {
							userMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							userMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							userMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});

		},
		handleEditUser: function () {
			$(".editUserBtn").click(function () {
				var userBtnId = $(this).data('id');
				$('#editUserBtn').data('id', userBtnId);
				jQuery.ajax({
					url: userMng.getEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_SINGLE_DATA",
						"Method": "GET",
					},
					data: {
						editUserId: userBtnId
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('editUserModalAnimo');
					},
					complete: function (data) {
						removeStandardWhirlAnime('editUserModalAnimo');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							$('#username').val(response.data.username);
							$('#email').val(response.data.email);
							$('#permission').val(response.data.permission);
							$('#status').val(response.data.status);
							$('#firstname').val(response.data.profile.firstname);
							$('#lastname').val(response.data.profile.lastname);
						} else {
							userMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							userMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							userMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleSaveEditShortcut: function () {
			userMng.$elements.$editUserElements.saveEditUserBtn.on('click', function () {
				var editBtn = $(this);
				var btnDiv = editBtn.parent();
				jQuery.ajax({
					url: userMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "UPDATE_DATA",
						"Method": "PATCH",
					},
					data: {
						editUserId: editBtn.data('id'),
						username: userMng.$elements.$editUserElements.editUserUsername.val(),
						email: userMng.$elements.$editUserElements.editUserEmail.val(),
						permission: userMng.$elements.$editUserElements.editUserPermission.val(),
						status: userMng.$elements.$editUserElements.editUserStatus.val(),
						firstname: userMng.$elements.$editUserElements.editUserFirstname.val(),
						lastname: userMng.$elements.$editUserElements.editUserLastname.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						userMng.$elements.$editUserElements.editUserModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							userMng.manageAlertBox('success', response.data);
							userMng.init();
							userMng.refreshDatatables();
						} else {
							userMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							userMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							userMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleCreateUser: function () {
			$('#addUserBtn').on('click', function () {
				var addBtn = $(this);
				var btnDiv = addBtn.parent();
				jQuery.ajax({
					url: userMng.getEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						addFirstname: userMng.$elements.$createUserElements.addFirstname.val(),
						addLastname: userMng.$elements.$createUserElements.addLastname.val(),
						addEmail: userMng.$elements.$createUserElements.addEmail.val(),
						addUsername: userMng.$elements.$createUserElements.addUsername.val(),
						addPermission: userMng.$elements.$createUserElements.addPermission.val(),
						addStatus: userMng.$elements.$createUserElements.addStatus.val(),
						addPassword: userMng.$elements.$createUserElements.addPassword.val(),
						addRePass: userMng.$elements.$createUserElements.addRePass.val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.ball-pulse').hide();
						btnDiv.find('.processingDiv').hide();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						userMng.$elements.$createUserElements.createUserModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							userMng.manageAlertBox('success', response.data);
							userMng.init();
							userMng.refreshDatatables();
						} else {
							userMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							userMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							userMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		bindButtonActions: function () {
			userMng.handleCreateUser();
			userMng.handleEditPassword();
			userMng.handleSaveEditShortcut();
		},
	}
	//called on first load
	userMng.init();
	userMng.bindButtonActions();
	Helpers.emptyField($('#usersTbl_filter input'));
});