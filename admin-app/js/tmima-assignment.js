$(document).ready(function () {

	var TmimaAssignmentMng = {

		$elements: {
			$editTmimaAssignment: {
				saveTmimaAssignmentBtn: $('#saveTmimaAssignmentBtn'),
				createTmimaAssignmentModal: $('#createTmimaAssignmentModal'),
			},
			$deleteTmimaAssignment: {
				deleteTmimaAssigmentBtn: $('#deleteTmimaAssigmentBtn'),
				deleteTmimaAssignmentModal: $('#deleteTmimaAssignmentModal'),
			},
			endpointUrl: ['..', 'php_actions', 'Controllers', 'TmimataAssignmentController.php'],
			userTmimataEndpointUrl: ['..', 'php_actions', 'Controllers', 'UserTmimataController.php'],
			userTmimataCard: $('#user-tmimata-card'),
			alertBox: $('#alertBox'),
			addAssignmentBtn: null,
			removeAssignmentBtn: null,
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		getUserTmimataEndPoint: function () {
			return this.$elements.userTmimataEndpointUrl.join('/');
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			TmimaAssignmentMng.$elements.alertBox.removeClass();
			TmimaAssignmentMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			TmimaAssignmentMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			TmimaAssignmentMng.$elements.alertBox.find('#alertMessage').html(message);
			TmimaAssignmentMng.$elements.alertBox.show();
		},
		init: function () {
			jQuery.ajax({
				url: TmimaAssignmentMng.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('animoSort');
				},
				complete: function (data) {
					removeStandardWhirlAnime('animoSort');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						var TmimataAssignmentBody = '';
						$.each(response.data, function (key, one_user) {
							TmimataAssignmentBody += `
							<div class="row d-flex py-2 align-items-center">
							<div class="col-xl-3 d-flex flex-column" title="${one_user.status == 1 ? '' : 'Ανενεργός Χρήστης'}">
								<div>
									<i class="fas fa-user fa-150x mr-2 ${one_user.status == 1 ? '' : 'text-danger'}"></i>
									${one_user.profile.lastname + ' ' + one_user.profile.firstname}
								</div>
								<div>
									<small class="mr-1 pr-1 br">${one_user.username}</small>
									<small class="text-bold ${one_user.status == 1 ? 'text-success-dark' : 'text-danger'}">${one_user.status == 1 ? 'Ενεργός' : 'Ανενεργός'}</small>
								</div>
							</div>
							<div class="input-group col-xl-9">
								<div class="input-group-prepend">
									<button ${one_user.status == 1 ? '' : 'disabled'} data-id="${one_user.id}" class="btn btn-primary inputBtn addAssignmentBtn" data-target="#createTmimaAssignmentModal" data-toggle="modal" title="Προσθήκη Αναθέσεων" type="button">
										<i class="fas fa-plus"></i>
									</button>
									<button ${one_user.status == 1 ? '' : 'disabled'} data-id="${one_user.id}" class="btn btn-danger inputBtn removeAssignmentBtn" data-target="#deleteTmimaAssignmentModal" data-toggle="modal" title="Διαγαφή Αναθέσεων" type="button">
										<i class="fas fa-minus"></i>
									</button>
								</div>
								<input class="form-control" type="text" placeholder="Προσθήκη Τμημάτων.." value="${TmimaAssignmentMng.arrayToCommaSeperatedString(one_user.tmimata)}" readonly>
							</div>
						</div>	
							`;
						})
						TmimaAssignmentMng.$elements.userTmimataCard.empty().append(TmimataAssignmentBody);
						TmimaAssignmentMng.$elements.addAssignmentBtn = $('.addAssignmentBtn');
						TmimaAssignmentMng.$elements.removeAssignmentBtn = $('.removeAssignmentBtn');
						TmimaAssignmentMng.handleManageAssignment();
					} else {
						TmimaAssignmentMng.manageAlertBox('danger', response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						TmimaAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						TmimaAssignmentMng.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		arrayToCommaSeperatedString: function (data) {
			var output = '';
			$.each(data, function (key, value) {
				output += value.name + ', ';
			});
			output = output.substring(0, output.length - 2);
			return output;
		},
		handleManageAssignment: function () {

			TmimaAssignmentMng.$elements.removeAssignmentBtn.click(function () {
				$('#deleteUserlessonsId').val($(this).data('id'));
			});

			TmimaAssignmentMng.$elements.addAssignmentBtn.click(function () {
				$('#addUserId').val($(this).data('id'));
				jQuery.ajax({
					url: TmimaAssignmentMng.getUserTmimataEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_ALL_DATA",
						"Method": "GET",
					},
					data: { userId: $('#addUserId').val() },
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('createAssignmentModalAnimo');
					},
					complete: function (data) {
						removeStandardWhirlAnime('createAssignmentModalAnimo');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							unselectAll();
							$(response.data).each(function () {
								$('#' + $(this).attr('tmima_id')).prop("checked", true);
							});
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							TmimaAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
			TmimaAssignmentMng.$elements.removeAssignmentBtn.click(function () {
				$('#deleteUserlessonsId').val($(this).data('id'));
				$('#deleteUserlessonsUsername').val($(this).data('username'));
			});
		},
		handleEditTmimaAssignment: function () {
			TmimaAssignmentMng.$elements.$editTmimaAssignment.saveTmimaAssignmentBtn.on('click', function () {
				var btnDiv = TmimaAssignmentMng.$elements.$editTmimaAssignment.saveTmimaAssignmentBtn.parent();
				var tmimataToSave = [];
				$("input[name='user_tmimata[]']:checked").each(function () {
					tmimataToSave.push(this.value);
				});
				jQuery.ajax({
					url: TmimaAssignmentMng.getUserTmimataEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						addUserId: $('#addUserId').val(),
						user_tmimata: tmimataToSave,
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div class="loaderDiv"><div style='float:left; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait </div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.loaderDiv').remove();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						TmimaAssignmentMng.$elements.$editTmimaAssignment.createTmimaAssignmentModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							TmimaAssignmentMng.manageAlertBox('success', response.data);
							TmimaAssignmentMng.init();
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							TmimaAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleDeleteTmimaAssignment: function () {
			TmimaAssignmentMng.$elements.$deleteTmimaAssignment.deleteTmimaAssigmentBtn.on('click', function () {
				var btnDiv = TmimaAssignmentMng.$elements.$deleteTmimaAssignment.deleteTmimaAssigmentBtn.parent();
				jQuery.ajax({
					url: TmimaAssignmentMng.getUserTmimataEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						deleteUserlessonsId: $('#deleteUserlessonsId').val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div class="loaderDiv"><div style='float:left; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait </div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.loaderDiv').remove();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						TmimaAssignmentMng.$elements.$deleteTmimaAssignment.deleteTmimaAssignmentModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							TmimaAssignmentMng.manageAlertBox('success', response.data);
							TmimaAssignmentMng.init();
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							TmimaAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							TmimaAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		bindButtonActions: function () {
			TmimaAssignmentMng.handleEditTmimaAssignment();
			TmimaAssignmentMng.handleDeleteTmimaAssignment();
		},
	}
	TmimaAssignmentMng.init();
	TmimaAssignmentMng.bindButtonActions();
});

function unselectAll() {
	$('#assignmentsBox').find(':input').each(function () {
		$(this).prop("checked", false);
	})
}