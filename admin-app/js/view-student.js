$(document).ready(function () {

    var viewStudentJs = {
        $elements: {
            dropdownMenu: {
                menuEditStudent: $('#menuEditStudent'),
                menuDeleteStudent: $('#menuDeleteStudent'),
            },
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                lessons: $('#searchLessons'),
                lessonIds: $('#searchLessonIds'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
                searchLoader: $('#searchingLoader'),
                lessonModal: $('#lessonModal'),
                removeLessonsBtn: $('.removeLessonsBtn'),
            },
            studentElements: {
                studentName: $('#studentName'),
                studentClass: $('#studentClass'),
                studentTmima: $('#studentTmima'),
                email: $('#email'),
                sendEmailLink: $('#sendEmailLink'),
                mobile: $('#mobile'),
                phone: $('#phone'),
                address: $('#address'),
                perioxi: $('#perioxi'),
                zip: $('#zip'),
                date_eggrafis: $('#date_eggrafis'),
                mname: $('#mname'),
                faname: $('#faname'),
                mphone: $('#mphone'),
                fphone: $('#fphone'),
                kid_name: $('#kid_name'),
                kidEmail: $('#kidEmail'),
                kid_phone: $('#kid_phone'),
                fee: $('#fee'),
                class: $('#class'),
                tmima: $('#tmima'),
                periodos: $('#periodos'),
                sx_etos: $('#sx_etos'),
                informed_by: $('#informed_by'),
                lessons: $('#lessons'),
                googleMapRedirect: $('#googleMapRedirect'),
                emailKidLink: $('#emailKidLink'),
            },
            modals: {
                studentlessonsPicker: {
                    theModal: $('#lessonsModal'),
                    saveLessons: $('#saveLessons'),
                },
                searchModal: {
                    theModal: $('#filterStudentGradesModal'),
                },
                deleteStudentModal: {
                    theModal: $('#deleteStudentModal'),
                    deleteStudentBtn: $('#dleteStudentBtn'),
                },
            },
            gradeStatsTables: null,
            statsContainer: $('#statsContainer'),
            studentGradesBarChart: $('#studentGradesBarChart'),
            gradesTable: $('#student_grades_tbl'),
            studentGradesCard: $('#studentGradesCard'),
            studentId: null,
            assignmentsBox: $('#assignmentsBox'),
            alertBox: $('#alertBox'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
            studentsUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
            gradesUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            url: ["..", "php_actions", "get_actions", "get_grade_search.php"],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
            statsUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php']
        },
        resetSearchFields: function () {
            Helpers.emptyFields(viewStudentJs.$elements.searchElements);
            unselectAll();
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            viewStudentJs.$elements.alertBox.removeClass();
            viewStudentJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            viewStudentJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            viewStudentJs.$elements.alertBox.find('#alertMessage').html(message);
            viewStudentJs.$elements.alertBox.show();
        },
        init: function () {
            viewStudentJs.$elements.studentId = Helpers.b64_to_utf8(Helpers.urlParam('sid'));
            viewStudentJs.renderStudentData();
            viewStudentJs.renderStudentGradesData();
            viewStudentJs.bindSaveLessons();
            viewStudentJs.bindRemoveLessons();
            viewStudentJs.bindSearchBtn();
            viewStudentJs.bindResetSearchBtn();
            viewStudentJs.renderStatsCards();
            viewStudentJs.handleDeleteStudent();
        },
        getGradeEndPoint: function () {
            return this.$elements.gradesUrl.join('/');
        },
        getStudentEndPoint: function () {
            return this.$elements.studentsUrl.join('/');
        },
        getEndPoint: function () {
            return this.$elements.url.join('/');
        },
        getSearchEndPoint: function () {
            return viewStudentJs.$elements.searchEndpointUrl.join('/');
        },
        getGradeStatsEndPoint: function () {
            return this.$elements.statsUrl.join('/');
        },
        renderStudentData: function () {
            jQuery.ajax({
                url: viewStudentJs.getStudentEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_SINGLE_DATA",
                    "Method": "GET",
                },
                data: {
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: "student_by_id"
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('theAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('theAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewStudentJs.$elements.studentElements.studentName.text(response.data.fullName);
                        viewStudentJs.$elements.studentElements.studentClass.text(response.data.class.list_entry);
                        viewStudentJs.$elements.studentElements.studentTmima.text(response.data.tmima.name);
                        //personal info
                        viewStudentJs.$elements.studentElements.email.val(response.data.email);
                        viewStudentJs.$elements.studentElements.phone.val(response.data.phone);
                        viewStudentJs.$elements.studentElements.mobile.val(response.data.mobile);
                        viewStudentJs.$elements.studentElements.address.val(response.data.address);
                        viewStudentJs.$elements.studentElements.perioxi.val(response.data.perioxi);
                        viewStudentJs.$elements.studentElements.zip.val(response.data.zip);
                        viewStudentJs.$elements.studentElements.date_eggrafis.val(Helpers.dateOnlyConverter(response.data.date_eggrafis));
                        //parents info
                        viewStudentJs.$elements.studentElements.mname.val(response.data.mname);
                        viewStudentJs.$elements.studentElements.faname.val(response.data.faname);
                        viewStudentJs.$elements.studentElements.mphone.val(response.data.mphone);
                        viewStudentJs.$elements.studentElements.fphone.val(response.data.fphone);
                        viewStudentJs.$elements.studentElements.kid_name.val(response.data.kid_name);
                        viewStudentJs.$elements.studentElements.kidEmail.val(response.data.kidEmail);
                        viewStudentJs.$elements.studentElements.kid_phone.val(response.data.kid_phone);
                        //studies info
                        viewStudentJs.$elements.studentElements.fee.val(response.data.fee);
                        viewStudentJs.$elements.studentElements.class.val(response.data.class.list_entry);
                        viewStudentJs.$elements.studentElements.tmima.val(response.data.tmima.name);
                        viewStudentJs.$elements.studentElements.periodos.val(response.data.periodos);
                        viewStudentJs.$elements.studentElements.sx_etos.val(response.data.sx_etos);
                        viewStudentJs.$elements.studentElements.informed_by.val(response.data.informed_by?response.data.informed_by.list_entry:'');
                        viewStudentJs.$elements.studentElements.lessons.val(response.data.lessonsAsString);

                        viewStudentJs.$elements.dropdownMenu.menuEditStudent.attr('href', `edit-student?sid=${Helpers.urlParam('sid')}`);
                        viewStudentJs.$elements.dropdownMenu.menuDeleteStudent.data('id', viewStudentJs.$elements.studentId);
                        viewStudentJs.$elements.dropdownMenu.menuDeleteStudent.data('studentname', response.data.fullName);
                        if (response.data.email) {
                            viewStudentJs.$elements.studentElements.sendEmailLink.attr('href', 'mailto:' + response.data.email);
                        } else {
                            viewStudentJs.$elements.studentElements.sendEmailLink.addClass('disabled');
                        }
                        if (response.data.address) {
                            viewStudentJs.$elements.studentElements.googleMapRedirect.attr('href', 'http://maps.google.com?q=' + response.data.fullAddress);
                        } else {
                            viewStudentJs.$elements.studentElements.googleMapRedirect.addClass('disabled');
                        }
                        if (response.data.kidEmail) {
                            viewStudentJs.$elements.studentElements.emailKidLink.attr('href', 'mailto:' + response.data.email_kidemona);
                        } else {
                            viewStudentJs.$elements.studentElements.emailKidLink.addClass('disabled');
                        }
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    if (jqXHR.responseJSON.errorMessage) {
                        editStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        editStudentJs.manageAlertBox('danger', errorThrown);
                    }
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderStudentGradesData: function () {
            jQuery.ajax({
                url: viewStudentJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: 'grades_by_student',
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSortStudentGrades');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSortStudentGrades');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewStudentJs.renderStudentGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    var tbody = `
                    <tr>
                        <td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Συνέβη Σφάλμα:</td>
                    </tr>
                    <tr>
                        <td class="text-center text-bold" style="font-size: 120%" colspan="8">${errorThrown}</td>
                    </tr>`;
                    viewStudentJs.$elements.gradesTable.find('tbody').empty().append(tbody);
                },
            });
        },
        bindSaveLessons: function () {
            viewStudentJs.$elements.modals.studentlessonsPicker.saveLessons.on('click', function () {
                var lessons = '';
                var ids = '';
                $('#assignmentsBox').find(':input').each(function () {
                    if ($(this).prop("checked")) {
                        lessons += $(this).attr("name") + ", ";
                        ids += $(this).attr("id") + ",";
                    }
                });
                viewStudentJs.$elements.searchElements.lessons.val(lessons.substring(0, lessons.length - 2));
                viewStudentJs.$elements.searchElements.lessonIds.val(ids.substring(0, ids.length - 1));
                viewStudentJs.$elements.modals.studentlessonsPicker.theModal.modal('hide');
            });
        },
        bindRemoveLessons: function () {
            viewStudentJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
                Helpers.emptyField(viewStudentJs.$elements.searchElements.lessons);
                Helpers.emptyField(viewStudentJs.$elements.searchElements.lessonIds);
                unselectAll();
            });
        },
        bindResetSearchBtn: function () {
            $('#removeFilterStudentGradesModal').on("click", function () {
                $('#removeFilterStudentGradesModal').hide();
                viewStudentJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            viewStudentJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterStudentGradesModal').hide();
                viewStudentJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.searchElements.lessons.prop('readonly', true);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        bindSearchBtn: function () {
            viewStudentJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!viewStudentJs.isTelestisAccepted(viewStudentJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterStudentGradesModal').show();
                var searchData = {
                    user_id: viewStudentJs.$elements.searchElements.teacher.val(),
                    exam_kind: viewStudentJs.$elements.searchElements.examKind.val(),
                    absence: viewStudentJs.$elements.searchElements.absence.val(),
                    grade: viewStudentJs.$elements.searchElements.telestis.val() + ":" + viewStudentJs.$elements.searchElements.grade.val(),
                    date_from: viewStudentJs.$elements.searchElements.examDateFrom.val(),
                    date_to: viewStudentJs.$elements.searchElements.examDateTo.val(),
                    lesson_id: viewStudentJs.$elements.searchElements.lessonIds.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        renderStatsCards: function () {
            jQuery.ajax({
                url: viewStudentJs.getGradeStatsEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                dataType: 'json',
                data: {
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: 'grades_by_student_per_lesson',
                },
                cache: false,
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        if (response.data === 1) {
                            return false;
                        }
                        var card = '';
                        var lessonNames = [];
                        var gradesAvgs = [];
                        $.each(response.data, function (key, lessonStats) {
                            var grade20 = (lessonStats.gradeAvg * 20 / 100).toFixed(1);
                            lessonNames.push(lessonStats.lessonName);
                            gradesAvgs.push(lessonStats.gradeAvg);
                            card += `
                            <div class="col-xl-4">
                                <div class="card b shadow-sm">
                                    <div class="card-header d-flex justify-content-between align-items-center py-2 border-bottom">
                                        <div>
                                            <h4 class="m-0">${lessonStats.lessonName}</h4>
                                            <small class="text-muted"><i class="far fa-calendar-check"></i> <strong>Τελευταία Βαθμολογία: </strong>${Helpers.dateOnlyConverter(lessonStats.latestExamDate)}</small>
                                        </div>
                                        <div class="${lessonStats.gradeAvg < 50 ? 'text-danger' : 'text-green'}">
                                            ${lessonStats.gradeAvg < 50 ? '<i class="fas fa-150x fa-arrow-down"></i>' : '<i class="fas fa-150x fa-arrow-up"></i>'}
									    </div>
                                    </div>
                                    <div class="card-body">
                                        <p class="display-4 text-center ${lessonStats.gradeAvg < 50 ? 'text-danger' : 'text-green'}">
                                            ${lessonStats.gradeAvg}% <span style="font-size: 80%">(${grade20.replace(".", ",")})</span>
                                        </p>
                                        <div>
                                            <table class="table lesson_stats" data-lessonid="${lessonStats.lesson_id}">
                                                <tbody>
                                                    <tr>
                                                        <td><strong>Αριθμός Βαθμολογιών</strong></td>
                                                        <td>${lessonStats.gradeCount}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Μέγιστη</strong></td>
                                                        <td>${lessonStats.maxGrade}%</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Ελάχιστη</strong></td>
                                                        <td>${lessonStats.minGrade}%</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Μέσος Όρος Test</strong></td>
                                                        <td>${lessonStats.testAvg ? Math.round(lessonStats.testAvg) + "%" : "---"}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Μέσος Όρος Διαγωνισμάτων</strong></td>
                                                        <td>${lessonStats.examAvg ? Math.round(lessonStats.examAvg) + "%" : "---"}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Μέσος Όρος Εργασιών</strong></td>
                                                        <td>${lessonStats.ergasiaAvg ? Math.round(lessonStats.ergasiaAvg) + "%" : "---"}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Απουσίες</strong></td>
                                                        <td>${lessonStats.absence}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="mt-3">
                                            <canvas id="lessonGradeLineChart:${lessonStats.lesson_id}"></canvas>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <a class="btn btn-primary btn-oval" href="view-lesson?lesid=${Helpers.utf8_to_b64(lessonStats.lesson_id)}&lesname=${Helpers.utf8_to_b64(lessonStats.lessonName)}">
                                            Προβολή Μαθήματος
                                        </a>
                                    </div>
                                </div>
                            </div>
                            `;
                        });
                        viewStudentJs.$elements.statsContainer.empty().append(card);
                        viewStudentJs.$elements.gradeStatsTables = $('.lesson_stats');
                        viewStudentJs.initFlotChard();
                        viewStudentJs.initStudentGradesBarChart(lessonNames, gradesAvgs);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                },
            });
        },
        initFlotChard: function () {
            var lessonIds = [];
            viewStudentJs.$elements.gradeStatsTables.each(function (key, table) {
                lessonIds.push($(this).data('lessonid'));
            });
            jQuery.ajax({
                url: viewStudentJs.getGradeStatsEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                dataType: 'json',
                data: {
                    lessonIds: lessonIds,
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: 'grades_by_student_per_lesson_for_chart',
                },
                cache: false,
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        $.each(response.data, function (lesson_id, lesson) {
                            var labels = [];
                            var grades = [];
                            $.each(lesson, function (key, data) {
                                labels.push(Helpers.dateConverterWithoutDay(data[0]));
                                grades.push(data[1]);
                            });
                            var lineData = {
                                labels: labels,
                                datasets: [{
                                    backgroundColor: 'rgba(114,102,186,0.2)',
                                    borderColor: 'rgba(114,102,186,1)',
                                    pointBorderColor: '#fff',
                                    data: grades,
                                    pointRadius: 5,
                                    pointBackgroundColor: '#443d71',
                                }]
                            };

                            var lineOptions = {
                                legend: {
                                    display: false
                                },
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            min: 0,
                                            max: 100,
                                            stepSize: 20,
                                        }
                                    }]
                                }
                            };
                            var linectx = document.getElementById('lessonGradeLineChart:' + lesson_id).getContext('2d');
                            var lineChart = new Chart(linectx, {
                                data: lineData,
                                type: 'line',
                                options: lineOptions
                            });
                        });
                    }
                }
            });
        },
        initStudentGradesBarChart: function (lessons, grades) {
            var barData = {
                labels: lessons,
                datasets: [{
                    data: grades,
                    backgroundColor: ["rgba(128, 0, 0, 0.5)", "rgba(170, 110, 40, 0.5)", "rgba(128, 128, 0, 0.5)", "rgba(0, 128, 128, 0.5)", "rgba(230, 25, 75, 0.5)", "rgba(245, 130, 48, 0.5)",
                        "rgba(255, 225, 25, 0.5)", "rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)", "rgba(0, 130, 200, 0.5)", "rgba(145, 30, 180, 0.5)", "rgba(240, 50, 230, 0.5)",
                        "rgba(128, 128, 128, 0.5)", "rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)", "rgba(51,51,51, 0.5)", "rgba(229,30,50, 0.5)", "rgba(181,182,30, 0.5)"
                        , "rgba(0,100,0, 0.5)", "rgba(0,0,139, 0.5)", "rgba(0,0,0, 0.5)", "rgba(0,255,255, 0.5)"],
                    borderColor: ["rgba(128, 0, 0, 1)", "rgba(170, 110, 40, 1)", "rgba(128, 128, 0, 1)", "rgba(0, 128, 128, 1)", "rgba(230, 25, 75, 1)", "rgba(245, 130, 48, 1)",
                        "rgba(255, 225, 25, 1)", "rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)", "rgba(0, 130, 200, 1)", "rgba(145, 30, 180, 1)", "rgba(240, 50, 230, 1)",
                        "rgba(128, 128, 128, 1)", "rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)", "rgba(51,51,51, 1)", "rgba(229,30,50, 1)", "rgba(181,182,30, 1)"
                        , "rgba(0,100,0, 1)", "rgba(0,0,139, 1)", "rgba(0,0,0, 1)", "rgba(0,255,255, 1)"],
                    borderWidth: 1.5,
                }]
            };
            var barOptions = {
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                            stepSize: 10,
                        }
                    }]
                }
            };
            var barctx = document.getElementById('studentGradesBarChart').getContext('2d');
            var barChart = new Chart(barctx, {
                data: barData,
                type: 'bar',
                options: barOptions
            });
        },
        initDataTables: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [1, 'desc'],
                    id: 'student_grades_tbl',
                    scrollx: true,                    
                    columnDefs: [{
                        targets: 0,
                        orderable: false
                    }],
                    scrolly: "60vh",
                    downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
                    buttons: true,
                },
            );
        },
        renderStudentGradesTableData: function (the_data) {
            destroyDatatable('student_grades_tbl');
            var tbody = '';
            var cntExam = 0;
            var cntTest = 0;
            var cntErgasia = 0;
            var cntParon = 0;
            var cntApon = 0;
            if (the_data == 1) {
                viewStudentJs.$elements.gradesTable.find('tbody').remove();
            } else {
                $.each(the_data, function (key, grade) {
                    grade.absence == "ΠΑΡΩΝ" ? cntParon++ : cntApon++;
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                        cntExam++;
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                        cntErgasia++;
                    } else {
                        cntTest++;
                    }
                    tbody += `
                    <tr>             
                        <td>
                            <div class="${grade.grade < 50 ? 'text-danger' : 'text-green'}">
                            ${grade.grade < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                            </div>
                        </td>
                        <td>${grade.id}</td>
                        <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                        <td>${grade.fullUser.profile.fullname}</td>
                        <td>
                            <span class="badge ${badgeColor}">${grade.exam_kind}</span>
                        </td>
                        <td>
                            <span class="badge ${grade.absence == "ΠΑΡΩΝ" ? 'badge-success' : 'badge-danger'}">${grade.absence}</span>
                        </td>
                        <td>
                            <a href="view-lesson?lesid=${Helpers.utf8_to_b64(grade.lesson_id)}&lesname=${Helpers.utf8_to_b64(grade.fullLesson.name)}">${grade.fullLesson.name}</a>
                        </td>
                        <td><strong>${grade.grade}</strong></td>
                        <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                        <td>${!Helpers.isEmpty(grade.updated_at) ? grade.updated_at : "Καμία"}</td>
                    </tr>									
                    `
                });
            }
            viewStudentJs.$elements.gradesTable.find('tbody').empty().append(tbody);
            var tfooter = `
            <div class="d-flex justify-content-between py-1">
                <div class="d-flex text-bold">
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-purple">ΔΙΑΓΩΝΙΣΜΑ</span> : ${cntExam}
                    </div>
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-primary">TEST</span> : ${cntTest}
                    </div>
                    <div>
                        <span class="badge badge-green">ΕΡΓΑΣΙΑ</span> : ${cntErgasia}
                    </div>                    
                </div>
                <div class="d-flex text-bold">
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-success">ΠΑΡΩΝ</span> : ${cntParon}
                    </div>
                    <div>
                        <span class="badge badge-danger">AΠΩΝ</span> : ${cntApon}
                    </div>
                </div>
            </div>
            `;
            viewStudentJs.$elements.studentGradesCard.find('div.card-footer').empty().append(tfooter);
            viewStudentJs.initDataTables();
        },
        handleDeleteStudent: function () {
            viewStudentJs.$elements.modals.deleteStudentModal.deleteStudentBtn.on('click', function () {
                var btnDiv = $(this).parent();
                jQuery.ajax({
                    url: viewStudentJs.getStudentEndPoint(),
                    type: 'POST',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "DELETE_DATA",
                        "Method": "DELETE",
                    },
                    data: {
                        studentDeleteId: viewStudentJs.$elements.studentId
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.modals.deleteStudentModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.manageAlertBox('success', response.data + "<p>Ανακατεύθυνση <b> σε 3 δευτερόλεπτα</b></p>");
                            setTimeout(function () {
                                window.location.href = "manage-students.php";
                            }, 3000);
                        } else {
                            viewStudentJs.manageAlertBox('danger', response.errorMessage);
                        }
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON.errorMessage) {
                            viewStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            viewStudentJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || viewStudentJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    }
    viewStudentJs.init();
});

function printAll() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;

    localStorage["header"] = document.getElementById('studentName').innerHTML;
    localStorage["phone"] = document.getElementById('phone').value;
    localStorage["email"] = document.getElementById('email').value;
    localStorage["mobile"] = document.getElementById('mobile').value;
    localStorage["address"] = document.getElementById('address').value;
    localStorage["zip"] = document.getElementById('zip').value;
    localStorage["perioxi"] = document.getElementById('perioxi').value;
    localStorage["date_eggrafis"] = document.getElementById('date_eggrafis').value;
    localStorage["fphone"] = document.getElementById('fphone').value;
    localStorage["mphone"] = document.getElementById('mphone').value;
    localStorage["mname"] = document.getElementById('mname').value;
    localStorage["faname"] = document.getElementById('faname').value;
    localStorage["kid_name"] = document.getElementById('kid_name').value;
    localStorage["kid_phone"] = document.getElementById('kid_phone').value;
    localStorage["kidEmail"] = document.getElementById('kidEmail').value;
    localStorage["lessons"] = document.getElementById('lessons').value;
    localStorage["fee"] = document.getElementById('fee').value;
    localStorage["periodos"] = document.getElementById('periodos').value;
    localStorage["sx_etos"] = document.getElementById('sx_etos').value;
    localStorage["class"] = document.getElementById('class').value;
    localStorage["tmima"] = document.getElementById('tmima').value;
    localStorage["print_date"] = today;
    document.location.href = 'print.html';
}

function selectCheckbox(args) {
    unselectAll();
    let dataArr = args.split(',');
    for (var i = 0; i < dataArr.length; i++) {
        document.getElementById(dataArr[i]).checked = true;
    }
}

function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}