$(document).ready(function () {

	var lessonAssignmentMng = {
		$elements: {
			$editLessonAssignment: {
				saveLessonAssignmentBtn: $('#saveLessonAssignmentBtn'),
				createLessonAssignmentModal: $('#createLessonAssignmentModal'),
			},
			$deleteLessonAssignment: {
				deleteLessonAssigmentBtn: $('#deleteLessonAssigmentBtn'),
				deleteLessonAssignmentModal: $('#deleteLessonAssignmentModal'),
			},
			alertBox: $('#alertBox'),
			endpointUrl: ['..', 'php_actions', 'Controllers', 'LessonAssignmentController.php'],
			userLessonsEndpointUrl: ['..', 'php_actions', 'Controllers', 'UserLessonsController.php'],
			userLessonsCard: $('#user-lessons-card'),
			alertBox: $('#alertBox'),
			addAssignmentBtn: null,
			removeAssignmentBtn: null,
		},
		getEndPoint: function () {
			return this.$elements.endpointUrl.join('/');
		},
		getUserLessonsEndPoint: function () {
			return this.$elements.userLessonsEndpointUrl.join('/');
		},
		manageAlertBox: function (mode, message) {
			let errorHeader = 'success';
			if (mode == "danger") {
				errorHeader = 'error';
			}
			lessonAssignmentMng.$elements.alertBox.removeClass();
			lessonAssignmentMng.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
			lessonAssignmentMng.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
			lessonAssignmentMng.$elements.alertBox.find('#alertMessage').html(message);
			lessonAssignmentMng.$elements.alertBox.show();
		},
		init: function () {
			jQuery.ajax({
				url: lessonAssignmentMng.getEndPoint(),
				type: 'GET',
				headers: {
					"Csrftoken": $('meta[name=csrf_token]').attr("content"),
					"Action": "GET_ALL_DATA",
					"Method": "GET",
				},
				dataType: 'json',
				cache: false,
				beforeSend: function () {
					addStandardWhirlAnime('animoSort');
				},
				complete: function (data) {
					removeStandardWhirlAnime('animoSort');
				},
				success: function (response, textStatus, jQxhr) {
					if (response.code === 200) {
						var lessonAssignmentBody = '';
						$.each(response.data, function (key, one_user) {
							lessonAssignmentBody += `
							<div class="row d-flex py-2 align-items-center">
							<div class="col-xl-3 d-flex flex-column" title="${one_user.status == 1 ? '' : 'Ανενεργός Χρήστης'}">
								<div>
									<i class="fas fa-user fa-150x mr-2 ${one_user.status == 1 ? '' : 'text-danger'}"></i>
									${one_user.profile.lastname + ' ' + one_user.profile.firstname}
								</div>
								<div>
									<small class="mr-1 pr-1 br">${one_user.username}</small>
									<small class="text-bold ${one_user.status == 1 ? 'text-success-dark' : 'text-danger'}">${one_user.status == 1 ? 'Ενεργός' : 'Ανενεργός'}</small>
								</div>
							</div>
							<div class="input-group col-xl-9">
								<div class="input-group-prepend">
									<button ${one_user.status == 1 ? '' : 'disabled'} data-id="${one_user.id}" class="btn btn-primary inputBtn addAssignmentBtn" data-target="#createLessonAssignmentModal" data-toggle="modal" title="Προσθήκη Αναθέσεων" type="button">
										<i class="fas fa-plus"></i>
									</button>
									<button ${one_user.status == 1 ? '' : 'disabled'} data-id="${one_user.id}" class="btn btn-danger inputBtn removeAssignmentBtn" data-target="#deleteLessonAssignmentModal" data-toggle="modal" title="Διαγαφή Αναθέσεων" type="button">
										<i class="fas fa-minus"></i>
									</button>
								</div>
								<input class="form-control" type="text" placeholder="Προσθήκη Μαθημάτων.." value="${lessonAssignmentMng.arrayToCommaSeperatedString(one_user.lessons)}" readonly>
							</div>
						</div>
							`;
						})
						lessonAssignmentMng.$elements.userLessonsCard.empty().append(lessonAssignmentBody);
						lessonAssignmentMng.$elements.addAssignmentBtn = $('.addAssignmentBtn');
						lessonAssignmentMng.$elements.removeAssignmentBtn = $('.removeAssignmentBtn');
						lessonAssignmentMng.handleManageAssignment();
					} else {
						lessonAssignmentMng.manageAlertBox('danger', response.errorMessage);
					}
				},
				error: function (jqXHR, error, errorThrown) {
					if (jqXHR.responseJSON.errorMessage) {
						lessonAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
					} else {
						lessonAssignmentMng.manageAlertBox('danger', errorThrown);
					}
					console.log(errorThrown);
					console.log(jqXHR);
					setTimeout(function () {
						$(window).scrollTop(0);
					}, 500);
				},
			});
		},
		arrayToCommaSeperatedString: function (data) {
			var output = '';
			$.each(data, function (key, value) {
				output += value.name + ', ';
			});
			output = output.substring(0, output.length - 2);
			return output;
		},
		handleManageAssignment: function () {

			lessonAssignmentMng.$elements.removeAssignmentBtn.click(function () {
				$('#deleteUserlessonsId').val($(this).data('id'));
			});

			lessonAssignmentMng.$elements.addAssignmentBtn.click(function () {
				$('#addUserId').val($(this).data('id'));
				jQuery.ajax({
					url: lessonAssignmentMng.getUserLessonsEndPoint(),
					type: 'GET',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "GET_ALL_DATA",
						"Method": "GET",
					},
					data: { userId: $('#addUserId').val() },
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						addStandardWhirlAnime('createAssignmentModalAnimo');
					},
					complete: function (data) {
						removeStandardWhirlAnime('createAssignmentModalAnimo');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							unselectAll();
							$(response.data).each(function () {
								$('#' + $(this).attr('lesson_id')).prop("checked", true);
							});
						} else {
							lessonAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
			lessonAssignmentMng.$elements.removeAssignmentBtn.click(function () {
				$('#deleteUserlessonsId').val($(this).data('id'));
				$('#deleteUserlessonsUsername').val($(this).data('username'));
			});
		},
		handleEditLessonAssignment: function () {
			lessonAssignmentMng.$elements.$editLessonAssignment.saveLessonAssignmentBtn.on('click', function () {
				var btnDiv = lessonAssignmentMng.$elements.$editLessonAssignment.saveLessonAssignmentBtn.parent();
				var lessonsToSave = [];
				$("input[name='user_lessons[]']:checked").each(function () {
					lessonsToSave.push(this.value);
				});
				jQuery.ajax({
					url: lessonAssignmentMng.getUserLessonsEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "INSERT_DATA",
						"Method": "POST",
					},
					data: {
						addUserId: $('#addUserId').val(),
						user_lessons: lessonsToSave,
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div class="loaderDiv"><div style='float:left; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait </div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.loaderDiv').remove();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						lessonAssignmentMng.$elements.$editLessonAssignment.createLessonAssignmentModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonAssignmentMng.manageAlertBox('success', response.data);
							lessonAssignmentMng.init();
						} else {
							lessonAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		handleDeleteLessonAssignment: function () {
			lessonAssignmentMng.$elements.$deleteLessonAssignment.deleteLessonAssigmentBtn.on('click', function () {
				var btnDiv = lessonAssignmentMng.$elements.$deleteLessonAssignment.deleteLessonAssigmentBtn.parent();
				jQuery.ajax({
					url: lessonAssignmentMng.getUserLessonsEndPoint(),
					type: 'POST',
					headers: {
						"Csrftoken": $('meta[name=csrf_token]').attr("content"),
						"Action": "DELETE_DATA",
						"Method": "DELETE",
					},
					data: {
						deleteUserlessonsId: $('#deleteUserlessonsId').val(),
					},
					dataType: 'json',
					cache: false,
					beforeSend: function () {
						btnDiv.find('button').hide();
						btnDiv.append(`<div class="loaderDiv"><div style='float:left; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait </div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div></div>`);
						btnDiv.parent().find('input').prop('readonly', true);
						btnDiv.parent().find('select').prop('disabled', true);
					},
					complete: function (data) {
						btnDiv.find('button').show();
						btnDiv.find('.loaderDiv').remove();
						btnDiv.parent().find('input').prop('readonly', false);
						btnDiv.parent().find('select').prop('disabled', false);
						lessonAssignmentMng.$elements.$deleteLessonAssignment.deleteLessonAssignmentModal.modal('hide');
					},
					success: function (response, textStatus, jQxhr) {
						if (response.code === 200) {
							lessonAssignmentMng.manageAlertBox('success', response.data);
							lessonAssignmentMng.init();
						} else {
							lessonAssignmentMng.manageAlertBox('danger', response.errorMessage);
						}
						$(window).scrollTop(0);
					},
					error: function (jqXHR, error, errorThrown) {
						if (jqXHR.responseJSON.errorMessage) {
							lessonAssignmentMng.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
						} else {
							lessonAssignmentMng.manageAlertBox('danger', errorThrown);
						}
						console.log(errorThrown);
						console.log(jqXHR);
						setTimeout(function () {
							$(window).scrollTop(0);
						}, 500);
					},
				});
			});
		},
		bindButtonActions: function () {
			lessonAssignmentMng.handleEditLessonAssignment();
			lessonAssignmentMng.handleDeleteLessonAssignment();
		},
	}
	lessonAssignmentMng.init();
	lessonAssignmentMng.bindButtonActions();
});

function unselectAll() {
	$('#assignmentsBox').find(':input').each(function () {
		$(this).prop("checked", false);
	})
}