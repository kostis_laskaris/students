<script src=<?php initHelper::getWebPath("/assets/vendor/modernizr/modernizr.custom.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/vendor/jquery/dist/jquery.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/vendor/popper.js/dist/umd/popper.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/vendor/bootstrap/dist/js/bootstrap.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/vendor/js-storage/js.storage.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/vendor/parsleyjs/dist/parsley.js"); ?>></script>
<script src=<?php initHelper::getWebPath("/assets/js/app.js"); ?>></script>