<?php
$current_page = basename($_SERVER['PHP_SELF']);
?>
<aside class="aside-container">
    <div class="aside-inner">
        <nav class="sidebar" data-sidebar-anyclick-close="">
            <ul class="sidebar-nav">
                <li class="has-user-block">
                    <div class="collapse" id="user-block">
                        <div class="item user-block">
                            <div class="user-block-picture">
                                <div class="user-block-status">
                                    <img class="img-thumbnail rounded-circle" src="../assets/img/user/man-avatar.png" alt="Avatar" width="60" height="60">
                                    <div class="circle bg-success circle-lg"></div>
                                </div> 
                            </div>
                            <div class="user-block-info">
                                <span class="user-block-name"><?php echo "Hello, " . $_SESSION['username'];?></span>
                                <span class="user-block-role"><?php echo $user->permission; ?></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-heading">
                    <span data-localize="sidebar.heading.HEADER">Configurations</span>
                </li>  
                <li class="<?php if($current_page == 'user-management.php') echo 'active'; ?>">
                    <a href="<?php echo initHelper::getWebAdminPath('/user-management.php'); ?>" title="Διαχείριση Χρηστών">
                        <em class="fas fa-users-cog"></em>
                        <span>User Management</span>
                    </a>
                </li>
                <li class="<?php if($current_page == 'manage-lessons.php' || $current_page == 'manage-shortcuts.php' || $current_page == 'lesson-assignment.php' || $current_page == 'tmima-assignment.php' || $current_page == 'manage-lists.php' || $current_page == 'manage-database.php') echo 'active'; ?>">
                    <a href="#manageLessonsMenu" title="Διαχείριση Τμημάτων" data-toggle="collapse">
                        <em class="fas fa-cogs"></em>
                        <span>App Configuration</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="manageLessonsMenu">
                        <li class="sidebar-subnav-header">App Configuration</li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-lessons.php'); ?>" title="Διαχείριση Μαθημάτων">
                                <span>Διαχείριση Μαθημάτων</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-shortcuts.php'); ?>" title="Διαχείριση Συντομεύσεων">
                                <span>Διαχείριση Συντομεύσεων</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-lists.php'); ?>" title="Διαχείριση Λιστών">
                                <span>Διαχείριση Λιστών</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/lesson-assignment.php'); ?>" title="Ανάθεση Μαθημάτων">
                                <span>Ανάθεση Μαθημάτων</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/tmima-assignment.php'); ?>" title="Ανάθεση Τμημάτων">
                                <span>Ανάθεση Τμημάτων</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-database.php'); ?>" title="Δημιουργία Νέας Βάσης">
                                <span>Δημιουργία Νέας Βάσης</span>
                            </a>
                        </li>
                    </ul>
                </li>  
                <li class="nav-heading ">
                    <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                </li>                
                <li class="<?php if($current_page == 'dashboard.php') echo 'active'; ?>">
                    <a href="<?php echo initHelper::getWebAdminPath('/dashboard.php'); ?>" title="Dashboard">
                        <em class="icon-speedometer"></em>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?php if($current_page == 'add-student.php' || $current_page == 'manage-students.php' || $current_page == 'edit-student.php' || $current_page == 'view-student.php' || $current_page == 'resub-students.php') echo 'active'; ?>">
                    <a href="#studentsMenu" title="Διαχείριση Μαθητών" data-toggle="collapse">
                        <em class="fas fa-user-cog"></em>
                        <span>Διαχείριση Μαθητών</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="studentsMenu">
                        <li class="sidebar-subnav-header">Διαχείριση Μαθητών</li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/add-student.php'); ?>" title="Εγγραφή Μαθητή">
                                <span>Εγγραφή Μαθητή</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/resub-students.php'); ?>" title="Επανεγγραφή Μαθητή">
                                <span>Επανεγγραφή Μαθητή</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-students.php'); ?>" title="Προβολή & Επεξεργασία Μαθητών">
                                <span>Προβολή Μαθητών</span>
                            </a>
                        </li>
                    </ul>
                </li>                               
                <li class="<?php if($current_page == 'add-class.php' || $current_page == 'manage-classes.php') echo 'active'; ?>">
                    <a href="#classMenu" title="Διαχείριση Τμημάτων" data-toggle="collapse">
                        <em class="fas fa-chalkboard"></em>
                        <span>Διαχείριση Τμημάτων</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="classMenu">
                        <li class="sidebar-subnav-header">Διαχείριση Τμημάτων</li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/add-class.php'); ?>" title="Δημιουργία Τμήματος">
                                <span>Δημιουργία Τμήματος</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/manage-classes.php'); ?>" title="Επεξεργασία Τμημάτων">
                                <span>Επεξεργασία Τμημάτων</span>
                            </a>
                        </li>
                    </ul>
                </li>  
                <li class="<?php if($current_page == 'add-grade.php' || $current_page == 'edit-grades.php' || $current_page == 'view-grades.php') echo 'active'; ?>">
                    <a href="#gradeMenu" title="Διαχείριση Βαθμολογιών" data-toggle="collapse">
                        <em class="far fa-star"></em>
                        <span>Διαχείριση Βαθμολογιών</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="gradeMenu">     
                        <li class="sidebar-subnav-header">Διαχείριση Βαθμολογιών</li>     
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebAdminPath('/add-grade.php'); ?>" title="Νέα Βαθμολογία">
                                <span>Νέα Βαθμολογία</span>
                            </a>
                        </li>
                        <li class=" ">  
                            <a href="<?php echo initHelper::getWebAdminPath('/edit-grades.php'); ?>" title="Επεξεργασία Βαθμολογιών">
                                <span>Επεξεργασία Βαθμών</span>
                            </a>
                        </li>
                        <li class=" ">  
                            <a href="<?php echo initHelper::getWebAdminPath('/view-grades.php'); ?>" title="Προβολή Βαθμολογιών">
                                <span>Προβολή Βαθμών</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if($current_page == 'view-logs.php') echo 'active'; ?>">
                    <a href="<?php echo initHelper::getWebAdminPath('/view-logs.php'); ?>" title="Προβολή Logs">
                        <em class="fas fa-file-signature"></em>
                        <span>Προβολή Logs</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>    