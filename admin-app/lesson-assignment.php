<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = 'user_lessons[]';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$tmimataBoxGroup = $checkbox->createLessonGroup();
?>

<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Ανάθεση Μαθημάτων</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Ανάθεση Μαθημάτων
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Ανάθεση Μαθημάτων',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<div class="card card-default" id="animoSort">
							<div class="card-header border-bottom">
								<div class="d-flex align-middle">
									<div class="card-title my-auto">
										<h4 class="mb-0">Προβολή Καταχωρημένων Αναθέσεων Μαθημάτων</h4>
									</div>
									<span class="float-right ml-auto">
										<a class="fa-150x text-info pointer-cursored" title="Βοήθεια" data-toggle="modal" data-target="#helpManual">
											<em class="far fa-question-circle"></em>
										</a>
									</span>
								</div>
							</div>
							<div class="card-body border-top" style="font-weight: 500;" id="user-lessons-card">

							</div>
						</div>
					</div>
				</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/lesson-assignment.js"); ?>></script>
	<?php
	//create assignment modal
	$modal = new Modal([
		'modalId' => 'createLessonAssignmentModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Ανάθεση Μαθημάτων σε Καθηγητή</h4>',
		'modalBody' => '
	<input type="text" id="addUserId" name="addUserId" style="display: none;">
	<input type="hidden" name="csrf_token" id="csrf_token" value="' . $_SESSION['csrf_token'] . '">
	' . $tmimataBoxGroup . '
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveLessonAssignmentBtn">Save</button>',
		'animoId' => 'createAssignmentModalAnimo'
	]);
	echo $modal->createModal();
	//delete shortcut modal
	$modal = new Modal([
		'modalId' => 'deleteLessonAssignmentModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Αναθέσεων</h4>',
		'modalBody' => '
	<input type="text" id="deleteUserlessonsId" name="deleteUserlessonsId" style="display: none;">
	<input type="hidden" name="delete_csrf_token" id="delete_csrf_token" value="' . $_SESSION['csrf_token'] . '">
	<p class="py-3" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε αυτή την ανάθεση;</p>
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="deleteLessonAssigmentBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//info modal
	$modal = new Modal([
		'modalId' => 'helpManual',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => '',
		'modalHeader' => '<h4 class="modal-title"><em class="far fa-question-circle float-left fa-150x mr-2 text-info"></em>Πληροφορίες</h4>',
		'modalBody' => '
	<ul style="margin-bottom: 0px;">
	<li class="mb-2">Σε αυτή τη σελίδα παρέχεται η δυνατότητα ανάθεσης μαθημάτων σε όλους τους καθηγητές που έχουν αποθηκευτεί στο σύστημα. Έτσι, ο κάθε καθηγητής θα μπορεί να βαθμολογεί μόνο για τα μαθήματα που τον αφορούν.
	</li>
	<li class="mb-2">Με τη χρήση του εικονιδίου <i class="far fa-plus-square text-primary ml-1"></i> εμφανίζεται ένα νέο παράθυρο που περιέχει όλα τα μαθήματα. Από εκεί μπορείτε να τα επιλέξετε ώστε να ολοκληρωθεί η ανάθεση. Τέλος, πατώντας το κουμπί <b>"Save"</b>, ολοκληρώνεται η διαδικασία.
	</li>
	<li>Με τη χρήση του εικονιδίου <i class="far fa-minus-square text-danger ml-1"></i> μπορείτε να διαγράψετε όλες τις αναθέσεις που έχουν γίνει στον καθηγήτη. Πατώντας το κουμπί <b>"Submit"</b>, ολοκληρώνεται η διαδικασία
	</li>					
	</ul>
	',
		'modalFooter' => '',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>