<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup('');

$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'class';
$appList->html_id = 'class';
$taksiList = $appList->getListByNameAsSelect('');

$appList->name = 'informed_by';
$appList->html_name = 'informed_by';
$appList->html_id = 'informed_by';
$informedbyList = $appList->getListByNameAsSelect('');

$tmimataDropdown = Tmima::getTmimataDropdown('tmima');

?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Επεξεργασία Μαθητή</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Επεξεργασία Μαθητή: <span class="text-green" id="student-name-span"></span>
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Επεξεργασία Μαθητή',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>

				<div class="row">
					<div class="col-lg-12" id="editStudentAnimo">
						<form class="form-horizontal mt-2" method="post" action="" id="updateStudentForm">

							<div class="card card-default mb-3">
								<div class="card-header">
									<div class="card-title">
										<h4 class="mb-0 py-2">Στοιχεία Μαθητή</h4>
									</div>
								</div>
								<input type="hidden" name="student_id" value="" id="student_id">
								<div class="card-body border-top">
									<div class="form-row">
										<div class="col-lg-3 mb-3">
											<label for="firstname">Όνομα <span class="reqField">(*)</span></label>
											<input class="form-control" id="firstname" name="firstname" type="text" placeholder="Όνομα.." value="" required>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="lastname">Επίθετο <span class="reqField">(*)</span></label>
											<input class="form-control" id="lastname" name="lastname" type="text" placeholder="Επίθετο.." value="" required>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="address">Διεύθυνση</label>
											<input class="form-control" id="address" name="address" type="text" placeholder="Διεύθυνση.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="perioxi">Περιοχή</label>
											<input class="form-control" id="perioxi" name="perioxi" type="text" placeholder="Περιοχή.." value="">
										</div>
									</div>

									<div class="form-row">
										<div class="col-xl-1 mb-3">
											<label for="zip">Τ.Κ.</label>
											<input maxlength="5" class="form-control" id="zip" name="zip" type="text" placeholder="Τ.Κ.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="mobile">Κινητό Τηλέφωνο</label>
											<input maxlength="10" class="form-control" id="mobile" name="mobile" type="text" placeholder="Κινητό Τηλέφωνο.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="phone">Σταθερό Τηλέφωνο</label>
											<input maxlength="10" class="form-control" id="phone" name="phone" type="text" placeholder="Σταθερό Τηλέφωνο.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="email">Email</label>
											<input class="form-control" id="email" name="email" type="email" placeholder="Email.." value="">
										</div>
										<div class="col-lg-2 mb-3">
											<label for="date_eggrafis">Ημερομηνία Εγγραφής <span class="reqField">(*)</span></label>
											<input class="form-control" id="date_eggrafis" name="date_eggrafis" type="date" value="">
										</div>
									</div>
								</div>
							</div>

							<div class="card card-default mb-3">
								<div class="card-header">
									<div class="card-title">
										<h4 class="mb-0 py-2">Στοιχεία Κηδεμόνων</h4>
									</div>
								</div>
								<div class="card-body border-top">
									<div class="form-row">
										<div class="col-lg-3 mb-3">
											<label for="mname">Όνομα Μητέρας</label>
											<input class="form-control" id="mname" name="mname" type="text" placeholder="Όνομα Μητέρας.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="faname">Όνομα Πατέρα</label>
											<input class="form-control" id="faname" name="faname" type="text" placeholder="Όνομα Πατέρα.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="mphone">Τηλέφωνο Μητέρας</label>
											<input class="form-control" maxlength="10" id="mphone" name="mphone" type="text" placeholder="Τηλέφωνο Μητέρας.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="fphone">Τηλέφωνο Πατέρα</label>
											<input class="form-control" maxlength="10" id="fphone" name="fphone" type="text" placeholder="Τηλέφωνο Πατέρα.." value="">
										</div>
									</div>

									<div class="form-row">
										<div class="col-lg-3 mb-3">
											<label for="email_kidemona">Email Κηδεμόνα</label>
											<input class="form-control" id="email_kidemona" name="email_kidemona" type="email" placeholder="Email Κηδεμόνα.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="kid_name">Όνομα Κηδεμόνα</label>
											<input class="form-control" id="kid_name" name="kid_name" type="text" placeholder="Όνομα Κηδεμόνα.." value="">
										</div>
										<div class="col-lg-3 mb-3">
											<label for="kid_phone">Τηλέφωνο Κηδεμόνα</label>
											<input class="form-control mb-2" maxlength="21" id="kid_phone" name="kid_phone" type="text" placeholder="Τηλέφωνο Κηδεμόνα.." value="">
											<span><i class="fas fa-info-circle mr-2 text-info"></i> <i>Μέχρι 2 τηλέφωνα χωρισμένα με κόμμα (,)</i></span>
										</div>
									</div>
								</div>
							</div>

							<div class="card card-default mb-3">
								<div class="card-header">
									<div class="card-title">
										<h4 class="mb-0 py-2">Στοιχεία Φοίτησης</h4>
									</div>
								</div>
								<div class="card-body border-top">
									<div class="form-row">
										<div class="col-lg-3 mb-3">
											<label for="fee">Δίδακτρα</label>
											<div class="input-group">
												<input class="form-control" id="fee" name="fee" type="number" placeholder="Δίδακτρα.." value="">
												<div class="input-group-append">
													<span class="input-group-text bg-green-dark"><i class="fas fa-euro-sign"></i></span>
												</div>
											</div>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="tmima">Τμήμα <span class="reqField">(*)</span></label>
											<?php echo $tmimataDropdown; ?>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="email_kidemona">Τάξη</label>
											<?php echo $taksiList; ?>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="informed_by">Ενημερώθηκε Από</label>
											<?php echo $informedbyList; ?>
										</div>
									</div>

									<div class="form-row">
										<div class="col-lg-12 mb-3">
											<label for="lessons">Μαθήματα</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<button data-id="" class="btn btn-primary inputBtn editLessonsBtn" data-target="#lessonModal" data-toggle="modal" title="Αλλαγή Μαθημάτων" type="button">
														<i class="fas fa-plus"></i>
													</button>
												</div>
												<input class="form-control" type="text" placeholder="Μαθήματα.." id="lessons" name="lessons" value="" readonly>
											</div>
											<input type="hidden" name="lesson_ids" id="lesson_ids" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<p style="text-align: center;">
									<button class="btn btn-primary btnw-4" name="submit_form" id="submitBtn" type="button" data-target="#updateStudentModal" data-toggle="modal">
										Αποθήκευση
									</button>
								</p>
							</div>
						</form>
					</div>
				</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/edit-student.js"); ?>></script>
	<?php
	//create lessons modal
	$modal = new Modal([
		'modalId' => 'lessonModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Προσθήκη Μαθημάτων</h4>',
		'modalBody' => '
	<input type="hidden" id="lessonSelected" name="lessonSelected">
	' . $lessonBoxGroup . '
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//submit update student modal
	$modal = new Modal([
		'modalId' => 'updateStudentModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-success-dark',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-edit float-left fa-150x mr-2"></em>Επεξεργασία Μαθητή</h4>',
		'modalBody' => '
		<p class="py-3 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να αποθηκεύσετε τις αλλαγές;</p>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveUpdateStudentBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>