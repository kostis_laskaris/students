<?php
session_start();
include("../init.php");

if (!isLoggedIn() ) { //|| !isAdmin()
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Προβολή Logs</title>
	<link rel="stylesheet" href="../assets/css/theme-e.css" id="maincss2">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Προβολή Logs
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'  => '#',
						'title' => 'Προβολή Logs',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<div class="row">
					<div class="col-xl-12 px-5">
						<div class="table-responsive">
							<table class="table table-striped table-hover w-100" id="logsTbl">
								<thead>
									<tr>
										<th>Ημερομηνία</th>
										<th>Χρήστης</th>
										<th>Σελίδα</th>
										<th>Είδος</th>
										<th class="text-center">SQL State</th>
										<th class="text-center">Error Code</th>
										<th style="max-width: 450px;">Error Message</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$error = new Error_Log;
									$logs = $error->getLogs();
									if (!is_array($logs)) { ?>
										<tr>
											<td colspan="7" class="text-center text-danger text-bold"><?php echo $logs; ?></td>
										</tr>
										<?php } else {
										$logs = array_reverse($logs);
										foreach ($logs as $log) {
										?>
											<tr>
												<td><?php echo $log['date']; ?></td>
												<td><?php echo $log['user']; ?></td>
												<td><?php echo $log['page']; ?></td>
												<td><?php echo $log['kind']; ?></td>
												<td class="text-center"><?php echo $log['sql_state']; ?></td>
												<td class="text-center"><?php echo $log['error_code']; ?></td>
												<td style="max-width: 450px;"><?php echo $log['error_message']; ?></td>
											</tr>
									<?php }
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
</body>

</html>