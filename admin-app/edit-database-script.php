<?php
session_start();
include("../init.php");
if (!isLoggedIn() || !isAdmin()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
global $db;
if (isset($_GET['db_suffix'])) {
    define('TARGET_DB', filter_input(INPUT_GET, 'db_suffix'));
} else {
    echo '<h2 style="padding: 20px;">Το script δεν εκτελέστηκε!!</h2>';
    echo '<a style="padding: 20px;" href="user-management.php">Επιστροφή</a>';
    return;
}
$servername = "localhost";
$username = "politopo_apopapa";
$password = "#P0litropo135$";
$dbname = filter_input(INPUT_GET, 'db_from'); // DB TO COPY FROM!!!
$mode = filter_input(INPUT_GET, 'mode');
//example: http://localhost/students/admin-app/edit-database-script.php?mode=delete&db_suffix=20_21_xeim&db_from=students
if (isset($dbname) && isset($mode)) {
    if ($mode == "copy") {
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $query = "SELECT * FROM students_" . TARGET_DB;
        // helperPrint($query, "print_r");
        $result = $conn->query($query);

        //get app lists        
        $query_appLists = "SELECT * FROM app_lists_" . TARGET_DB;
        $result_appLists = $conn->query($query_appLists);
        $appLists = mysqli_fetch_all($result_appLists, MYSQLI_ASSOC);
        // helperPrint($appLists, "print_r");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                //lessons
                $queryStLessons = "SELECT * FROM students_lessons_" . TARGET_DB . " WHERE student_id = " . (int) $row['id'];
                $resultStLessons = $conn->query($queryStLessons);
                $lessonString = '';
                if ($resultStLessons->num_rows > 0) {
                    while ($rowLessons = $resultStLessons->fetch_assoc()) {
                        $lessonString .= $rowLessons['lesson_id'] . ',';
                    }
                    $lessonString = substr($lessonString, 0, -1);
                }
                $class_id = 0;
                foreach ($appLists as $key => $value) {
                    if($value['list_entry'] == $row['class']){
                        $class_id = $value['id'];
                    }
                }
                //add student
                $student                    = new Student;
                $student->id                = $row['id'];
                $student->firstname         = $row['firstname'];
                $student->lastname          = $row['lastname'];
                $student->address           = $row['address'];
                $student->perioxi           = $row['perioxi'];
                $student->zip               = $row['tk'];
                $student->mobile            = $row['mobile'];
                $student->phone             = $row['phone'];
                $student->email             = $row['email'];
                $student->date_eggrafis     = $row['date_eggrafis'];
                $student->mname             = $row['mname'];
                $student->faname            = $row['faname'];
                $student->mphone            = $row['mphone'];
                $student->fphone            = $row['fphone'];
                $student->email_kidemona    = $row['email_kidemona'];
                $student->kid_name          = $row['kid_name'];
                $student->kid_phone         = $row['kid_phone'];
                $student->fee               = $row['fee'];
                $student->tmima             = $row['tmima'];
                $student->sx_etos           = $_SESSION['curr_school_year'];
                $student->periodos          = $row['periodos'];
                $student->class             = $class_id;
                $student->informed_by       = null;
                $student->lessons           = explode(",", $lessonString);
                $student->created_at        = date('Y-m-d H:i:s');
                $student->updated_at        = null;

                $submitted = $student->addStudent();
                helperPrint($row, "print_r");

                // helperPrint(substr($lessonString, 0, -1), "print_r");
                //grades
                $queryGrades = "SELECT * FROM grades_" . TARGET_DB . " WHERE student_id = " . (int) $row['id'];
                $resultGrades = $conn->query($queryGrades);
                if ($resultGrades->num_rows > 0) {
                    while ($rowGrades = $resultGrades->fetch_assoc()) {
                        $grade                  = new Grade();
                        $grade->user_id         = (int) $rowGrades['teacher_id'];
                        $grade->lesson_id       = (int) $rowGrades['lesson_id'];
                        $grade->student_id      = (int) $rowGrades['student_id'];
                        $grade->class           = $class_id;
                        $grade->tmima           = (int) Tmima::getTmimaIdFromName($rowGrades['tmima']);
                        $grade->exam_date       = $rowGrades['exam_date'];
                        $grade->exam_kind       = $rowGrades['exam_kind'];
                        $grade->absence         = $rowGrades['absence'];
                        $grade->grade           = $rowGrades['grade'];
                        $grade->comments        = $rowGrades['comments'];
                        $grade->created_at      = $rowGrades['submission_date'];
                        $grade->updated_at      = null;

                        $submitted = $grade->addGrade();
                        helperPrint($grade, "print_r");
                    }
                }
                // helperPrint(substr($lessonString, 0, -1), "print_r");
            }
        } else {
            echo "0 results";
        }

        //Teachers Tmima
        $query = "SELECT * FROM teachers_tmimata_" . TARGET_DB;
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $teacherTmima = new UserTmimata();
                $teacherTmima->user_id = (int) $row['teacher_id'];
                $teacherTmima->tmima_id = (int) $row['tmimata_id'];
                $teacherTmima->save();
            }
        } else {
            echo "0 results";
        }

        $conn->close();
    } elseif ($mode == "delete") {
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        mysqli_set_charset($conn, "utf8");
        $cnt_errors = 0;
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $query = "DROP TABLE IF EXISTS grades_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS shortcut_lessons_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS shortcuts_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS students_lessons_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS teachers_tmimata_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS teacher_lessons_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS lessons_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS students_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP TABLE IF EXISTS tmimata_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }        
        $query = "DROP TABLE IF EXISTS app_lists_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        //DROP VIEWS
        $query = "DROP VIEW IF EXISTS full_grades_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        $query = "DROP VIEW IF EXISTS full_grades_stats_" . TARGET_DB . '; ';
        if (!$conn->query($query)) {
            $cnt_errors++;
            printf("Error message: %s\n", $conn->error);
        }
        if ($cnt_errors == 0) {
            echo '<h2 style="padding: 20px;">Τα δεδομένα διαγράφηκαν με επιτυχία!</h2>';
        } else {
            echo '<h2 style="padding: 20px;">Εμφανίστηκαν ' . $cnt_errors . ' λάθη!</h2>';
        }
        echo '<a style="padding: 20px;" href="user-management.php">Επιστροφή</a>';
    } else {
        echo '<h2 style="padding: 20px;">Το script δεν εκτελέστηκε!!</h2>';
        echo '<a style="padding: 20px;" href="user-management.php">Επιστροφή</a>';
    }
} else {
    echo '<h2 style="padding: 20px;">Το script δεν εκτελέστηκε!!</h2>';
    echo '<a style="padding: 20px;" href="user-management.php">Επιστροφή</a>';
}
