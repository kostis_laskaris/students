<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$lesson_id = base64_decode(urldecode(filter_input(INPUT_GET, 'lesid')));
$usersDropdown = User::getUsersByLessonAsDropdown($lesson_id, 'searchUser');
$studentsDropdown = Student::getStudentsByLessonAsDropdown($lesson_id, 'searchStudent');

?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Προβολή Μαθήματος</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href="../assets/css/theme-e.css" id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div class="d-flex align-items-center">
                        <div>
                            <span id="lessonNameHeader"></span>
                            <div class="d-flex mt-2">
                                <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                                <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                            </div>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Προβολή Μαθήματος',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>

                <div class="row" id="viewLessonStudentAnimate">
                    <div class="col-lg-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="card-title my-auto">
                                        <h4 class="mb-0">Προβολή Μαθητών</h4>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <a class="btn btn-primary text-bold" href=<?php initHelper::getWebAdminPath("/add-student.php"); ?>>
                                            <i class="fas fa-plus mr-2"></i>Προσθήκη Μαθητή
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <table class="table table-striped table-hover w-100" id="student_tbl">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Επίθετο</th>
                                            <th>Όνομα</th>
                                            <th>Σταθερό Τηλέφωνο</th>
                                            <th>Κινητό Τηλέφωνο</th>
                                            <th>Email</th>
                                            <th>Τμήμα</th>
                                            <th>Διεύθυνση</th>
                                            <th class="d-none">Τηλέφωνο Πατέρα</th>
                                            <th class="d-none">Τηλέφωνο Μητέρα</th>
                                            <th class="d-none">Τάξη</th>
                                            <th class="d-none">Όνομα Μητέρας</th>
                                            <th class="d-none">Όνομα Πατέρα</th>
                                            <th class="d-none">Όνομα Κηδεμόνα</th>
                                            <th class="d-none">Τηλέφωνο Κηδεμόνα</th>
                                            <th class="d-none">Email Κηδεμόνα</th>
                                            <th class="d-none">Δίδακτρα</th>
                                            <th class="d-none">Περίοδος Μαθημάτων</th>
                                            <th class="d-none">Σχολικό Έτος</th>
                                            <th class="d-none">Ημ. Εγγραφής</th>
                                            <th class="d-none">Ενημερώθηκε Από</th>
                                            <th class="d-none">Μαθήματα</th>
                                            <th style="max-width: 90px; min-width: 90px;" class="text-center">Επιλογές</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="viewLessonGradesAnimate">
                    <div class="col-lg-12">
                        <div class="card card-default" id="lessonGradesCard">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <div class="card-title">
                                    <h4 class="mb-0 py-2">Πίνακας Βαθμολογιών</h4>
                                </div>
                                <div class="d-flex align-items-center">
                                    <a class="btn btn-info mr-2 text-bold" href="<?php echo initHelper::getWebAdminPath('/view-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                        <i class="fas fa-align-justify mr-2"></i>Προβολή Όλων
                                    </a>
                                    <a class="btn btn-purple mr-2 text-bold" href="#!" data-toggle="modal" data-target="#filterLessonGradesModal" title="Φίλτρα Αναζήτησης">
                                        <i class="fas fa-filter mr-2"></i>Φίλτρα
                                    </a>
                                    <a class="btn btn-warning mr-2 text-bold" style="display:none;" href="#!" id="removeFilterLessonGradesModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
                                        <i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
                                    </a>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <table class="table table-striped table-hover w-100" id="lessonGradesTbl">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Ημερομηνία Εξέτασης</th>
                                            <th>Καθηγητής</th>
                                            <th>Ονοματεπώνυμο Μαθητή</th>
                                            <th>Είδος Εξέτασης</th>
                                            <th>Παρουσία</th>
                                            <th>Βαθμός</th>
                                            <th>Καταχωρήθηκε</th>
                                            <th>Τροποποίηθηκε</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer"></div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script src=<?php initHelper::getWebAdminPath("/js/view-lessons.js"); ?>></script>
    <?php
    //filter modal
    $modal = new Modal([
        'modalId' => 'filterLessonGradesModal',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-purple',
        'modalHeader' => '<h4 id="filter-student-modal-title" class="modal-title"><i class="fas fa-filter mr-2"></i>Φίλτρα Αναζήτησης Βαθμολογίας</h4>',
        'modalBody' => '
		<div class="row mt-2">
			<div class="col-lg-4 mt-2 pr-1">
				<label for="searchUser">Όνομα Καθηγητή</label>
				' . $usersDropdown . '
            </div>
            <div class="col-lg-4 mt-2 px-1">
				<label for="searchStudent">Όνομα Μαθητή</label>
				' . $studentsDropdown . '
			</div>
			<div class="col-lg-2 mt-2 px-1">
                <label for="">Είδος Εξέτασης</label>
                <select class="custom-select custom-select-md" name="searchExam_kind" id="searchExam_kind">
                    <option value="">Επιλογή</option>
                    <option value="TEST">TEST</option>
                    <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                    <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                </select>
            </div>
			<div class="col-lg-2 mt-2 pl-1">
				<label for="">Παρουσία</label>
				<select class="custom-select custom-select-md" name="searchAbsence" id="searchAbsence">
					<option value="">Επιλογή</option>
					<option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
					<option value="ΑΠΩΝ">ΑΠΩΝ</option>
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-4 mt-2">
				<label for="">Βαθμός</label>
				<div class="d-flex pb-1">
					<input class="form-control w-45 mr-1" type="text" placeholder="Τελεστής" id="searchTelestis" name="searchTelestis" maxlength="2">
					<input class="form-control w-55" type="number" min="0" max="100" id="searchGrade" name="searchGrade" placeholder="Βαθμός">
				</div>
				<span class="small text-bold text-warning" id="accepted-telestes">* Αποδεκτές Τιμές: >, <, =, >=, <=, !=</span>
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Από</label>
				<input type="date" class="form-control" id="searchDate_from" name="searchDate_from">
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Έως</label>
				<input type="date" class="form-control" id="searchDate_to" name="searchDate_to">
			</div>
		</div>
	',
        'modalFooter' => '<button class="btn btn-primary" id="initSearchBtn">Search</button><button class="btn btn-warning" id="clearSearchBtn">Clear Filters</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>