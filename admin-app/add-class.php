<?php
session_start();
include("../init.php");
if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'addClass';
$appList->html_id = 'addClass';
$taksiList = $appList->getListByNameAsSelect('');
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Δημιουργία Τμήματος</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Δημιουργία Τμήματος
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'	=> '#',
						'title' => 'Δημιουργία Τμήματος',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row" id="addClassAnimo">
					<div class="col-md-10 offset-md-2 m-auto">
						<form class="form-horizontal" method="post" action="" id="addClassForm">
							<!-- START card-->
							<div class="card card-default">
								<div class="card-header d-flex justify-content-between align-items-center py-3">
									<h4 class="mb-0">Δημιουργία Τμήματος</h4>
								</div>
								<div class="card-body bt">
									<div class="form-row" style="margin-top: 15px;">
										<div class="col-lg-3 mb-3">
											<label for="tmimaName">Όνομα Τμήματος <span class="reqField">(*)</span></label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-chalkboard"></i></span>
												</div>
												<input class="form-control" id="tmimaName" name="tmimaName" maxlength="100" type="text" placeholder="Όνομα.." required>
											</div>
										</div>
										<div class="col-lg-3 mb-3">
											<label for="className">Τάξη <span class="reqField">(*)</span></label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-chalkboard"></i></span>
												</div>
												<?php echo $taksiList; ?>
											</div>
										</div>
										<div class="col-lg-6 mb-3">
											<label for="comments">Σχόλια</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-comment"></i></span>
												</div>
												<input class="form-control" id="comments" name="comments" maxlength="250" type="text" placeholder="Σχόλια..">
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer text-center">
									<button class="btn btn-primary btnw-4" data-target="#submitSaveClassModal" data-toggle="modal" type="button" id="submitBtn">Αποθήκευση</button>
								</div>
							</div>
						</form>
						<!-- END card-->
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/add-class.js"); ?>></script>
	<?php
	// info modal
	$modal = new Modal([
		'modalId' => 'submitSaveClassModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-success-dark',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-plus float-left fa-150x mr-2"></em>Προσθήκη Τμήματος</h4>',
		'modalBody' => '
	<p class="py-2 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να αποθηκεύσετε το νέο τμήμα;</p>
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="submitSaveClass" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>