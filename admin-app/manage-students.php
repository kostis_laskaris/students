<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$tmimataDropdown = Tmima::getTmimataDropdown('searchTmima');

$shortcutBtnsGrp = Shortcut::getShortcutsAsButtonGroup();

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup($shortcutBtnsGrp);

$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'searchTaksi';
$appList->html_id = 'searchTaksi';
$taksiList = $appList->getListByNameAsSelect('');
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Προβολή & Επεξεργασία Μαθητών</title>
	<?php initHelper::getDatatablesCss(); ?>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container" id="theAnimo">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Προβολή & Επεξεργασία Μαθητών
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Προβολή & Επεξεργασία Μαθητή',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>

				<div class="row" id="addStudentAnimate">
					<div class="col-lg-12">
						<div class="card card-default">
							<div class="card-header">
								<div class="d-flex justify-content-between align-items-center">
									<div class="card-title my-auto">
										<h4 class="mb-0">Προβολή Καταχωρημένων Μαθητών</h4>
									</div>
									<div class="d-flex align-items-center">
										<a class="btn btn-primary mr-2 text-bold" href=<?php initHelper::getWebAdminPath("/add-student.php"); ?>>
											<i class="fas fa-plus mr-2"></i>Προσθήκη Μαθητή
										</a>
										<a class="btn btn-purple mr-2 text-bold" href="#!" data-toggle="modal" data-target="#filterStudentsModal" title="Φίλτρα Αναζήτησης">
											<i class="fas fa-filter mr-2"></i>Φίλτρα
										</a>
										<a class="btn btn-warning mr-2 text-bold" style="display:none;" href="#!" id="removeFilterStudentsModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
											<i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
										</a>
									</div>
								</div>
							</div>
							<div class="card-body border-top">
								<table class="table table-striped table-hover w-100" id="studentsTbl">
									<thead>
										<tr>
											<th>ID</th>
											<th>Επίθετο</th>
											<th>Όνομα</th>
											<th>Σταθερό Τηλέφωνο</th>
											<th>Κινητό Τηλέφωνο</th>
											<th>Email</th>
											<th>Τμήμα</th>
											<th>Διεύθυνση</th>
											<th class="d-none">Τηλέφωνο Πατέρα</th>
											<th class="d-none">Τηλέφωνο Μητέρα</th>
											<th class="d-none">Τάξη</th>
											<th class="d-none">Όνομα Μητέρας</th>
											<th class="d-none">Όνομα Πατέρα</th>
											<th class="d-none">Όνομα Κηδεμόνα</th>
											<th class="d-none">Τηλέφωνο Κηδεμόνα</th>
											<th class="d-none">Email Κηδεμόνα</th>
											<th class="d-none">Δίδακτρα</th>
											<th class="d-none">Περίοδος Μαθημάτων</th>
											<th class="d-none">Σχολικό Έτος</th>
											<th class="d-none">Ημ. Εγγραφής</th>
											<th class="d-none">Ενημερώθηκε Από</th>
											<th class="d-none">Μαθήματα</th>
											<th style="max-width: 145px; min-width: 145px;" class="text-center">Επιλογές</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-6">
						<div class="card card-default" id="card1-loader">
							<div class="card-header d-flex justify-content-between">
								<div class="card-title my-auto">
									<h4 class="mb-0">Μαθητές / Τάξη</h4>
								</div>
								<div>
									<i class="fas fa-users" style="font-size: 250%"></i>
								</div>
							</div>
							<div class="card-body border-top">
								<div class="w-75 mb-4 mx-auto">
									<canvas id="studentClassesChart"></canvas>
								</div>
								<div class="student-per-class-container">
									<table class="table table-striped table-hover w-100" id="studentsPerClassTbl">
										<thead>
											<tr>
												<th>Τάξη</th>
												<th class="text-center">Αριθμός Μαθητών</th>
												<th class="text-center">Ποσοστά</th>
												<th class="text-center">Προβολή Μαθητών</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="card card-default" id="card2-loader">
							<div class="card-header d-flex justify-content-between">
								<div class="card-title my-auto">
									<h4 class="mb-0">Μαθητές / Περιοχή</h4>
								</div>
								<div>
									<i class="fas fa-map-marked-alt" style="font-size: 250%"></i>
								</div>
							</div>
							<div class="card-body border-top">
								<div class="w-75 mb-4 mx-auto">
									<canvas id="studentPerioxesChart"></canvas>
								</div>
								<table class="table table-striped table-hover w-100" id="studentsPerPerioxiTbl">
									<thead>
										<tr>
											<th>Περιοχή</th>
											<th class="text-center">Αριθμός Μαθητών</th>
											<th class="text-center">Ποσοστά</th>
											<th class="text-center">Προβολή Μαθητών</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xl-12">
						<div class="card card-default">
							<div class="card-header d-flex justify-content-between">
								<div class="card-title my-auto">
									<h4 class="mb-0">Μαθητές / Μάθημα</h4>
								</div>
								<div>
									<i class="fas fa-chalkboard-teacher" style="font-size: 250%"></i>
								</div>
							</div>
							<div class="card-body border-top">
								<div class="w-100">
									<canvas id="studentLessonsChart" style="height: 450px;"></canvas>
								</div>
								<table class="table table-striped table-hover w-100" id="studentsPerLessonTbl">
									<thead>
										<tr>
											<th>Μάθημα</th>
											<th>Κωδικός Μαθήματος</th>
											<th class="text-center">Αριθμός Μαθητών</th>
											<th class="text-center">Ποσοστά</th>
											<th>Δημιουργήθηκε</th>
											<th class="text-center" style="max-width: 200px;">Προβολή Μαθητών</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<?php initHelper::getDatatablesJs(); ?>
	<script src=<?php initHelper::getWebPath("/assets/vendor/chart.js/dist/Chart.js"); ?>></script>
	<script src=<?php initHelper::getWebAdminPath("/js/manage-students.js"); ?>></script>
	<?php
	//filter modal
	$modal = new Modal([
		'modalId' => 'filterStudentsModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-purple',
		'modalHeader' => '<h4 id="filter-student-modal-title" class="modal-title"><i class="fas fa-filter mr-2"></i>Φίλτρα Αναζήτησης Μαθητή</h4>',
		'modalBody' => '
		<div class="row">
			<div class="col-lg-5">
				<label for="searchFirstname">Όνομα Μαθητή</label>
				<input class="form-control" type="text" placeholder="Όνομα.." id="searchFirstname" name="searchFirstname">
			</div>
			<div class="col-lg-5">
				<label for="searchLastname">Επίθετο Μαθητή</label>
				<input class="form-control" type="text" placeholder="Επίθετο.." id="searchLastname" name="searchLastname">
			</div>
			<div class="col-lg-2">
				<label for="searchTmima">Τμήμα</label>
				' . $tmimataDropdown . '
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-4">
				<label for="searchTaksi">Τάξη</label>
				'.$taksiList.'
			</div>
			<div class="col-lg-8">
				<label for="searchEmail">Email Μαθητή</label>
				<input class="form-control" type="email" placeholder="Email.." id="searchEmail" name="searchEmail">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-12">
				<label for="searchLessons">Μαθήματα που παρακολουθεί</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<button data-id="" class="btn btn-primary inputBtn addLessonsBtn" data-target="#lessonModal" data-toggle="modal" title="Προσθήκη Μαθημάτων" type="button">
							<i class="fas fa-plus"></i>
						</button>
						<button data-id="" class="btn btn-danger inputBtn removeLessonsBtn" title="Απαλοιφή Μαθημάτων" type="button">
							<i class="fas fa-minus"></i>
						</button>
					</div>
					<input class="form-control" type="text" placeholder="Μαθήματα.." id="searchLessons" name="searchLessons" readonly>
					<input type="hidden" name="searchLessonIds" id="searchLessonIds">
				</div>
			</div>
		</div>
	',
		'modalFooter' => '<button class="btn btn-primary" id="initSearchBtn">Search</button><button class="btn btn-warning" id="clearSearchBtn">Clear Filters</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//create lessons modal
	$modal = new Modal([
		'modalId' => 'lessonModal',
		'modalSize' => 'modal-xl',
        'modalStyle' => 'z-index: 1060',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Προσθήκη Μαθημάτων</h4>',
		'modalBody' => '
<input type="hidden" id="lessonSelected" name="lessonSelected">
' . $lessonBoxGroup . '
',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();

	$modal = new Modal([
		'modalId' => 'classStudentModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 id="students-selected-title" class="modal-title"></h4>',
		'modalBody' => '
	<div style="max-height: 450px; overflow: auto;">
	<table class="table table-striped table-hover w-100">
	<thead>
	<tr>
	<th>Επίθετο</th>
	<th>Όνομα</th>
	<th>Τμήμα</th>
	<th>Ημ. Εγγραφή</th>
	<th style="width:115px;">Ενέργειες</th>
	</tr>
	</thead>
	<tbody id="students-selected-tbl"></tbody>
	</table>
	</div>
	',
		'modalFooter' => 'Αριθμός Μαθητών: <span class="font-weight-bold ml-1 mr-auto" id="StudentClassSum"></span>',
		'animoId' => 'classModalAnimo'
	]);
	echo $modal->createModal();

	$modal = new Modal([
		'modalId' => 'perioxiStudentModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 id="perioxi-selected-title" class="modal-title"></h4>',
		'modalBody' => '
	<div style="max-height: 450px; overflow: auto;">
	<table class="table table-striped table-hover w-100">
	<thead>
	<tr>	
	<th>Επίθετο</th>
	<th>Όνομα</th>
	<th>Τμήμα</th>
	<th>Ημ. Εγγραφή</th>
	<th style="width:115px;">Ενέργειες</th>
	</tr>
	</thead>
	<tbody id="perioxi-selected-tbl"></tbody>
	</table>
	</div>
	',
		'modalFooter' => 'Μαθητές: <span class="font-weight-bold ml-1 mr-auto" id="StudentPerioxiSum"></span>',
		'animoId' => 'perioxiModalAnimo'
	]);
	echo $modal->createModal();

	$modal = new Modal([
		'modalId' => 'lessonStudentModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 id="lesson-selected-title" class="modal-title"></h4>',
		'modalBody' => '
	<div style="max-height: 450px; overflow: auto;">
	<table class="table table-striped table-hover w-100">
	<thead>
	<tr>
	<th>Επίθετο</th>
	<th>Όνομα</th>
	<th>Τμήμα</th>
	<th>Ημ. Εγγραφή</th>
	<th style="width:115px;">Ενέργειες</th>
	</tr>
	</thead>
	<tbody id="lesson-selected-tbl"></tbody>
	</table>
	</div>
	',
		'modalFooter' => 'Μαθητές: <span class="font-weight-bold ml-1 mr-auto" id="StudentLessonSum"></span>',
		'animoId' => 'lessonModalAnimo'
	]);
	echo $modal->createModal();

	//delete student modal
	$modal = new Modal([
		'modalId' => 'deleteStudentModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Μαθητή</h4>',
		'modalBody' => '
	<input type="text" id="studentDeleteId" name="studentDeleteId" style="display: none;">
	<p class="py-3 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε τον/την μαθητή/τρια <span class="text-danger" id="fillStudentItem"></span>;</p>
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveDeleteStudentBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>

</body>

</html>