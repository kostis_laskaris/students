<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$allListName = AppLists::getListNames(); 
$listDropdown = AppLists::getListsDropdown('listDropdown');
$listModalDropdown = AppLists::getListsDropdown('listModalDropdown');
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Διαχείριση Μαθημάτων</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Διαχείριση Λιστών
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Διαχείριση Λιστών',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<div class="card card-default" id="animoSpin">
							<div class="card-header border-bottom py-3">
								<div class="d-flex">
									<div class="card-title my-auto">
										<h4 class="mb-0">Προβολή Καταχωρημένων Λιστών</h4>
									</div>
								</div>
							</div>
							<div class="card-body border-top">
								<div class="d-flex justify-content-between align-items-center">
									<div class="d-flex align-items-center">
										<label class="mr-2 mb-0">Επιλογή: </label>
										<?php echo $listDropdown; ?>
									</div>
									<button class="btn btn-primary addListBtn" type="button" data-toggle="modal" data-target="#addListEntryModal">
										<i class="fas fa-plus mr-1"></i>Προσθήκη Στοιχείου
									</button>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-hover my-2 w-100" id="ListsTbl">
										<thead>
											<tr>
												<th style="max-width: 35px;">ID</th>
												<th>Στοιχείο Λίστας</th>
												<th>Δημιουργήθηκε</th>
												<th>Τροποποιήθηκε</th>
												<th style="max-width: 102px;min-width: 102px;" class="text-center">Ενέργειες</th>
											</tr>
										</thead>
										<tbody id="list-table-body">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="csrf_token" id="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/manage-lists.js"); ?>></script>
	<?php
	//add list entry modal
	$modal = new Modal([
		'modalId' => 'addListEntryModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge6"><em class="fas fa-plus-circle float-left fa-150x mr-2"></em>Προσθήκη Στοιχείου σε Λίστα</h4>',
		'modalBody' => '
		<div class="col-md-12">
		<div class="modal-contents">
		<input type="hidden" id="listEntryAddAppearanceName" name="listEntryAddAppearanceName">
		<div class="row">
		<div class="col-md-12 mb-3">
		<label for="listModalDropdown">Λίστα <span class="reqField">(*)</span></label> 
		' . $listModalDropdown . '
		</div>	
		<div class="col-md-12 mb-3">
		<label for="listEntryName">Όνομα Στοιχείου Λίστας <span class="reqField">(*)</span></label> 
		<input class="form-control" type="text" name="listEntryName" id="listEntryName" required placeholder="Όνομα Νέου Στοιχείου..">
		</div>
		</div>
		<input type="hidden" name="add_csrf_token" id="add_csrf_token" value="' . $_SESSION['csrf_token'] . '">
		</div>
		</div>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="addListEntryBtn" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();

	//edit list entry modal
	$modal = new Modal([
		'modalId' => 'editListModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-info',
		'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge6"><em class="fas fa-pencil-alt float-left fa-150x mr-2"></em>Επεξεργασία Στοιχείου Λίστας</h4>',
		'modalBody' => '
		<div class="col-md-12">
		<div class="modal-contents">
		<input type="hidden" id="listItemEditId" name="listItemEditId">
		<div class="row">
		<div class="col-md-12 mb-3">
		<label for="updateListItemName">Όνομα Στοιχείου Λίστας <span class="reqField">(*)</span></label> 
		<input class="form-control" type="text" name="updateListItemName" id="updateListItemName" required placeholder="Όνομα Στοιχείου..">
		</div>
		</div>
		<input type="hidden" name="csrf_token_edit_list" id="csrf_token_edit_list" value="' . $_SESSION['csrf_token'] . '">
		</div>
		</div>
		',
		'modalFooter' => 'Λίστα: <span class="font-weight-bold ml-1 mr-auto" id="fillEditListItem"></span>
		<button class="btn btn-success btnw-3" id="saveEditListBtn" type="button">Save</button>',
		'animoId' => 'editListModalAnimo'
	]);
	echo $modal->createModal();

	//delete list entry modal
	$modal = new Modal([
		'modalId' => 'deleteListModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Στοιχείου Λίστας</h4>',
		'modalBody' => '
		<input type="hidden" id="listItemDeleteId" name="listItemDeleteId">
		<input type="hidden" name="delete_csrf_token" id="delete_csrf_token" value="' . $_SESSION['csrf_token'] . '">
		<p class="mb-0 py-2 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε το στοιχείο λίστας <span class="badge badge-primary" id="fillListItem"></span> ;</p>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="saveDeleteListBtn" type="button">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>