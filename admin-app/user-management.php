<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
$allUsers = $user->getAllUsers(true, false, false);
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: User Management</title>
	<?php initHelper::getDatatablesCss(); ?>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Διαχείριση Χρηστών
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'User Management',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<!---------  STUDENTS DATATABLE ---------->
						<div class="card card-default" id="animoSort">
							<div class="card-header border-bottom">
								<div class="d-flex">
									<div class="card-title">
										<h4 class="mb-0">Πίνακας Χρηστών Εφαρμογής</h4>
										<div class="text-sm">
											<b style="letter-spacing: 1px;">Επιλογές:&nbsp;</b> Copy - Αντιγράφει τον πίνακα στη μνήμη του Η/Υ <b>&nbsp;|&nbsp;</b> CSV - Εξάγει αρχείο csv <b>&nbsp;|&nbsp;</b> Excel - Εξάγει αρχείο xlsx
										</div>
									</div>
									<div class="ml-auto">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#createUserModal">
											<i class="fas fa-user-plus mr-1"></i>Προσθήκη Χρήστη
										</button>
									</div>
								</div>
							</div>
							<div class="card-body">
								<table class="table table-striped table-hover mt-4 w-100 t-datatable" id="usersTbl">
									<thead>
										<tr>
											<th></th>
											<th style="max-width: 45px;">ID</th>
											<th>Όνομα</th>
											<th>Επώνυμο</th>
											<th>Username</th>
											<th>Email</th>
											<th>Κατάσταση</th>
											<th class="text-center">Ρόλος</th>
											<th>Τελευταία Δραστηριότητα</th>
											<th>Ημερομηνία Εγγραφής</th>
											<th style="max-width: 90px; min-width: 90px;" class="text-center">Ενέργειες</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<!-- Datatables-->
	<?php include('_parts/scripts.php'); ?>
	<?php initHelper::getDatatablesJs(); ?>
	<script src=<?php initHelper::getWebPath("/assets/vendor/animo/animo.js"); ?>></script>
	<script src=<?php initHelper::getWebAdminPath("/js/user-management.js"); ?>></script>
	<?php
	//create user modal
	$modal = new Modal([
		'modalId' 				=> 'createUserModal',
		'modalSize' 			=> 'modal-xl',
		'modalBgHeaderColor' 	=> 'bg-primary',
		'modalHeader' 			=> '<h4 class="modal-title"><em class="fas fa-user-plus float-left fa-150x mr-2"></em>Δημιουργία Νέου Χρήστη</h4>',
		'modalBody'				=> '
	<div class="col-md-12">
	<div class="modal-contents">
	<div class="row">
	<div class="col-lg-4 mb-3">
	<label for="addFirstname">Όνομα <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" name="addFirstname" id="addFirstname" required placeholder="Όνομα..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="addLastname">Επίθετο <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" id="addLastname" name="addLastname" required placeholder="Επίθετο..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="addEmail">Email</label>
	<input class="form-control" type="text" id="addEmail" name="addEmail" placeholder="Email..">
	<div class="invalid-feedback"></div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-4 mb-3">
	<label for="addUsername">Username <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" id="addUsername" name="addUsername" required placeholder="Username..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="addPermission">Ρόλος <span class="reqField">(*)</span></label>
	<select class="custom-select custom-select-md" id="addPermission" name="addPermission" required>
	<option value="Administrator">Administrator</option>
	<option value="Teacher" selected>Teacher</option>
	</select>
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="addStatus">Κατάσταση <span class="reqField">(*)</span></label>
	<select class="custom-select custom-select-md" id="addStatus" name="addStatus" required>
	<option value="0">Ανενεργός</option>
	<option value="1" selected>Ενεργός</option>
	</select>
	<div class="invalid-feedback"></div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-6 mb-3">
	<label for="addPassword">Κωδικός Χρήστη <span class="reqField">(*)</span></label>
	<input class="form-control" type="password" id="addPassword" name="addPassword" required
	placeholder="Κωδικός..">
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-6 mb-3">
	<label for="addRePass">Επαλήθευση Κωδικού <span class="reqField">(*)</span></label> 
	<input class="form-control" type="password" id="addRePass" name="addRePass" required placeholder="Επαλήθευση Κωδικού..">
	<div class="invalid-feedback"></div>
	</div>
	</div>							
	<div class="row">
	<div class="col-md-12">
	<p class="mb-0" style="font-size: 12px; text-align: justify">
	<i class="fas fa-exclamation-circle mr-1 text-primary fa-120x"></i>
	<b>Προσοχή: </b> Ο κωδικός πρέπει να είναι τουλάχιστον 8 λατινικών χαρακτήρων, να περιέχει τουλάχιστον έναν αριθμό και ένα σύμβολο από τα #?!@$%^&*-
	</p>
	</div>
	</div>
	<input type="hidden" name="csrf_token" value="' . $_SESSION['csrf_token'] . '">
	</div>
	</div>
	',
		'modalFooter' 		=> '<button class="btn btn-success btnw-3" id="addUserBtn" type="button">Save</button>',
		'animoId' 			=> ''
	]);
	echo $modal->createModal();
	//password modal
	$modal = new Modal([
		'modalId' 				=> 'passModal',
		'modalSize' 			=> '',
		'modalBgHeaderColor' 	=> 'bg-purple',
		'modalHeader' 			=> '<h4 class="modal-title" id="myModalLabelLarge4"><em class="fas fa-key float-left fa-150x mr-2"></em>Αλλαγή Κωδικού Χρήστη</h4>',
		'modalBody'				=> '
	<div class="col-md-12">
	<div class="row">
	<input type="text" id="passUserId" name="passUserId" style="display: none;">
	<input type="text" id="passUsername" name="passUsername" style="display: none;">
	<div class="col-md-12 mb-3">
	<label for="newPass">Νέος Κωδικός <span class="reqField">(*)</span></label>
	<input class="form-control" type="password" name="newPass" id="newPass" placeholder="Κωδικός.." required>
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-md-12 mb-3">
	<label for="reNewPass">Επαλήθευση Νέου Κωδικού <span class="reqField">(*)</span></label>
	<input class="form-control" type="password" id="reNewPass" name="reNewPass"	placeholder="Επαλήθευση Κωδικού.." required>
	<div class="invalid-feedback"></div>
	</div>
	</div>								
	<div class="row">
	<div class="col-md-12">
	<p class="mb-0" style="font-size: 12px; text-align: justify">
	<i class="fas fa-exclamation-circle mr-1 text-primary fa-120x"></i>
	<b>Προσοχή: </b> Ο κωδικός πρέπει να είναι τουλάχιστον 8 λατινικών χαρακτήρων, να περιέχει τουλάχιστον έναν αριθμό και ένα σύμβολο από τα #?!@$%^&*-
	</p>
	</div>
	</div>
	<input type="hidden" name="csrf_token" value="' . $_SESSION['csrf_token'] . '">
	</div>
	',
		'modalFooter' 		=> '<button class="btn btn-success btnw-3 editPassBtn" type="button">Save</button>',
		'animoId' 			=> ''
	]);
	echo $modal->createModal();
	//edit user modal
	$modal = new Modal([
		'modalId' 				=> 'editUserModal',
		'modalSize' 			=> 'modal-lg',
		'modalBgHeaderColor'	=> 'bg-info',
		'modalHeader' 			=> '<h4 class="modal-title" id="myModalLabelLarge4"><em class="fas fa-user-edit float-left fa-150x mr-2"></em>Επεξεργασία Χρήστη</h4>',
		'modalBody'				=> '
	<div class="col-md-12">
	<div class="modal-contents" id="animoSpin">
	<div class="row">
	<div class="col-lg-4 mb-3">
	<label for="firstname">Όνομα <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" name="firstname" id="firstname" placeholder="-- No Value --" required>
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="lastname">Επίθετο <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" id="lastname" name="lastname" placeholder="-- No Value --">
	<div class="invalid-feedback" required></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="email">Email</label>
	<input class="form-control" type="text" id="email" name="email" placeholder="-- No Value --">
	<div class="invalid-feedback"></div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-4 mb-3">
	<label for="username">Username <span class="reqField">(*)</span></label>
	<input class="form-control" type="text" id="username" name="username" placeholder="-- No Value --" required>
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="permission">Ρόλος <span class="reqField">(*)</span></label>
	<select class="custom-select custom-select-md" id="permission" name="permission" required>
	<option value="Administrator">Administrator</option>
	<option value="Teacher">Teacher</option>
	</select>
	<div class="invalid-feedback"></div>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="status">Κατάσταση <span class="reqField">(*)</span></label>
	<select class="custom-select custom-select-md" id="status" name="status" required>
	<option value="0">Ανενεργός</option>
	<option value="1">Ενεργός</option>
	</select>
	<div class="invalid-feedback"></div>
	</div>
	<input type="hidden" name="csrf_token_edit_user" id="csrf_token_edit_user" value="' . $_SESSION['csrf_token'] . '">
	</div>
	</div>
	</div>
	',
		'modalFooter'	=> '<button class="btn btn-success btnw-3 animation" id="editUserBtn" data-id="" type="button">Save</button>',
		'animoId' 		=> 'editUserModalAnimo'
	]);
	echo $modal->createModal();
	?>
</body>

</html>