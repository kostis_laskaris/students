<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'new-taksi-dropdown';
$appList->html_id = 'new-taksi-dropdown';
$taksiList = $appList->getListByNameAsSelect('');

$tmimataDropdown = Tmima::getTmimataDropdown('new-tmima-dropdown');
?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Επανεγγραφή Μαθητών</title>
	<?php initHelper::getDatatablesCss(); ?>
	<?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
    <style>
        .colorTr {
            background-color: #37bc9b !important;
            color: #fff !important;
        }
    </style>
</head>

<body class="layout-fixed">
    <div class="d-none">
        <?php echo $taksiList; ?>
    </div>
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container" id="theAnimo">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div>
                        Επανεγγραφή Μαθητών
                        <div class="d-flex mt-2">
                            <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                            <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Επανεγγραφή Μαθητών',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <?php renderCustomMessages(); ?>

                <div class="row">
                    <div class="col-lg-10" id="resubStudentAnimo">
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="card-title my-auto">
                                        <h4 class="mb-0">Προβολή Καταχωρημένων Μαθητών</h4>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-green mr-2" id="viewOptionModal" data-toggle="modal" data-target="#resubStudentOptionModal">
                                            <i class="fas fa-check mr-2"></i>Ολοκλήρωση
                                        </button>
                                        <a class="btn btn-warning mr-2" style="display:none;" href="#!" id="removeFilterStudentsModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
                                            <i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <table class="table table-striped table-hover w-100" id="studentsTbl">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th>ID</th>
                                            <th>Επίθετο</th>
                                            <th>Όνομα</th>
                                            <th>Σταθερό Τηλέφωνο</th>
                                            <th>Κινητό Τηλέφωνο</th>
                                            <th>Email</th>
                                            <th>Τμήμα</th>
                                            <th style="min-width: 220px;">Διεύθυνση</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="card card-default">
                            <div class="card-header py-3">
                                <div class="card-title my-auto">
                                    <h4 class="mb-0">Επιλογή Σχολικού Έτους</h4>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <label for="fromSchoolYear">Από Σχολικό Έτος</label>
                                <?php echo getSchoolYearsForResubStudents(); ?>
                                <p class="pt-3 small mb-3">
                                    <i class="fas fa-info-circle mr-1 text-info"></i> Επιλέξτε το σχολικό έτος και την περίοδο μαθημάτων από τα οποία θέλετε να επανεγράψετε μαθητές.
                                </p>
                                <div class="pt-3 text-bold border-top">
                                    Επιλεγμένα:<span class="pl-2" id="count-selected">0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <script>
        $(document).ready(function() {
            model = {
                taksi: <?php echo json_encode($taksiList, JSON_UNESCAPED_UNICODE); ?>,
                tmima: <?php echo json_encode($tmimataDropdown, JSON_UNESCAPED_UNICODE); ?>,
            }
        });
    </script>    
    
    <script src=<?php initHelper::getWebAdminPath("/js/resub-students.js"); ?>></script>
    <?php
    //create lessons modal
    $modal = new Modal([
        'modalId' => 'resubStudentOptionModal',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-primary',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Αλλαγή Στοιχείων Φοίτησης</h4>',
        'modalBody' => '<div class="table-responsive" id="resubStudentOptionContainer" style="max-height:70vh;overflow: hidden; overflow-y:auto;"></div>',
        'modalFooter' => '<button class="btn btn-success btnw-3" id="saveResubData" type="button">Save</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>