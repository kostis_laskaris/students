<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Δημιουργία Σχολικού Έτους</title>
	<?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container" id="theAnimo">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div>
                        Δημιουργία Σχολικού Έτους
                        <div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Δημιουργία Σχολικού Έτους',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <?php renderCustomMessages(); ?>

                <div class="row py-3 align-items-center">
                    <div class="col-md-4 offset-md-1">
                        <div class="card card-default">
                            <div class="card-body py-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="text-bold" for="">Από Σχολικό Έτος</label>
                                        <?php echo getSchoolYears(true, 'from_sx_year'); ?>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <label class="text-bold" for="">Από Περίοδο Μαθημάτων</label>
                                        <?php echo getPeriodosMathimaton('from_periodos'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <i class="fas fa-exchange-alt fa-5x m-auto"></i>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-default">
                            <div class="card-body py-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="text-bold" for="">Σε Σχολικό Έτος</label>
                                        <?php echo getSchoolYears(true, 'to_sx_year'); ?>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <label class="text-bold" for="">Σε Περίοδο Μαθημάτων</label>
                                        <?php echo getPeriodosMathimaton('to_periodos'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p class="pt-3 small mb-3 text-center">
                            <i class="fas fa-info-circle mr-1 text-info"></i> Επιλέξτε το σχολικό έτος και την περίοδο μαθημάτων (αριστερά) από τα οποία επιθυμείτε να αντιγράψετε βασικές επιλογές (τμήματα, μαθήματα, συντομεύσεις κλπ) στη νέα Βάση Δεδομένων (δεξιά).
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary px-5" id="saveNewDb">Δημιουργία</button>
                    </div>
                </div>
            </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <script src=<?php initHelper::getWebAdminPath("/js/manage-database.js") ?>></script>
</body>

</html>