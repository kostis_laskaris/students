<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$usersDropdown = User::getUsersAsDropdown('searchUser');
$studentsDropdown = Student::getAllStudentsAsDropdown('searchStudent');

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup('');
?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Προβολή Βαθμολογιών</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div class="d-flex align-items-center">
                        <div>
                            Προβολή Βαθμολογιών
                            <div class="d-flex mt-2">
                                <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                                <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                            </div>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Προβολή Βαθμολογιών',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <div class="row mt-2" id="animoSort">
                    <div class="col-md-12">
                        <div class="card card-default" id="viewGradesCard">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <div class="card-title">
                                    <h4 class="mb-0 py-2">Πίνακας Βαθμολογιών</h4>
                                </div>
                                <div class="d-flex align-items-center">
                                    <a class="btn btn-green mr-2 text-bold" href="<?php echo initHelper::getWebAdminPath('/edit-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                        <i class="fas fa-edit mr-2"></i>Επεξεργασία Βαθμολογιών
                                    </a>
                                    <a class="btn btn-purple mr-2 text-bold" href="#!" data-toggle="modal" data-target="#filterGradesModal" title="Φίλτρα Αναζήτησης">
                                        <i class="fas fa-filter mr-2"></i>Φίλτρα
                                    </a>
                                    <a class="btn btn-warning mr-2 text-bold" style="display:none;" href="#!" id="removeFilterGradesModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
                                        <i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
                                    </a>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover w-100" id="view_grades_tbl">
                                        <thead>
                                            <tr>
                                                <th>Ημερομηνία Εξέτασης</th>
                                                <th>Καθηγητής/τρια</th>
                                                <th>Ονοματεπώνυμο Μαθητή</th>
                                                <th>Μάθημα</th>
                                                <th>Τμήμα</th>
                                                <th>Είδος Εξέτασης</th>
                                                <th>Παρουσία</th>
                                                <th>Βαθμός</th>
                                                <th>Καταχωρήθηκε</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-default">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4 class="mb-0 py-2">Μέσος Όρος / Μάθημα</h4>
                                <i class="fas fa-chart-bar fa-2x"></i>
                            </div>
                            <div class="card-body border-top">
                                <canvas id="chartjs-barchart"></canvas>
                            </div>
                            <div class="card-footer avg-card"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-default">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4 class="mb-0 py-2">Πλήθος Βαθμολογιών</h4>
                                <i class="fas fa-chart-pie fa-2x"></i>
                            </div>
                            <div class="card-body border-top">
                                <canvas id="chartjs-piechart"></canvas>
                            </div>
                            <div class="card-footer count-card"></div>
                        </div>
                    </div>
                </div>

                <div class="row" id="gradeStatsAnimo">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="card-title">
                                    <h4 class="mb-0 pt-2">Στατιστικά</h4>
                                    <small> Αφορούν μόνο παρόντες</small>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <div class="row mt-2">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover w-100" id="stats_grades_tbl">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Μάθημα</th>
                                                        <th>Απουσίες</th>
                                                        <th>Πλήθος Βαθμολογιών</th>
                                                        <th>Μέσος Όρος /100</th>
                                                        <th>Μέσος Όρος /20</th>
                                                        <th>Υψηλότερος</th>
                                                        <th>Χαμηλότερος</th>
                                                        <th>Τελευταία Εγγραφή</th>
                                                        <th>Τελευταία Τροποποίηση</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script src=<?php initHelper::getWebAdminPath("/js/view-grades.js"); ?>></script>
    <script src=<?php initHelper::getWebPath("/assets/vendor/chart.js/dist/Chart.js"); ?>></script>
    <?php
    //filter modal
    $modal = new Modal([
        'modalId' => 'filterGradesModal',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-purple',
        'modalHeader' => '<h4 id="filter-student-modal-title" class="modal-title"><i class="fas fa-filter mr-2"></i>Φίλτρα Αναζήτησης Βαθμολογίας</h4>',
        'modalBody' => '
		<div class="row mt-2">
			<div class="col-lg-3 mt-2 pr-1">
				<label for="searchUser">Όνομα Καθηγητή</label>
				' . $usersDropdown . '
			</div>
            <div class="col-lg-3 mt-2 px-1">
                <label for="searchStudent">Όνομα Μαθητή</label>
                ' . $studentsDropdown . '
            </div>
			<div class="col-lg-3 mt-2 px-1">
                <label for="">Είδος Εξέτασης</label>
                <select class="custom-select custom-select-md" name="searchExam_kind" id="searchExam_kind">
                    <option value="">Επιλογή</option>
                    <option value="TEST">TEST</option>
                    <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                    <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                </select>
            </div>
			<div class="col-lg-3 mt-2 pl-1">
				<label for="">Παρουσία</label>
				<select class="custom-select custom-select-md" name="searchAbsence" id="searchAbsence">
					<option value="">Επιλογή</option>
					<option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
					<option value="ΑΠΩΝ">ΑΠΩΝ</option>
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-4 mt-2">
				<label for="">Βαθμός</label>
				<div class="d-flex pb-1">
					<input class="form-control w-45 mr-1" type="text" placeholder="Τελεστής" id="searchTelestis" name="searchTelestis" maxlength="2">
					<input class="form-control w-55" type="number" min="0" max="100" id="searchGrade" name="searchGrade" placeholder="Βαθμός">
				</div>
				<span class="small text-bold text-warning" id="accepted-telestes">* Αποδεκτές Τιμές: >, <, =, >=, <=, !=</span>
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Από</label>
				<input type="date" class="form-control" id="searchDate_from" name="searchDate_from">
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Έως</label>
				<input type="date" class="form-control" id="searchDate_to" name="searchDate_to">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-12 mt-2">
                <label for="">Μαθήματα</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary inputBtn addLessonsBtn" title="Επιλογή μαθημάτων" data-target="#lessonsModal" data-toggle="modal" type="button">
                            <i class="fas fa-plus"></i>
                        </button>
                        <button class="btn btn-danger inputBtn removeLessonsBtn" type="button" title="Αποεπιλογή όλων">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <input class="form-control" type="text" placeholder="Μαθήματα.." id="searchLessons" name="searchLessons" readonly>
					<input type="hidden" name="searchLessonIds" id="searchLessonIds">
                </div>
            </div>
		</div>
	',
        'modalFooter' => '<button class="btn btn-primary" id="initSearchBtn">Search</button><button class="btn btn-warning" id="clearSearchBtn">Clear Filters</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    //create lesson modal
    $modal = new Modal([
        'modalId' => 'lessonsModal',
        'modalSize' => 'modal-xl',
        'modalStyle' => 'z-index: 1060',
        'modalBgHeaderColor' => 'bg-primary',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-search float-left fa-150x mr-2"></em>Αναζήτηση με κριτήριο μαθήματα</h4>',
        'modalBody' => '
		<input type="hidden" id="lessonSelected" name="lessonSelected">
		' . $lessonBoxGroup . '
		',
        'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>