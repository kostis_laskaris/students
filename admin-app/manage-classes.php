<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'editTaksi';
$appList->html_id = 'editTaksi';
$taksiList = $appList->getListByNameAsSelect('');
?>

<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Διαχείριση Τμημάτων</title>
	<?php initHelper::getDatatablesCss(); ?>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href="../assets/css/theme-e.css" id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Διαχείριση Τμημάτων
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'  => '#',
						'title' => 'Διαχείριση Τμημάτων',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row" id="tmimataTblAnimo">
					<div class="col-xl-12">
						<div class="card card-default">
							<div class="card-header">
								<div class="card-title py-2">
									<h4 class="mb-0">Προβολή Καταχωρημένων Τμημάτων</h4>
								</div>
							</div>
							<div class="card-body border-top">
								<table class="table table-striped table-hover my-4 w-100" id="tmimataTbl">
									<thead>
										<tr>
											<th style="max-width: 35px;">ID</th>
											<th>Όνομα Τμήματος</th>
											<th>Τάξη</th>
											<th>Σχόλια</th>
											<th>Δημιουργήθηκε</th>
											<th>Τροποποιήθηκε</th>
											<th style="max-width: 100px; min-width: 90px;" class="text-center">Ενέργειες</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<?php initHelper::getDatatablesJs(); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/manage-classes.js"); ?>></script>
	<?php
	$modal = new Modal([
		'modalId' => 'editTmimaModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-info',
		'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge6"><em class="fa fa-edit float-left fa-150x mr-2"></em>Επεξεργασία Τμήματος</h4>',
		'modalBody' => '
		<div class="col-md-12">  
			<div class="row">
				<div class="col-md-6 mb-3">
					<label for="editTmima">Τμήμα <span class="reqField">(*)</span></label>
					<input class="form-control" type="text" id="editTmima" name="editTmima" maxlength="100" required placeholder="-- No Value --">
				</div>
				<div class="col-md-6 mb-3">
					<label for="editTaksi">Τάξη <span class="reqField">(*)</span></label>
					' . $taksiList . '
				</div>
				<div class="col-md-12 mb-3">
					<label for="editComments">Σχόλια</label>
					<input class="form-control" type="text" id="editComments" name="editComments" maxlength="250" placeholder="-- No Value --">
				</div>
			</div>
		</div>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="submitSaveEditClass" type="button">Save</button>',
		'animoId' => 'editClassModalAnimo'
	]);
	echo $modal->createModal();
	//delete class modal
	$modal = new Modal([
		'modalId' => 'deleteTmimaModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Τμήματος</h4>',
		'modalBody' => '
	<p class="py-3 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε το τμήμα;</p>
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="submitDeleteTmimaBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>