<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
$stdPerClass = Student::getStudentPerClass();
$usersDropdown = User::getUsersAsDropdown('searchUser');

?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div class="d-flex align-items-center">
                        <div>
                            Dashboard
                            <div class="d-flex mt-2">
                                <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                                <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                            </div>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs();
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-green-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="fas fa-user-graduate"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-green rounded-right">
                                <div class="h2 mt-0"><?php echo Student::getStudentsCount(); ?></div>
                                <div class="text-uppercase">ΜΑΘΗΤΕΣ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="fas fa-chalkboard"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-primary rounded-right">
                                <div class="h2 mt-0"><?php echo Tmima::getTmimataCount(); ?></div>
                                <div class="text-uppercase">ΤΜΗΜΑΤΑ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="far fa-star"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-purple rounded-right">
                                <div class="h2 mt-0"><?php echo Grade::getGradesCount(); ?></div>
                                <div class="text-uppercase">ΒΑΘΜΟΛΟΓΙΕΣ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-green justify-content-center rounded-left">
                                <div class="text-center">
                                    <div class="text-sm" data-now="" data-format="MMMM"></div>
                                    <br>
                                    <div class="h2 mt-0" data-now="" data-format="D"></div>
                                </div>
                            </div>
                            <div class="col-8 py-3 rounded-right">
                                <div class="text-uppercase" data-now="" data-format="dddd"></div>
                                <br>
                                <div class="h2 mt-0" data-now="" data-format="HH:mm"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- average per user chart -->
                    <div class="col-xl-12 col-lg-12" id="teacherGradesAverageChartAnimo">
                        <div class="card card-default" id="teacherGradesAverageCard">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h5 class="mb-0">Μέσος Όρος Βαθμολογιών / Καθηγητή</h5>
                                <i class="fas fa-chart-bar fa-2x"></i>
                            </div>
                            <div class="card-body border-top">
                                <canvas id="teacherGradesAverageBarchart" style="height: 300px;"></canvas>
                            </div>
                            <div class="card-footer text-center"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <!-- Charts -->
                        <div class="row">
                            <!-- all grades chart -->
                            <div class="col-xl-6 col-lg-12" id="teacherGradesChartAnimo">
                                <div class="card card-default" id="teacherGradesCard">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <h5 class="mb-0">Πλήθος Βαθμολογιών / Καθηγητή</h5>
                                        <i class="fas fa-chart-bar fa-2x"></i>
                                    </div>
                                    <div class="card-body border-top">
                                        <canvas id="teacherGradesBarchart"></canvas>
                                    </div>
                                    <div class="card-footer text-center"></div>
                                </div>
                            </div>

                            <!-- student per class chart -->
                            <div class="col-xl-6 col-lg-12" id="studentPerClassChartAnimo">
                                <div class="card card-default" id="studentPerClassCard">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <h5 class="mb-0">Μαθητές / Τάξη</h5>
                                        <i class="fas fa-chart-pie fa-2x"></i>
                                    </div>
                                    <div class="card-body border-top">
                                        <canvas id="studentPerClassPiechart"></canvas>
                                    </div>
                                    <div class="card-footer text-center"></div>
                                </div>
                            </div>
                        </div>

                        <!-- Tables -->
                        <div class="row" id="gradeStatsPerMonthPerUser">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <div class="card-title">
                                            <h5 class="mb-0 py-2">Στατιστικά Καθηγητών / Μήνα</h5>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <span class="text-bold mr-2">Καθηγητής:</span> <?php echo $usersDropdown; ?>
                                            <a class="btn btn-purple ml-2 text-bold" id="renderLineChartBtn" href="#!" data-toggle="modal" data-target="#userGradesPerMonthChar" title="Γράφημα">
                                                <i class="fas fa-chart-line mr-2"></i>Γράφημα
                                            </a>
                                            <a class="text-muted ml-2" href="#!" data-tool="card-collapse" data-toggle="tooltip" title="">
                                                <em class="fa fa-minus"></em>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-wrapper collapse show">
                                        <div class="card-body bt">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover w-100" id="gradeStatsPerMonthPerUserTable">
                                                    <thead>
                                                        <tr>
                                                            <th>Καθηγητής</th>
                                                            <th>Μήνας</th>
                                                            <th class="text-center">Μέσος Όρος</th>
                                                            <th class="text-center">Αριθμός Βαθμολογιών</th>
                                                            <th class="text-center">Μέγιστη</th>
                                                            <th class="text-center">Ελάχιστη</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="gradesPerUserAnimo">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <div class="card-title">
                                            <h5 class="mb-0 py-2">Βαθμολογίες / Καθηγητή</h5>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <a class="btn btn-green mr-2 text-bold" href="<?php echo initHelper::getWebAdminPath('/edit-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                                <i class="fas fa-edit mr-2"></i>Επεξεργασία Βαθμολογιών
                                            </a>
                                            <a class="btn btn-info mr-2 text-bold" href="<?php echo initHelper::getWebAdminPath('/view-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                                <i class="fas fa-align-justify mr-2"></i>Προβολή Όλων
                                            </a>
                                            <a class="text-muted ml-2" href="#!" data-tool="card-collapse" data-toggle="tooltip" title="">
                                                <em class="fa fa-minus"></em>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-wrapper collapse show">
                                        <div class="card-body bt">
                                            <table class="table table-striped table-hover w-100" id="gradesPerUserTable">
                                                <thead>
                                                    <tr>
                                                        <th>Καθηγητής</th>
                                                        <th>Μάθημα</th>
                                                        <th>Αριθμός Βαθμολογιών</th>
                                                        <th>Μέσος Όρος</th>
                                                        <th>Τελευταία Βαθμολογία</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Right Sidebar -->
                    <div class="col-lg-3">
                        <div class="card card-default" id="latestStudentsAnimo">
                            <div class="card-header">
                                <div class="d-flex py-2">
                                    <div class="card-title">
                                        <h5 class="mb-0">Τελευταίες Εγγραφές</h5>
                                    </div>
                                    <em class="ml-auto text-muted icon-list fa-150x"></em>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table w-100" id="latestStudentsTable">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="card-footer bt-3 clearfix btn-block d-flex border-top" href="<?php echo initHelper::getWebAdminPath('/manage-students.php'); ?>">
                                <span>Προβολή όλων</span>
                                <span class="ml-auto">
                                    <em class="fa fa-chevron-circle-right"></em>
                                </span>
                            </a>
                        </div>

                        <div class="card card-default" id="latestGradesAnimo">
                            <div class="card-header">
                                <div class="d-flex py-2">
                                    <div class="card-title">
                                        <h5 class="mb-0">Τελευταίες Βαθμολογίες</h5>
                                    </div>
                                    <em class="ml-auto text-muted icon-list fa-150x"></em>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table w-100" id="latestGradesTable">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="card-footer bt-3 clearfix btn-block d-flex border-top" href="<?php echo initHelper::getWebAdminPath('/view-grades.php'); ?>">
                                <span>Προβολή όλων</span>
                                <span class="ml-auto">
                                    <em class="fa fa-chevron-circle-right"></em>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script src=<?php initHelper::getWebPath("/assets/vendor/moment/min/moment-with-locales.js"); ?>></script>
    <script src=<?php initHelper::getWebPath("/assets/vendor/easy-pie-chart/dist/jquery.easypiechart.js"); ?>></script>
    <script src=<?php initHelper::getWebPath("/assets/vendor/chart.js/dist/Chart.js"); ?>></script>
    <script src=<?php initHelper::getWebAdminPath("/js/dashboard.js"); ?>></script>
    <?php
    //line chart modal
    $modal = new Modal([
        'modalId' => 'userGradesPerMonthChar',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-purple',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-chart-line float-left fa-150x mr-2"></em>Γράφημα Βαθμολογιών Καθηγητών / Μήνα</h4>',
        'modalBody' => '
        <h4 class="text-center mb-4" id="teacherNameSpan"></h4>
		<div class="w-100"  id="userGradesPerMonthLineChart">		
		</div>
		',
        'modalFooter' => '<div class="mr-auto" id="finalAverage">Συνολικό Μέσο Όρο:</div>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>