<?php
session_start();
include("../init.php");
if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$tmimataDropdown = Tmima::getTmimataDropdown('tmima');

$checkbox = new CheckboxGroup;

$appList = new AppLists;
$appList->name = 'taksi';
$appList->html_name = 'class';
$appList->html_id = 'class';
$taksiList = $appList->getListByNameAsSelect('');

$appList->name = 'informed_by';
$appList->html_name = 'informed_by';
$appList->html_id = 'informed_by';
$informedbyLists = $appList->getListByNameAsSelect('');

$shortcutBtnsGrp = Shortcut::getShortcutsAsButtonGroup();

$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup($shortcutBtnsGrp);

?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Εγγραφή Μαθητή</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Εγγραφή Μαθητή
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'	=> '#',
						'title' => 'Εγγραφή Μαθητή',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal mt-2" method="post" action="" id="addStudentForm">
							<!-- 1ST ACCORDION-->
							<div class="card card-default card-demo mb-3" id="addStudentCard">
								<div class="card-header d-flex justify-content-between align-items-center py-3 border-bottom">
									<div class="card-title">
										<h4 class="mb-0">Στοιχεία Μαθητή</h4>
									</div>
									<a class="fa-120x text-muted" href="#!" data-tool="card-collapse" title="">
										<em class="fa fa-minus"></em>
									</a>
								</div>
								<div class="card-wrapper collapse show">
									<div class="card-body">
										<div class="col-lg-12 pl-0 pr-0">
											<!----- 1st ROW ----->
											<div class="form-row">
												<div class="col-lg-2 mb-3">
													<label for="firstname">Όνομα <span class="reqField">(*)</span></label>
													<input class="form-control" id="firstname" name="firstname" type="text" placeholder="Όνομα.." required>
												</div>
												<div class="col-lg-2 mb-3">
													<label for="lastname">Επίθετο <span class="reqField">(*)</span></label>
													<input class="form-control" id="lastname" name="lastname" type="text" placeholder="Επίθετο.." required>
												</div>
												<div class="col-lg-3 mb-3">
													<label for="address">Διεύθυνση Κατοικίας</label>
													<input class="form-control" id="address" name="address" type="text" placeholder="Διεύθυνση..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="perioxi">Περιοχή</label>
													<input class="form-control" id="perioxi" name="perioxi" type="text" placeholder="Περιοχή..">
												</div>
												<div class="col-lg-2 mb-3">
													<label for="zip">Τ.Κ.</label>
													<input class="form-control" id="zip" name="zip" maxlength="5" type="text" placeholder="ΤΚ..">
												</div>
											</div>

											<!----- 2nd Row ----->
											<div class="form-row" style="margin-top: 15px;">
												<div class="col-lg-3 mb-3">
													<label for="mobile">Τηλέφωνο Μαθητή</label>
													<input class="form-control" id="mobile" name="mobile" maxlength="10" type="text" placeholder="Τηλεφωνο..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="phone">Σταθερό Τηλέφωνο</label>
													<input class="form-control" id="phone" name="phone" maxlength="10" type="text" placeholder="Τηλεφωνο..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="email">Email Μαθητή</label>
													<input class="form-control" id="email" name="email" placeholder="Email..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="sub_date">Ημερομηνία Εγγραφής <span class="reqField">(*)</span></label>
													<input class="form-control" id="sub_date" name="sub_date" type="date" required>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 2ND ACCORDION-->
							<div class="card card-default card-demo mb-3" id="cardDemo2">
								<div class="card-header d-flex justify-content-between align-items-center py-3 border-bottom">
									<div class="card-title">
										<h4 class="mb-0">Στοιχεία Κηδεμόνων</h4>
									</div>
									<a class="fa-120x text-muted" href="#!" data-tool="card-collapse" title="">
										<em class="fa fa-minus"></em>
									</a>
								</div>
								<div class="card-wrapper collapse show">
									<div class="card-body">
										<div class="col-md-12 pl-0 pr-0">
											<!----- 1st ROW ----->
											<div class="form-row">
												<div class="col-lg-3 mb-3">
													<label for="mname">Όνομα Μητέρας</label>
													<input class="form-control" id="mname" name="mname" type="text" placeholder="Όνομα..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="faname">Όνομα Πατέρα</label>
													<input class="form-control" id="faname" name="faname" type="text" placeholder="Όνομα..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="mphone">Τηλέφωνο Μητέρας</label>
													<input class="form-control" id="mphone" name="mphone" maxlength="10" type="text" placeholder="Τηλεφωνο..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="fphone">Τηλέφωνο Πατέρα</label>
													<input class="form-control" id="fphone" name="fphone" maxlength="10" type="text" placeholder="Τηλεφωνο..">
												</div>
											</div>

											<!----- 2nd Row ----->
											<div class="form-row" style="margin-top: 15px;">
												<div class="col-lg-3 mb-3">
													<label for="email_kidemona">Email Κηδεμόνα</label>
													<input class="form-control" id="email_kidemona" name="email_kidemona" placeholder="Email..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="kid_name">Όνομα Κηδεμόνα</label>
													<input class="form-control" id="kid_name" name="kid_name" type="text" placeholder="Όνομα..">
												</div>
												<div class="col-lg-3 mb-3">
													<label for="kid_phone">Τηλέφωνο Κηδεμόνα</label>
													<input class="form-control" id="kid_phone" name="kid_phone" maxlength="22" type="text" placeholder="Τηλεφωνο..">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 3RD ACCORDION-->
							<div class="card card-default card-demo" id="cardDemo3">
								<div class="card-header d-flex justify-content-between align-items-center border-bottom py-3">
									<div class="card-title">
										<h4 class="mb-0">Στοιχεία Φοίτησης</h4>
									</div>
									<a class="float-right fa-120x text-muted" href="#!" data-tool="card-collapse" title="">
										<em class="fa fa-minus"></em>
									</a>
								</div>
								<div class="card-wrapper collapse show">
									<div class="card-body">
										<div class="col-md-12 pl-0 pr-0">
											<!----- 1st ROW ----->
											<div class="form-row">
												<div class="col-lg-2 mb-3">
													<label for="fee">Δίδακτρα</label>
													<input class="form-control" id="fee" name="fee" maxlength="3" type="text" placeholder="Δίδακτρα..">
												</div>
												<div class="col-lg-2 mb-3">
													<label for="tmima">Τμήμα <span class="reqField">(*)</span></label>
													<?php echo $tmimataDropdown; ?>
												</div>
												<div class="col-lg-3 mb-3">
													<label for="school_year">Σχολικό Έτος <span class="reqField">(*)</span></label>
													<input class="form-control" id="sx_etos" name="sx_etos" type="text" readonly value="<?php echo $_SESSION['curr_school_year']; ?>">
												</div>
												<div class="col-lg-2 mb-3">
													<label for="taksi">Τάξη <span class="reqField">(*)</span></label>
													<?php echo $taksiList; ?>
												</div>
												<div class="col-lg-3 mb-3">
													<label for="informed_by">Ενημερώθηκε Από</label>
													<?php echo $informedbyLists; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 4TH ACCORDION-->
							<div class="card card-default card-default card-demo" id="cardDemo4">
								<div class="card-header d-flex justify-content-between align-items-center py-3 border-bottom">
									<div class="card-title">
										<h4 class="mb-0">Μαθήματα που παρακολουθεί</h4>
									</div>
									<a class="fa-120x text-muted" href="#!" data-tool="card-collapse" title="">
										<em class="fa fa-minus"></em>
									</a>
								</div>
								<div class="card-wrapper collapse show">
									<div class="card-body border-top">
										<div class="col-md-12 pl-0 pr-0">
											<div class="form-row">
												<div class="col-lg-12 mb-3">
													<label for="lessons">Μαθήματα <span class="reqField">(*)</span></label>
													<div class="input-group">
														<div class="input-group-prepend">
															<button data-id="" class="btn btn-primary inputBtn addLessonsBtn" data-target="#lessonModal" data-toggle="modal" title="Προσθήκη Μαθημάτων" type="button">
																<i class="fas fa-plus"></i>
															</button>
															<button data-id="" class="btn btn-danger inputBtn removeLessonsBtn" title="Απαλοιφή Μαθημάτων" type="button">
																<i class="fas fa-minus"></i>
															</button>
														</div>
														<input class="form-control" type="text" placeholder="Μαθήματα.." id="lessons" name="lessons" readonly>
													</div>
												</div>
												<input type="hidden" name="lesson_ids" id="lesson_ids">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<p style="text-align: center;">
									<button class="btn btn-primary btnw-4" data-target="#submitSaveStudentModal" data-toggle="modal" name="submit_form" id="submitBtn" type="button">Αποθήκευση</button>
								</p>
							</div>
						</form>
					</div>
				</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/add-student.js"); ?>></script>
	<?php
	//create lessons modal
	$modal = new Modal([
		'modalId' => 'lessonModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Προσθήκη Μαθημάτων</h4>',
		'modalBody' => '
	<input type="hidden" id="lessonSelected" name="lessonSelected">
	' . $lessonBoxGroup . '
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	// info modal
	$modal = new Modal([
		'modalId' => 'submitSaveStudentModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-success-dark',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-plus float-left fa-150x mr-2"></em>Προσθήκη Μαθητή</h4>',
		'modalBody' => '
		<p class="py-2 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να αποθηκεύσετε τον/την μαθητή/τρια;</p>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="submitSaveStudent" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>