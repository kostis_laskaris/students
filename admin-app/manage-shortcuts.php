<?php
session_start();
include("../init.php");

if (!isLoggedIn() || !isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = 'shortcut_lessons[]';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = '';
$tmimataDropdown = $checkbox->createLessonGroup();

$checkbox->group_input_name = 'shortcut_lessons_edit[]';
$checkbox->cols = 4;
$checkbox->input_id_prefix = 'e_';
$checkbox->container_id = 'editLessonBox';
$tmimataDropdownForEdit = $checkbox->createLessonGroup();
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Διαχείριση Συντομεύσεων</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Διαχείριση Συντομεύσεων
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Διαχείριση Συντομεύσεων',
                    ]);
                    ?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<div class="card card-default" id="animoSort">
							<div class="card-header border-bottom">
								<div class="d-flex align-items-center">
									<div class="card-title my-auto">
										<h4 class="mb-0">Προβολή Καταχωρημένων Συντομεύσεων</h4>
										<small class="" id="shortcutsCount"></small>
									</div>
									<span class="float-right ml-auto">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#createSortcutModal" id="createShortcutBtn">
											<i class="fas fa-plus mr-2"></i>Προσθήκη Συντόμευσης
										</button>
									</span>
								</div>
							</div>
							<div class="card-body border-top">
								<div class="table-responsive">
									<table class="table table-striped table-hover w-100" id="shortcutsTbl">
										<thead>
											<tr>
												<th style="max-width: 40px;">ID</th>
												<th>Όνομα Συντόμευσης</th>
												<th>Μαθήματα</th>
												<th>Δημιουργήθηκε</th>
												<th>Τροποποιήθηκε</th>
												<th style="max-width: 100px; min-width: 100px;" class="text-center">Ενέργειες</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebAdminPath("/js/manage-shortcuts.js"); ?>></script>
	<?php
	//create shortcut modal
	$modal = new Modal([
		'modalId' => 'createSortcutModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="far fa-check-square float-left fa-150x mr-2"></em>Δημιουργία Νέας Συντόμευσης</h4>',
		'modalBody' => '
	<div class="row pb-3">
	<div class="col-md-12">
	<label class="pb-2" for="addShortcutName">Όνομα Συντόμευσης <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" name="addShortcutName" id="addShortcutName" required placeholder="Όνομα Συντόμευσης..">
	</div>
	<input type="hidden" name="csrf_token" id="csrf_token" value="' . $_SESSION['csrf_token'] . '">
	</div>
	' . $tmimataDropdown . '
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="addShortcutBtn">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//edit shortcut modal
	$modal = new Modal([
		'modalId' => 'editSortcutModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-info',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-pencil-alt float-left fa-150x mr-2"></em>Επεξεργασία Συντόμευσης</h4>',
		'modalBody' => '
	<div class="row pb-3">
	<div class="col-md-12">
	<label class="pb-2" for="editShortcutName">Όνομα Συντόμευσης <span class="reqField">(*)</span></label> 
	<input class="form-control" type="text" name="editShortcutName" id="editShortcutName" required placeholder="Όνομα Συντόμευσης..">
	</div>
	<input type="hidden" name="edit_csrf_token" id="edit_csrf_token" value="' . $_SESSION['csrf_token'] . '">
	</div>
	' . $tmimataDropdownForEdit . '
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveEditShortcutBtn" data-id="">Save</button>
	<button class="btn btn-warning btnw-3" type="button" id="clearShortcutEditFormBtn" name="clearForm">Clear</button>',
		'animoId' => 'editSortcutModalAnimo'
	]);
	echo $modal->createModal();
	//delete shortcut modal
	$modal = new Modal([
		'modalId' => 'deleteSortcutModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Συντόμευσης</h4>',
		'modalBody' => '
	<input type="text" id="shortcutDeleteId" name="shortcutDeleteId" style="display: none;">
	<input type="hidden" name="delete_csrf_token" id="delete_csrf_token" value="' . $_SESSION['csrf_token'] . '">
	<p class="py-3" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε αυτή τη συντόμευση;</p>
	',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveDeleteShortcutBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>