<?php
include('init.php');
?>
<!DOCTYPE html>
<html lang="el">
<head>   
    <?php include('_parts/header.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Error 204 - No Content</title> 
</head>
<body>
    <div class="wrapper pt-5">
            <!-- Page content-->
                <div class="row py-5">
                    <div class="col-lg-12 text-center">
                        <div class="pb-5">
                            <em class="fas fa-unlink fa-7x text-danger"></em>
                        </div>
                        <div class="pb-3">
                            <p class="display-1">204</p>
                        </div>
                        <div class="pb-3">
                            <p class="display-4">- No Content -</p>                    
                        </div>
                        <div class="pb-3">
                            <p class="lead m-1">Έχετε επιλέξει λάθος σχολικό έτος!</p>
                            <p>Προσπαθείστε ξανά</p>
                        </div>
                        <div class="pb-5">
                            <a class="btn btn-lg btn-primary text-md" href="login.php"><i class="fas fa-home mr-2"></i> Αρχική Σελίδα</a>
                        </div>
                        <div>
                            <i class="far fa-copyright"></i> Φροντιστήριο Πολύτροπο
                        </div>  
                    </div>    
                </div>
    </div>
    <?php include('_parts/scripts.php'); ?>
</body>

</html>