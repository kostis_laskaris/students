<?php
session_start();
include("init.php");
setCsrfToken();

if (isLoggedIn()) {
	if (isAdmin()) {
		redirectTo(WEB_ADMIN_ROOT . '/dashboard.php');
	} elseif (isTeacher()) {
		redirectTo(WEB_TEACHER_ROOT . '/dashboard.php');
	}
	exit();
}
$username = $password = "";
$username_err = $password_err = $sx_year_err = $periodos_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty(trim(filter_input(INPUT_POST, 'username')))) {
		$username_err = 'Please enter username.';
	} else {
		$username = trim(filter_input(INPUT_POST, 'username'));
	}

	if (empty(filter_input(INPUT_POST, 'password'))) {
		$password_err = 'Please enter your password.';
	} else {
		$password = filter_input(INPUT_POST, 'password') . filter_input(INPUT_POST, 'username') . hash('sha256', filter_input(INPUT_POST, 'password'));
	}

	if (empty(filter_input(INPUT_POST, 'school_year'))) {
		$sx_year_err = 'Please enter a school year.';
	}

	if (empty(filter_input(INPUT_POST, 'periodos_login'))) {
		$periodos_err = 'Please enter a lesson period.';
	}

	if (empty($username_err) && empty($password_err) && empty($sx_year_err) && empty($periodos_err)) {
		global $db;
		$newUser = new User($username);
		$newUser->password = $password;
		$flag = $newUser->validateUser();
		if ($flag == 0) {
			$allString = filter_input(INPUT_POST, 'school_year');
			$periodos_mathimaton = filter_input(INPUT_POST, 'periodos_login');
			$parts = explode(':', $allString);
			$periodos_parts = explode(':', $periodos_mathimaton);
			$tbl = 'students_' . $parts[0] . "_" . $periodos_parts[0];
			$getRow = $db->executeQuery("SELECT 1 FROM " . $tbl . " LIMIT 1;");
			if ($getRow) {
				$exists = true;
			} else {
				$exists = false;
			}

			if ($exists && validateCsrfToken($_POST['csrf_token'])) {
				$_SESSION['shortcut_tbl'] = 'shortcuts_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['shortcut_lessons_tbl'] = 'shortcut_lessons_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['students_tbl'] = 'students_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['tmimata_tbl'] = 'tmimata_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['students_lessons_tbl'] = 'students_lessons_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['teachers_tmimata_tbl'] = 'teachers_tmimata_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['grade_db_tbl'] = 'grades_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['lessons_tbl'] = 'lessons_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['teacher_lessons_tbl'] = 'teacher_lessons_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['app_lists_tbl'] = 'app_lists_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['full_grades_tbl'] = 'full_grades_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['full_grades_stats_tbl'] = 'full_grades_stats_' . $parts[0] . "_" . $periodos_parts[0];
				$_SESSION['user_tbl'] = 'register';
				$_SESSION['curr_school_year'] = $parts[1];
				$_SESSION['login_time'] = date('H:i:s');
				$_SESSION['periodos_mathimaton'] = $periodos_parts[1];

				if (isAdmin()) {
					redirectTo(WEB_ADMIN_ROOT . '/dashboard.php');
					$_SESSION['app'] = 'admin-app';
				} elseif (isTeacher()) {
					$_SESSION['app'] = 'teacher-app';
					redirectTo(WEB_TEACHER_ROOT . '/dashboard.php');
				}
			} else {
				destroySession();
				redirectTo('204.php');
			}
		} elseif ($flag == 1) {
			$password_err = $username_err = 'Username or password is incorrect';
		} else {
			echo "<span class='text-danger text-bold text-center mx-auto d-block pt-2'>You are banned from that app.</span>";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/header.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Είσοδος Χρήστη</title>
</head>

<body>
	<div class="wrapper">
		<div class="block-center mt-4 wd-xxl">
			<div class="card card-flat">
				<div class="card-header text-center">
					<h2 class="mb-3" style="letter-spacing: 1px;">ΣΥΣΤΗΜΑ ΜΑΘΗΤΟΛΟΓΙΟΥ</h2>
					<a href="#">
						<img class="block-center rounded" src="assets/img/politropo/LOGO_tr.png" style="width:250px; height:auto;" alt="Image">
					</a>
				</div>
				<div class="card-body">
					<p class="text-center py-2 formHeaders">ΣΥΝΔΕΣΗ ΧΡΗΣΤΗ</p>
					<form class="mb-3" id="loginForm" novalidate action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
						<div class="form-group">
							<div class="input-group with-focus <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
								<input class="form-control border-right-0" name="username" value="<?php echo $username; ?>" id="exampleInputEmail1" type="text" placeholder="Enter username" autocomplete="username" required>
								<div class="input-group-append">
									<span class="input-group-text text-muted bg-transparent border-left-0">
										<em class="fas fa-user"></em>
									</span>
								</div>
							</div>
							<span class="help-block <?php echo (!empty($username_err)) ? '' : 'no-error'; ?>"><?php echo $username_err; ?></span>
						</div>
						<div class="form-group">
							<div class="input-group with-focus <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
								<input class="form-control border-right-0" autocomplete="current-password" name="password" id="exampleInputPassword1" type="password" placeholder="Password" required>
								<div class="input-group-append">
									<span class="input-group-text text-muted bg-transparent border-left-0">
										<em class="fa fa-lock"></em>
									</span>
								</div>
							</div>
							<span class="help-block <?php echo (!empty($password_err)) ? '' : 'no-error'; ?>"><?php echo $password_err; ?></span>
						</div>
						<div class="form-group">
							<div class="input-group with-focus">
								<?php echo getLoginSchoolYears(); ?>
								<div class="input-group-append">
									<span class="input-group-text text-muted bg-transparent border-left-0">
										<em class="fas fa-calendar-alt"></em>
									</span>
								</div>
							</div>
							<span class="help-block <?php echo (!empty($sx_year_err)) ? '' : 'no-error'; ?>"><?php echo $sx_year_err; ?></span>
						</div>
						<!-- Periodos -->
						<div class="form-group">
							<div class="input-group with-focus">
								<select name="periodos_login" id="periodos_login" class="custom-select custom-select-md cursor-pointer">
									<option value="">Περίοδος Μαθημάτων</option>
									<option value="ther:Θερινή">Θερινή</option>
									<option value="xeim:Χειμερινή">Χειμερινή</option>
								</select>
								<div class="input-group-append">
									<span class="input-group-text text-muted bg-transparent border-left-0">
										<em class="fas fa-book"></em>
									</span>
								</div>
							</div>
							<span class="help-block <?php echo (!empty($periodos_err)) ? '' : 'no-error'; ?>"><?php echo $periodos_err; ?></span>
						</div>
						<input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
						<button class="btn btn-block btn-primary mt-3" type="submit">Login</button>
					</form>
				</div>
			</div>
			<div class="p-3 text-center">
				<span class="mr-2"><i class="far fa-copyright"></i></span>
				<span><?php echo date('Y'); ?></span>
				<span class="mr-2">-</span>
				<span>Πολύτροπο</span>
				<br>
				<span>Φοντιστήρια Μέσης Εκπαίδευσης</span>
			</div>
		</div>
	</div>
	<?php include('_parts/scripts.php'); ?>
</body>

</html>