<?php
$home_url = "/students";
$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
define('PROJECT_ROOT', $_SERVER['DOCUMENT_ROOT'] . $home_url);
define('WEB_ROOT', $root . $home_url);
define('WEB_ADMIN_ROOT', $root . $home_url . '/admin-app');
define('WEB_TEACHER_ROOT', $root . $home_url . '/teacher-app');

include_once PROJECT_ROOT . "/functions.php";
include_once PROJECT_ROOT . "/db/Database.php";
include_once PROJECT_ROOT . "/db/Query.php";
foreach (glob(PROJECT_ROOT . "/objects/*.php") as $file) {
    include_once $file;
}
if (isset($_SESSION['username'])) {
    $user = new User($_SESSION['username']);
    $user->id = $_SESSION['user_id'];
    $user = $user->getUserById();
}
