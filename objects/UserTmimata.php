<?php

/**
 *
 */
class UserTmimata
{
    //table name
    private static $db_table_name;
    //attribs
    public $id;
    public $tmima_id;
    public $user_id;

    private static function initDbTables()
    {
        if (!empty(self::$db_table_name)) {
            return false;
        }
        self::$db_table_name = $_SESSION['teachers_tmimata_tbl'];
    }

    public function save()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (user_id, tmima_id) 
		VALUES (?, ?)
		";
        $params = [$this->user_id, $this->tmima_id];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getAllEntries()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $UserTmima                 = new UserTmimata;
                $UserTmima->id             = $row['id'];
                $UserTmima->tmima_id     = $row['tmima_id'];
                $UserTmima->user_id     = $row['user_id'];
                array_push($output, $UserTmima);
            }
        }
        return $output;
    }

    public function index()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $tmima  = Tmima::getTmimaById((int)$row['tmima_id']);
                array_push($output, $tmima);
            }
        }
        return $output;
    }

    public function delete()
    {
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function teacherTmimataByTmimaIdExists($tmimaId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_table_name . " 
        WHERE tmima_id = ?
        LIMIT 1;
        ";
        $params = [$tmimaId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }
}
