<?php

/**
 *
 */
class StudentLessons
{

    //table name
    private static $db_table_name;
    //attribs
    public $id;
    public $lesson_id;
    public $student_id;

    private static function initDbTables()
    {
        if (!empty(self::$db_table_name)) {
            return false;
        }
        self::$db_table_name = $_SESSION['students_lessons_tbl'];
    }

    public function save()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (student_id, lesson_id) 
		VALUES (?, ?)
		";
        $params = [$this->student_id, $this->lesson_id];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function index()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE student_id = ?
		";
        $params = [$this->student_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $lesson = Lesson::getLessonById($row['lesson_id']);
                array_push($output, $lesson);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function indexBySxEtos($student_id, $sx_etos)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM students_lessons_" . $sx_etos . " 
		WHERE student_id = ?
		";
        $params = [$student_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $lesson = Lesson::getLessonByIdAndSxEtos($row['lesson_id'], $sx_etos);
                array_push($output, $lesson);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentsByLessonId($lesson_id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
        WHERE lesson_id = ?
		";
        $params = [$lesson_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $student = Student::getStudentById($row['student_id']);
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public function findStudentsByLesson()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE lesson_id = ?
		";
        $params = [$this->lesson_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $student         = new Student;
                $student->id     = $row['student_id'];
                $student         = $student->getStudentNameById();
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public function delete()
    {
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE student_id = ?
		";
        $params = [$this->student_id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }
}
