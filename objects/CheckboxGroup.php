<?php

/**
 * 
 */
class CheckboxGroup
{
	public $cols, $group_input_name, $input_id_prefix, $container_id;

	function __construct()
	{
		# code...
	}
	
	public function createLessonGroup()
	{
		$allLessons = Lesson::getAllLessons();
		$output = '<div class="row"><div class="col-md-12"><label class="pb-3">Επιλογή Μαθημάτων <span class="reqField">(*)</span></label></div><div class="col-md-12" id="'.$this->container_id.'">';
		$colNum = $this->cols;
		$inputName = $this->group_input_name;
		$idPrefix = $this->input_id_prefix;
		foreach ($allLessons as $tmpLesson) {
			$output .= '
			<div class="col-lg-' . (intdiv(12, $colNum)) . ' mb-3" style="float:left;">
		<div class="checkbox c-checkbox">
		<label class="lessonModalLbl">
		<input type="checkbox" class="dropdown-entry" name="' . $inputName . '" value="' .  $tmpLesson->id . '" id="' .  $idPrefix . $tmpLesson->id . '">
		<span class="fa fa-check"></span>' . $tmpLesson->name . '
		</label>
		</div>
		</div>
			';
		}
		$output .= '</div></div>';
		return $output;
	}

	public function createTmimataGroup()
	{
		$tmima = new Tmima;
		$allTmimata = $tmima->getAllTmima();
		$output = '<div class="row"><div class="col-md-12"><label class="pb-3">Επιλογή Τμημάτων <span class="reqField">(*)</span></label></div><div class="col-md-12" id="'.$this->container_id.'">';
		$colNum = $this->cols;
		$inputName = $this->group_input_name;
		$idPrefix = $this->input_id_prefix;
		foreach ($allTmimata as $tmpTmima) {
			$output .= '
			<div class="col-lg-' . (intdiv(12, $colNum)) . ' mb-3" style="float:left;">
		<div class="checkbox c-checkbox">
		<label class="lessonModalLbl">
		<input type="checkbox" class="dropdown-entry" name="' . $inputName . '" value="' .  $tmpTmima->id . '" id="' .  $idPrefix . $tmpTmima->id . '">
		<span class="fa fa-check"></span>' . $tmpTmima->name . '
		</label>
		</div>
		</div>
			';
		}
		$output .= '</div></div>';
		return $output;
	}

	public function createLessonWithShortcutsGroup($shortcutBtnsGrp)
	{
		$allLessons = Lesson::getAllLessons();
		if(!empty($shortcutBtnsGrp))
			$output = '<div class="row">'.$shortcutBtnsGrp.'<div class="col-md-12" id="'.$this->container_id.'">';
		else
			$output = '<div class="row"><div class="col-md-12" id="'.$this->container_id.'">';			
		$colNum = $this->cols;
		$idPrefix = $this->input_id_prefix;
		foreach ($allLessons as $tmpLesson) {
			$output .= '
			<div class="col-lg-' . (intdiv(12, $colNum)) . ' mb-3" style="float:left;">
		<div class="checkbox c-checkbox">
		<label class="lessonModalLbl">
		<input type="checkbox" class="dropdown-entry" name="' . $tmpLesson->name . '" value="' .  $tmpLesson->id . '" id="' .  $idPrefix . $tmpLesson->id . '">
		<span class="fa fa-check"></span>' . $tmpLesson->name . '
		</label>
		</div>
		</div>
			';
		}
		$output .= '</div></div>';
		return $output;
	}
}
