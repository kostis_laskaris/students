<?php

class Error_Log
{
	public $date;
	public $user;
	public $page;
	public $kind;
	public $sql_state = '';
	public $error_code = '';
	public $error_message;
	
	function __construct()
	{
		date_default_timezone_set('Europe/Athens');
		$this->date = date("Y-m-d H:i:s");
	}

	public function writeLog($errInfo){
		global $db;
		$logFile = PROJECT_ROOT . '/logs/error_logs/errlogs.json';
		$this->sql_state		= $errInfo[0];
		$this->error_code		= $errInfo[1];
		$this->error_message 	= $errInfo[2];
		$this->user 			= $_SESSION['username'];

		$current_data = file_get_contents($logFile);  
		$array_data = json_decode($current_data, true);
		$array_data[] = $this;  
		$final_data = json_encode($array_data, JSON_UNESCAPED_UNICODE);  
		file_put_contents($logFile, $final_data); 
	}

	public function getLogs(){
		$logFile = PROJECT_ROOT . '/logs/error_logs/errlogs.json';
		$string = file_get_contents($logFile);
		if($string === '')
			return 'The log file is currently empty!';
		$json_a = json_decode($string, true);
		if ($json_a === null) {
    		return 'Error occured while JSON decoding the log file. Contact your system administrator.';
		}
		$output = array();
		foreach ($json_a as $obj) {
			array_push($output, $obj);
		}
		return $output;
	}
}
?>