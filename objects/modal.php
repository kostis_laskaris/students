<?php
/**
 * 
 */
class Modal
{
	public $modalId, $modalSize, $modalBgHeaderColor, $modalHeader, $modalBody, $modalFooter, $animoId;
	public $modalStyle = '';
	
	function __construct($args)
	{
		if(!isset($args['modalStyle'])){
			$args['modalStyle'] = '';
		}
		$this->modalId 				= $args['modalId'];
		$this->modalSize 			= $args['modalSize'];
		$this->modalStyle 			= $args['modalStyle'];
		$this->modalBgHeaderColor 	= $args['modalBgHeaderColor'];
		$this->modalHeader 			= $args['modalHeader'];
		$this->modalBody 			= $args['modalBody'];
		$this->modalFooter 			= $args['modalFooter'];
		$this->animoId 				= $args['animoId'];
	}

	public function createModal(){
		return '<div class="modal fade" style="' . $this->modalStyle . '" id="'.$this->modalId.'" tabindex="-1" role="dialog" aria-hidden="true">
	      <div class="modal-dialog '.$this->modalSize.'">
	         <div class="modal-content" id="'.$this->animoId.'">
	            <div class="modal-header '.$this->modalBgHeaderColor.'">
	               '.$this->modalHeader.'
	               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	            '.$this->modalBody.'
	            </div>
	            <div class="modal-footer bg-light">
	               '.$this->modalFooter.'
	               <button class="btn btn-danger btnw-3" type="button" data-dismiss="modal">Close</button>
	            </div>
	         </div>
	      </div>
	   </div>';
	}

	public function createModalForm($formId, $formAction, $formMethod){
		return '<div class="modal fade" id="'.$this->modalId.'" tabindex="-1" role="dialog" aria-hidden="true">
	      <div class="modal-dialog '.$this->modalSize.'">
	         <div class="modal-content" id="'.$this->animoId.'">
	         <form method="'.$formMethod.'" action="'.$formAction.'" id="'.$formId.'">
	            <div class="modal-header '.$this->modalBgHeaderColor.'">
	               '.$this->modalHeader.'
	               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span>
	               </button>
	            </div>
	            <div class="modal-body">
	            '.$this->modalBody.'
	            </div>
	            <div class="modal-footer bg-light">
	               '.$this->modalFooter.'
	               <button class="btn btn-danger btnw-3" type="button" data-dismiss="modal">Close</button>
	            </div>
	            </form>
	         </div>
	      </div>
	   </div>';
	}
}
