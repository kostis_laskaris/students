<?php

/**
 *
 */
class Shortcut
{
    //table name
    private static $db_table_name;
    //attribs
    public $id;
    public $name;
    public $lessons;
    public $created_at;
    public $updated_at;

    private static function initDbTables()
    {
        if (!empty(self::$db_table_name)) {
            return false;
        }
        self::$db_table_name = $_SESSION['shortcut_tbl'];
    }

    public static function getShortcutById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $shortcutOut                    = new Shortcut;
            $shortcutLesson                 = new ShortcutLessons;
            $shortcutLesson->shortcut_id    = $getRow['id'];
            $shortcutOut->id                = $getRow['id'];
            $shortcutOut->name              = $getRow['name'];
            $shortcutOut->lessons           = $shortcutLesson->index();
            $shortcutOut->created_at        = $getRow['created_at'];
            $shortcutOut->updated_at        = $getRow['updated_at'];
            return $shortcutOut;
        } else {
            return null;
        }
    }

    public static function getAllShortcuts()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		ORDER BY name ASC
		";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $shortcut                         = new Shortcut;
                $shortcutLesson                   = new ShortcutLessons;
                $shortcutLesson->shortcut_id      = $row['id'];
                $shortcut->lessons                = $shortcutLesson->index();
                $shortcut->lessonsAsString        = $shortcut->getLessonsAsString($shortcut->lessons);
                $shortcut->id                     = $row['id'];
                $shortcut->name                   = $row['name'];
                $shortcut->created_at             = $row['created_at'];
                $shortcut->updated_at             = $row['updated_at'];
                array_push($output, $shortcut);
            }
        }
        return $output;
    }

    public function updateShortcut()
    {
        self::initDbTables();
        global $db;
        $query = "
		UPDATE " . self::$db_table_name . " 
		SET name=?, updated_at=? 
		WHERE id = ?
		";
        $params = [$this->name, $this->updated_at, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            $shortcutId                  = $this->id;
            $allLessons                  = $this->lessons;
            $shortcutLesson              = new ShortcutLessons;
            $shortcutLesson->shortcut_id = $shortcutId;
            $deleted                      = $shortcutLesson->delete();
            if ($deleted == 0) {
                foreach ($allLessons as $lesson) {
                    $shortcutLesson              = new ShortcutLessons;
                    $shortcutLesson->lesson_id      = $lesson;
                    $shortcutLesson->shortcut_id = $shortcutId;
                    $shortcutLesson->save();
                }
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    public function addShortcut()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (name, created_at, updated_at) 
		VALUES (?, ?, ?)
		";
        $params = [$this->name, $this->created_at, $this->updated_at];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            $latestShortcutId = $db->lastInsertedId();
            foreach ($this->lessons as $lesson) {
                $shortcutLesson              = new ShortcutLessons;
                $shortcutLesson->lesson_id      = $lesson;
                $shortcutLesson->shortcut_id = $latestShortcutId;
                $shortcutLesson->save();
            }
            return 0;
        } else {
            return 1;
        }
    }

    public static function deleteShortcut($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }

    //------------------------ HELPERS ------------------------//

    public static function getShortcutsAsButtonGroup()
    {
        $allShortcut = self::getAllShortcuts();
        $shortcutBtnsGrp = '<div class="col-md-12 text-center pb-4">';
        foreach ($allShortcut as $tmpShortcut) {
            $shortcutBtnsGrp .= '<button class="btn btn-purple btn-sm mr-1 my-1 shortcutBtn" onclick="selectCheckbox(' . "'" . $tmpShortcut->getShortcutLessonsForView()['ids'] . "'" . ')"><i class="fas fa-tag mr-1"></i>' . $tmpShortcut->name . '</button>';
        }
        $shortcutBtnsGrp .= '</div>';
        return $shortcutBtnsGrp;
    }

    public function getShortcutLessonsForView()
    {
        $les = $ids = '';
        $output = array();
        foreach ($this->lessons as $lesson) {
            $les .= $lesson->name . ", ";
            $ids .= $lesson->id . ",";
        }
        $output['names'] = substr($les, 0, -2);
        $output['ids'] = substr($ids, 0, -1);
        return $output;
    }

    private function getLessonsAsString($lessons)
    {
        return implode(', ', array_map(function ($lesson) {
            return $lesson->name;
        }, $lessons));
    }
}
