<?php
class Grade
{
    //table name
    private static $db_table_name;
    private static $db_view_table;
    private static $db_grade_stats_view_table;
    private static $teachers_tmimata_tbl;
    private static $teacher_lessons_tbl;
    //attribs
    public $id;
    public $lesson_id;
    public $user_id;
    public $student_id;
    public $class;
    public $tmima;
    public $exam_date;
    public $exam_kind;
    public $absence;
    public $grade;
    public $comments;
    public $fullLesson;
    public $fullStudent;
    public $fullUser;
    public $fullTmima;
    public $created_at;
    public $updated_at;
    //view attribs
    public $teacherFirstname;
    public $teacherLastname;
    public $tmimaName;
    public $lessonName;
    public $studentFirstname;
    public $studentLastname;

    private static function initDbTables()
    {
        self::$db_table_name             = $_SESSION['grade_db_tbl'];
        self::$db_view_table             = $_SESSION['full_grades_tbl'];
        self::$db_grade_stats_view_table = $_SESSION['full_grades_stats_tbl'];
        self::$teachers_tmimata_tbl      = $_SESSION['teachers_tmimata_tbl'];
        self::$teacher_lessons_tbl       = $_SESSION['teacher_lessons_tbl'];
    }

    public function addGrade()
    {
        self::initDbTables();
        global $db;
        $query = "
        INSERT INTO " . self::$db_table_name . " (user_id, student_id, taksi, tmima, lesson_id, exam_date, exam_kind, absence, grade, comments, created_at, updated_at)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ";
        $grade = 0;
        if ($this->grade)
            $grade = $this->grade;
        $params = [
            $this->user_id,
            $this->student_id,
            $this->class,
            $this->tmima,
            $this->lesson_id,
            $this->exam_date,
            $this->exam_kind,
            $this->absence,
            $grade,
            $this->comments,
            $this->created_at,
            $this->updated_at
        ];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function updateGrade()
    {
        self::initDbTables();
        global $db;
        $query = "
		UPDATE " . self::$db_table_name . " 
		SET lesson_id=?, exam_date=?, exam_kind=?, absence=?, grade=?, comments=?, updated_at=? 
		WHERE id = ?;
		";
        $params = [
            $this->lesson_id,
            $this->exam_date,
            $this->exam_kind,
            $this->absence,
            $this->grade,
            $this->comments,
            $this->updated_at,
            $this->id
        ];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function deleteGrade($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function getGradeById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $grade              = new Grade();
            $grade->id          = $getRow['id'];
            $grade->user_id     = $getRow['user_id'];
            $grade->lesson_id   = $getRow['lesson_id'];
            $grade->fullLesson  = self::getGradeLesson($getRow['lesson_id']);
            $grade->student_id  = $getRow['student_id'];
            $grade->fullStudent = self::getGradeStudent($getRow['student_id']);
            $grade->class       = $getRow['taksi'];
            $grade->tmima       = $getRow['tmima'];
            $grade->exam_date   = $getRow['exam_date'];
            $grade->exam_kind   = $getRow['exam_kind'];
            $grade->grade       = $getRow['grade'];
            $grade->created_at  = $getRow['created_at'];
            $grade->updated_at  = $getRow['updated_at'];
            $grade->absence     = $getRow['absence'];
            $grade->comments    = $getRow['comments'];
            $grade->fullTmima   = self::getGradeTmima($getRow['tmima']);
            return $grade;
        } else {
            return null;
        }
    }

    public static function getAllGradesByStudentId($the_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT *
        FROM " . self::$db_table_name . " 
        WHERE student_id = ?
        ORDER BY exam_date DESC
        ";
        $params = [$the_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $grade              = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->fullUser    = self::getGradeUser($row['user_id']);
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGradesByLessonId($lesson_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT *
        FROM " . self::$db_table_name . " 
        WHERE lesson_id = ?
        ORDER BY exam_date DESC
        ";
        $params = [$lesson_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $grade              = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->fullUser    = self::getGradeUser($row['user_id']);
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return false;
        }
    }

    public static function getLastNGradesByLoggedUserId($num)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_table_name . " 
        WHERE user_id = ?
        ORDER BY exam_date DESC
        LIMIT $num
        ";
        $params = [$_SESSION['user_id']];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getLastNGradesByLoggedUserIdAndExamKind($num, $exam_kind)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_table_name . " 
        WHERE user_id = ?
        AND exam_kind = ?
        ORDER BY exam_date DESC
        LIMIT $num
        ";
        $params = [$_SESSION['user_id'], $exam_kind];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGradesByUserId($user_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_table_name . " 
        WHERE user_id = ?
        ORDER BY exam_date DESC
        ";
        $params = [$user_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }
    public static function getAllGradesByUserIdAndStudentId($user_id, $student_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_table_name . " 
        WHERE user_id = ?
        AND student_id = ?
        ORDER BY exam_date DESC
        ";
        $params = [$user_id, $student_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGrades()
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_view_table . " 
        ORDER BY exam_date DESC
        ";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->teacherFirstname    = $row['teacherFirstname'];
                $grade->teacherLastname     = $row['teacherLastname'];
                $grade->tmimaName           = $row['tmimaName'];
                $grade->lessonName          = $row['lessonName'];
                $grade->studentFirstname    = $row['studentFirstname'];
                $grade->studentLastname     = $row['studentLastname'];
                $grade->id                  = $row['id'];
                $grade->user_id             = $row['user_id'];
                $grade->lesson_id           = $row['lesson_id'];
                $grade->student_id          = $row['student_id'];
                $grade->class               = $row['taksi'];
                $grade->tmima               = $row['tmima'];
                $grade->exam_date           = $row['exam_date'];
                $grade->exam_kind           = $row['exam_kind'];
                $grade->grade               = $row['grade'];
                $grade->created_at          = $row['created_at'];
                $grade->updated_at          = $row['updated_at'];
                $grade->absence             = $row['absence'];
                $grade->comments            = $row['comments'];
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGradesStatsByUser()
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT COUNT(id) countGrades, user_id, lesson_id, ANY_VALUE(teacherLastname) as teacherLastname, ANY_VALUE(teacherFirstname) as teacherFirstname, lessonName, ROUND(AVG(grade), 1) as gradeAvg, MAX(created_at) as latestDate 
        FROM " . self::$db_view_table . "  
        GROUP BY user_id, lesson_id
        ORDER BY teacherLastname ASC
        ";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $stat                   = new stdClass();
                $stat->countGrades      = $row['countGrades'];
                $stat->user_id          = $row['user_id'];
                $stat->lesson_id        = $row['lesson_id'];
                $stat->teacherLastname  = $row['teacherLastname'];
                $stat->teacherFirstname = $row['teacherFirstname'];
                $stat->lessonName       = $row['lessonName'];
                $stat->gradeAvg         = $row['gradeAvg'];
                $stat->latestDate       = $row['latestDate'];
                array_push($output, $stat);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getGradesCountByUser()
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT COUNT(id) countGrades, user_id as userId, ANY_VALUE(teacherLastname) as teacherLastname, ANY_VALUE(teacherFirstname) as teacherFirstname, 
        (SELECT ROUND(AVG(grade), 1) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND user_id = userId ) as gradeAvg  
        FROM " . self::$db_view_table . " 
        GROUP BY user_id 
        ORDER BY teacherLastname ASC
        ";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $stat                   = new stdClass();
                $stat->countGrades      = $row['countGrades'];
                $stat->gradeAvg         = $row['gradeAvg'];
                $stat->user_id          = $row['userId'];
                $stat->teacherLastname  = $row['teacherLastname'];
                $stat->teacherFirstname = $row['teacherFirstname'];
                array_push($output, $stat);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGradesAvgPerDateByUserAndLessonAndKind($lesson_id, $kind)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT ROUND(AVG(grade), 1) as gradeAvg, exam_date, COUNT(id) as gradeCount, lessonName, lesson_id 
        FROM " . self::$db_view_table . " 
        WHERE lesson_id = ? 
        AND user_id = ? 
        AND exam_kind = ? 
        AND absence = 'ΠΑΡΩΝ' 
        GROUP BY exam_date 
        ORDER BY exam_date ASC
        ";
        $params = [$lesson_id, $_SESSION['user_id'], $kind];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $stat                = new stdClass();
                $stat->gradeAvg      = $row['gradeAvg'];
                $stat->exam_date     = $row['exam_date'];
                $stat->gradeCount    = $row['gradeCount'];
                $stat->lessonName    = $row['lessonName'];
                $stat->lesson_id     = $row['lesson_id'];
                array_push($output, $stat);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getGradeStatsPerStudentByCurrentUserIdAndLessonId($lesson_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT " . self::$db_view_table . ".lessonName, 
        " . self::$db_view_table . ".student_id as studentId, 
        " . self::$db_view_table . ".lesson_id as lessonId, 
        (SELECT ROUND(AVG(grade), 1) FROM " . self::$db_view_table . " WHERE student_id = studentId AND absence = 'ΠΑΡΩΝ' AND lesson_id = lessonId) as avgGrade, 
        " . self::$db_view_table . ".studentLastname, 
        " . self::$db_view_table . ".studentFirstname, 
        COUNT(" . self::$db_view_table . ".id) as countGrade, 
        max(" . self::$db_view_table . ".grade) as maxGrade, 
        (SELECT min(" . self::$db_view_table . ".grade) FROM " . self::$db_view_table . " WHERE student_id = studentId AND absence = 'ΠΑΡΩΝ' AND lesson_id = lessonId) as minGrade,
        (SELECT COUNT(id) FROM " . self::$db_view_table . " WHERE absence = 'ΑΠΩΝ' AND student_id = studentId AND lesson_id = lessonId) as countAbsence 
        FROM " . self::$db_view_table . "
        LEFT JOIN " . self::$teacher_lessons_tbl . " ON " . self::$db_view_table . ".lesson_id = " . self::$teacher_lessons_tbl . ".lesson_id 
        WHERE " . self::$db_view_table . ".user_id = ? 
        AND " . self::$teacher_lessons_tbl . ".user_id = ? 
        AND " . self::$teacher_lessons_tbl . ".lesson_id = ? 
        GROUP BY " . self::$db_view_table . ".student_id, " . self::$db_view_table . ".lesson_id 
        ORDER BY " . self::$db_view_table . ".lesson_id
        ";
        $params = [$_SESSION['user_id'], $_SESSION['user_id'], $lesson_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $stat                   = new stdClass();
                $stat->lessonName       = $row['lessonName'];
                $stat->avgGrade         = $row['avgGrade'];
                $stat->studentLastname  = $row['studentLastname'];
                $stat->studentFirstname = $row['studentFirstname'];
                $stat->countGrade       = $row['countGrade'];
                $stat->maxGrade         = $row['maxGrade'];
                $stat->minGrade         = $row['minGrade'];
                $stat->countAbsence     = $row['countAbsence'];
                $stat->studentId       = $row['studentId'];
                array_push($output, $stat);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getLastNGrades($n)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT * 
        FROM " . self::$db_view_table . " 
        ORDER BY exam_date DESC 
        LIMIT " . $n . "
        ";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->teacherFirstname    = $row['teacherFirstname'];
                $grade->teacherLastname     = $row['teacherLastname'];
                $grade->tmimaName           = $row['tmimaName'];
                $grade->lessonName          = $row['lessonName'];
                $grade->studentFirstname    = $row['studentFirstname'];
                $grade->studentLastname     = $row['studentLastname'];
                $grade->id                  = $row['id'];
                $grade->user_id             = $row['user_id'];
                $grade->lesson_id           = $row['lesson_id'];
                $grade->student_id          = $row['student_id'];
                $grade->class               = $row['taksi'];
                $grade->tmima               = $row['tmima'];
                $grade->exam_date           = $row['exam_date'];
                $grade->exam_kind           = $row['exam_kind'];
                $grade->grade               = $row['grade'];
                $grade->created_at          = $row['created_at'];
                $grade->updated_at          = $row['updated_at'];
                $grade->absence             = $row['absence'];
                $grade->comments            = $row['comments'];
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllGradeStats()
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT " . self::$db_grade_stats_view_table . ".*, (SELECT COUNT(absence) FROM " . self::$db_view_table . " WHERE absence = 'ΑΠΩΝ' AND " . self::$db_view_table . ".lesson_id = " . self::$db_grade_stats_view_table . ".lesson_id) as apon
        FROM " . self::$db_grade_stats_view_table . ", " . self::$db_view_table . " 
        GROUP BY lessonName
        ";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut               = new stdClass();
                $statsOut->lessonName   = $row['lessonName'];
                $statsOut->lesson_id    = $row['lesson_id'];
                $statsOut->countGrades  = $row['countGrades'];
                $statsOut->avgGrades    = $row['avgGrades'];
                $statsOut->maxGrade     = $row['maxGrade'];
                $statsOut->minGrade     = $row['minGrade'];
                $statsOut->countApon    = $row['apon'];
                $statsOut->maxCreatedAt = $row['maxCreatedAt'];
                $statsOut->minCreatedAt = $row['minCreatedAt'];
                $statsOut->maxUpdatedAt = $row['maxUpdatedAt'];
                $statsOut->minUpdatedAt = $row['minUpdatedAt'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public function getStatsPerStudentByUserId()
    {
        self::initDbTables();
        global $db;
        $absence = "ΠΑΡΩΝ";
        $query = "
        SELECT student_id as studentId, AVG(grade) as gradeAverage, MIN(grade) as gradeMin, MAX(grade) as gradeMax, COUNT(grade) as gradeCount, (SELECT COUNT(grade) FROM grades_19_20 WHERE absence='ΑΠΩΝ' AND studentId = student_id AND user_id=?) as absence
        FROM " . self::$db_table_name . " 
        WHERE absence = ?
        AND user_id = ?
        GROUP BY student_id
        ";
        $params = [$this->user_id, $absence, $this->user_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut = new stdClass();
                $statsOut->student_id       = $row['studentId'];
                $statsOut->gradeAverage     = $row['gradeAverage'];
                $statsOut->gradeMin         = $row['gradeMin'];
                $statsOut->gradeMax         = $row['gradeMax'];
                $statsOut->gradeCount       = $row['gradeCount'];
                $statsOut->absence          = $row['absence'];
                $statsOut->student          = self::getGradeStudent($row['studentId']);
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentStatsPerLessonId($student_id)
    {
        self::initDbTables();
        global $db;
        $absence = ["ΠΑΡΩΝ", "ΑΠΩΝ"];
        $kind = ["TEST", "ΔΙΑΓΩΝΙΣΜΑ", "ΕΡΓΑΣΙΑ"];
        $query = "
        SELECT AVG(grade) as gradeAvg, COUNT(grade) as gradeCount, MIN(grade) as minGrade, MAX(grade) as maxGrade, lessonName, lesson_id as lesId, MAX(exam_date) as latestExamDate,
        (SELECT COUNT(grade) FROM " . self::$db_view_table . " WHERE student_id = ? AND lesson_id = lesId AND absence = ?) as absence,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[0] . "') as testAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[1] . "') as examAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[2] . "') as ergasiaAvg
        FROM " . self::$db_view_table . " 
        WHERE student_id = ? 
        AND absence = ? 
        GROUP BY lesson_id
        ";
        $params = [$student_id, $absence[1], $student_id, $absence[0]];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut = new stdClass();
                $statsOut->lesson_id        = $row['lesId'];
                $statsOut->lessonName       = $row['lessonName'];
                $statsOut->gradeAvg         = round($row['gradeAvg']);
                $statsOut->gradeCount       = $row['gradeCount'];
                $statsOut->minGrade         = $row['minGrade'];
                $statsOut->maxGrade         = $row['maxGrade'];
                $statsOut->latestExamDate   = $row['latestExamDate'];
                $statsOut->absence          = $row['absence'];
                $statsOut->testAvg          = $row['testAvg'];
                $statsOut->examAvg          = $row['examAvg'];
                $statsOut->ergasiaAvg       = $row['ergasiaAvg'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentStatsPerLessonIdForCurrentUser($student_id)
    {
        self::initDbTables();
        global $db;
        $absence = ["ΠΑΡΩΝ", "ΑΠΩΝ"];
        $kind = ["TEST", "ΔΙΑΓΩΝΙΣΜΑ", "ΕΡΓΑΣΙΑ"];
        $query = "
        SELECT AVG(grade) as gradeAvg, COUNT(grade) as gradeCount, MIN(grade) as minGrade, MAX(grade) as maxGrade, lessonName, " . self::$db_view_table . ".lesson_id as lesId, MAX(exam_date) as latestExamDate,
        (SELECT COUNT(grade) FROM " . self::$db_view_table . " WHERE student_id = ? AND lesson_id = lesId AND absence = ?) as absence,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[0] . "') as testAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[1] . "') as examAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[2] . "') as ergasiaAvg
        FROM " . self::$db_view_table . " 
        LEFT JOIN " . self::$teacher_lessons_tbl . " ON " . self::$teacher_lessons_tbl . ".lesson_id = " . self::$db_view_table . ".lesson_id
        WHERE " . self::$db_view_table . ".student_id = ? 
        AND " . self::$db_view_table . ".absence = ? 
        AND " . self::$teacher_lessons_tbl . ".user_id = ?
        AND " . self::$db_view_table . ".user_id = ? 
        GROUP BY " . self::$db_view_table . ".lesson_id
        ";
        $params = [$student_id, $absence[1], $student_id, $absence[0], $_SESSION['user_id'], $_SESSION['user_id']];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut = new stdClass();
                $statsOut->lesson_id        = $row['lesId'];
                $statsOut->lessonName       = $row['lessonName'];
                $statsOut->gradeAvg         = round($row['gradeAvg']);
                $statsOut->gradeCount       = $row['gradeCount'];
                $statsOut->minGrade         = $row['minGrade'];
                $statsOut->maxGrade         = $row['maxGrade'];
                $statsOut->latestExamDate   = $row['latestExamDate'];
                $statsOut->absence          = $row['absence'];
                $statsOut->testAvg          = $row['testAvg'];
                $statsOut->examAvg          = $row['examAvg'];
                $statsOut->ergasiaAvg       = $row['ergasiaAvg'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentStatsByLessonIdForCurrentUser($student_id, $lesson_id)
    {
        self::initDbTables();
        global $db;
        $absence = ["ΠΑΡΩΝ", "ΑΠΩΝ"];
        $kind = ["TEST", "ΔΙΑΓΩΝΙΣΜΑ", "ΕΡΓΑΣΙΑ"];
        $query = "
        SELECT COUNT(" . self::$db_view_table . ".id) as gradeCount, MIN(grade) as minGrade, MAX(grade) as maxGrade, lessonName, " . self::$db_view_table . ".lesson_id as lesId, MAX(exam_date) as latestExamDate,
        (SELECT COUNT(grade) FROM " . self::$db_view_table . " WHERE student_id = ? AND lesson_id = lesId AND absence = ?) as absence,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[0] . "') as testAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[1] . "') as examAvg,
        (SELECT AVG(grade) FROM " . self::$db_view_table . " WHERE absence = 'ΠΑΡΩΝ' AND student_id = " . $student_id . " AND lesson_id = lesId AND exam_kind = '" . $kind[2] . "') as ergasiaAvg,
        (SELECT ROUND(AVG(grade), 1) FROM " . self::$db_view_table . " WHERE student_id = " . $student_id . " AND lesson_id = lesId) as gradeAvg
        FROM " . self::$db_view_table . " 
        LEFT JOIN " . self::$teacher_lessons_tbl . " ON " . self::$teacher_lessons_tbl . ".lesson_id = " . self::$db_view_table . ".lesson_id
        WHERE " . self::$db_view_table . ".student_id = ? 
        AND " . self::$teacher_lessons_tbl . ".user_id = ?
        AND " . self::$db_view_table . ".user_id = ? 
        AND " . self::$db_view_table . ".lesson_id = ?
        ";
        $params = [$student_id, $absence[1], $student_id, $_SESSION['user_id'], $_SESSION['user_id'], $lesson_id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $statsOut = new stdClass();
            $statsOut->lesson_id        = $getRow['lesId'];
            $statsOut->lessonName       = $getRow['lessonName'];
            $statsOut->gradeAvg         = $getRow['gradeAvg'];
            $statsOut->gradeCount       = $getRow['gradeCount'];
            $statsOut->minGrade         = $getRow['minGrade'];
            $statsOut->maxGrade         = $getRow['maxGrade'];
            $statsOut->latestExamDate   = $getRow['latestExamDate'];
            $statsOut->absence          = $getRow['absence'];
            $statsOut->testAvg          = $getRow['testAvg'];
            $statsOut->examAvg          = $getRow['examAvg'];
            $statsOut->ergasiaAvg       = $getRow['ergasiaAvg'];
            return $statsOut;
        } else {
            return 1;
        }
    }

    public static function getGradesPerStudentAndLesson($student_id, $lesson_id)
    {
        self::initDbTables();
        global $db;
        $absence = "ΠΑΡΩΝ";
        $query = "
        SELECT grade, exam_date
        FROM " . self::$db_view_table . " 
        WHERE student_id = ? 
        AND lesson_id = ? 
        AND absence = ?
        ";
        $params = [$student_id, $lesson_id, $absence];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut = [$row['exam_date'], $row['grade']];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getGradesByAppListId($appListId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT *
        FROM " . self::$db_view_table . " 
        WHERE taksi = ?
        ";
        $params = [$appListId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->teacherFirstname    = $row['teacherFirstname'];
                $grade->teacherLastname     = $row['teacherLastname'];
                $grade->tmimaName           = $row['tmimaName'];
                $grade->lessonName          = $row['lessonName'];
                $grade->studentFirstname    = $row['studentFirstname'];
                $grade->studentLastname     = $row['studentLastname'];
                $grade->id                  = $row['id'];
                $grade->user_id             = $row['user_id'];
                $grade->lesson_id           = $row['lesson_id'];
                $grade->student_id          = $row['student_id'];
                $grade->class               = $row['taksi'];
                $grade->tmima               = $row['tmima'];
                $grade->exam_date           = $row['exam_date'];
                $grade->exam_kind           = $row['exam_kind'];
                $grade->grade               = $row['grade'];
                $grade->created_at          = $row['created_at'];
                $grade->updated_at          = $row['updated_at'];
                $grade->absence             = $row['absence'];
                $grade->comments            = $row['comments'];
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getGradeStatsPerMonthPerUserId($user_id)
    {
        self::initDbTables();
        global $db;
        $absence = 'ΠΑΡΩΝ';
        $query = "
        SELECT MONTH(ANY_VALUE(exam_date)) as theMonth, COUNT(id) as countGrades, MAX(grade) as maxGrade,
        (SELECT ROUND(AVG(grade), 1) FROM " . self::$db_view_table . " WHERE user_id = ".$user_id." AND absence = 'ΠΑΡΩΝ' AND MONTH(ANY_VALUE(exam_date)) = theMonth GROUP BY MONTH(ANY_VALUE(exam_date))) as gradeAvg,
        (SELECT MIN(grade) FROM " . self::$db_view_table . " WHERE user_id = ".$user_id." AND absence = 'ΠΑΡΩΝ' AND MONTH(ANY_VALUE(exam_date)) = theMonth GROUP BY MONTH(ANY_VALUE(exam_date))) as minGrade 
        FROM " . self::$db_view_table . " 
        WHERE user_id = ? 
        GROUP BY MONTH(ANY_VALUE(exam_date)) 
        ORDER BY ANY_VALUE(exam_date) ASC
        ";
        $params = [$user_id];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $stats              = new stdClass();
                $stats->gradeAvg    = $row['gradeAvg'];
                $stats->theMonth    = $row['theMonth'];
                $stats->maxGrade    = $row['maxGrade'];
                $stats->minGrade    = $row['minGrade'];
                $stats->countGrades = $row['countGrades'];
                array_push($output, $stats);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function gradeByAppListIdExists($appListId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_view_table . " 
        WHERE taksi = ?
        LIMIT 1;
        ";
        $params = [$appListId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    public static function gradeByTmimaIdExists($tmimaId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_view_table . " 
        WHERE tmima = ?
        LIMIT 1;
        ";
        $params = [$tmimaId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    public static function gradeByStudentIdExists($studentId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_view_table . " 
        WHERE student_id = ?
        LIMIT 1;
        ";
        $params = [$studentId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getGradesCount()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(id) as gradesCount 
		FROM " . self::$db_table_name . "
		";
        $getRow = $db->getRow($query);
        if ($getRow) {
            return $getRow['gradesCount'];
        } else {
            return null;
        }
    }

    public static function getGradesCountOfCurrentUser()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(id) as gradesCount 
		FROM " . self::$db_table_name . " 
        WHERE user_id = ?
		";
        $params = [$_SESSION['user_id']];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return $getRow['gradesCount'];
        } else {
            return null;
        }
    }

    public static function getGradeLesson($lesson_id)
    {
        $lesson = Lesson::getLessonById($lesson_id);
        return $lesson;
    }

    public static function getGradeStudent($student_id)
    {
        $student     = Student::getStudentNameById($student_id);
        return $student;
    }

    public static function getGradeTmima($tmima_id)
    {
        $tmima     = new Tmima;
        $tmima->id = $tmima_id;
        $tmima     = $tmima->getTmimaById($tmima_id);
        return $tmima;
    }

    public static function getGradeUser($user_id)
    {
        $user     = new User();
        $user->id = $user_id;
        $user     = $user->getUserById();
        return $user;
    }

    public static function query($sql)
    {
        global $db;
        $getRows = $db->getRows($sql);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $grade = new Grade();
                $grade->teacherFirstname    = $row['teacherFirstname'];
                $grade->teacherLastname     = $row['teacherLastname'];
                $grade->tmimaName           = $row['tmimaName'];
                $grade->lessonName          = $row['lessonName'];
                $grade->studentFirstname    = $row['studentFirstname'];
                $grade->studentLastname     = $row['studentLastname'];
                $grade->id          = $row['id'];
                $grade->user_id     = $row['user_id'];
                $grade->fullUser    = self::getGradeUser($row['user_id']);
                $grade->lesson_id   = $row['lesson_id'];
                $grade->fullLesson  = self::getGradeLesson($row['lesson_id']);
                $grade->student_id  = $row['student_id'];
                $grade->fullStudent = self::getGradeStudent($row['student_id']);
                $grade->class       = $row['taksi'];
                $grade->tmima       = $row['tmima'];
                $grade->exam_date   = $row['exam_date'];
                $grade->exam_kind   = $row['exam_kind'];
                $grade->grade       = $row['grade'];
                $grade->created_at  = $row['created_at'];
                $grade->updated_at  = $row['updated_at'];
                $grade->absence     = $row['absence'];
                $grade->comments    = $row['comments'];
                $grade->fullTmima   = self::getGradeTmima($row['tmima']);
                array_push($output, $grade);
            }
            return $output;
        } else {
            return 1;
        }
    }
}
