<?php

/**
 *
 */
class Tmima
{
    //table name
    private static $db_table_name;
    private static $teachers_tmimata_tbl;
    //attribs
    public $id;
    public $name;
    public $class;
    public $comments;
    public $created_ad;
    public $updated_ad;

    private static function initDbTables()
    {
        self::$db_table_name         = $_SESSION['tmimata_tbl'];
        self::$teachers_tmimata_tbl  = $_SESSION['teachers_tmimata_tbl'];
    }

    public static function getTmimaById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $tmimaOut               = new Tmima;
            $tmimaOut->id           = $getRow['id'];
            $tmimaOut->name         = $getRow['name'];
            $tmimaOut->class        = $getRow['class'];
            $tmimaOut->comments     = $getRow['comments'];
            $tmimaOut->created_at     = $getRow['created_at'];
            $tmimaOut->updated_at     = $getRow['updated_at'];
            return $tmimaOut;
        } else {
            return null;
        }
    }

    public function addClass()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (name, class, comments, created_at) 
		VALUES (?, ?, ?, ?)
		";
        $params = [$this->name, $this->class, $this->comments, $this->created_at];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            include_once  PROJECT_ROOT . '/objects/error.php';
            return 1;
        }
    }

    public function updateTmima()
    {
        if ($this->checkTmimaExistance()) {
            return 2;
        }
        self::initDbTables();
        global $db;
        $query = "
		UPDATE " . self::$db_table_name . " 
		SET name=?, class=?, comments = ?, updated_at= ? 
		WHERE id = ?
		";
        $params = [$this->name, $this->class, $this->comments, $this->updated_at, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function deleteTmima($id)
    {
        if (Grade::gradeByTmimaIdExists($id)) {
            return 2;
        } elseif (Student::studentByTmimaIdExists($id)) {
            return 3;
        } elseif (UserTmimata::teacherTmimataByTmimaIdExists($id)) {
            return 4;
        }
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function getAllTmima()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . "  
		ORDER BY name ASC
		";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $tmima              = new Tmima;
                $tmima->id          = $row['id'];
                $tmima->name        = $row['name'];
                $tmima->class       = AppLists::getListEntryById((int) $row['class']);
                $tmima->comments    = $row['comments'];
                $tmima->created_at  = $row['created_at'];
                $tmima->updated_at  = $row['updated_at'];
                array_push($output, $tmima);
            }
        }
        return $output;
    }

    public static function getTmimataDropdown($the_id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
        FROM " . self::$db_table_name . " 
        ORDER BY name ASC
		";
        $getRows = $db->getRows($query);
        $output = '<select class="custom-select custom-select-md" id="' . $the_id . '" name="' . $the_id . '" style="height: 35px;" required=""><option value="">Επιλογή</option>';
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }
        }
        $output .= '</select>';
        return $output;
    }

    public static function getTmimaIdFromName($name)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT id 
		FROM " . self::$db_table_name . " 
		WHERE name = ?
		";
        $params = [$name];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return $getRow['id'];
        } else {
            return null;
        }
    }

    public static function getTmimataCount()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(id) as tmimaCount 
        FROM " . self::$db_table_name . " 
		";
        $getRow = $db->getRow($query);
        if ($getRow) {
            return $getRow['tmimaCount'];
        } else {
            return null;
        }
    }

    public static function getTmimataCountOfCurrentUser()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(" . self::$db_table_name . ".id) as tmimaCount 
		FROM " . self::$db_table_name . " 
        LEFT JOIN " . self::$teachers_tmimata_tbl . " ON " . self::$db_table_name . ".id = " . self::$teachers_tmimata_tbl . ".tmima_id 
        WHERE " . self::$teachers_tmimata_tbl . ".user_id = ?
		";
        $params = [$_SESSION['user_id']];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return $getRow['tmimaCount'];
        } else {
            return null;
        }
    }

    public static function tmimaByAppListIdExists($appListId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_table_name . " 
        WHERE class = ?
        LIMIT 1;
        ";
        $params = [$appListId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    private function checkTmimaExistance()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT id 
		FROM " . self::$db_table_name . " 
		WHERE name = ? 
		AND id <> ?
		";
        $params = [$this->name, $this->id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return true;
        } else {
            return false;
        }
    }
}
