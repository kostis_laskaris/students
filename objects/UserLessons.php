<?php
/**
 *
 */
class UserLessons
{
    //table name
    private static $db_table_name;
    private static $grade_tbl;
    private static $students_lessons_tbl;
    //attribs
    public $id;
    public $lesson_id;
    public $user_id;

    private static function initDbTables()
    {
        self::$grade_tbl            = $_SESSION["grade_db_tbl"];
        self::$students_lessons_tbl = $_SESSION['students_lessons_tbl'];
        if (!empty(self::$db_table_name)) {
            return false;
        }
        self::$db_table_name = $_SESSION['teacher_lessons_tbl'];
    }

    public function save()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (user_id, lesson_id) 
		VALUES (?, ?)
		";
        $params = [$this->user_id, $this->lesson_id];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getAllEntries()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $userLesson 			= new UserLessons;
                $userLesson->id 		= $row['id'];
                $userLesson->lesson_id 	= $row['lesson_id'];
                $userLesson->user_id 	= $row['user_id'];
                array_push($output, $userLesson);
            }
        }
        return $output;
    }

    public function index()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $lesson = Lesson::getLessonById($row['lesson_id']);
                array_push($output, $lesson);
            }
        }
        return $output;
    }

    public function indexThatBeenGraded($student_id)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT ANY_VALUE(" . self::$db_table_name . ".id) as id, " . self::$db_table_name . ".user_id, " . self::$db_table_name . ".lesson_id
        FROM " . self::$db_table_name . ", " . self::$students_lessons_tbl . ", " . self::$grade_tbl . "
        WHERE  " . self::$db_table_name . ".user_id = ?
        AND " . self::$grade_tbl . ".lesson_id =  " . self::$db_table_name . ".lesson_id
        AND " . self::$students_lessons_tbl . ".lesson_id = " . self::$grade_tbl . ".lesson_id
        AND " . self::$students_lessons_tbl . ".student_id = " . self::$grade_tbl . ".student_id
        AND " . self::$db_table_name . ".user_id = " . self::$grade_tbl . ".user_id
        AND " . self::$students_lessons_tbl . ".student_id = ?
        GROUP BY  " . self::$db_table_name . ".lesson_id
		";
        $params = [$this->user_id, $student_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $lesson = Lesson::getLessonById($row['lesson_id']);
                array_push($output, $lesson);
            }
        }
        return $output;
    }

    public function delete()
    {
        self::initDbTables();
        global $db;
        $query="
		DELETE FROM " . self::$db_table_name . " 
		WHERE user_id = ?
		";
        $params = [$this->user_id];
        $deleteRows = $db->deleteRow($query, $params);
        
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }
}
