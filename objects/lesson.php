<?php

/**
 *
 */
class Lesson
{
    //table name
    private static $db_table_name;
    private static $students_lessons_tbl;
    private static $teacher_lessons_tbl;
    private static $grade_tbl;
    //attribs
    public $id;
    public $name;
    public $description;
    public $created_at;
    public $updated_at;

    private static function initDbTables()
    {
        self::$db_table_name        = $_SESSION['lessons_tbl'];
        self::$students_lessons_tbl = $_SESSION['students_lessons_tbl'];
        self::$grade_tbl            = $_SESSION["grade_db_tbl"];
        self::$teacher_lessons_tbl  = $_SESSION["teacher_lessons_tbl"];
    }

    public static function getAllLessons()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		ORDER BY name ASC
		";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $lesson                 = new Lesson;
                $lesson->id             = $row['id'];
                $lesson->name           = $row['name'];
                $lesson->description    = $row['description'];
                $lesson->created_at     = $row['created_at'];
                $lesson->updated_at     = $row['updated_at'];
                array_push($output, $lesson);
            }
        }
        return $output;
    }

    public static function getLessonById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $lessonOut                       = new Lesson;
            $lessonOut->id                   = $getRow['id'];
            $lessonOut->name                 = $getRow['name'];
            $lessonOut->description          = $getRow['description'];
            $lessonOut->created_at           = $getRow['created_at'];
            $lessonOut->updated_at           = $getRow['updated_at'];
            return $lessonOut;
        } else {
            return null;
        }
    }

    public static function getLessonByIdAndSxEtos($id, $sx_etos)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM lessons_" . $sx_etos . " 
		WHERE id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $lessonOut                       = new Lesson;
            $lessonOut->id                   = $getRow['id'];
            $lessonOut->name                 = $getRow['name'];
            $lessonOut->description          = $getRow['description'];
            $lessonOut->created_at           = $getRow['created_at'];
            $lessonOut->updated_at           = $getRow['updated_at'];
            return $lessonOut;
        } else {
            return null;
        }
    }

    public static function getLessonsAsDropdown($the_id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT id, name 
		FROM " . self::$db_table_name . " 
		ORDER BY name ASC
        ";
        $lessonsDropdown = '';
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $lessonsDropdown = '<select class="custom-select custom-select-md" id="'.$the_id.'" name="'.$the_id.'" required><option value="">Επιλογή</option>';
            foreach ($getRows as $row) {
                $lessonsDropdown .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }
            $lessonsDropdown .= '</select>';
        }
        return $lessonsDropdown;
    }

    public function updateLesson()
    {
        self::initDbTables();
        if ($this->checkLessonExistance()) {
            return 2;
        }
        global $db;
        $query = "
		UPDATE " . self::$db_table_name . " 
		SET name=?, description=?, updated_at= ? 
		WHERE id = ?
		";
        $params = [$this->name, $this->description, $this->updated_at, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function addLesson()
    {
        self::initDbTables();
        if ($this->checkLessonDescriptionExistance()) {
            return 2;
        }

        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (name, description, created_at, updated_at) 
		VALUES (?, ?, ?, ?)
		";
        $params = [$this->name, $this->description, $this->created_at, $this->updated_at];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function deleteLesson($id)
    {
        self::initDbTables();
        $checked = self::checkLessonAssignments($id);
        if ($checked == 0) {
            global $db;
            $query = "
			DELETE FROM " . self::$db_table_name . " 
			WHERE id = ?
			";
            $params = [$id];
            $deleteRows = $db->deleteRow($query, $params);
            if ($deleteRows) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return $checked;
        }
    }

    private function checkLessonDescriptionExistance()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT id 
		FROM " . self::$db_table_name . " 
		WHERE description = ?
		";
        $params = [$this->description];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return true;
        } else {
            return false;
        }
    }

    private function checkLessonExistance()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE description = ? 
		AND id <> $this->id
		";
        $params = [$this->description];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkLessonAssignments($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(id) as cnt 
		FROM " . self::$students_lessons_tbl . " 
		WHERE lesson_id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow['cnt'] == 0) {
            global $db;
            $query = "
			SELECT COUNT(id) as cnt_grade 
			FROM " . self::$grade_tbl . " 
			WHERE lesson_id = ?
			";
            $params = [$id];
            $getRow = $db->getRow($query, $params);
            if ($getRow['cnt_grade'] == 0) {
                global $db;
                $query = "
				SELECT COUNT(id) as user_cnt 
				FROM  " . self::$teacher_lessons_tbl . "  
				WHERE lesson_id = ?
				";
                $params = [$id];
                $getRow = $db->getRow($query, $params);
                if ($getRow['user_cnt'] == 0) {
                    return 0;
                } else {
                    return 4;
                }
            } else {
                return 3;
            }
        } else {
            return 2;
        }
    }
    // 0 = ok, 2 = student lesson exists, 3 = grade exist, 4 = teacher lesson exist
}
