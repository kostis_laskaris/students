<?php
class Student
{
    //table name
    private static $db_table_name;
    private static $students_lessons_tbl;
    private static $teachers_tmimata_tbl;
    private static $lessons_tbl;
    private static $class_tbl;
    private static $teacher_lessons_tbl;
    private static $app_list_tbl;
    private static $grade_tbl;
    //attribs
    public $id;
    public $firstname;
    public $lastname;
    public $fullName;
    public $fullAddress;
    public $mobile;
    public $address;
    public $perioxi;
    public $zip;
    public $email;
    public $fphone;
    public $mphone;
    public $class;
    public $phone;
    public $mname;
    public $faname;
    public $kid_name;
    public $kid_phone;
    public $lessons;
    public $grades;
    public $fee;
    public $periodos;
    public $sx_etos;
    public $tmima;
    public $email_kidemona;
    public $informed_by;
    public $lessonsAsString;
    public $lessonsIdsAsString;
    public $lessonsByUserId;
    public $date_eggrafis;
    public $created_at;
    public $updated_at;

    private static function initDbTables()
    {
        self::$db_table_name         = $_SESSION["students_tbl"];
        self::$students_lessons_tbl  = $_SESSION['students_lessons_tbl'];
        self::$teachers_tmimata_tbl  = $_SESSION['teachers_tmimata_tbl'];
        self::$lessons_tbl           = $_SESSION["lessons_tbl"];
        self::$class_tbl             = $_SESSION["tmimata_tbl"];
        self::$teacher_lessons_tbl   = $_SESSION["teacher_lessons_tbl"];
        self::$grade_tbl             = $_SESSION["grade_db_tbl"];
        self::$app_list_tbl          = $_SESSION['app_lists_tbl'];
    }

    public function addStudent()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (id, firstname, lastname, address, perioxi, zip, mobile, phone, email, fphone, mphone, class, mname, faname, fee, periodos, sx_etos, tmima, email_kidemona, kid_name, kid_phone, informed_by, date_eggrafis, created_at, updated_at)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
		";
        $params = [
            $this->id,
            $this->firstname,
            $this->lastname,
            $this->address,
            $this->perioxi,
            $this->zip,
            $this->mobile,
            $this->phone,
            $this->email,
            $this->fphone,
            $this->mphone,
            $this->class,
            $this->mname,
            $this->faname,
            $this->fee,
            $this->periodos,
            $this->sx_etos,
            $this->tmima,
            $this->email_kidemona,
            $this->kid_name,
            $this->kid_phone,
            $this->informed_by,
            $this->date_eggrafis,
            $this->created_at,
            $this->updated_at
        ];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            $latestShortcutId = $db->lastInsertedId();
            foreach ($this->lessons as $lesson) {
                $studentLessons                 = new StudentLessons;
                $studentLessons->lesson_id      = $lesson;
                $studentLessons->student_id     = $latestShortcutId;
                $studentLessons->save();
            }
            return 0;
        } else {
            return 1;
        }
    }

    public static function resubStudent(array $allDatas, $from_sx_etos)
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (firstname, lastname, address, perioxi, zip, mobile, phone, email, fphone, mphone, class, mname, faname, fee, periodos, tmima, email_kidemona, kid_name, kid_phone, informed_by, date_eggrafis, sx_etos)
        SELECT firstname, lastname, address, perioxi, zip, mobile, phone, email, fphone, mphone, '" . $allDatas['taksi'] . "', mname, faname, fee, periodos, '" . $allDatas['tmima'] . "', email_kidemona, kid_name, kid_phone, informed_by, '" . date('Y-m-d') . "','" . str_replace(' ', '', $_SESSION['curr_school_year']) . "'
        FROM students_" . $from_sx_etos . " WHERE id = " . $allDatas['student_id'] . "
        AND CONCAT(firstname, lastname, faname) NOT IN (
            SELECT CONCAT(firstname, lastname, faname)
                FROM " . self::$db_table_name . "
        )
        ;";
        $insertRow = $db->executeQuery($query);
        if ($insertRow) {
            $latestShortcutId = $db->lastInsertedId();
            $lessons_ids = explode(',', $allDatas['lessons_ids']);
            foreach ($lessons_ids as $lesson) {
                $studentLessons             = new StudentLessons;
                $studentLessons->lesson_id  = $lesson;
                $studentLessons->student_id = $latestShortcutId;
                $studentLessons->save();
            }
            return 0;
        } else {
            return 1;
        }
    }

    public function updateStudent()
    {
        self::initDbTables();
        global $db;
        $query = "
		UPDATE " . self::$db_table_name . " 
		SET firstname=?, lastname=?, address=?, perioxi=?, zip=?, mobile=?, phone=?, email=?, fphone=?, mphone=?, class=?, mname=?, faname=?, fee=?, tmima=?, email_kidemona=?, kid_name=?, kid_phone=?, informed_by=?, date_eggrafis=?, updated_at=? 
		WHERE id = ?;
		";
        $params = [
            $this->firstname,
            $this->lastname,
            $this->address,
            $this->perioxi,
            $this->zip,
            $this->mobile,
            $this->phone,
            $this->email,
            $this->fphone,
            $this->mphone,
            $this->class,
            $this->mname,
            $this->faname,
            $this->fee,
            $this->tmima,
            $this->email_kidemona,
            $this->kid_name,
            $this->kid_phone,
            $this->informed_by,
            $this->date_eggrafis,
            $this->updated_at,
            $this->id
        ];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            $student_id                  = $this->id;
            $allLessons                  = $this->lessons;
            $studentLessons              = new StudentLessons;
            $studentLessons->student_id  = $student_id;
            $deleted                     = $studentLessons->delete($this->con);
            if ($deleted == 0) {
                foreach ($allLessons as $lesson) {
                    $studentLessons              = new StudentLessons;
                    $studentLessons->lesson_id   = $lesson;
                    $studentLessons->student_id  = $student_id;
                    $studentLessons->save();
                }
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    public static function deleteStudent($id)
    {
        if (Grade::gradeByStudentIdExists($id)) {
            return 2;
        }
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE id = ?
		";
        $params = [$id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }

    public static function getStudentsCount()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(id) as studentCount 
		FROM " . self::$db_table_name . "
		";
        $getRow = $db->getRow($query);
        if ($getRow) {
            return $getRow['studentCount'];
        } else {
            return null;
        }
    }

    public static function getStudentsCountOfCurrentUser()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT COUNT(DISTINCT(" . self::$db_table_name . ".id)) as studentCount
        FROM " . self::$db_table_name . ", " . self::$teacher_lessons_tbl . ", " . self::$students_lessons_tbl . ", " . self::$teachers_tmimata_tbl . "
        WHERE " . self::$teachers_tmimata_tbl . ".user_id = ? 
        AND " . self::$teacher_lessons_tbl . ".user_id = ? 
        AND " . self::$students_lessons_tbl . ".student_id = " . self::$db_table_name . ".id
        AND " . self::$students_lessons_tbl . ".lesson_id = " . self::$teacher_lessons_tbl . ".lesson_id 
        AND " . self::$db_table_name . ".tmima = " . self::$teachers_tmimata_tbl . ".tmima_id
        ";
        $params = [$_SESSION['user_id'], $_SESSION['user_id']];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return $getRow['studentCount'];
        } else {
            return null;
        }
    }

    public static function getAllStudents()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT  " . self::$db_table_name . " .*,  " . self::$class_tbl . " .name as tmimaName 
		FROM " . self::$db_table_name . ", " . self::$class_tbl . " 
		WHERE " . self::$db_table_name . ".tmima = " . self::$class_tbl . ".id 
		ORDER BY lastname ASC
		";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $student                        = new Student;
                $student->id                    = $row['id'];
                $student->firstname             = $row['firstname'];
                $student->lastname              = $row['lastname'];
                $student->fullName              = $student->getFullName();
                $student->periodos              = $row['periodos'];
                $student->sx_etos               = $row['sx_etos'];
                $student->class                 = AppLists::getListEntryById((int) $row['class']);
                $student->mobile                = $row['mobile'];
                $student->phone                 = $row['phone'];
                $student->address               = $row['address'];
                $student->perioxi               = $row['perioxi'];
                $student->zip                   = $row['zip'];
                $student->email                 = $row['email'];
                $student->mphone                = $row['mphone'];
                $student->fphone                = $row['fphone'];
                $student->mname                 = $row['mname'];
                $student->faname                = $row['faname'];
                $student->kid_name              = $row['kid_name'];
                $student->kid_phone             = $row['kid_phone'];
                $student->email_kidemona        = $row['email_kidemona'];
                $student->fee                   = $row['fee'];
                $student->informed_by           = AppLists::getListEntryById((int) $row['informed_by']);
                $student->date_eggrafis         = $row['date_eggrafis'];
                $student->created_at            = $row['created_at'];
                $student->updated_at            = $row['updated_at'];
                $student->tmima                 = $row['tmimaName'];
                $student->fullAddress           = $student->getFullAddress();
                $student_lessons                = new StudentLessons;
                $student_lessons->student_id    = $row['id'];
                $student->lessons               = $student_lessons->index();
                $student->lessonsAsString       = $student->getLessonsAsString();
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getLastNStudentsSub($n)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT  " . self::$db_table_name . " .*,  " . self::$class_tbl . " .name as tmimaName 
		FROM " . self::$db_table_name . ", " . self::$class_tbl . " 
        WHERE " . self::$db_table_name . ".tmima = " . self::$class_tbl . ".id 
        ORDER BY date_eggrafis DESC 
		LIMIT " . $n . "
        ";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $student                        = new Student;
                $student->id                    = $row['id'];
                $student->firstname             = $row['firstname'];
                $student->lastname              = $row['lastname'];
                $student->fullName              = $student->getFullName();
                $student->periodos              = $row['periodos'];
                $student->sx_etos               = $row['sx_etos'];
                $student->class                 = AppLists::getListEntryById((int) $row['class']);
                $student->mobile                = $row['mobile'];
                $student->phone                 = $row['phone'];
                $student->address               = $row['address'];
                $student->perioxi               = $row['perioxi'];
                $student->zip                   = $row['zip'];
                $student->email                 = $row['email'];
                $student->mphone                = $row['mphone'];
                $student->fphone                = $row['fphone'];
                $student->mname                 = $row['mname'];
                $student->faname                = $row['faname'];
                $student->kid_name              = $row['kid_name'];
                $student->kid_phone             = $row['kid_phone'];
                $student->email_kidemona        = $row['email_kidemona'];
                $student->fee                   = $row['fee'];
                $student->informed_by           = AppLists::getListEntryById((int) $row['informed_by']);
                $student->date_eggrafis         = $row['date_eggrafis'];
                $student->created_at            = $row['created_at'];
                $student->updated_at            = $row['updated_at'];
                $student->tmima                 = $row['tmimaName'];
                $student->fullAddress           = $student->getFullAddress();
                $student_lessons                = new StudentLessons;
                $student_lessons->student_id    = $row['id'];
                $student->lessons               = $student_lessons->index();
                $student->lessonsAsString       = $student->getLessonsAsString();
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllStudentsBySxEtos($sx_etos)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT  students_" . $sx_etos . ".*, tmimata_" . $sx_etos . ".name as tmimaName 
		FROM students_" . $sx_etos . ", tmimata_" . $sx_etos . " 
        WHERE students_" . $sx_etos . ".tmima = tmimata_" . $sx_etos . ".id 
        AND CONCAT(firstname, lastname, faname) NOT IN (
            SELECT CONCAT(firstname, lastname, faname)
                FROM " . self::$db_table_name . "
        ) 
		ORDER BY lastname ASC
        ";
        // return $query;
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $student                        = new Student;
                $student->id                    = $row['id'];
                $student->firstname             = $row['firstname'];
                $student->lastname              = $row['lastname'];
                $student->fullName              = $student->getFullName();
                $student->periodos              = $row['periodos'];
                $student->sx_etos               = $row['sx_etos'];
                $student->class                 = $row['class'];
                $student->mobile                = $row['mobile'];
                $student->phone                 = $row['phone'];
                $student->address               = $row['address'];
                $student->perioxi               = $row['perioxi'];
                $student->zip                   = $row['zip'];
                $student->email                 = $row['email'];
                $student->mphone                = $row['mphone'];
                $student->fphone                = $row['fphone'];
                $student->mname                 = $row['mname'];
                $student->faname                = $row['faname'];
                $student->kid_name              = $row['kid_name'];
                $student->kid_phone             = $row['kid_phone'];
                $student->email_kidemona        = $row['email_kidemona'];
                $student->fee                   = $row['fee'];
                $student->informed_by           = $row['informed_by'];
                $student->date_eggrafis         = $row['date_eggrafis'];
                $student->created_at            = $row['created_at'];
                $student->updated_at            = $row['updated_at'];
                $student->tmima                 = $row['tmimaName'];
                $student->fullAddress           = $student->getFullAddress();
                $student->lessons               = StudentLessons::indexBySxEtos($row['id'], $sx_etos);
                $student->lessonsAsString       = $student->getLessonsAsString();
                $student->lessonsIdsAsString    = $student->getLessonIdsAsString();
                // $student_grades					= new Grade;
                // $student_grades->student_id		= $row['id'];
                // $student->grades				= $student_grades->getAllGradesByStudentId();
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function query($sql)
    {
        self::initDbTables();
        global $db;
        $getRows = $db->getRows($sql);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $student                        = new Student;
                $student->id                    = $row['id'];
                $student->firstname             = $row['firstname'];
                $student->lastname              = $row['lastname'];
                $student->fullName              = $student->getFullName();
                $student->periodos              = $row['periodos'];
                $student->sx_etos               = $row['sx_etos'];
                $student->class                 = $row['class'];
                $student->mobile                = $row['mobile'];
                $student->phone                 = $row['phone'];
                $student->address               = $row['address'];
                $student->perioxi               = $row['perioxi'];
                $student->zip                   = $row['zip'];
                $student->email                 = $row['email'];
                $student->mphone                = $row['mphone'];
                $student->fphone                = $row['fphone'];
                $student->mname                 = $row['mname'];
                $student->faname                = $row['faname'];
                $student->kid_name              = $row['kid_name'];
                $student->kid_phone             = $row['kid_phone'];
                $student->email_kidemona        = $row['email_kidemona'];
                $student->fee                   = $row['fee'];
                $student->informed_by           = $row['informed_by'];
                $student->date_eggrafis         = $row['date_eggrafis'];
                $student->created_at            = $row['created_at'];
                $student->updated_at            = $row['updated_at'];
                $student->tmima                 = $row['tmimaName'];
                $student->fullAddress           = $student->getFullAddress();
                $student_lessons                = new StudentLessons;
                $student_lessons->student_id    = $row['id'];
                $student->lessons               = $student_lessons->index();
                $student->lessonsAsString       = $student->getLessonsAsString();
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".*, " . self::$class_tbl . ".name as tmimaName 
		FROM " . self::$db_table_name . ", " . self::$class_tbl . " 
		WHERE " . self::$db_table_name . ".tmima = " . self::$class_tbl . ".id 
        AND " . self::$db_table_name . ".id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $student                        = new Student;
            $student->id                    = $getRow['id'];
            $student->firstname             = $getRow['firstname'];
            $student->lastname              = $getRow['lastname'];
            $student->fullName              = $student->getFullName();
            $student->periodos              = $getRow['periodos'];
            $student->sx_etos               = $getRow['sx_etos'];
            $student->class                 = AppLists::getListEntryById((int) $getRow['class']);
            $student->mobile                = $getRow['mobile'];
            $student->phone                 = $getRow['phone'];
            $student->address               = $getRow['address'];
            $student->perioxi               = $getRow['perioxi'];
            $student->zip                   = $getRow['zip'];
            $student->email                 = $getRow['email'];
            $student->mphone                = $getRow['mphone'];
            $student->fphone                = $getRow['fphone'];
            $student->mname                 = $getRow['mname'];
            $student->faname                = $getRow['faname'];
            $student->kid_name              = $getRow['kid_name'];
            $student->kid_phone             = $getRow['kid_phone'];
            $student->email_kidemona        = $getRow['email_kidemona'];
            $student->fee                   = $getRow['fee'];
            $student->informed_by           = AppLists::getListEntryById((int) $getRow['informed_by']);
            $student->date_eggrafis         = $getRow['date_eggrafis'];
            $student->created_at            = $getRow['created_at'];
            $student->updated_at            = $getRow['updated_at'];
            $student->tmima                 = array('name' => $getRow['tmimaName'], 'id' => $getRow['tmima']);
            $student->fullAddress           = $student->getFullAddress();
            $student_lessons                = new StudentLessons;
            $student_lessons->student_id    = $getRow['id'];
            $student->lessons               = $student_lessons->index();
            $student->lessonsAsString       = $student->getLessonsAsString();
            $student->lessonsIdsAsString    = $student->getLessonIdsAsString();
            $student->lessonsByUserId       = $student->getStudentLessonsByUserId();
            return $student;
        } else {
            return 1;
        }
    }

    public static function getStudentNameById($id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".firstname, " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".date_eggrafis 
		FROM " . self::$db_table_name . " 
		WHERE " . self::$db_table_name . ".id = ?
		";
        $params = [$id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $student                    = new Student;
            $student->id                = $id;
            $student->firstname         = $getRow['firstname'];
            $student->lastname          = $getRow['lastname'];
            $student->fullName          = $student->getFullName();
            $student->date_eggrafis     = $getRow['date_eggrafis'];
            return $student;
        } else {
            return 1;
        }
    }

    public function getStudentsByClass()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".firstname, " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".date_eggrafis 
		FROM " . self::$db_table_name . " 
        WHERE " . self::$db_table_name . ".class = ? 
        ORDER BY " . self::$db_table_name . ".lastname ASC
		";
        $params = [$this->class];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $student                        = new Student;
                $student->firstname             = $row['firstname'];
                $student->lastname              = $row['lastname'];
                $student->date_eggrafis         = $row['date_eggrafis'];
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public function getStudentsByPerioxi()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".firstname, " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".date_eggrafis 
		FROM " . self::$db_table_name . " 
		WHERE " . self::$db_table_name . ".perioxi = ?
        ORDER BY " . self::$db_table_name . ".lastname ASC
		";
        $params = [$this->perioxi];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $student                         = new Student;
                $student->firstname              = $row['firstname'];
                $student->lastname               = $row['lastname'];
                $student->date_eggrafis          = $row['date_eggrafis'];
                array_push($output, $student);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentPerClass()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT class, COUNT(" . self::$db_table_name . ".id) as total, list_entry
        FROM " . self::$db_table_name . " 
        LEFT JOIN " . self::$app_list_tbl . " ON " . self::$db_table_name . ".class =  " . self::$app_list_tbl . ".id 
		GROUP BY class 
		ORDER BY class ASC
		";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut               = new stdClass();
                $statsOut->class_id     = $row['class'];
                $statsOut->total        = $row['total'];
                $statsOut->class_name   = $row['list_entry'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentPerPerioxi()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT perioxi, COUNT(id) as total 
		FROM " . self::$db_table_name . " 
		GROUP BY perioxi 
		ORDER BY perioxi ASC
		";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut                 = new stdClass();
                $statsOut->perioxi_name   = $row['perioxi'];
                $statsOut->total          = $row['total'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public static function getStudentPerLesson()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$students_lessons_tbl . ".lesson_id, " . self::$lessons_tbl . ".name as lesson_name, " . self::$lessons_tbl . ".description as lesson_code, " . self::$lessons_tbl . ".created_at as created_at, COUNT(" . self::$students_lessons_tbl . ".id) as total 
		FROM " . self::$students_lessons_tbl . ", " . self::$lessons_tbl . " WHERE " . self::$lessons_tbl . ".id = " . self::$students_lessons_tbl . ".lesson_id 
		GROUP BY " . self::$students_lessons_tbl . ".lesson_id ;
		";
        $getRows = $db->getRows($query);
        if (!empty($getRows)) {
            $output = array();
            foreach ($getRows as $row) {
                $statsOut = new stdClass();
                $statsOut->lesson_name     = $row['lesson_name'];
                $statsOut->lesson_code     = $row['lesson_code'];
                $statsOut->total           = $row['total'];
                $statsOut->created_at      = $row['created_at'];
                $statsOut->lesson_id       = $row['lesson_id'];
                array_push($output, $statsOut);
            }
            return $output;
        } else {
            return 1;
        }
    }

    public function getStudentLessonsForView()
    {
        $les = $ids = '';
        $output = array();
        foreach ($this->lessons as $lesson) {
            $les .= $lesson->name . ", ";
            $ids .= $lesson->id . ",";
        }
        $output['names'] = substr($les, 0, -2);
        $output['ids'] = substr($ids, 0, -1);
        return $output;
    }

    public static function getStudentsForGrade($teacher_id, $as_list = false)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT DISTINCT(" . self::$db_table_name . ".id) , " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".firstname
		FROM " . self::$db_table_name . ", " . self::$students_lessons_tbl . ", " . self::$teacher_lessons_tbl . ", " . self::$teachers_tmimata_tbl . "
		WHERE " . self::$teacher_lessons_tbl . ".user_id = ?
		AND " . self::$students_lessons_tbl . ".lesson_id = " . self::$teacher_lessons_tbl . ".lesson_id
		AND " . self::$students_lessons_tbl . ".student_id = " . self::$db_table_name . ".id
		AND " . self::$teachers_tmimata_tbl . ".user_id = ?
		AND " . self::$teachers_tmimata_tbl . ".tmima_id = " . self::$db_table_name . ".tmima
		ORDER BY " . self::$db_table_name . ".lastname ASC
		";
        $params = [$_SESSION['user_id'], $_SESSION['user_id']];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            if ($as_list) {
                $st_dropdown = '<select class="custom-select custom-select-md" id="student_name" name="student_name" required><option value=""></option>';
                foreach ($getRows as $row) {
                    $st_dropdown .= '<option value="' . $row['id'] . '">' . $row['lastname'] . " " . $row['firstname'] . "</option>";
                }
                $st_dropdown .= '</select>';
                return $st_dropdown;
            } else {
                $output = array();
                foreach ($getRows as $row) {
                    $student                = new Student();
                    $student->firstname     = $row['firstname'];
                    $student->lastname      = $row['lastname'];
                    $student->id            = $row['id'];
                    array_push($output, $student);
                }
                return $output;
            }
        } else {
            return 1;
        }
    }

    public function getStudentClass()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".class
		FROM " . self::$db_table_name . "
		WHERE id = ?
		";
        $params = [$this->id];
        $getRow = $db->getRow($query, $params);
        $output = '';
        if ($getRow) {
            $output = $getRow['class'];
            return $output;
        } else {
            return 1;
        }
    }

    public function getStudentTmima()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$class_tbl . ".name as tmimaName, " . self::$class_tbl . ".id as tmimaId
		FROM " . self::$db_table_name . ", " . self::$class_tbl . " 
		WHERE " . self::$db_table_name . ".tmima = " . self::$class_tbl . ".id 
		AND " . self::$db_table_name . ".id = ?
		";
        $params = [$this->id];
        $getRow = $db->getRow($query, $params);
        $output = [];
        if ($getRow) {
            $output['name'] = $getRow['tmimaName'];
            $output['id']   = $getRow['tmimaId'];
            return $output;
        } else {
            return 1;
        }
    }

    public static function getAllStudentsAsDropdown($element_id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".firstname, " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".id 
        FROM " . self::$db_table_name . " 
        ORDER BY " . self::$db_table_name . ".lastname ASC
		";
        $getRows = $db->getRows($query);
        $studentDropdown = '<select class="custom-select custom-select-md" id="' . $element_id . '" name="' . $element_id . '" required><option value="">Επιλογή</option>';

        foreach ($getRows as $row) {
            $studentDropdown .= '<option value="' . $row["id"] . '">' . $row["lastname"] . " " . $row["firstname"] . '</option>';
        }
        $studentDropdown .= '</select>';
        return $studentDropdown;
    }

    public static function getStudentsByLessonAsDropdown($lesson_id, $element_id)
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT " . self::$db_table_name . ".firstname, " . self::$db_table_name . ".lastname, " . self::$db_table_name . ".id 
        FROM " . self::$db_table_name . "
        LEFT JOIN " . self::$students_lessons_tbl . " ON " . self::$db_table_name . ".id = " . self::$students_lessons_tbl . ".student_id
        WHERE " . self::$students_lessons_tbl . ".lesson_id = ? 
        ORDER BY " . self::$db_table_name . ".lastname ASC
        ";
        $params = [$lesson_id];
        $getRows = $db->getRows($query, $params);
        $studentDropdown = '<select class="custom-select custom-select-md" id="' . $element_id . '" name="' . $element_id . '" required><option value="">Επιλογή</option>';

        foreach ($getRows as $row) {
            $studentDropdown .= '<option value="' . $row["id"] . '">' . $row["lastname"] . " " . $row["firstname"] . '</option>';
        }
        $studentDropdown .= '</select>';
        return $studentDropdown;
    }

    public static function studentByAppListIdExists($appListId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_table_name . " 
        WHERE class = ? OR informed_by = ?
        LIMIT 1;
        ";
        $params = [$appListId, $appListId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    public static function studentByTmimaIdExists($tmimaId)
    {
        self::initDbTables();
        global $db;
        $query = "
        SELECT id
        FROM " . self::$db_table_name . " 
        WHERE tmima = ?
        LIMIT 1;
        ";
        $params = [$tmimaId];
        $getRows = $db->getRows($query, $params);
        if (!empty($getRows)) {
            return true;
        } else {
            return false;
        }
    }

    public function getStudentLessons()
    {
        $student_lessons                 = new StudentLessons();
        $student_lessons->student_id     = $this->id;
        return $student_lessons->index();
    }

    public function getAllStudentPhones()
    {
        if ($this->mobile && $this->phone) {
            return $this->phone . ", " . $this->mobile;
        } elseif (!$this->mobile && $this->phone) {
            return $this->phone;
        } elseif ($this->mobile && !$this->phone) {
            return $this->mobile;
        } else {
            return '';
        }
    }

    public function getFullName()
    {
        return $this->lastname . " " . $this->firstname;
    }

    public function getFullAddress()
    {
        if (empty($this->address)) {
            return '';
        }

        if (empty($this->perioxi) && !empty($this->zip)) {
            return $this->address . ", " . $this->zip;
        } elseif (!empty($this->perioxi) && empty($this->zip)) {
            return $this->address . ", " . $this->perioxi;
        } elseif (empty($this->perioxi) && empty($this->zip)) {
            return $this->address;
        } else {
            return $this->address . ", " . $this->perioxi . ", " . $this->zip;
        }
    }

    public function getLessonsAsString()
    {
        if (empty($this->lessons)) {
            return '';
        }
        $output = "";
        foreach ($this->lessons as $key => $lesson) {
            $output .= $lesson->name . ", ";
        }
        return substr($output, 0, -2);
    }

    public function getLessonIdsAsString()
    {
        if (empty($this->lessons)) {
            return '';
        }

        $output = "";
        foreach ($this->lessons as $key => $lesson) {
            $output .= $lesson->id . ",";
        }
        return substr($output, 0, -1);
    }

    public function getStudentLessonsByUserId()
    {
        $outputArr = [];
        foreach ($this->lessons as $s_lesson) {
            foreach (User::getUserLessons($_SESSION['user_id']) as $u_lesson) {
                if ($s_lesson->id == $u_lesson->id) {
                    array_push($outputArr, $u_lesson);
                }
            }
        }
        return $outputArr;
    }
}
