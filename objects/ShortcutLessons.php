<?php

/**
 *
 */
class ShortcutLessons
{
    //table name
    private static $db_table_name;
    //attribs
    public $id;
    public $lesson_id;
    public $shortcut_it;

    private static function initDbTables()
    {
        if (!empty(self::$db_table_name)) {
            return false;
        }
        self::$db_table_name = $_SESSION['shortcut_lessons_tbl'];
    }

    public function save()
    {
        self::initDbTables();
        global $db;
        $query = "
		INSERT INTO " . self::$db_table_name . " (shortcut_id, lesson_id) 
		VALUES (?, ?)
		";
        $params = [$this->shortcut_id, $this->lesson_id];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function index()
    {
        self::initDbTables();
        global $db;
        $query = "
		SELECT * 
		FROM " . self::$db_table_name . " 
		WHERE shortcut_id = ?
		";
        $params = [$this->shortcut_id];
        $getRows = $db->getRows($query, $params);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $lesson     = Lesson::getLessonById($row['lesson_id']);
                array_push($output, $lesson);
            }
        }
        return $output;
    }

    public function delete()
    {
        self::initDbTables();
        global $db;
        $query = "
		DELETE FROM " . self::$db_table_name . " 
		WHERE shortcut_id = ?
		";
        $params = [$this->shortcut_id];
        $deleteRows = $db->deleteRow($query, $params);
        if ($deleteRows) {
            return 0;
        } else {
            return 1;
        }
    }
}
