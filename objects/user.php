<?php

class User
{
    //table name
    private $db_table_name = "register";
    //attribs
    public $id;
    public $email;
    public $username;
    public $password;
    public $rePassword;
    public $permission;
    public $status;
    public $date_submitted;
    public $lessons;
    public $tmimata;
    public $last_activity_date;
    public $profile;

    public function __construct($usrname = '')
    {
        $this->username = $usrname;
    }

    public function addUser()
    {
        if ($this->checkUsernameExistance()) {
            return 2;
        }
        if ($this->checkEmailExistance()) {
            return 3;
        }
        $sha256Password = hash('sha256', $this->password);
        $password = password_hash($this->password . $this->username . $sha256Password, PASSWORD_DEFAULT, ['cost' => 10]);
        $date_submitted = date('Y-m-d');
        global $db;
        $query = "
		INSERT INTO $this->db_table_name (email, username, password, permission, status, date_submitted) 
		VALUES (?,?,?,?,?,?)
		";
        $params = [$this->email, $this->username, $password, $this->permission, $this->status, $date_submitted];
        $insertRow = $db->insertRow($query, $params);
        if ($insertRow) {
            if ($this->profile->firstname != '' || $this->profile->lastname != '') {
                $this->profile->user_id = $db->lastInsertedId();
                $success = $this->profile->addProfile();
            }
            return 0;
        } else {
            return 1;
        }
    }

    public function updatePass()
    {
        $sha256Password = hash('sha256', $this->password);
        $password = password_hash($this->password . $this->username . $sha256Password, PASSWORD_DEFAULT, ['cost' => 10]);
        global $db;
        $query = "
		UPDATE $this->db_table_name 
		SET $this->db_table_name.password= ? 
		WHERE $this->db_table_name.id = ?
		";
        $params = [$password, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            return 0;
        } else {
            return 1;
        }
    }

    public function updateUser()
    {
        global $db;
        $query = "
		UPDATE $this->db_table_name 
		SET email=?, username=?, permission= ?, status = ? 
		WHERE id = ?
		";
        $params = [$this->email, $this->username, $this->permission, $this->status, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            $this->profile->user_id = $this->id;
            $success = $this->profile->updateProfile();
            return 0;
        } else {
            return 1;
        }
    }

    public function updateUserAndProfile()
    {
        global $db;
        $query = "
		UPDATE $this->db_table_name 
		SET email=? 
		WHERE id = ?
		";
        $params = [$this->email, $this->id];
        $updateRow = $db->updateRow($query, $params);
        if ($updateRow) {
            $this->profile->user_id = $this->id;
            $success = $this->profile->updateFullProfile();
            return 0;
        } else {
            return 1;
        }
    }

    public function addUserLessons()
    {
        $deleted = false;
        foreach ($this->lessons as $lesson) {
            $userLesson                 = new UserLessons;
            $userLesson->lesson_id          = $lesson;
            $userLesson->user_id         = $this->id;
            if (!$deleted) {
                $userLesson->delete();
                $deleted = true;
            }
            $userLesson->save();
        }
        return 0;
    }

    public function addUserTmimata()
    {
        $deleted = false;
        foreach ($this->tmimata as $tmima) {
            $userTmima                     = new UserTmimata;
            $userTmima->tmima_id          = $tmima;
            $userTmima->user_id         = $this->id;
            if (!$deleted) {
                $userTmima->delete();
                $deleted = true;
            }
            $userTmima->save();
        }
        return 0;
    }

    public function deleteUserLessons()
    {
        $userLesson                 = new UserLessons;
        $userLesson->user_id         = $this->id;
        $success                     = $userLesson->delete();
        return $success;
    }

    public function deleteUserTmimata()
    {
        $userTmimata                 = new UserTmimata;
        $userTmimata->user_id         = $this->id;
        $success                     = $userTmimata->delete();
        return $success;
    }

    public static function getUserLessons($user_id)
    {
        $userLessons                 = new UserLessons;
        $userLessons->user_id        = $user_id;
        return $userLessons->index();
    }

    public static function getUserLessonsByStudentIdThatBeenGraded($user_id, $student_id)
    {
        $userLessons                 = new UserLessons;
        $userLessons->user_id        = $user_id;
        return $userLessons->indexThatBeenGraded($student_id);
    }

    public static function getCurrentUserLessonsThatGradedByStudentIdAsDropdown($dropdown_id, $student_id)
    {
        $userLessons = self::getUserLessonsByStudentIdThatBeenGraded($_SESSION['user_id'], $student_id);
        $userLessonsDropdown = '<select class="custom-select custom-select-md" id="' . $dropdown_id . '" name="' . $dropdown_id . '" required><option value="">Επιλογή</option>';
        foreach ($userLessons as $lesson) {
            $userLessonsDropdown .= '<option value="' . $lesson->id . '">' . $lesson->name . '</option>';
        }
        $userLessonsDropdown .= '</select>';
        return $userLessonsDropdown;
    }

    public static function getCurrentUserLessonsAsDropdown($dropdown_id)
    {
        $userLessons = self::getUserLessons($_SESSION['user_id']);
        $userLessonsDropdown = '<select class="custom-select custom-select-md" id="' . $dropdown_id . '" name="' . $dropdown_id . '" required><option value="">Επιλογή</option>';
        foreach ($userLessons as $lesson) {
            $userLessonsDropdown .= '<option value="' . $lesson->id . '">' . $lesson->name . '</option>';
        }
        $userLessonsDropdown .= '</select>';
        return $userLessonsDropdown;
    }

    public static function getUsersAsDropdown($dropdown_id)
    {
        $profile = new Profile();
        $profiles = $profile->getAllUsersProfile();
        $userDropdown = '<select class="custom-select custom-select-md" id="' . $dropdown_id . '" name="' . $dropdown_id . '" required><option value="">Επιλογή</option>';
        foreach ($profiles as $profile) {
            $userDropdown .= '<option value="' . $profile->user_id . '">' . $profile->lastname . " " . $profile->firstname . '</option>';
        }
        $userDropdown .= '</select>';
        return $userDropdown;
    }

    public static function getUsersByLessonAsDropdown($lesson_id, $dropdown_id)
    {
        $profile = new Profile();
        $profiles = $profile->getUsersProfileByLesson($lesson_id);
        if (empty($profiles)) {
            return '';
        }
        $userDropdown = '<select class="custom-select custom-select-md" id="' . $dropdown_id . '" name="' . $dropdown_id . '" required><option value="">Επιλογή</option>';
        foreach ($profiles as $profile) {
            $userDropdown .= '<option value="' . $profile->user_id . '">' . $profile->lastname . " " . $profile->firstname . '</option>';
        }
        $userDropdown .= '</select>';
        return $userDropdown;
    }

    public function getUserById()
    {
        global $db;
        $query = "SELECT * FROM $this->db_table_name WHERE id = ?";
        $params = [$this->id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $this->email                  = $getRow['email'];
            $this->permission             = $getRow['permission'];
            $this->last_activity_date     = $getRow['last_activity_date'];
            $profile              = new Profile;
            $profile->user_id     = $this->id;
            $this->profile        = $profile->getUserProfile();
            return $this;
        }
    }

    public function getAllUsers($withProfile, $withLessons, $withTmimata)
    {
        global $db;
        $query = "
		SELECT * 
		FROM $this->db_table_name";
        $getRows = $db->getRows($query);
        $output = array();
        if (!empty($getRows)) {
            foreach ($getRows as $row) {
                $userOut                        = new User($row['username']);
                $userOut->id                    = $row['id'];
                $userOut->email                 = $row['email'];
                $userOut->permission            = $row['permission'];
                $userOut->status                 = $row['status'];
                $userOut->last_activity_date    = $row['last_activity_date'];
                $userOut->date_submitted           = $row['date_submitted'];
                if ($withProfile) {
                    $profile                 = new Profile;
                    $profile->user_id         = $row['id'];
                    $userOut->profile         = $profile->getUserProfile();
                }
                if ($withLessons) {
                    $userLessons             = new UserLessons;
                    $userLessons->user_id     = $row['id'];
                    $userOut->lessons         = $userLessons->index();
                }
                if ($withTmimata) {
                    $userTmima                 = new UserTmimata;
                    $userTmima->user_id     = $row['id'];
                    $userOut->tmimata         = $userTmima->index();
                }
                array_push($output, $userOut);
            }
        }
        return $output;
    }

    public function getUserAndProfileById()
    {
        global $db;
        $query = "
		SELECT * 
		FROM $this->db_table_name 
		WHERE id = ?
		";
        $params = [$this->id];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            $userOut                        = new User($getRow['username']);
            $userOut->id                    = $getRow['id'];
            $userOut->email                 = $getRow['email'];
            $userOut->permission            = $getRow['permission'];
            $userOut->status                 = $getRow['status'];
            $userOut->last_activity_date    = $getRow['last_activity_date'];
            $userOut->date_submitted           = $getRow['date_submitted'];
            $profile             = new Profile;
            $profile->user_id     = $getRow['id'];
            $userOut->profile     = $profile->getUserProfile();
            return $userOut;
        } else {
            return null;
        }
    }

    public function validateUser()
    {
        global $db;
        $query = "
		SELECT username, permission, password, id 
		FROM $this->db_table_name 
		WHERE username = ? 
		AND status = 1
		";
        $params = [$this->username];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            if (password_verify($this->password, $getRow['password'])) {
                $this->username         = $getRow['username'];
                $this->permission         = $getRow['permission'];
                $_SESSION['username']     = $this->username;
                $_SESSION['permission']   = $this->permission;
                $_SESSION['user_id']      = $getRow['id'];
                return 0;
            } else {
                return 1;
            }
        } else {
            return 3;
        }
    }

    private function checkUsernameExistance()
    {
        global $db;
        $query = "
		SELECT id 
		FROM $this->db_table_name 
		WHERE username = ?
		";
        $params = [$this->username];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return true;
        } else {
            return false;
        }
    }

    private function checkEmailExistance()
    {
        global $db;
        $query = "
		SELECT id 
		FROM $this->db_table_name 
		WHERE email = ?
		";
        $params = [$this->email];
        $getRow = $db->getRow($query, $params);
        if ($getRow) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserLessonsForView()
    {
        $les = $ids = '';
        $output = array();
        foreach ($this->lessons as $lesson) {
            $les .= $lesson->name . ", ";
            $ids .= $lesson->id . ",";
        }
        $output['names'] = substr($les, 0, -2);
        $output['ids'] = substr($ids, 0, -1);
        return $output;
    }
}
