<?php

class Profile
{
	// table name
	private static $db_table_name = "user_profile";
    private static $teacher_lessons_tbl;
	//attribs
	public $id;
	public $user_id;
	public $firstname;
	public $lastname;
	public $fullname;
	public $mobile_phone;
	public $phone;
	public $address;
	public $zip;
	public $city;
	
	private static function initDbTables()
    {
        self::$teacher_lessons_tbl   = $_SESSION["teacher_lessons_tbl"];
	}
	
	public function addProfile(){
		global $db;
        $query = "
		INSERT INTO ". self::$db_table_name ." (user_id, firstname, lastname, mobile_phone, phone, address, zip, city) 
		VALUES (?,?,?,?,?,?,?,?)
		";
		$params = [$this->user_id, $this->firstname, $this->lastname, $this->mobile_phone, $this->phone, $this->address, $this->zip, $this->city];
		$insertRow = $db->insertRow($query, $params);
		if ( $insertRow ) 
		{ 			
			return 0;              
		}
		else
		{
			return 1;
		}
	}

	public function updateProfile(){
		global $db;
        $query = "
		UPDATE ". self::$db_table_name ."
		SET firstname=?, lastname=? 
		WHERE user_id = ?
		";
		$params = [$this->firstname, $this->lastname, $this->user_id];
		$updateRow = $db->updateRow($query, $params);
		if ( $updateRow ) 
		{ 			
			return 0;              
		}
		else
		{
			return 1;
		}
	}

	public function updateFullProfile(){
		global $db;
		$query = "
		UPDATE ". self::$db_table_name ." 
		SET firstname=?, lastname=?, phone=?, mobile_phone=?, address=?, zip=?, city=? 
		WHERE user_id = ?
		";
		$params = [$this->firstname, $this->lastname, $this->phone, $this->mobile_phone, $this->address, $this->zip, $this->city ,$this->user_id];
		$updateRow = $db->updateRow($query, $params);
		if ( $updateRow ) 
		{ 			
			return 0;              
		}
		else
		{
			return 1;
		}
	}

	public function getUserProfile(){
		global $db;
		$query = "
		SELECT * 
		FROM ". self::$db_table_name ." 
		WHERE user_id=?
		";
        $params = [$this->user_id];
        $getRow = $db->getRow($query, $params);
		if($getRow)
		{
			$this->firstname 		= $getRow['firstname'];
			$this->lastname 		= $getRow['lastname'];
			$this->fullname 		= $this->getFullName();
			$this->mobile_phone 	= $getRow['mobile_phone'];
			$this->phone 			= $getRow['phone'];
			$this->address 			= $getRow['address'];
			$this->zip 				= $getRow['zip'];
			$this->city 			= $getRow['city'];
			return $this;
		}
	}

	public function getAllUsersProfile(){
		global $db;
		$query = "
		SELECT * 
		FROM ". self::$db_table_name ."
		ORDER BY lastname ASC
		";
		$getRows = $db->getRows($query);
		$output = array();
		if(	!empty($getRows) )
		{
			foreach ($getRows as $row) {
                $profile 				= new Profile;
				$profile->user_id 		= $row['user_id'];
				$profile->firstname		= $row['firstname'];
				$profile->lastname		= $row['lastname'];
				array_push($output, $profile);
            }
			return $output;
		}
	}

	public static function getUsersProfileByLesson($lesson_id){
        self::initDbTables();
		global $db;
		$query = "
		SELECT * 
		FROM ". self::$db_table_name ."
		LEFT JOIN ".self::$teacher_lessons_tbl." ON ".self::$db_table_name.".user_id = ".self::$teacher_lessons_tbl.".user_id
		WHERE ".self::$teacher_lessons_tbl.".lesson_id = ?
		ORDER BY lastname ASC
		";
		$params = [$lesson_id];
		$getRows = $db->getRows($query, $params);
		$output = array();
		if(	!empty($getRows) )
		{
			foreach ($getRows as $row) {
                $profile 				= new Profile;
				$profile->user_id 		= $row['user_id'];
				$profile->firstname		= $row['firstname'];
				$profile->lastname		= $row['lastname'];
				array_push($output, $profile);
            }
			return $output;
		}
	}

	public function getFullName()
    {
        return $this->lastname . " " . $this->firstname;
    } 
}
?>