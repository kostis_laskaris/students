<?php

class AppLists
{
	//table name
	private static $db_table_name;
	//attribs
	public $id, $html_id, $html_name, $list_name, $list_entry, $created_at, $updated_at, $appearance_name;

	private static function initDbTables()
	{
		if (!empty(self::$db_table_name)) {
			return false;
		}
		self::$db_table_name = $_SESSION['app_lists_tbl'];
	}

	public static function getListNames()
	{
		self::initDbTables();
		global $db;
		$query = "
		SELECT DISTINCT(appearance_name), list_name 
		FROM " . self::$db_table_name;

		$getRows = $db->getRows($query);
		$output = array();
		if (!empty($getRows)) {
			foreach ($getRows as $row) {
				$appList             		= new AppLists;
				$appList->list_name         = $row['list_name'];
				$appList->appearance_name	= $row['appearance_name'];
				array_push($output, $appList);
			}
		}
		return $output;
	}

	public function getListByNameAsSelect($selected_value)
	{
		self::initDbTables();
		global $db;
		$query = "
		SELECT * 
		FROM " . self::$db_table_name  . " 
		WHERE list_name = ?
		";
		$params = [$this->name];
		$getRows = $db->getRows($query, $params);
		$list = '<select class="custom-select custom-select-md" id="' . $this->html_id . '" name="' . $this->html_name . '" style="height: 35px;" required><option value="">Επιλογή</option>';
		if (!empty($getRows)) {
			foreach ($getRows as $row) {
				if ($selected_value == $row['list_entry'])
					$selected = 'selected';
				else
					$selected = '';
				$list .= '<option ' . $selected . ' value="' . $row['id'] . '">' . $row['list_entry'] . '</option>';
			}
		}
		$list .= '</select>';
		return $list;
	}

	public static function getListsDropdown($the_id)
	{
		self::initDbTables();
		$allListName = self::getListNames();
		$listDropdown = '<select class="custom-select custom-select-md" id="'.$the_id.'" name="'.$the_id.'"><option value="">Επιλογή Λίστας</option>';
		foreach ($allListName as $listEntry) {
			$listDropdown .= '<option value="' . $listEntry->list_name . '">' . $listEntry->appearance_name . '</option>';
		}
		$listDropdown .= '</select>';
		return $listDropdown;
	}

	public static function getListByName($name)
	{
		self::initDbTables();
		global $db;
		$query = "
		SELECT * 
		FROM " . self::$db_table_name  . " 
		WHERE list_name = ?
		";
		$params = [$name];
		$getRows = $db->getRows($query, $params);
		$output = array();
		if (!empty($getRows)) {
			foreach ($getRows as $row) {
				$appList             		= new AppLists;
				$appList->id         		= $row['id'];
				$appList->list_name			= $row['list_name'];
				$appList->appearance_name	= $row['appearance_name'];
				$appList->list_entry 		= $row['list_entry'];
				$appList->created_at 		= $row['created_at'];
				$appList->updated_at 		= $row['updated_at'];
				array_push($output, $appList);
			}
		}
		return $output;
	}

	public function getAllLists()
	{
		self::initDbTables();
		global $db;
		$query = "
		SELECT * 
		FROM  " . self::$db_table_name;
		$getRows = $db->getRows($query);
		$output = array();
		if (!empty($getRows)) {
			foreach ($getRows as $row) {
				$appList             		= new AppLists;
				$appList->id         		= $row['id'];
				$appList->list_name			= $row['list_name'];
				$appList->appearance_name	= $row['appearance_name'];
				$appList->list_entry 		= $row['list_entry'];
				$appList->created_at 		= $row['created_at'];
				$appList->updated_at 		= $row['updated_at'];
				array_push($output, $appList);
			}
		}
		return $output;
	}

	public function addListEntry()
	{
		self::initDbTables();
		global $db;
		$query = "
		INSERT INTO " . self::$db_table_name  . " (list_name, appearance_name, list_entry, created_at, updated_at) 
		VALUES (?, ?, ?, ?, ?)
		";
		$params = [$this->list_name, $this->appearance_name, $this->list_entry, $this->created_at, $this->updated_at];
		$insertRow = $db->insertRow($query, $params);
		if ($insertRow) {
			return 0;
		} else {
			return 1;
		}
	}

	public static function getListEntryById($id)
	{
		self::initDbTables();
		global $db;
		$query = "
		SELECT * 
		FROM " . self::$db_table_name  . " 
		WHERE id = ?
		";
		$params = [$id];
		$getRow = $db->getRow($query, $params);
		if ($getRow) {
			$appList             		= new AppLists;
			$appList->id         		= $getRow['id'];
			$appList->list_name			= $getRow['list_name'];
			$appList->appearance_name	= $getRow['appearance_name'];
			$appList->list_entry 		= $getRow['list_entry'];
			$appList->created_at 		= $getRow['created_at'];
			$appList->updated_at 		= $getRow['updated_at'];
			return $appList;
		}
	}

	public function updateList()
	{
		self::initDbTables();
		global $db;
		$query = "
		UPDATE " . self::$db_table_name  . " 
		SET list_entry=?, updated_at=? 
		WHERE id = ?
		";
		$params = [$this->list_entry, $this->updated_at, $this->id];
		$updateRow = $db->updateRow($query, $params);
		if ($updateRow) {
			return 0;
		} else {
			return 1;
		}
	}

	public static function deleteList($id)
	{
		if( Grade::gradeByAppListIdExists($id) ){
			return 2;
		}elseif( Student::studentByAppListIdExists($id) ){
			return 3;
		}elseif( Tmima::tmimaByAppListIdExists($id) ){
			return 4;
		}
		self::initDbTables();
		global $db;
		$query = "
		DELETE FROM " . self::$db_table_name  . " 
		WHERE id = ?
		";
		$params = [$id];
		$deleteRows = $db->deleteRow($query, $params);
		if ($deleteRows) {
			return 0;
		} else {
			return 1;
		}
	}
}
