<?php
class Database
{
    public $isConnected;
    protected $database;
    protected $host = "localhost";
    protected $db_name = "students";
    protected $username = "politopo_apopapa";
    protected $password = "#P0litropo135$";

    public function __construct($options = [])
    {
        try {
            $this->database = new PDO("mysql:host={$this->host};dbname={$this->db_name};charset=utf8", $this->username, $this->password, $options);
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->isConnected = true;
            $this->logLastActivity();
            date_default_timezone_set('Europe/Athens');
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function disconnect()
    {
        $this->database     = null;
        $this->isConnected  = false;
    }

    public function lastInsertedId()
    {
        return $this->database->lastInsertId();
    }

    public function getRow($query, $params = [])
    {
        try {
            $stmt = $this->database->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch();
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function getRows($query, $params = [])
    {
        try {
            $stmt = $this->database->prepare($query);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function insertRow($query, $params = [])
    {
        try {
            $stmt = $this->database->prepare($query);
            $stmt->execute($params);
            return true;
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function updateRow($query, $params = [])
    {
        try {
            $stmt = $this->database->prepare($query);
            $stmt->execute($params);
            return true;
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function deleteRow($query, $params = [])
    {
        try {
            $stmt = $this->database->prepare($query);
            $stmt->execute($params);
            return true;
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    public function executeQuery($query)
    {
        try {
            $this->database->query($query);
            return true;
        } catch (PDOException $e) {
            include_once  PROJECT_ROOT . '/objects/error.php';
            $error          = new Error_Log;
            $error->page    = $_SERVER['HTTP_REFERER'];
            $error->kind    = 'Database Error';
            $error->writeLog($e->errorInfo);
            return false;
        }
    }

    private function logLastActivity()
    {
        if (isset($_SESSION['username'])) {
            $last_date  = date("Y-m-d H:i:s");
            $query      = "UPDATE register SET last_activity_date = ? WHERE username = ?;";
            $params     = [$last_date, $_SESSION['username']];
            $stmt       = $this->database->prepare($query);
            $stmt->execute($params);
        }
    }
}
$db = new Database();
