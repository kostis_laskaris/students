<?php
class Query
{
    public $the_query;

    public function select(...$args)
    {
        $params = implode(", ", $args);
        $this->the_query .= "SELECT $params";
    }

    public function from(...$args)
    {
        $params = implode(", ", $args);
        $this->the_query .= " FROM $params";
    }

    public function where1()
    {
        $this->the_query .= ' WHERE 1';
    }

    public function where($firstArg, $operator, $lastArg)
    {
        $this->the_query .= ' WHERE ';
        if ($operator == '')
            $this->the_query .= $firstArg . ' = ' . $lastArg;
        else
            $this->the_query .= $firstArg . ' ' . $operator . " " . $lastArg;
    }

    public function whereAnd($firstArg, $operator, $lastArg)
    {
        $this->the_query .= " AND ";
        if ($operator == '')
            $this->the_query .= $firstArg . ' = ' . $lastArg;
        else
            $this->the_query .= $firstArg . ' ' . $operator . " " . $lastArg;
    }

    public function whereOr($firstArg, $operator, $lastArg)
    {
        $this->the_query .= " OR ";
        if ($operator == '')
            $this->the_query .= $firstArg . ' = ' . $lastArg;
        else
            $this->the_query .= $firstArg . ' ' . $operator . " " . $lastArg;
    }

    public function leftJoin($table, $arg1, $arg2)
    {
        $this->the_query .= " LEFT JOIN " . $table . " ON " . $arg1 . " = " . $arg2;
    }

    public function orderBy($firstArg, $orderKind)
    {
        $this->the_query .= " ORDER BY $firstArg $orderKind";
    }

    public function groupBy($value)
    {
        $this->the_query .= " GROUP BY $value";
    }

    public function limit($num)
    {
        $this->the_query .= " LIMIT $num ";
    }
}
