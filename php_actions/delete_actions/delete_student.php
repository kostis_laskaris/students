<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['delete_csrf_token']) ){
	$id 			= sanitizeField($_POST['studentDeleteId']);
	$student		= new Student;
	$student->id 	= $id;
	$submitted 		= $student->deleteStudent();
	if($submitted == 0)
	{
		$_SESSION['did'] = createInfoMessage('success', 'Ο μαθητής διαγράφηκε με επιτυχία!');		
	}
	elseif($submitted == 1)
	{
		$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
	}

	redirectTo('../../admin-app/manage-students.php');	
}else{
	redirectTo('../../401.php');	
}

?>