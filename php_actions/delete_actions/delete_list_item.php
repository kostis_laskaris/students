<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['delete_csrf_token']) ){
	$id 			= sanitizeField($_POST['listItemDeleteId']);
	$appList		= new AppLists;
	$appList->id 	= $id;
	$submitted 		= $appList->deleteList();
	if($submitted == 0)
	{
		$_SESSION['did'] = createInfoMessage('success', 'Το στοιχείο λίστας διαγράφηκε με επιτυχία!');		
	}
	elseif($submitted == 1)
	{
		$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
	}

	redirectTo('../../admin-app/manage-lists.php');	
}else{
	redirectTo('../../401.php');	
}

?>