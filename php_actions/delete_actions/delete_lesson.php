<?php
session_start();
include("../../init.php");

if (isLoggedIn() && isAdmin() && validateCsrfToken($_POST['delete_csrf_token'])) {
    $id 			= sanitizeField($_POST['lessonDeleteId']);
    $lesson		= new Lesson;
    $lesson->id 	= $id;
    $submitted 		= $lesson->deleteLesson();
    if ($submitted == 0) {
        $_SESSION['did'] = createInfoMessage('success', 'Το μάθημα διαγράφηκε με επιτυχία!');
    } elseif ($submitted == 1) {
        $_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
    } elseif ($submitted == 2) {
        $_SESSION['did'] = createInfoMessage('danger', 'Βρέθηκε ανάθεση του μαθήματος σε μαθητή. Για να πραγματοποιηθεί η διαγραφή παρακαλώ αφαιρέστε το μάθημα από τον μαθητή.');
    } elseif ($submitted == 3) {
        $_SESSION['did'] = createInfoMessage('danger', 'Βρέθηκε βαθμολογία για αυτό μάθημα. Για να πραγματοποιηθεί η διαγραφή παρακαλώ διαγράψτε την βαθμολογία.');
    } elseif ($submitted == 4) {
        $_SESSION['did'] = createInfoMessage('danger', 'Βρέθηκε ανάθεση του μαθήματος σε καθηγητή. Για να πραγματοποιηθεί η διαγραφή παρακαλώ διαγράψτε την ανάθεση.');
    }
    redirectTo('../../admin-app/manage-lessons.php');
} else {
    redirectTo('../../401.php');
}
// 0 = ok, 2 = student lesson exists, 3 = grade exist, 4 = teacher lesson exist
