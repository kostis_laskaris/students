<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['delete_csrf_token']) ){
	$id 			= sanitizeField($_POST['shortcutDeleteId']);
	$shortcut		= new Shortcut;
	$shortcut->id 	= $id;
	$submitted 		= $shortcut->deleteShortcut();
	if($submitted == 0)
	{
		$_SESSION['did'] = createInfoMessage('success', 'Η συντόμευση διαγράφηκε με επιτυχία!');		
	}
	elseif($submitted == 1)
	{
		$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
	}

	redirectTo('../../admin-app/manage-shortcuts.php');	
}else{
	redirectTo('../../401.php');	
}

?>