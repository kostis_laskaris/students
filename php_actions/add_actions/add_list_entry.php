<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['add_csrf_token']) ){

	$validator = new Validator;

	$validation = $validator->make($_POST + $_FILES, [
		'listEntryName'   				=> 'required',
		'listModalDropdown' 			=> 'required',
		'listEntryAddAppearanceName' 	=> 'required'
	]);

	$validation->setAlias('listEntryName', 'Όνομα Στοιχείου Λίστας');
	$validation->setAlias('listModalDropdown', 'Όνομα Λίστας');
	$validation->setAlias('listEntryAddAppearanceName', 'Εμφανιζόμενο Όνομα Λίστας');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$appList 					= new AppLists;
		$appList->list_name 		= sanitizeField($_POST['listModalDropdown']);
		$appList->list_entry 		= sanitizeField($_POST['listEntryName']);
		$appList->appearance_name 	= sanitizeField($_POST['listEntryAddAppearanceName']);
		$appList->created_at 		= date('Y-m-d H:i:s');
		$appList->updated_at 		= date('Y-m-d H:i:s');

		$submitted = $appList->addListEntry();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Το στοιχείο λίστας αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}	
	}
	redirectTo('../../admin-app/manage-lists.php');
}else{
	redirectTo('../../401.php');	
}
?>