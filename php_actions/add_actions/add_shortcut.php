<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',		
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'shortcut_lessons'   => 'array',
		'addShortcutName'    => 'required|max:30'
	]);

	$validation->setAlias('shortcut_lessons', 'Μαθήματα');
	$validation->setAlias('addShortcutName', 'Τίτλος Συντόμευσης');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$shortcut 				= new Shortcut;
		$shortcut->name 		= sanitizeField($_POST['addShortcutName']);
		$shortcut->lessons 		= $_POST['shortcut_lessons'];
		$shortcut->created_at 	= date('Y-m-d H:i:s');
		$shortcut->updated_at 	= date('Y-m-d H:i:s');

		$submitted = $shortcut->addShortcut();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Η συντόμευση αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}	
	}
	redirectTo('../../admin-app/manage-shortcuts.php');
}else{
	redirectTo('../../401.php');	
}
?>