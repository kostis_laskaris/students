<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;

	$validation = $validator->make($_POST + $_FILES, [
		'user_tmimata'   => 'array'
	]);

	$validation->setAlias('user_tmimata', 'Τμήματα');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$user 					= new User(sanitizeField($_POST['addUsername']));
		$user->id 				= sanitizeField($_POST['addUserId']);
		$user->tmimata 			= $_POST['user_tmimata'];

		$submitted = $user->addUserTmimata();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Η ανάθεση εκτελέστηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}	
	}
	redirectTo('../../admin-app/tmima-assignment.php');
}else{
	redirectTo('../../401.php');	
}
?>