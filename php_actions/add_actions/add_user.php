<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'email' => 'Το email έχει λάθος μορφή',
		'same' => 'Οι κωδικοί δεν είναι ίδιοι',
		'regex' => 'Ο κωδικός πρέπει να είναι τουλάχιστον 8 χαρακτήρων, να περιέχει τουλάχιστον 1 λατινικό γράμμα, τουλάχιστον 1 αριθμό και τουλάχιστον 1 από τα σύμβολα #?!@$%^&*-',
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'addUsername'   => 'required',
		'addEmail'      => 'email',
		'addPassword'   => 'required|min:8|same:addRePass|regex:/^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
		'addPermission'	=> 'required',
		'addStatus'     => 'required|numeric',
		'addFirstname'  => 'required|max:50',
		'addLastname'	=> 'required|max:50',
	]);

	$validation->setAlias('addUsername', 'Username');
	$validation->setAlias('addPassword', 'Κωδικός');
	$validation->setAlias('addPermission', 'Δικαιώματα');
	$validation->setAlias('addStatus', 'Κατάσταση');
	$validation->setAlias('addEmail', 'Email');
	$validation->setAlias('addFirstname', 'Όνομα');
	$validation->setAlias('addLastname', 'Επίθετο');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$username 			= sanitizeField($_POST['addUsername']);
		$user 				= new User($username);
		$profile 			= new Profile();
		$user->email 		= sanitizeField($_POST['addEmail']);
		$user->password 	= sanitizeField($_POST['addPassword']);
		$user->rePassword 	= sanitizeField($_POST['addRePass']);
		$user->permission 	= sanitizeField($_POST['addPermission']);
		$user->status 		= sanitizeField($_POST['addStatus']);
		$profile->firstname = sanitizeField($_POST['addFirstname']);
		$profile->lastname  = sanitizeField($_POST['addLastname']);
		$user->profile 		= $profile;

		$submitted = $user->addUser();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Ο χρήστης αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}
		elseif($submitted == 2)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Το username <b>' . $user->username . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.');
		}
		elseif($submitted == 3)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Το email <b>' . $user->email . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.');		
		}
	}
	redirectTo('../../admin-app/user-management.php');
}else{
	redirectTo('../../401.php');	
}
?>