<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',		
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'tmimaName'   => 'required|max:100',
		'className'   => 'required',
		'comments'    => 'max:250'
	]);

	$validation->setAlias('tmimaName', 'Όνομα Τμήματος');
	$validation->setAlias('className', 'Τάξη');
	$validation->setAlias('comments', 'Σχόλια');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$tmima 				= new Tmima;
		$tmima->name 		= sanitizeField($_POST['tmimaName']);
		$tmima->class 		= sanitizeField($_POST['className']);
		$tmima->comments 	= sanitizeField($_POST['comments']);
		$tmima->created_at 	= date('Y-m-d H:i:s');
		$tmima->updated_at  = date('Y-m-d H:i:s');
		

		$submitted = $tmima->addClass();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Το τμήμα αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}		
	}
	redirectTo('../../admin-app/add-class.php');
}else{
	redirectTo('../../401.php');	
}
?>