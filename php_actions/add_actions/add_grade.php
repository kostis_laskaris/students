<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' 			=> 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
		'email' 			=> 'Το email έχει λάθος μορφή',
		'max' 				=> 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
		'min' 				=> 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
		'digits' 			=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
		'date' 				=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
        'digits_between'	=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 1 έως 3 ψηφία',
        'integer'           => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο ακέραιους',
        'before'            => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο προηγούμενες, από την τρέχουσα, ημερομηνίες',
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'student_name'  => 'required',
		'class'         => 'required',
		'tmima_id'   	=> 'required',
		'lesson'		=> 'required',
		'absence'     	=> 'required',
		'grade'  		=> 'digits_between:1,3|integer|required',
		'exam_kind'		=> 'required',
		'exam_date'  	=> 'required|date|before:'. date('Y-m-d',strtotime("+1 days")),
		'comments'		=> 'max:300'		
	]);

	$validation->setAlias('student_name', 'Μαθητής');
	$validation->setAlias('class', 'Τάξη');
	$validation->setAlias('tmima_id', 'Τμήμα');
	$validation->setAlias('lesson', 'Μάθημα');
	$validation->setAlias('absence', 'Παρουσία');
	$validation->setAlias('grade', 'Βαθμός');
	$validation->setAlias('exam_kind', 'Είδος Εξέτασης');
	$validation->setAlias('exam_date', 'Ημερομηνία Εξέτασης');
    $validation->setAlias('comments', 'Σχόλια');
    	
	$validation->validate();

	if ($validation->fails() || sanitizeField($_POST['grade']) > 100) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
        }
        if(sanitizeField($_POST['grade']) > 100)
        $tmp .= '<li>Το πεδίο <b>"Βαθμός"</b> παίρνει τιμές από 0 έως 100</li>';
        $tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$grade 				    = new Grade();
		$grade->lesson_id		= sanitizeField($_POST['lesson']);
		$grade->user_id 		= $_SESSION['user_id'];
		$grade->student_id 		= sanitizeField($_POST['student_name']);
		$grade->class 		    = sanitizeField($_POST['class']);
		$grade->tmima 			= sanitizeField($_POST['tmima_id']);		
		$grade->exam_date		= sanitizeField($_POST['exam_date']);
		$grade->exam_kind 		= sanitizeField($_POST['exam_kind']);	
		$grade->absence		    = sanitizeField($_POST['absence']);
		$grade->grade 		    = sanitizeField($_POST['grade']);	
		$grade->comments 		= sanitizeField($_POST['comments']);		
		$grade->created_at 	    = date('Y-m-d H:i:s');
		$grade->updated_at 	    = date('Y-m-d H:i:s');

		$submitted = $grade->addGrade();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Η βαθμολογία αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}		
	}
	redirectTo('../../admin-app/add-grade.php');
}else{
	redirectTo('../../401.php');	
}
?>