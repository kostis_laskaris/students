<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' 			=> 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
		'email' 			=> 'Το email έχει λάθος μορφή',
		'max' 				=> 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
		'min' 				=> 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
		'digits' 			=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
		'zip:digits'		=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 5 ψηφία',
		'date' 				=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
		'digits_between'	=> 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 έως 22 ψηφία',
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'firstname'   	=> 'required|max:50',
		'lastname'      => 'required|max:50',
		'address'   	=> 'max:200',
		'perioxi'		=> 'max:100',
		'zip'     		=> 'digits:5',
		'mobile'  		=> 'digits:10',
		'phone'			=> 'digits:10',
		'email'  		=> 'max:200|email',
		'sub_date'		=> 'required|date',
		'mname'   		=> 'max:50',
		'faname'      	=> 'max:50',
		'mphone'   		=> 'digits:10',
		'fphone'      	=> 'digits:10',
		'email_kidemona'     => 'max:150|email',
		'kid_name'     	=> 'max:100',
		'kid_phone'     => 'min:10|max:21|regex:/^[0-9]{10,10}([,][0-9]{10,10})?$/',
		'fee'     	=> 'digits_between:2,5',
		'tmima'     	=> 'required',
		'sx_etos'   => 'required',
		'periodos'     	=> 'required',
		'class'     	=> 'required',
		'lesson_ids'    => 'required',
	]);

	$validation->setAlias('firstname', 'Όνομα');
	$validation->setAlias('lastname', 'Επίθετο');
	$validation->setAlias('address', 'Διεύθυνση Κατοικίας');
	$validation->setAlias('perioxi', 'Περιοχή');
	$validation->setAlias('zip', 'Τ.Κ.');
	$validation->setAlias('mobile', 'Τηλέφωνο Μαθητή');
	$validation->setAlias('phone', 'Σταθερό Τηλέφωνο');
	$validation->setAlias('email', 'Email Μαθητή');
	$validation->setAlias('sub_date', 'Ημερομηνία Εγγραφής');
	$validation->setAlias('mname', 'Όνομα Μητέρας');
	$validation->setAlias('faname', 'Όνομα Πατέρα');
	$validation->setAlias('mphone', 'Τηλέφωνο Μητέρας');
	$validation->setAlias('fphone', 'Τηλέφωνο Πατέρα');
	$validation->setAlias('email_kidemona', 'Email Κηδεμόνα');
	$validation->setAlias('kid_name', 'Όνομα Κηδεμόνα');
	$validation->setAlias('kid_phone', 'Τηλέφωνο Κηδεμόνα');
	$validation->setAlias('fee', 'Δίδακτρα');
	$validation->setAlias('tmima', 'Τμήμα');
	$validation->setAlias('sx_etos', 'Σχολικό Έτος');
	$validation->setAlias('periodos', 'Περίοδος');
	$validation->setAlias('class', 'Τάξη');
	$validation->setAlias('lesson_ids', 'Μαθήματα');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$student 				 = new Student;
		$student->firstname		 = sanitizeField($_POST['firstname']);
		$student->lastname 		 = sanitizeField($_POST['lastname']);
		$student->address 		 = sanitizeField($_POST['address']);
		$student->perioxi 		 = sanitizeField($_POST['perioxi']);
		$student->zip 			 = sanitizeField($_POST['zip']);		
		$student->mobile		 = sanitizeField($_POST['mobile']);
		$student->phone 		 = sanitizeField($_POST['phone']);
		$student->email 		 = sanitizeField($_POST['email']);
		$student->date_eggrafis  = sanitizeField($_POST['sub_date']);
		$student->mname 		 = sanitizeField($_POST['mname']);		
		$student->faname		 = sanitizeField($_POST['faname']);
		$student->mphone 		 = sanitizeField($_POST['mphone']);
		$student->fphone 		 = sanitizeField($_POST['fphone']);
		$student->email_kidemona = sanitizeField($_POST['email_kidemona']);
		$student->kid_name 		 = sanitizeField($_POST['kid_name']);	
		$student->kid_phone 	 = sanitizeField($_POST['kid_phone']);
		$student->fee 			 = sanitizeField($_POST['fee']);
		$student->tmima 		 = explode(":", sanitizeField($_POST['tmima']))[1];
		$student->sx_etos 		 = sanitizeField($_POST['sx_etos']);	
		$student->periodos 		 = sanitizeField($_POST['periodos']);
		$student->class 		 = sanitizeField($_POST['class']);
		$student->informed_by 	 = sanitizeField($_POST['informed_by']);
		$student->lessons 		 = explode(",", sanitizeField($_POST['lesson_ids']));
		$student->created_at 	 = date('Y-m-d H:i:s');
		$student->updated_at 	 = date('Y-m-d H:i:s');

		$submitted = $student->addStudent();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Ο χρήστης αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}		
	}
	redirectTo('../../admin-app/add-student.php');
}else{
	redirectTo('../../401.php');	
}
?>