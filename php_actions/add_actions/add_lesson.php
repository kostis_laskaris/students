<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',		
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'addLessonName'   => 'required|max:40',
		'addLessonDesc'   => 'required|max:10'
	]);

	$validation->setAlias('addLessonName', 'Όνομα Μαθήματος');
	$validation->setAlias('addLessonDesc', 'Κωδικός Μαθήματος');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$lesson 				= new Lesson;
		$lesson->name 			= sanitizeField($_POST['addLessonName']);
		$lesson->description 	= sanitizeField($_POST['addLessonDesc']);
		$lesson->created_at 	= date('Y-m-d H:i:s');
		$lesson->updated_at 	= date('Y-m-d H:i:s');
		

		$submitted = $lesson->addLesson();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Το μάθημα αποθηκεύτηκε με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}	
		elseif($submitted == 2)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Το μάθημα με κωδικό <b>' . $lesson->description . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.');
		}		
	}
	redirectTo('../../admin-app/manage-lessons.php');
}else{
	redirectTo('../../401.php');	
}
?>