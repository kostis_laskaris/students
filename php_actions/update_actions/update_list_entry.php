<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token_edit_list']) ){

	$validator = new Validator;

	$validation = $validator->make($_POST + $_FILES, [
		'listItemEditId'   => 'required',
		'updateListItemName' => 'required'
	]);

	$validation->setAlias('updateListItemName', 'Όνομα Στοιχείου Λίστας');
	$validation->setAlias('listItemEditId', 'ID Λίστας');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$appList 				= new AppLists;
		$appList->id 			= sanitizeField($_POST['listItemEditId']);
		$appList->list_entry 	= sanitizeField($_POST['updateListItemName']);
		$appList->updated_at 	= date('Y-m-d H:i:s');

		$submitted = $appList->updateList();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}	
	}
	redirectTo('../../admin-app/manage-lists.php');
}else{
	redirectTo('../../401.php');	
}
?>