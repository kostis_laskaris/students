<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && validateCsrfToken($_POST['csrf_token']) ){
	$validator = new Validator;

	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'email' => 'Το email έχει λάθος μορφή',
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες',
		'numeric' => 'Το πεδίο ":attribute" δέχεται μόνο αριθμούς'
	]);

	$validation = $validator->make($_POST + $_FILES, [		
		'firstname'  => 'required|max:50',
		'lastname'	 => 'required|max:50',
		'email'      => 'required|email|max:150',
		'phone'   	 => 'numeric|max:10',
		'mobile' 	 => 'numeric|max:10',
		'address'    => 'max:150',
		'zip'     	 => 'numeric|max:5|min:5',
		'city'     	 => 'max:100',
	]);

	$validation->setAlias('firstname', 'Όνομα');
	$validation->setAlias('lastname', 'Επίθετο');
	$validation->setAlias('phone', 'Τηλέφωνο');
	$validation->setAlias('mobile', 'Κινητό Τηλέφωνο');
	$validation->setAlias('address', 'Διεύθυνση');
	$validation->setAlias('zip', 'Τ.Κ');
	$validation->setAlias('city', 'Πόλη');
	$validation->setAlias('email', 'Email');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	}
	else 
	{
		$username 				= $_SESSION['username'];
		$user 					= new User($username);
		$profile 				= new Profile();
		$user->id 				= sanitizeField($_POST['userId']);
		$user->email 			= sanitizeField($_POST['email']);		
		$profile->firstname 	= sanitizeField($_POST['firstname']);
		$profile->lastname  	= sanitizeField($_POST['lastname']);
		$profile->phone 		= sanitizeField($_POST['phone']);
		$profile->mobile_phone  = sanitizeField($_POST['mobile']);
		$profile->address 		= sanitizeField($_POST['address']);
		$profile->zip  			= sanitizeField($_POST['zip']);
		$profile->city  		= sanitizeField($_POST['city']);
		$user->profile 			= $profile;

		$submitted = $user->updateUserAndProfile();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία');		
		}
		else
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}		
	}
	redirectTo('../../admin-app/user-profile.php'); 
}else{
	redirectTo('../../401.php');	
}
?>