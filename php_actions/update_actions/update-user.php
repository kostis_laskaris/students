<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token_edit_user']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'email' => 'Το email έχει λάθος μορφή',
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'username'   => 'required',
		'email'      => 'email',
		'permission' => 'required',
		'status'     => 'required|numeric',
		'firstname'  => 'required|max:50',
		'lastname'	 => 'required|max:50'
	]);

	$validation->setAlias('username', 'Username');
	$validation->setAlias('permission', 'Δικαιώματα');
	$validation->setAlias('status', 'Κατάσταση');
	$validation->setAlias('email', 'Email');
	$validation->setAlias('firstname', 'Όνομα');
	$validation->setAlias('lastname', 'Επίθετο');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		// validation passes
		$username 			= sanitizeField($_POST['username']);
		$user 				= new User($username);
		$profile 			= new Profile();
		$user->id 			= sanitizeField($_POST['editUserId']);
		$user->email 		= sanitizeField($_POST['email']);
		$user->permission 	= sanitizeField($_POST['permission']);
		$user->status 		= sanitizeField($_POST['status']);
		$profile->firstname = sanitizeField($_POST['firstname']);
		$profile->lastname  = sanitizeField($_POST['lastname']);
		$user->profile 		= $profile;

		$submitted = $user->updateUser();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία');		
		}
		else
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}		
	}
	redirectTo('../../admin-app/user-management.php');
}else{
	redirectTo('../../401.php');	
}
?>