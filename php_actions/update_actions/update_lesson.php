<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token_edit_lesson']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'updateLessonName'   => 'required|max:40',
		'updateLessonDesc'   => 'required|max:10'
	]);

	$validation->setAlias('updateLessonName', 'Όνομα Μαθήματος');
	$validation->setAlias('updateLessonDesc', 'Κωδικός Μαθήματος');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		$lesson 			 = new Lesson;
		$lesson->id 		 = sanitizeField($_POST['lessonEditId']);
		$lesson->name 		 = sanitizeField($_POST['updateLessonName']);
		$lesson->description = sanitizeField($_POST['updateLessonDesc']);
		$lesson->updated_at  = date('Y-m-d H:i:s');

		$submitted = $lesson->updateLesson();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}
		elseif($submitted == 2)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Το μάθημα με κωδικό <b>' . $lesson->description . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.');
		}		
	}
	redirectTo('../../admin-app/manage-lessons.php');
}else{
	redirectTo('../../401.php');	
}
?>