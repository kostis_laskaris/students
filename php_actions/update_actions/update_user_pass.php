<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && validateCsrfToken($_POST['csrf_token']) ){
	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'same' => 'Οι κωδικοί δεν είναι ίδιοι',
		'regex' => 'Ο κωδικός πρέπει να είναι τουλάχιστον 8 χαρακτήρων, να περιέχει τουλάχιστον 1 λατινικό γράμμα, τουλάχιστον 1 αριθμό και τουλάχιστον 1 από τα σύμβολα #?!@$%^&*-',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'reNewPass'	=> 'required',
		'newPass'	=> 'required|min:8|same:reNewPass|regex:/^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/'
	]);

	$validation->setAlias('reNewPass', 'Επαλήθευση Κωδικού');
	$validation->setAlias('newPass', 'Κωδικός');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	}

	else 
	{
		$id = sanitizeField($_POST['passUserId']);
		$usrname 			= sanitizeField($_POST['passUsername']);
		$user 				= new User($usrname);
		$user->id 			= $id;
		$user->password 	= sanitizeField($_POST['newPass']);

		$submitted = $user->updatePass();

		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Ο κωδικός άλλαξε με επιτυχία!');		
		}
		else
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}
	}
		redirectTo('../../admin-app/user-management.php');
}else{
	redirectTo('../../401.php');	
}
?>