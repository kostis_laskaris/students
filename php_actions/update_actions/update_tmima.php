<?php
session_start();
require('../../vendor/autoload.php');
include("../../init.php");
use Rakit\Validation\Validator;

if( isLoggedIn() && isAdmin() && validateCsrfToken($_POST['csrf_token_edit_tmima']) ){

	$validator = new Validator;
	$validator->setMessages([
		'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
		'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
		'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
	]);

	$validation = $validator->make($_POST + $_FILES, [
		'editTmima'   => 'required|max:100',
		'editTaksi'   => 'required',
		'comments'	  => 'max:250'
	]);

	$validation->setAlias('editTmima', 'Όνομα Τμήματος');
	$validation->setAlias('editTaksi', 'Τάξη');
	$validation->setAlias('comments', 'Σχόλια');
	
	$validation->validate();

	if ($validation->fails()) 
	{
		$errors = $validation->errors();
		$messages = $errors->all();
		$tmp = '<ul>';
		foreach ($messages as $message) {
			$tmp.= '<li>'.$message.'</li>';
		}
		$tmp .= '</ul>';
		$_SESSION['did'] = createInfoMessage('danger', $tmp);
	} 
	else 
	{
		$tmima 			 	= new Tmima;
		$tmima->id 		 	= sanitizeField($_POST['classId']);
		$tmima->name 		= sanitizeField($_POST['editTmima']);
		$tmima->class 	  	= sanitizeField($_POST['editTaksi']);
		$tmima->comments  	= sanitizeField($_POST['comments']);
		$tmima->updated_at  = date('Y-m-d H:i:s');

		$submitted = $tmima->updateTmima();	
		if($submitted == 0)
		{
			$_SESSION['did'] = createInfoMessage('success', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία');		
		}
		elseif($submitted == 1)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.');
		}
		elseif($submitted == 2)
		{
			$_SESSION['did'] = createInfoMessage('danger', 'Το τμήμα <b>' . $tmima->name . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.');
		}		
	}
	redirectTo('../../admin-app/manage-classes.php');
}else{
	redirectTo('../../401.php');	
}
?>