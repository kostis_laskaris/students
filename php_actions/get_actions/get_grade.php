<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$id 			= sanitizeField($_GET['gradeId']);
	echo json_encode(Grade::getGradeById($id), JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}
