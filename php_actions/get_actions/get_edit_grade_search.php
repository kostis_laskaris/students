<?php
session_start();
include("../../init.php");
$csrf = getallheaders()['csrf_token'];

if (isLoggedIn() && isAdmin() && validateCsrfToken($csrf)) {	
    $searchUser = sanitizeField($_GET['searchUser']);
	$searchAbsence = sanitizeField($_GET['searchAbsence']);
    $searchExam_kind = sanitizeField($_GET['searchExam_kind']);
	$searchDate_from = sanitizeField($_GET['searchDate_from']);
    $searchDate_to = sanitizeField($_GET['searchDate_to']);
	$searchTelestis = $_GET['searchTelestis'];
    $searchGrade = sanitizeField($_GET['searchGrade']);
    $lesson_ids = sanitizeField($_GET['lesson_ids']);
    $studentId = sanitizeField($_GET['searchStudent']);
    
    $query = new Query;
    $query->select('*');
    $query->from($_SESSION['full_grades_tbl']);
    $query->where1();

    if(!empty($searchUser))
        $query->whereAnd('user_id', '', $searchUser);
    if(!empty($searchAbsence))
        $query->whereAnd('absence', '', "'". $searchAbsence . "'");
    if(!empty($searchExam_kind))
        $query->whereAnd('exam_kind', '', "'". $searchExam_kind . "'");
    if(!empty($searchDate_from))
        $query->whereAnd('exam_date', '>=', "'". $searchDate_from . "'");
    if(!empty($searchDate_to))
        $query->whereAnd('exam_date', '<=', "'". $searchDate_to . "'");
    if(!empty($searchTelestis) && !empty($searchGrade))
        $query->whereAnd('grade', $searchTelestis, $searchGrade);
    if(!empty($studentId))
        $query->whereAnd('student_id', '', $studentId);
    if(!empty($lesson_ids)){
        fixLessonSearch($lesson_ids, $query);
    }
    $grade = new Grade;
    $response = new AjaxController(200, $grade->query($query->the_query));
    AjaxController::spitResponse($response);
}else{
    $response = new AjaxController(698, null,'Σφάλμα κατά την αυθεντικοποίηση της φόρμας');
    AjaxController::spitResponse($response);
}

function fixLessonSearch($value, $query)
{
    $cnt=0;
    $ids = explode(',', $value);
    $is_more_one = (sizeof($ids) > 1) ? true : false;
    $flag = true;
    foreach ($ids as $id) {
        if ($flag) {
            if ($is_more_one)
                $query->whereAnd('(lesson_id', '', sanitizeField($id));
            else
                $query->whereAnd('lesson_id', '', sanitizeField($id));
            $cnt = false;
            $flag = false;
        } else {
            $query->whereOr('lesson_id', '', sanitizeField($id));
        }
    }
    if ($is_more_one)
        $query->the_query .= ")";
}