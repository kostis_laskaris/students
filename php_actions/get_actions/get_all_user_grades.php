<?php
session_start();
include("../../init.php");

if (isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token'])) {
    $output         = array();
    $grade		    = new Grade;
    $grade->user_id = $_SESSION['user_id'];
    $userGrades     = $grade->getAllGradesByUserId();
    $statsGrades    = $grade->getStatsPerStudentByUserId();
    $output['allGrades'] = $userGrades;
    $output['gradesStats'] = $statsGrades;
    echo json_encode($output, JSON_UNESCAPED_UNICODE);
} else {
    echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
    exit();
}
