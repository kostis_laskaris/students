<?php
session_start();
include("../../init.php");
$csrf = getallheaders()['csrf_token'];

if (isLoggedIn() && isAdmin() && validateCsrfToken($csrf)) {
    $output     = array();
    $grade      = new Grade;
    $grade->lesson_id = sanitizeField($_GET['lessonId']);
    $grade->student_id = sanitizeField($_GET['studentId']);
    $response   = new AjaxController(200, $grade->getGradesPerStudentAndLesson());
    AjaxController::spitResponse($response);
} else {
    $response   = new AjaxController(698, null,'Σφάλμα κατά την αυθεντικοποίηση της φόρμας');
    AjaxController::spitResponse($response);
}
