<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$id 		= sanitizeField($_GET['shortcutId']);
	$shortcut		= new Shortcut;
	$shortcut->id 	= $id;
	$output 		= $shortcut->getShortcutById();
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

?>