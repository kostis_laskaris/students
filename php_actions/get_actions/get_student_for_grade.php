<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$id 			= sanitizeField($_GET['studentId']);
	$student		= new Student();
	$student->id 	= $id;
	$output = array();
    $output['class'] 			= $student->getStudentClass();
	$output['tmima'] 			= $student->getStudentTmima();
	$output['lessons'] 			= removeArrayDuplicates($student->getStudentLessons(), User::getUserLessons( $_SESSION['user_id']));
	echo json_encode($output, JSON_UNESCAPED_UNICODE);	
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

function removeArrayDuplicates($array1, $array2){
	$outputArr = [];
	foreach ($array1 as $item1) {
		$curId = $item1->id;
		foreach ($array2 as $item2) {
			if($curId == $item2->id){
				array_push($outputArr, $item2);
			}
		}
	}
	return $outputArr;
}
