<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$id 		= sanitizeField($_GET['editTmimaId']);
	$tmima		= new Tmima;
	$tmima->id 	= $id;
	$output 	= $tmima->getTmimaById();
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

?>