<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$perioxiName 		= sanitizeField($_GET['perioxiName']);
	$student			= new Student;
	$student->perioxi 	= $perioxiName;
	$output 			= $student->getStudentsByPerioxi();
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

?>