<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$id 			= sanitizeField($_GET['studentId']);
	$student		= new Student;
	$student->id 	= $id;
	echo json_encode($student->getStudentById(), JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

?>