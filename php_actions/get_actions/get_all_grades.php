<?php
session_start();
include("../../init.php");

if (isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token'])) {
    $output     = array();
    $output     = Grade::getAllGrades();
    echo json_encode($output, JSON_UNESCAPED_UNICODE);
} else {
    echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
    exit();
}
