<?php
session_start();
include("../../init.php");
$csrf = getallheaders()['csrf_token'];

if (isLoggedIn() && isAdmin() && validateCsrfToken($csrf)) {
    $query = new Query;
    $query->select('*');
    $query->from($_SESSION['full_grades_tbl']);
    $query->where1();
    $attribs = $_GET['attribs'];
    $studentId = sanitizeField($_GET['studentId']);
    $cnt = true;
    foreach ($attribs as $key => $value) {
        if (!empty($value)) {
            if ($key == 'grade') {
                $telestis = explode(':', $value)[0];
                $grade = sanitizeField(explode(':', $value)[1]);
                if($grade != '')
                    $query->whereAnd($key, $telestis, $grade);
                continue;
            }
            if ($key == 'date_from' || $key == 'date_to') {
                $telestis =  ($key == 'date_from' ? '>=' : '<=');
                $query->whereAnd('exam_date', $telestis, "'".sanitizeField($value)."'");
                continue;
            }
            if ($key == 'lesson_ids') {
                fixLessonSearch($value, $query);
                continue;
            }
            $query->whereAnd($key, '', "'".sanitizeField($value)."'");
        }
    }
    $query->whereAnd('student_id', '', sanitizeField($studentId));
    $query->orderBy('exam_date', 'DESC');
    $grade = new Grade;
    $response = new AjaxController(200, $grade->query($query->the_query));
    AjaxController::spitResponse($response);
} else {
    $response = new AjaxController(698, null,'Σφάλμα κατά την αυθεντικοποίηση της φόρμας');
    AjaxController::spitResponse($response);
}

function fixLessonSearch($value, $query)
{
    global $cnt;
    $ids = explode(',', $value);
    $is_more_one = (sizeof($ids) > 1) ? true : false;
    $flag = true;
    foreach ($ids as $id) {
        if ($flag) {
            if ($is_more_one)
                $query->whereAnd('(lesson_id', '', sanitizeField($id));
            else
                $query->whereAnd('lesson_id', '', sanitizeField($id));
            $cnt = false;
            $flag = false;
        } else {
            $query->whereOr('lesson_id', '', sanitizeField($id));
        }
    }
    if ($is_more_one)
        $query->the_query .= ")";
}
