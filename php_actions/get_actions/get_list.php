<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$appListName	= sanitizeField($_GET['listName']);
	$appList		= new AppLists;
	$appList->name 	= $appListName;
	$output 		= $appList->getListByName();
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}

?>