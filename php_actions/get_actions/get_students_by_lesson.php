<?php	
session_start();
include("../../init.php");

if( isLoggedIn() && isAdmin() && validateCsrfToken($_GET['csrf_token']) ){
	$lessonId 					= sanitizeField($_GET['lessonId']);
	$student_lesson				= new StudentLessons;
	$student_lesson->lesson_id 	= $lessonId;
	$output 					= $student_lesson->findStudentsByLesson();
	usort($output, function($a, $b) {return strcmp($a->lastname, $b->lastname);});
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}else{	
	echo json_encode(['error' => 'Σφάλμα κατά την αυθεντικοποίηση της φόρμας']);
	exit();
}
?>