<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class UserController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $user       = new User();
            $response   = new AjaxController(200, $user->getAllUsers(true, false, false));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $user       = new User();
            $user->id   = filter_input(INPUT_GET, 'editUserId');
            $response   = new AjaxController(200, $user->getUserAndProfileById());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'email' => 'Το email έχει λάθος μορφή',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'username'   => 'required',
                'email'      => 'email',
                'permission' => 'required',
                'status'     => 'required|numeric',
                'firstname'  => 'required|max:50',
                'lastname'     => 'required|max:50'
            ]);

            $validation->setAlias('username', 'Username');
            $validation->setAlias('permission', 'Δικαιώματα');
            $validation->setAlias('status', 'Κατάσταση');
            $validation->setAlias('email', 'Email');
            $validation->setAlias('firstname', 'Όνομα');
            $validation->setAlias('lastname', 'Επίθετο');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $user                 = new User(filter_input(INPUT_POST,'username'));
                $profile              = new Profile();
                $user->id             = filter_input(INPUT_POST, 'editUserId');
                $user->email          = filter_input(INPUT_POST, 'email');
                $user->permission     = filter_input(INPUT_POST, 'permission');
                $user->status         = filter_input(INPUT_POST, 'status');
                $profile->firstname   = filter_input(INPUT_POST, 'firstname');
                $profile->lastname    = filter_input(INPUT_POST, 'lastname');
                $user->profile        = $profile;

                $submitted = $user->updateUser();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } else {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function customUpdate($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'same' => 'Οι κωδικοί δεν είναι ίδιοι',
                'regex' => 'Ο κωδικός πρέπει να είναι τουλάχιστον 8 χαρακτήρων, να περιέχει τουλάχιστον 1 λατινικό γράμμα, τουλάχιστον 1 αριθμό και τουλάχιστον 1 από τα σύμβολα #?!@$%^&*-',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'reNewPass'    => 'required',
                'newPass'    => 'required|min:8|same:reNewPass|regex:/^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/'
            ]);

            $validation->setAlias('reNewPass', 'Επαλήθευση Κωδικού');
            $validation->setAlias('newPass', 'Κωδικός');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $user                = new User(filter_input(INPUT_POST, 'passUsername'));
                $user->id            = filter_input(INPUT_POST, 'passUserId');
                $user->password      = filter_input(INPUT_POST, 'newPass');

                $submitted = $user->updatePass();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Ο κωδικός άλλαξε με επιτυχία!';
                } else {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'email' => 'Το email έχει λάθος μορφή',
                'same' => 'Οι κωδικοί δεν είναι ίδιοι',
                'regex' => 'Ο κωδικός πρέπει να είναι τουλάχιστον 8 χαρακτήρων, να περιέχει τουλάχιστον 1 λατινικό γράμμα, τουλάχιστον 1 αριθμό και τουλάχιστον 1 από τα σύμβολα #?!@$%^&*-',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'addUsername'   => 'required',
                'addEmail'      => 'email',
                'addPassword'   => 'required|min:8|same:addRePass|regex:/^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
                'addPermission' => 'required',
                'addStatus'     => 'required|numeric',
                'addFirstname'  => 'required|max:50',
                'addLastname'   => 'required|max:50',
            ]);

            $validation->setAlias('addUsername', 'Username');
            $validation->setAlias('addPassword', 'Κωδικός');
            $validation->setAlias('addPermission', 'Δικαιώματα');
            $validation->setAlias('addStatus', 'Κατάσταση');
            $validation->setAlias('addEmail', 'Email');
            $validation->setAlias('addFirstname', 'Όνομα');
            $validation->setAlias('addLastname', 'Επίθετο');

            $validation->validate();
            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $username            = filter_input(INPUT_POST, 'addUsername');
                $user                = new User($username);
                $profile             = new Profile();
                $user->email         = filter_input(INPUT_POST, 'addEmail');
                $user->password      = filter_input(INPUT_POST, 'addPassword');
                $user->rePassword    = filter_input(INPUT_POST, 'addRePass');
                $user->permission    = filter_input(INPUT_POST, 'addPermission');
                $user->status        = filter_input(INPUT_POST, 'addStatus');
                $profile->firstname  = filter_input(INPUT_POST, 'addFirstname');
                $profile->lastname   = filter_input(INPUT_POST, 'addLastname');
                $user->profile       = $profile;

                $submitted = $user->addUser();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Ο χρήστης αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Το username <b>' . $user->username . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.';
                } elseif ($submitted == 3) {
                    $error = 'Το email <b>' . $user->email . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        //TODO
    }
}

$controller = new UserController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}