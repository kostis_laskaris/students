<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class StudentController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $student = new Student;
            $response   = new AjaxController(200, $student->getAllStudents());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    public function getCustomData($method)
    {
        if ($method === "GET") {
            $action_to_do   = filter_input(INPUT_GET, 'fireAction');
            if ($action_to_do == 'manage_students_data') {
                $output = array();
                $output['allStudents']          = Student::getAllStudents();
                $output['studentsPerClass']     = Student::getStudentPerClass();
                $output['studentsPerPerioxi']   = Student::getStudentPerPerioxi();
                $output['studentsPerLesson']    = Student::getStudentPerLesson();
                $response   = new AjaxController(200, $output);
            } elseif ($action_to_do == 'students_for_grades') {
                $teacher_id = filter_input(INPUT_GET, 'teacherId');
                $response   = new AjaxController(200, Student::getStudentsForGrade($teacher_id));
            } elseif ($action_to_do == 'latest_students') {
                $response   = new AjaxController(200, Student::getLastNStudentsSub(filter_input(INPUT_GET, 'studentCount')));
            } elseif ($action_to_do === "student_per_class") {
                $response   = new AjaxController(200, Student::getStudentPerClass());
            } else {
                $response   = new AjaxController(400, '', 'Bad Request. No action found on the request.');
            }
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $action_to_do   = filter_input(INPUT_GET, 'fireAction');
            $output = 1;
            if ($action_to_do === "student_by_id") {
                $student_id     = filter_input(INPUT_GET, 'studentId');
                $output         = Student::getStudentById($student_id);
            } elseif ($action_to_do === "students_by_lesson_id") {
                $lesson_id  = filter_input(INPUT_GET, 'lessonId');
                $output     = StudentLessons::getStudentsByLessonId($lesson_id);
            }
            $response   = new AjaxController(200, $output);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required'              => 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
                'email'                 => 'Το email έχει λάθος μορφή',
                'max'                   => 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
                'min'                   => 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
                'digits'                => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
                'date'                  => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
                'digits_between'        => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 έως 22 ψηφία',
                'regex'                 => 'Το πεδίο <b>":attribute"</b> δεν έχει τη σωστή μορφή'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'firstname'         => 'required|max:50',
                'lastname'          => 'required|max:50',
                'address'           => 'max:200',
                'perioxi'           => 'max:100',
                'zip'               => 'digits:5',
                'mobile'            => 'digits:10',
                'phone'             => 'digits:10',
                'email'             => 'max:200|email',
                'date_eggrafis'     => 'required|date',
                'mname'             => 'max:50',
                'faname'            => 'max:50',
                'mphone'            => 'digits:10',
                'fphone'            => 'digits:10',
                'email_kidemona'    => 'max:150|email',
                'kid_name'          => 'max:100',
                'kid_phone'         => 'min:10|max:21|regex:/^[0-9]{10,10}([,][0-9]{10,10})?$/',
                'fee'               => 'digits_between:2,5',
                'tmima'             => 'required',
                'class'             => 'required',
                'lesson_ids'        => 'required',
            ]);

            $validation->setAlias('firstname', 'Όνομα');
            $validation->setAlias('lastname', 'Επίθετο');
            $validation->setAlias('address', 'Διεύθυνση Κατοικίας');
            $validation->setAlias('perioxi', 'Περιοχή');
            $validation->setAlias('zip', 'Τ.Κ.');
            $validation->setAlias('mobile', 'Τηλέφωνο Μαθητή');
            $validation->setAlias('phone', 'Σταθερό Τηλέφωνο');
            $validation->setAlias('email', 'Email Μαθητή');
            $validation->setAlias('date_eggrafis', 'Ημερομηνία Εγγραφής');
            $validation->setAlias('mname', 'Όνομα Μητέρας');
            $validation->setAlias('faname', 'Όνομα Πατέρα');
            $validation->setAlias('mphone', 'Τηλέφωνο Μητέρας');
            $validation->setAlias('fphone', 'Τηλέφωνο Πατέρα');
            $validation->setAlias('email_kidemona', 'Email Κηδεμόνα');
            $validation->setAlias('kid_name', 'Όνομα Κηδεμόνα');
            $validation->setAlias('kid_phone', 'Τηλέφωνο Κηδεμόνα');
            $validation->setAlias('fee', 'Δίδακτρα');
            $validation->setAlias('tmima', 'Τμήμα');
            $validation->setAlias('class', 'Τάξη');
            $validation->setAlias('lesson_ids', 'Μαθήματα');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $student                    = new Student;
                $student->id                = filter_input(INPUT_POST, 'student_id');
                $student->firstname         = filter_input(INPUT_POST, 'firstname');
                $student->lastname          = filter_input(INPUT_POST, 'lastname');
                $student->address           = filter_input(INPUT_POST, 'address');
                $student->perioxi           = filter_input(INPUT_POST, 'perioxi');
                $student->zip               = filter_input(INPUT_POST, 'zip');
                $student->mobile            = filter_input(INPUT_POST, 'mobile');
                $student->phone             = filter_input(INPUT_POST, 'phone');
                $student->email             = filter_input(INPUT_POST, 'email');
                $student->date_eggrafis     = filter_input(INPUT_POST, 'date_eggrafis');
                $student->mname             = filter_input(INPUT_POST, 'mname');
                $student->faname            = filter_input(INPUT_POST, 'faname');
                $student->mphone            = filter_input(INPUT_POST, 'mphone');
                $student->fphone            = filter_input(INPUT_POST, 'fphone');
                $student->email_kidemona    = filter_input(INPUT_POST, 'email_kidemona');
                $student->kid_name          = filter_input(INPUT_POST, 'kid_name');
                $student->kid_phone         = filter_input(INPUT_POST, 'kid_phone');
                $student->fee               = filter_input(INPUT_POST, 'fee');
                $student->tmima             = filter_input(INPUT_POST, 'tmima');
                $student->class             = filter_input(INPUT_POST, 'class');
                $student->informed_by       = filter_input(INPUT_POST, 'informed_by') ? filter_input(INPUT_POST, 'informed_by') : null;
                $student->lessons           = explode(",", filter_input(INPUT_POST, 'lesson_ids'));
                $student->updated_at        = date('Y-m-d H:i:s');

                $submitted = $student->updateStudent();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Η διαγραφή των παλιών δεδομένων του μαθητή απέτυχε. Επικοινωνήστε με τον Διαχειριστή.';
                }

                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function customInsert($method)
    {
        if ($method === "POST") {
            $code = 200;
            $message = 'Οι μαθητές επανεγγράφηκαν με επιτυχία!';
            $error = '';
            $datas = filter_input(INPUT_POST, 'attribs', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            if (isset($datas['student_data'])) {
                foreach ($datas['student_data'] as $key => $data) {
                    $submitted = Student::resubStudent($data, $datas['from_sx_etos']);
                    if ($submitted == 1) {
                        $code = 603;
                        $message = '';
                        $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                        $response   = new AjaxController($code, $message, $error);
                        AjaxController::spitResponse($response);
                    }
                }
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required'             => 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
                'email'                => 'Το email έχει λάθος μορφή',
                'max'                  => 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
                'min'                  => 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
                'digits'               => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
                'zip:digits'           => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 5 ψηφία',
                'date'                 => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
                'digits_between'       => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 έως 22 ψηφία',
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'firstname'         => 'required|max:50',
                'lastname'          => 'required|max:50',
                'address'           => 'max:200',
                'perioxi'           => 'max:100',
                'zip'               => 'digits:5',
                'mobile'            => 'digits:10',
                'phone'             => 'digits:10',
                'email'             => 'max:200|email',
                'sub_date'          => 'required|date',
                'mname'             => 'max:50',
                'faname'            => 'max:50',
                'mphone'            => 'digits:10',
                'fphone'            => 'digits:10',
                'email_kidemona'    => 'max:150|email',
                'kid_name'          => 'max:100',
                'kid_phone'         => 'min:10|max:21|regex:/^[0-9]{10,10}([,][0-9]{10,10})?$/',
                'fee'               => 'digits_between:2,5',
                'tmima'             => 'required',
                'sx_etos'           => 'required',
                'class'             => 'required',
                'lesson_ids'        => 'required',
            ]);

            $validation->setAlias('firstname', 'Όνομα');
            $validation->setAlias('lastname', 'Επίθετο');
            $validation->setAlias('address', 'Διεύθυνση Κατοικίας');
            $validation->setAlias('perioxi', 'Περιοχή');
            $validation->setAlias('zip', 'Τ.Κ.');
            $validation->setAlias('mobile', 'Τηλέφωνο Μαθητή');
            $validation->setAlias('phone', 'Σταθερό Τηλέφωνο');
            $validation->setAlias('email', 'Email Μαθητή');
            $validation->setAlias('sub_date', 'Ημερομηνία Εγγραφής');
            $validation->setAlias('mname', 'Όνομα Μητέρας');
            $validation->setAlias('faname', 'Όνομα Πατέρα');
            $validation->setAlias('mphone', 'Τηλέφωνο Μητέρας');
            $validation->setAlias('fphone', 'Τηλέφωνο Πατέρα');
            $validation->setAlias('email_kidemona', 'Email Κηδεμόνα');
            $validation->setAlias('kid_name', 'Όνομα Κηδεμόνα');
            $validation->setAlias('kid_phone', 'Τηλέφωνο Κηδεμόνα');
            $validation->setAlias('fee', 'Δίδακτρα');
            $validation->setAlias('tmima', 'Τμήμα');
            $validation->setAlias('sx_etos', 'Σχολικό Έτος');
            $validation->setAlias('class', 'Τάξη');
            $validation->setAlias('lesson_ids', 'Μαθήματα');
            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $student                    = new Student;
                $student->firstname         = filter_input(INPUT_POST, 'firstname');
                $student->lastname          = filter_input(INPUT_POST, 'lastname');
                $student->address           = filter_input(INPUT_POST, 'address');
                $student->perioxi           = filter_input(INPUT_POST, 'perioxi');
                $student->zip               = filter_input(INPUT_POST, 'zip');
                $student->mobile            = filter_input(INPUT_POST, 'mobile');
                $student->phone             = filter_input(INPUT_POST, 'phone');
                $student->email             = filter_input(INPUT_POST, 'email');
                $student->date_eggrafis     = filter_input(INPUT_POST, 'sub_date');
                $student->mname             = filter_input(INPUT_POST, 'mname');
                $student->faname            = filter_input(INPUT_POST, 'faname');
                $student->mphone            = filter_input(INPUT_POST, 'mphone');
                $student->fphone            = filter_input(INPUT_POST, 'fphone');
                $student->email_kidemona    = filter_input(INPUT_POST, 'email_kidemona');
                $student->kid_name          = filter_input(INPUT_POST, 'kid_name');
                $student->kid_phone         = filter_input(INPUT_POST, 'kid_phone');
                $student->fee               = filter_input(INPUT_POST, 'fee');
                $student->tmima             = filter_input(INPUT_POST, 'tmima');
                $student->sx_etos           = $_SESSION['curr_school_year'];
                $student->periodos          = $_SESSION['periodos_mathimaton'];
                $student->class             = filter_input(INPUT_POST, 'class');
                $student->informed_by       = filter_input(INPUT_POST, 'informed_by') ? filter_input(INPUT_POST, 'informed_by') : null;
                $student->lessons           = explode(",", filter_input(INPUT_POST, 'lesson_ids'));
                $student->created_at        = date('Y-m-d H:i:s');
                // $student->updated_at        = date('Y-m-d H:i:s');

                $submitted = $student->addStudent();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Ο μαθητής αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $id        = filter_input(INPUT_POST, 'studentDeleteId');
            $submitted = Student::deleteStudent($id);

            $code = 603;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Ο Μαθητής διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            } elseif ($submitted == 2) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένες βαθμολογίες</b> που σχετίζονται με αυτόν τον μαθητή. Παρακαλώ διαγράψτε τις προτού προβείτε στη διαγραφή του μαθητή.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new StudentController();
$validationResult = $controller->validate(['Admin', 'Teacher']);

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
