<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class UserProfileController extends Controller
{

    function getAllData($method)
    {
        //TODO
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $user       = new User();
            $user->id   = filter_input(INPUT_GET, 'userId');
            $response   = new AjaxController(200, $user->getUserById());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;

            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'email' => 'Το email έχει λάθος μορφή',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες',
                'numeric' => 'Το πεδίο ":attribute" δέχεται μόνο αριθμούς'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'firstname'  => 'required|max:50',
                'lastname'     => 'required|max:50',
                'email'      => 'required|email|max:150',
                'phone'        => 'numeric|max:10',
                'mobile'      => 'numeric|max:10',
                'address'    => 'max:150',
                'zip'          => 'numeric|max:5|min:5',
                'city'          => 'max:100',
            ]);

            $validation->setAlias('firstname', 'Όνομα');
            $validation->setAlias('lastname', 'Επίθετο');
            $validation->setAlias('phone', 'Τηλέφωνο');
            $validation->setAlias('mobile', 'Κινητό Τηλέφωνο');
            $validation->setAlias('address', 'Διεύθυνση');
            $validation->setAlias('zip', 'Τ.Κ');
            $validation->setAlias('city', 'Πόλη');
            $validation->setAlias('email', 'Email');

            $validation->validate();
            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $user                   = new User();
                $profile                = new Profile();
                $user->id               = filter_input(INPUT_POST, 'userId');
                $user->email            = filter_input(INPUT_POST, 'email');
                $profile->firstname     = filter_input(INPUT_POST, 'firstname');
                $profile->lastname      = filter_input(INPUT_POST, 'lastname');
                $profile->phone         = filter_input(INPUT_POST, 'phone');
                $profile->mobile_phone  = filter_input(INPUT_POST, 'mobile');
                $profile->address       = filter_input(INPUT_POST, 'address');
                $profile->zip           = filter_input(INPUT_POST, 'zip');
                $profile->city          = filter_input(INPUT_POST, 'city');
                $user->profile          = $profile;

                $submitted = $user->updateUserAndProfile();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } else {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        //TODO
    }

    function deleteData($method)
    {
        //TODO
    }
}

$controller = new UserProfileController();
$validationResult = $controller->validate(['Admin', 'Teacher']);

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
