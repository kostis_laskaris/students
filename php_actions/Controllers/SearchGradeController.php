<?php
require_once 'autoload.php';
session_start();

class SearchGradeController extends Controller
{

    function getAllData($method)
    {
        //TODO
    }

    public function getCustomData($method)
    {
        $datas = filter_input(INPUT_GET, 'attribs', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $studentId  = filter_input(INPUT_GET, 'studentId');
        $lessonId  = filter_input(INPUT_GET, 'lessonId');
        $userId  = filter_input(INPUT_GET, 'userId');
        if (!empty($studentId)) {
            if (isset($datas['reset'])) {
                if (!empty($userId)) {
                    if ($_SESSION['app'] == 'admin-app') {
                        $response   = new AjaxController(200, Grade::getAllGradesByUserId($userId));
                    } elseif ($_SESSION['app'] == 'teacher-app') {
                        $response   = new AjaxController(200, Grade::getAllGradesByUserIdAndStudentId($userId, $studentId));
                    }
                } else {
                    $response   = new AjaxController(200, Grade::getAllGradesByStudentId($studentId));
                }
                AjaxController::spitResponse($response);
            } else {
                $query = new Query;
                $query->select('*');
                $query->from($_SESSION['full_grades_tbl']);
                $query->where1();
                $query->whereAnd('student_id', '', $studentId);
                foreach ($datas as $key => $value) {
                    if (!empty($value)) {
                        if ($key == 'grade') {
                            $telestis = explode(':', $value)[0];
                            $grade = explode(':', $value)[1];
                            if ($grade != '')
                                $query->whereAnd($key, $telestis, $grade);
                            continue;
                        }
                        if ($key == 'date_from' || $key == 'date_to') {
                            $telestis =  ($key == 'date_from' ? '>=' : '<=');
                            $query->whereAnd('exam_date', $telestis, "'" . $value . "'");
                            continue;
                        }
                        if ($key == 'lesson_id') {
                            $lesson_ids = explode(',', $value);
                            for ($i = 0; $i < sizeof($lesson_ids); $i++) {
                                if ($i == 0) {
                                    $query->whereAnd("(" . $key, '=', $lesson_ids[$i]);
                                } else {
                                    $query->whereOr($key, '=', $lesson_ids[$i]);
                                }
                            }
                            $query->the_query .= ")";
                            continue;
                        }
                        $query->whereAnd($key, '', "'" . $value . "'");
                    }
                }
                $query->orderBy('exam_date', 'DESC');
                $grade = new Grade;
                $response = new AjaxController(200, Grade::query($query->the_query));
                AjaxController::spitResponse($response);
            }
        } elseif (!empty($lessonId)) {
            if (isset($datas['reset'])) {
                if (!empty($userId)) {
                    $response   = new AjaxController(200, Grade::getAllGradesByUserId($userId));
                } else {
                    $response   = new AjaxController(200, Grade::getAllGradesByLessonId($lessonId));
                }
                AjaxController::spitResponse($response);
            } else {
                $query = new Query;
                $query->select('*');
                $query->from($_SESSION['full_grades_tbl']);
                $query->where1();
                $query->whereAnd('lesson_id', '', $lessonId);
                foreach ($datas as $key => $value) {
                    if (!empty($value)) {
                        if ($key == 'grade') {
                            $telestis = explode(':', $value)[0];
                            $grade = explode(':', $value)[1];
                            if ($grade != '')
                                $query->whereAnd($key, $telestis, $grade);
                            continue;
                        }
                        if ($key == 'date_from' || $key == 'date_to') {
                            $telestis =  ($key == 'date_from' ? '>=' : '<=');
                            $query->whereAnd('exam_date', $telestis, "'" . $value . "'");
                            continue;
                        }
                        $query->whereAnd($key, '', "'" . $value . "'");
                    }
                }
                $query->orderBy('exam_date', 'DESC');
                $grade = new Grade;
                $response = new AjaxController(200, Grade::query($query->the_query));
                AjaxController::spitResponse($response);
            }
        } else {
            if (isset($datas['reset'])) {
                if (!empty($userId)) {
                    $response   = new AjaxController(200, Grade::getAllGradesByUserId($userId));
                } else {
                    $response   = new AjaxController(200, Grade::getAllGrades());
                }
                AjaxController::spitResponse($response);
            } else {
                $query = new Query;
                $query->select('*');
                $query->from($_SESSION['full_grades_tbl']);
                $query->where1();
                foreach ($datas as $key => $value) {
                    if (!empty($value)) {
                        if ($key == 'grade') {
                            $telestis = explode(':', $value)[0];
                            $grade = explode(':', $value)[1];
                            if ($grade != '')
                                $query->whereAnd($key, $telestis, $grade);
                            continue;
                        }
                        if ($key == 'date_from' || $key == 'date_to') {
                            $telestis =  ($key == 'date_from' ? '>=' : '<=');
                            $query->whereAnd('exam_date', $telestis, "'" . $value . "'");
                            continue;
                        }
                        if ($key == 'lesson_id') {
                            $lesson_ids = explode(',', $value);
                            for ($i = 0; $i < sizeof($lesson_ids); $i++) {
                                if ($i == 0) {
                                    $query->whereAnd("(" . $key, '=', $lesson_ids[$i]);
                                } else {
                                    $query->whereOr($key, '=', $lesson_ids[$i]);
                                }
                            }
                            $query->the_query .= ")";
                            continue;
                        }
                        $query->whereAnd($key, '', "'" . $value . "'");
                    }
                }
                $query->orderBy('exam_date', 'DESC');
                $grade = new Grade;
                $response = new AjaxController(200, Grade::query($query->the_query));
                AjaxController::spitResponse($response);
            }
        }
    }

    function getSingleData($method)
    {
        //TODO
    }

    function updateData($method)
    {
        //TODO
    }

    function insertData($method)
    {
        //TODO
    }

    function deleteData($method)
    {
        //TODO
    }
}

$controller = new SearchGradeController();
$validationResult = $controller->validate(['Admin', 'Teacher']);

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
