<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class ManageShortcutsController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $response   = new AjaxController(200, Shortcut::getAllShortcuts());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $id = filter_input(INPUT_GET, 'shortcutId');
            $response   = new AjaxController(200, Shortcut::getShortcutById($id));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'shortcut_lessons_edit'   => 'array|required',
                'editShortcutName'    => 'required|max:30'
            ]);

            $validation->setAlias('shortcut_lessons_edit', 'Μαθήματα');
            $validation->setAlias('editShortcutName', 'Τίτλος Συντόμευσης');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $shortcut                = new Shortcut;
                $shortcut->id            = filter_input(INPUT_POST, 'shortcutEditId');
                $shortcut->name          = filter_input(INPUT_POST, 'editShortcutName');
                $shortcut->lessons       = filter_input(INPUT_POST, 'shortcut_lessons_edit', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $shortcut->updated_at    = date('Y-m-d H:i:s');

                $submitted = $shortcut->updateShortcut();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Η συντόμευση αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Η διαγραφή των παλιών μαθημάτων της συντόμευσης απέτυχε. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'shortcut_lessons'   => 'array|required',
                'addShortcutName'    => 'required|max:30'
            ]);

            $validation->setAlias('shortcut_lessons', 'Επιλογή Μαθημάτων');
            $validation->setAlias('addShortcutName', 'Τίτλος Συντόμευσης');

            $validation->validate();
            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $shortcut               = new Shortcut;
                $shortcut->name         = filter_input(INPUT_POST, 'addShortcutName');
                $shortcut->lessons      = filter_input(INPUT_POST, 'shortcut_lessons', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $shortcut->created_at   = date('Y-m-d H:i:s');
                $shortcut->updated_at   = date('Y-m-d H:i:s');

                $submitted = $shortcut->addShortcut();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Η συντόμευση αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $id        = filter_input(INPUT_POST, 'shortcutDeleteId');
            $submitted = Shortcut::deleteShortcut($id);

            $code = 603;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Η συντόμευση διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new ManageShortcutsController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
