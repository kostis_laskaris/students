<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class ClassController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $response   = new AjaxController(200, Tmima::getAllTmima());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    public function getCustomData($method)
    {
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $tmima_id   = filter_input(INPUT_GET, 'tmimaId');
            $response   = new AjaxController(200, Tmima::getTmimaById($tmima_id));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'editTmima'   => 'required|max:100',
                'editTaksi'   => 'required',
                'comments'      => 'max:250'
            ]);

            $validation->setAlias('editTmima', 'Όνομα Τμήματος');
            $validation->setAlias('editTaksi', 'Τάξη');
            $validation->setAlias('comments', 'Σχόλια');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $tmima              = new Tmima;
                $tmima->id          = filter_input(INPUT_POST, 'classId');
                $tmima->name        = filter_input(INPUT_POST, 'editTmima');
                $tmima->class       = filter_input(INPUT_POST, 'editTaksi');
                $tmima->comments    = filter_input(INPUT_POST, 'comments');
                $tmima->updated_at  = date('Y-m-d H:i:s');

                $submitted = $tmima->updateTmima();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Το τμήμα <b>' . $tmima->name . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function customInsert($method)
    {
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'tmimaName'   => 'required|max:100',
                'addClass'   => 'required',
                'comments'    => 'max:250'
            ]);

            $validation->setAlias('tmimaName', 'Όνομα Τμήματος');
            $validation->setAlias('addClass', 'Τάξη');
            $validation->setAlias('comments', 'Σχόλια');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $tmima              = new Tmima;
                $tmima->name        = filter_input(INPUT_POST, 'tmimaName');
                $tmima->class       = filter_input(INPUT_POST, 'addClass');
                $tmima->comments    = filter_input(INPUT_POST, 'comments');
                $tmima->created_at  = date('Y-m-d H:i:s');

                $submitted = $tmima->addClass();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Το νέο τμήμα αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $id        = filter_input(INPUT_POST, 'tmimaDeleteId');
            $submitted = Tmima::deleteTmima($id);

            $code = 603;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Το τμήμα διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            } elseif ($submitted == 2) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένες βαθμολογίες</b> που σχετίζονται με αυτό το τμήμα. Παρακαλώ διαγράψτε τις προτού προβείτε στη διαγραφή του τμήματος.';
            } elseif ($submitted == 3) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένοι μαθητές</b> που σχετίζονται με αυτό το τμήμα. Παρακαλώ διαγράψτε τους προτού προβείτε στη διαγραφή του τμήματος.';
            } elseif ($submitted == 4) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένες αναθέσεις καθηγητών</b> που σχετίζονται με αυτό το τμήμα. Παρακαλώ διαγράψτε τους προτού προβείτε στη διαγραφή του τμήματος.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new ClassController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
