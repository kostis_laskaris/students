<?php
require_once 'autoload.php';
session_start();

class SearchStudentController extends Controller
{

    function getAllData($method)
    {
        //TODO
    }

    public function getCustomData($method)
    {
        $datas = filter_input(INPUT_GET, 'attribs', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        if (isset($datas['reset'])) {
            $response   = new AjaxController(200, Student::getAllStudents());
            AjaxController::spitResponse($response);
        } elseif (isset($datas['from_sx_etos'])) {
            $response   = new AjaxController(200, Student::getAllStudentsBySxEtos($datas['from_sx_etos']));
            AjaxController::spitResponse($response);
        } else {
            $query = new Query;
            $query->select($_SESSION['students_tbl'] . '.' . '*', $_SESSION['tmimata_tbl'] . '.' . 'name as tmimaName');
            $query->from($_SESSION['students_tbl']);
            $query->leftJoin($_SESSION['tmimata_tbl'], $_SESSION['tmimata_tbl'] . '.' . 'id', $_SESSION['students_tbl'] . '.' . 'tmima');
            if (!empty($datas['lesson_id'])) {
                $query->leftJoin($_SESSION['students_lessons_tbl'], $_SESSION['students_lessons_tbl'] . '.' . 'student_id', $_SESSION['students_tbl'] . '.' . 'id');
            }
            $query->where1();
            foreach ($datas as $filter_name => $filter_value) {
                if (!empty($filter_value)) {
                    if ($filter_name !== 'lesson_id') {
                        $query->whereAnd($_SESSION['students_tbl'] . '.' . $filter_name, 'LIKE', "'%" . $filter_value . "%'");
                    } else {
                        $lesson_ids = explode(',', $filter_value);
                        for ($i = 0; $i < sizeof($lesson_ids); $i++) {
                            if ($i == 0) {
                                $query->whereAnd($_SESSION['students_lessons_tbl'] . '.' . $filter_name, '=', "'" . $lesson_ids[$i] . "'");
                            } else {
                                $query->whereOr($_SESSION['students_lessons_tbl'] . '.' . $filter_name, '=', "'" . $lesson_ids[$i] . "'");
                            }
                        }
                    }
                }
            }
        }
        $query->groupBy($_SESSION['students_tbl'] . '.' . 'lastname');
        $query->orderBy($_SESSION['students_tbl'] . '.' . 'lastname', 'ASC');
        $response = new AjaxController(200, Student::query($query->the_query));
        AjaxController::spitResponse($response);
    }
    
    function getSingleData($method)
    {
        //TODO
    }

    function updateData($method)
    {
        //TODO
    }

    function insertData($method)
    {
        //TODO
    }

    function deleteData($method)
    {
        //TODO
    }
}

$controller = new SearchStudentController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
