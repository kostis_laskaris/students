<?php
require_once 'autoload.php';
session_start();

class GradeStatsController extends Controller
{

    function getAllData($method)
    {
    }

    public function getCustomData($method)
    {
        if ($method === "GET") {
            $action_to_do   = filter_input(INPUT_GET, 'fireAction');
            $student_id = filter_input(INPUT_GET, 'studentId');
            if ($action_to_do === "grades_by_student_per_lesson") {
                $response   = new AjaxController(200, Grade::getStudentStatsPerLessonId($student_id));
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "get_student_stats_per_lesson_id_for_current_user") {
                $response   = new AjaxController(200, Grade::getStudentStatsPerLessonIdForCurrentUser($student_id));
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "grades_by_student_per_lesson_for_chart") {
                $lessonIds = filter_input(INPUT_GET, 'lessonIds', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $output = array();
                foreach ($lessonIds as $key => $lesson_id) {
                    $output[$lesson_id] = Grade::getGradesPerStudentAndLesson($student_id, $lesson_id);
                }
                $response   = new AjaxController(200, $output);
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "all_grades_stats") {
                $response   = new AjaxController(200, Grade::getAllGradeStats());
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "grade_stats_per_month") {
                $response   = new AjaxController(200, Grade::getGradeStatsPerMonthPerUserId(filter_input(INPUT_GET, 'user_id')));
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "grade_avg_per_month_per_user_and_lesson_and_kind") {
                $response   = new AjaxController(200, Grade::getAllGradesAvgPerDateByUserAndLessonAndKind(filter_input(INPUT_GET, 'lesson_id'), filter_input(INPUT_GET, 'exam_kind')));
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "grade_stats_per_student_by_current_user_id") {
                $response   = new AjaxController(200, Grade::getGradeStatsPerStudentByCurrentUserIdAndLessonId(filter_input(INPUT_GET, 'lesson_id')));
                AjaxController::spitResponse($response);
            } elseif ($action_to_do === "get_student_stats_by_lesson_id_for_current_user") {
                $response   = new AjaxController(200, Grade::getStudentStatsByLessonIdForCurrentUser($student_id, filter_input(INPUT_GET, 'lessonId')));
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(405, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
    }

    function updateData($method)
    {
    }

    function customInsert($method)
    {
    }

    function insertData($method)
    {
    }

    function deleteData($method)
    {
    }
}

$controller = new GradeStatsController();
$validationResult = $controller->validate(['Admin', 'Teacher']);

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
