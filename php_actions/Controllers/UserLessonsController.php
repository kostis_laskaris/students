<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class UserLessonsController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $userlessons = new Userlessons;
            $userlessons->user_id = filter_input(INPUT_GET, 'userId');
            $response   = new AjaxController(200, $userlessons->getAllEntries());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        //TODO
    }

    function updateData($method)
    {
        //TODO
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;

            $validation = $validator->make($_POST + $_FILES, [
                'user_lessons'   => 'array'
            ]);

            $validation->setAlias('user_lessons', 'Μαθήματα');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $user                     = new User();
                $user->id                 = filter_input(INPUT_POST, 'addUserId');
                $user->lessons            = filter_input(INPUT_POST, 'user_lessons', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

                $submitted = $user->addUserLessons();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Η ανάθεση εκτελέστηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $user             = new User();
            $user->id         = filter_input(INPUT_POST, 'deleteUserlessonsId');
            $submitted        = $user->deleteUserLessons();

            $code = 603;
            $message = '';
            $error = '';
            if ($submitted == 0) {
                $code = 200;
                $message = 'Η ανάθεση μαθήματος διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new UserLessonsController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
