<?php
require_once 'autoload.php';
session_start();

class TmimataAssignmentController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $user = new User;
            $response   = new AjaxController(200, $user->getAllUsers(true, false, true));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        //TODO
    }

    function updateData($method)
    {
        //TODO
    }

    function insertData($method)
    {
        //TODO
    }

    function deleteData($method)
    {
        //TODO
    }
    
}

$controller = new TmimataAssignmentController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}