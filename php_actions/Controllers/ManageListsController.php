<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class ManageListsController extends Controller
{

    function getAllData($method)
    {
        //TODO
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $name       = filter_input(INPUT_GET, 'listName');
            $response   = new AjaxController(200, AppLists::getListByName($name));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;

            $validation = $validator->make($_POST + $_FILES, [
                'listItemEditId'   => 'required',
                'updateListItemName' => 'required'
            ]);

            $validation->setAlias('updateListItemName', 'Όνομα Στοιχείου Λίστας');
            $validation->setAlias('listItemEditId', 'ID Λίστας');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $appList                 = new AppLists;
                $appList->id             = filter_input(INPUT_POST, 'listItemEditId');
                $appList->list_entry     = filter_input(INPUT_POST, 'updateListItemName');
                $appList->updated_at     = date('Y-m-d H:i:s');

                $submitted = $appList->updateList();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } else {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;

            $validation = $validator->make($_POST + $_FILES, [
                'listEntryName'                 => 'required',
                'listModalDropdown'             => 'required',
                'listEntryAddAppearanceName'    => 'required'
            ]);

            $validation->setAlias('listEntryName', 'Όνομα Στοιχείου Λίστας');
            $validation->setAlias('listModalDropdown', 'Όνομα Λίστας');
            $validation->setAlias('listEntryAddAppearanceName', 'Εμφανιζόμενο Όνομα Λίστας');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $appList                     = new AppLists;
                $appList->list_name          = filter_input(INPUT_POST, 'listModalDropdown');
                $appList->list_entry         = filter_input(INPUT_POST, 'listEntryName');
                $appList->appearance_name    = filter_input(INPUT_POST, 'listEntryAddAppearanceName');
                $appList->created_at         = date('Y-m-d H:i:s');
                $appList->updated_at         = date('Y-m-d H:i:s');

                $submitted = $appList->addListEntry();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Το στοιχείο λίστας αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $id         = filter_input(INPUT_POST, 'listItemDeleteId');
            $submitted  = AppLists::deleteList($id);

            $code = 400;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Το στοιχείο λίστας διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            } elseif ($submitted == 2) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένες βαθμολογίες</b> που σχετίζονται με αυτό το στοιχείο της λίστας. Παρακαλώ διαγράψτε τις προτού προβείτε στη διαγραφή της τάξης.';
            } elseif ($submitted == 3) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένοι μαθητές</b> που σχετίζονται με αυτό το στοιχείο της λίστας. Παρακαλώ διαγράψτε τους προτού προβείτε στη διαγραφή της τάξης.';
            }   elseif ($submitted == 4) {
                $code = 406;
                $error = 'Υπάρχουν <b>καταχωρημένα τμήματα</b> που σχετίζονται με αυτό το στοιχείο της λίστας. Παρακαλώ διαγράψτε τους προτού προβείτε στη διαγραφή της τάξης.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new ManageListsController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
