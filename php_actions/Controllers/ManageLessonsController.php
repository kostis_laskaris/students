<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class ManageLessonsController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $response   = new AjaxController(200,  Lesson::getAllLessons());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $id = filter_input(INPUT_GET, 'editLessonId');
            $response   = new AjaxController(200, Lesson::getLessonById($id));
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες',
                'min' => 'Το πεδίο ":attribute" δέχεται τουλάχιστον :min χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'updateLessonName'   => 'required|max:40',
                'updateLessonDesc'   => 'required|max:10'
            ]);

            $validation->setAlias('updateLessonName', 'Όνομα Μαθήματος');
            $validation->setAlias('updateLessonDesc', 'Κωδικός Μαθήματος');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $lesson                 = new Lesson;
                $lesson->id             = filter_input(INPUT_POST, 'lessonEditId');
                $lesson->name           = filter_input(INPUT_POST, 'updateLessonName');
                $lesson->description    = filter_input(INPUT_POST, 'updateLessonDesc');
                $lesson->updated_at     = date('Y-m-d H:i:s');

                $submitted = $lesson->updateLesson();
                $code = 603;
                $message = '';
                $error = '';

                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Το μάθημα με κωδικό <b>' . $lesson->description . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required' => 'Το πεδίο ":attribute" είναι υποχρεωτικό',
                'max' => 'Το πεδίο :attribute δέχεται μέχρι :max χαρακτήρες'
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'addLessonName'   => 'required|max:40',
                'addLessonDesc'   => 'required|max:10'
            ]);

            $validation->setAlias('addLessonName', 'Όνομα Μαθήματος');
            $validation->setAlias('addLessonDesc', 'Κωδικός Μαθήματος');

            $validation->validate();
            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $lesson                 = new Lesson;
                $lesson->name           = filter_input(INPUT_POST, 'addLessonName');
                $lesson->description    = filter_input(INPUT_POST, 'addLessonDesc');
                $lesson->created_at     = date('Y-m-d H:i:s');
                $lesson->updated_at     = date('Y-m-d H:i:s');

                $submitted = $lesson->addLesson();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Το μάθημα αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                } elseif ($submitted == 2) {
                    $error = 'Το μάθημα με κωδικό <b>' . $lesson->description . '</b> υπάρχει ήδη. Δοκιμάστε ένα διαφορετικό.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $submitted = Lesson::deleteLesson(filter_input(INPUT_POST, 'lessonDeleteId'));

            $code = 603;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Το μάθημα διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            } elseif ($submitted == 2) {
                $error = 'Βρέθηκε ανάθεση του μαθήματος σε μαθητή. Για να πραγματοποιηθεί η διαγραφή παρακαλώ αφαιρέστε το μάθημα από τον μαθητή.';
            } elseif ($submitted == 3) {
                $error = 'Βρέθηκε βαθμολογία για αυτό μάθημα. Για να πραγματοποιηθεί η διαγραφή παρακαλώ διαγράψτε την βαθμολογία.';
            } elseif ($submitted == 4) {
                $error = 'Βρέθηκε ανάθεση του μαθήματος σε καθηγητή. Για να πραγματοποιηθεί η διαγραφή παρακαλώ διαγράψτε την ανάθεση.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new ManageLessonsController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
