<?php
require_once 'autoload.php';
session_start();

use Rakit\Validation\Validator;

class GradeController extends Controller
{

    function getAllData($method)
    {
        if ($method === "GET") {
            $response   = new AjaxController(200, Grade::getAllGrades());
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(405, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    public function getCustomData($method)
    {
        if ($method === "GET") {
            $action_to_do   = filter_input(INPUT_GET, 'fireAction');
            $output = "";
            if ($action_to_do === "grades_by_student") {
                $id     = filter_input(INPUT_GET, 'studentId');
                $output = Grade::getAllGradesByStudentId($id);
            } elseif ($action_to_do === "grades_by_lesson_id") {
                $id     = filter_input(INPUT_GET, 'lessonId');
                $output = Grade::getAllGradesByLessonId($id);
            } elseif($action_to_do === "grades_by_user_id"){
                $id     = filter_input(INPUT_GET, 'userId');
                $output = Grade::getAllGradesByUserId($id);
            } elseif($action_to_do === "grades_by_user_id_and_student_id"){
                $user_id        = filter_input(INPUT_GET, 'userId');
                $student_id     = filter_input(INPUT_GET, 'studentId');
                $output = Grade::getAllGradesByUserIdAndStudentId($user_id, $student_id);
            }elseif ($action_to_do === "last_n_grades_by_user") {
                $output = Grade::getLastNGradesByLoggedUserId(10);
            } elseif($action_to_do === "last_n_grades_by_user_and_exam_kind"){
                $output = [];
                $gradeCount = filter_input(INPUT_GET, 'gradeCount');
                $output['exams'] = Grade::getLastNGradesByLoggedUserIdAndExamKind($gradeCount, 'ΔΙΑΓΩΝΙΣΜΑ');
                $output['test'] = Grade::getLastNGradesByLoggedUserIdAndExamKind($gradeCount, 'TEST');
                $output['ergasia'] = Grade::getLastNGradesByLoggedUserIdAndExamKind($gradeCount, 'ΕΡΓΑΣΙΑ');
            }elseif($action_to_do === "last_n_grades"){
                $output = Grade::getLastNGrades(filter_input(INPUT_GET, 'gradeCount'));
            }elseif($action_to_do === "grades_per_user_and_lesson"){
                $output = Grade::getAllGradesStatsByUser();
            }elseif($action_to_do === "count_grades_per_user"){
                $output = Grade::getGradesCountByUser();
            }else {
                $response   = new AjaxController(400, '', 'Bad Request. No action found on the request.');
                AjaxController::spitResponse($response);
            }
            $response   = new AjaxController(200, $output);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(405, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function getSingleData($method)
    {
        if ($method === "GET") {
            $grade_id   = filter_input(INPUT_GET, 'gradeId');
            $output     = Grade::getGradeById($grade_id);
            $response   = new AjaxController(200, $output);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function updateData($method)
    {
        if ($method === "PATCH") {
            $validator = new Validator;
            $validator->setMessages([
                'required'          => 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
                'email'             => 'Το email έχει λάθος μορφή',
                'max'               => 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
                'min'               => 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
                'digits'            => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
                'date'              => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
                'digits_between'    => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 1 έως 3 ψηφία',
                'integer'           => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο ακέραιους',
                'before'            => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο μεταγενέστερες, από την τρέχουσα, ημερομηνίες',
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'lesson'        => 'required',
                'absence'       => 'required',
                'grade'         => 'digits_between:1,3|integer|required',
                'exam_kind'     => 'required',
                'exam_date'     => 'required|date|before:' . date('Y-m-d', strtotime("+1 days")),
                'comments'      => 'max:300'
            ]);

            $validation->setAlias('lesson', 'Μάθημα');
            $validation->setAlias('absence', 'Παρουσία');
            $validation->setAlias('grade', 'Βαθμός');
            $validation->setAlias('exam_kind', 'Είδος Εξέτασης');
            $validation->setAlias('exam_date', 'Ημερομηνία Εξέτασης');
            $validation->setAlias('comments', 'Σχόλια');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                $grade                 = new Grade();
                $grade->id             = filter_input(INPUT_POST, 'editGradeId');
                $grade->lesson_id      = filter_input(INPUT_POST, 'lesson');
                $grade->exam_date      = filter_input(INPUT_POST, 'exam_date');
                $grade->exam_kind      = filter_input(INPUT_POST, 'exam_kind');
                $grade->absence        = filter_input(INPUT_POST, 'absence');
                $grade->grade          = filter_input(INPUT_POST, 'grade');
                $grade->comments       = filter_input(INPUT_POST, 'comments');
                $grade->updated_at     = date('Y-m-d H:i:s');

                $submitted = $grade->updateGrade();
                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }

                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function customInsert($method)
    {
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $validator = new Validator;
            $validator->setMessages([
                'required'          => 'Το πεδίο <b>":attribute"</b> είναι υποχρεωτικό',
                'email'             => 'Το email έχει λάθος μορφή',
                'max'               => 'Το πεδίο <b>":attribute"</b> δέχεται μέχρι :max χαρακτήρες',
                'min'               => 'Το πεδίο <b>":attribute"</b> δέχεται τουλάχιστον :min χαρακτήρες',
                'digits'            => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 10 ψηφία',
                'date'              => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι σωστή ημερομηνία',
                'digits_between'    => 'Το πεδίο <b>":attribute"</b> πρέπει να είναι αριθμός και να αποτελείται από 1 έως 3 ψηφία',
                'integer'           => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο ακέραιους',
                'before'            => 'Το πεδίο <b>":attribute"</b> δέχεται μόνο προηγούμενες, από την τρέχουσα, ημερομηνίες',
            ]);

            $validation = $validator->make($_POST + $_FILES, [
                'student_name'  => 'required',
                'class'         => 'required',
                'tmima_id'      => 'required',
                'lesson'        => 'required',
                'absence'       => 'required',
                'grade'         => 'digits_between:1,3|integer|required',
                'exam_kind'     => 'required',
                'exam_date'     => 'required|date|before:' . date('Y-m-d', strtotime("+1 days")),
                'comments'      => 'max:300'
            ]);

            $validation->setAlias('student_name', 'Μαθητής');
            $validation->setAlias('class', 'Τάξη');
            $validation->setAlias('tmima_id', 'Τμήμα');
            $validation->setAlias('lesson', 'Μάθημα');
            $validation->setAlias('absence', 'Παρουσία');
            $validation->setAlias('grade', 'Βαθμός');
            $validation->setAlias('exam_kind', 'Είδος Εξέτασης');
            $validation->setAlias('exam_date', 'Ημερομηνία Εξέτασης');
            $validation->setAlias('comments', 'Σχόλια');

            $validation->validate();

            if ($validation->fails()) {
                $errors = $validation->errors();
                $messages = $errors->all();
                $tmp = '<ul>';
                foreach ($messages as $message) {
                    $tmp .= '<li>' . $message . '</li>';
                }
                $tmp .= '</ul>';
                $response   = new AjaxController(605, null, $tmp);
                AjaxController::spitResponse($response);
            } else {
                // validation passes
                $grade                     = new Grade();
                $grade->lesson_id          = filter_input(INPUT_POST, 'lesson');
                $grade->user_id            = $_SESSION['user_id'];
                $grade->student_id         = filter_input(INPUT_POST, 'student_name');
                $grade->class              = filter_input(INPUT_POST, 'class_id');
                $grade->tmima              = filter_input(INPUT_POST, 'tmima_id');
                $grade->exam_date          = filter_input(INPUT_POST, 'exam_date');
                $grade->exam_kind          = filter_input(INPUT_POST, 'exam_kind');
                $grade->absence            = filter_input(INPUT_POST, 'absence');
                $grade->grade              = filter_input(INPUT_POST, 'grade');
                $grade->comments           = filter_input(INPUT_POST, 'comments');
                $grade->created_at         = date('Y-m-d H:i:s');
                $submitted = $grade->addGrade();

                $code = 603;
                $message = '';
                $error = '';
                if ($submitted == 0) {
                    $code = 200;
                    $message = 'Η βαθμολογία αποθηκεύτηκε με επιτυχία!';
                } elseif ($submitted == 1) {
                    $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
                }
                $response   = new AjaxController($code, $message, $error);
                AjaxController::spitResponse($response);
            }
        } else {
            $response   = new AjaxController(405, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        if ($method === "DELETE") {
            $id        = filter_input(INPUT_POST, 'gradeDeleteId');
            $submitted = Grade::deleteGrade($id);
            $code = 603;
            $message = '';
            $error = '';

            if ($submitted == 0) {
                $code = 200;
                $message = 'Η βαθμολογία διαγράφηκε με επιτυχία!';
            } elseif ($submitted == 1) {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }
}

$controller = new GradeController();
$validationResult = $controller->validate(['Admin', 'Teacher']);

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
