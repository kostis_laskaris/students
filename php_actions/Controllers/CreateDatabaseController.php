<?php
require_once 'autoload.php';
session_start();

class CreateDatabaseController extends Controller
{

    function getAllData($method)
    {
        //TODO
    }

    public function getCustomData($method)
    {
        //TODO
    }

    function getSingleData($method)
    {
        //TODO
    }

    function updateData($method)
    {
        //TODO
    }

    function insertData($method)
    {
        if ($method === "POST") {
            $from   = filter_input(INPUT_POST, 'fromValue');
            $to     = filter_input(INPUT_POST, 'toValue');
            $insertRow = 1;
            global $db;
            $appList_old = 'app_lists_' . $from;
            $appList_new = 'app_lists_' . $to;

            $grades_old = 'grades_' . $from;
            $grades_new = 'grades_' . $to;

            $lesson_old = 'lessons_' . $from;
            $lesson_new = 'lessons_' . $to;

            $shortcuts_old = 'shortcuts_' . $from;
            $shortcuts_new = 'shortcuts_' . $to;

            $shortcut_lessons_old = 'shortcut_lessons_' . $from;
            $shortcut_lessons_new = 'shortcut_lessons_' . $to;

            $tmima_old = 'tmimata_' . $from;
            $tmima_new = 'tmimata_' . $to;

            $students_old = 'students_' . $from;
            $students_new = 'students_' . $to;

            $students_lessons_old = 'students_lessons_' . $from;
            $students_lessons_new = 'students_lessons_' . $to;

            $teachers_tmimata_old = 'teachers_tmimata_' . $from;
            $teachers_tmimata_new = 'teachers_tmimata_' . $to;

            $teacher_lessons_old = 'teacher_lessons_' . $from;
            $teacher_lessons_new = 'teacher_lessons_' . $to;

            $query = "
            CREATE TABLE IF NOT EXISTS {$appList_new} LIKE {$appList_old};
            INSERT INTO {$appList_new} SELECT * FROM {$appList_old};
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$lesson_new} LIKE {$lesson_old};
            INSERT INTO {$lesson_new} SELECT * FROM {$lesson_old};
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$shortcuts_new} LIKE {$shortcuts_old};
            INSERT INTO {$shortcuts_new} SELECT * FROM {$shortcuts_old};
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$shortcut_lessons_new} LIKE {$shortcut_lessons_old};
            INSERT INTO {$shortcut_lessons_new} SELECT * FROM {$shortcut_lessons_old};
            ";
            $query .= "
            ALTER TABLE {$shortcut_lessons_new} ADD CONSTRAINT link_lesson_shortcuts_id_{$to} FOREIGN KEY (lesson_id) REFERENCES {$lesson_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE {$shortcut_lessons_new} ADD CONSTRAINT link_shortcuts_id_{$to} FOREIGN KEY (shortcut_id) REFERENCES {$shortcuts_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$tmima_new} LIKE {$tmima_old};
            INSERT INTO {$tmima_new} SELECT * FROM {$tmima_old};
            ALTER TABLE {$tmima_new} ADD CONSTRAINT link_taksi_tmima_id_{$to} FOREIGN KEY (class) REFERENCES {$appList_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$students_new} LIKE {$students_old};
            ALTER TABLE {$students_new} ADD CONSTRAINT link_tmima_id_{$to} FOREIGN KEY (tmima) REFERENCES {$tmima_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$students_new} ADD CONSTRAINT link_taksi_student_id_{$to} FOREIGN KEY (class) REFERENCES {$appList_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$students_new} ADD CONSTRAINT link_informed_by_id_{$to} FOREIGN KEY (informed_by) REFERENCES {$appList_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ";
            $query .= "            
            CREATE TABLE IF NOT EXISTS {$students_lessons_new} LIKE {$students_lessons_old};
            ALTER TABLE {$students_lessons_new} ADD CONSTRAINT link_student_id_{$to} FOREIGN KEY (student_id) REFERENCES {$students_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE {$students_lessons_new} ADD CONSTRAINT link_lesson_id_{$to} FOREIGN KEY (lesson_id) REFERENCES {$lesson_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$teacher_lessons_new} LIKE {$teacher_lessons_old};
            ALTER TABLE {$teacher_lessons_new} ADD CONSTRAINT link_lessons_id_{$to} FOREIGN KEY (lesson_id) REFERENCES {$lesson_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE {$teacher_lessons_new} ADD CONSTRAINT link_user_id_{$to} FOREIGN KEY (user_id) REFERENCES register(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$teachers_tmimata_new} LIKE {$teachers_tmimata_old};
            ALTER TABLE {$teachers_tmimata_new} ADD CONSTRAINT link_tmimata_id_{$to} FOREIGN KEY (tmima_id) REFERENCES {$tmima_new}(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ALTER TABLE {$teachers_tmimata_new} ADD CONSTRAINT link_teacher_user_id_{$to} FOREIGN KEY (user_id) REFERENCES register(id) ON DELETE CASCADE ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE TABLE IF NOT EXISTS {$grades_new} LIKE {$grades_old};
            ALTER TABLE {$grades_new} ADD CONSTRAINT link_grade_lesson_id_{$to} FOREIGN KEY (lesson_id) REFERENCES {$lesson_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$grades_new} ADD CONSTRAINT link_grade_student_id_{$to} FOREIGN KEY (student_id) REFERENCES {$students_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$grades_new} ADD CONSTRAINT link_grade_user_id_{$to} FOREIGN KEY (user_id) REFERENCES register(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$grades_new} ADD CONSTRAINT link_grade_tmima_id_{$to} FOREIGN KEY (tmima) REFERENCES {$tmima_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ALTER TABLE {$grades_new} ADD CONSTRAINT link_taksi_id_{$to} FOREIGN KEY (taksi) REFERENCES {$appList_new}(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
            ";
            $query .= "
            CREATE VIEW full_grades_{$to} AS
            SELECT user_profile.firstname as teacherFirstname, user_profile.lastname teacherLastname, {$tmima_new}.name as tmimaName, {$lesson_new}.name as lessonName, {$students_new}.firstname as studentFirstname, {$students_new}.lastname as studentLastname, {$grades_new}.*
            FROM {$grades_new}
            LEFT JOIN user_profile ON user_profile.user_id = {$grades_new}.user_id
            LEFT JOIN {$tmima_new} ON {$tmima_new}.id = {$grades_new}.tmima
            LEFT JOIN {$students_new} ON {$students_new}.id = {$grades_new}.student_id
            LEFT JOIN {$lesson_new} ON {$lesson_new}.id = {$grades_new}.lesson_id;
            ";
            $query .= "
            CREATE VIEW full_grades_stats_{$to} AS 
            SELECT COUNT(id) as countGrades, AVG(grade) as avgGrades, lessonName, ANY_VALUE(lesson_id), MAX(grade) as maxGrade, MIN(grade) as minGrade, MAX(created_at) as maxCreatedAt, MIN(created_at) as minCreatedAt, MAX(updated_at) as maxUpdatedAt, MIN(updated_at) as minUpdatedAt 
            FROM full_grades_{$to} 
            WHERE absence = 'ΠΑΡΩΝ'
            GROUP BY lessonName;
            ";
            $insertRow = $db->executeQuery($query);
            $code = 603;
            $message = '';
            $error = '';
            if ($insertRow) {
                $code = 200;
                $message = 'Οι νέες Βάσεις Δεδομένων δημιουργήθηκαν με επιτυχία.';
            } else {
                $error = 'Πιθανό πρόβλημα με τη βάση δεδομένων. Επικοινωνήστε με τον Διαχειριστή.';
            }
            $response   = new AjaxController($code, $message, $error);
            AjaxController::spitResponse($response);
        } else {
            $response   = new AjaxController(601, null, 'Invalid request method.');
            AjaxController::spitResponse($response);
        }
    }

    function deleteData($method)
    {
        //TODO
    }
}

$controller = new CreateDatabaseController();
$validationResult = $controller->validate('Admin');

if ($validationResult === true) {
    $controller->getRoute();
} else {
    $response   = new AjaxController(601, null, $validationResult);
    AjaxController::spitResponse($response);
}
