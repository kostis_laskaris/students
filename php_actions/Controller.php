<?php

abstract class Controller
{
    public $availableActions = [
        'GET_ALL_DATA',
        'GET_SINGLE_DATA',
        'GET_CUSTOM_DATA',
        'CUSTOM_INSERT_DATA',
        'INSERT_DATA',
        'UPDATE_DATA',
        'CUSTOM_UPDATE_DATA',
        'DELETE_DATA',
    ];

    public $availableMethods = [
        'GET',
        'POST',
        'PATCH',
        'DELETE',
    ];

    public $havePermission = [
        'Admin',
        'Teacher',
    ];

    public $csrf;
    public $action;
    public $method;

    function __construct()
    {
        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
                return $headers;
            }
        }
        $allHeaders       = getallheaders();
        if ($this->checkHeaders($allHeaders)) {
            $this->csrf       = $allHeaders['Csrftoken'];
            $this->action     = $allHeaders['Action'];
            $this->method     = $allHeaders['Method'];
        } else {
            $response   = new AjaxController(401, null, 'Unauthorized Access.');
            AjaxController::spitResponse($response);
        }
    }

    function checkHeaders(array $the_headers)
    {
        if (empty($the_headers['Csrftoken']) || empty($the_headers['Action']) || empty($the_headers['Method'])) {
            return false;
        }
        return true;
    }

    function getRoute()
    {
        switch ($this->action) {
            case "GET_ALL_DATA":
                $this->getAllData($this->method);
                break;
            case "GET_SINGLE_DATA":
                $this->getSingleData($this->method);
                break;
            case "GET_CUSTOM_DATA":
                $this->getCustomData($this->method);
                break;
            case "CUSTOM_INSERT_DATA":
                $this->customInsert($this->method);
                break;
            case "INSERT_DATA":
                $this->insertData($this->method);
                break;
            case "CUSTOM_UPDATE_DATA":
                $this->customUpdate($this->method);
                break;
            case "UPDATE_DATA":
                $this->updateData($this->method);
                break;
            case "DELETE_DATA":
                $this->deleteData($this->method);
                break;
            default:
                $response   = new AjaxController(404, null, 'Action not found.');
                AjaxController::spitResponse($response);
                break;
        }
    }

    function validate($valPermission = false)
    {
        if (!isLoggedIn()) {
            $response   = new AjaxController(401, null, 'User must be logged in.');
            AjaxController::spitResponse($response);
        }
        if (!is_array($valPermission)) {
            if (!in_array($valPermission, $this->havePermission)) {
                $response   = new AjaxController(401, null, 'You have no permission for this action.');
                AjaxController::spitResponse($response);
            }
        } else {
            if (empty(array_intersect($valPermission, $this->havePermission))) {
                $response   = new AjaxController(401, null, 'You have no permission for this action.');
                AjaxController::spitResponse($response);
            }
        }

        if ($valPermission === 'Admin' && !isAdmin() || $valPermission === 'Teacher' && !isTeacher()) {
            $response   = new AjaxController(401, null, 'You have no permission for this action.');
            AjaxController::spitResponse($response);
        }

        if (!in_array($this->action, $this->availableActions) || !in_array($this->method, $this->availableMethods)) {
            $response   = new AjaxController(404, null, 'Action or Method not found.');
            AjaxController::spitResponse($response);
        }

        if (!validateCsrfToken($this->csrf)) {
            $response   = new AjaxController(401, null, 'Unauthorized Access.');
            AjaxController::spitResponse($response);
        }

        return true;
    }

    abstract public function getAllData($method);
    abstract public function getSingleData($method);
    abstract public function updateData($method);
    abstract public function insertData($method);
    abstract public function deleteData($method);
    public function customUpdate($method)
    {
        //TODO
    }
    public function getCustomData($method)
    {
        //TODO
    }
    public function customInsert($method)
    {
        //TODO
    }
}
