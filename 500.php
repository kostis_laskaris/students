<?php
include('init.php');
?>
<!DOCTYPE html>
<html lang="el">
<head>
   <?php include('_parts/header.php'); ?>
   <title>Μαθητολόγιο Πολύτροπο :: Error 500</title>
</head>
<body>
   <div class="wrapper">
      <div class="abs-center abs-fixed wd-xl mt-5">
         <!-- START card-->
         <div class="text-center mb-4">
            <div class="mb-3">
               <em class="fa fa-wrench fa-5x text-muted"></em>
            </div>
            <div class="text-lg mb-3">500</div>
            <p class="lead m-0">Επιλέξατε Λάθος Σχολικό Έτος</p>
            <p>Δεν υπάρχουν εγγραφές.</p>
            <p>Παρακαλώ ξαναπροσπαθήστε, επιλέγοντας κάποιο διαφορετικό.</p>
         </div>
         <ul class="list-inline text-center text-sm mb-4">
            <li class="list-inline-item">
               <a class="text-muted" href="login.php">Login</a>
            </li>
            <li class="text-muted list-inline-item">|</li>
            <li class="list-inline-item">
               <a class="text-muted" href="https://www.google.com">Google</a>
            </li>
            <li class="text-muted list-inline-item">|</li>
            <li class="list-inline-item">
               <a class="text-muted" href="https://www.politropo.gr">Politropo.gr</a>
            </li>
         </ul>
         <div class="p-3 text-center">
            <span class="mr-2">&copy;</span>
            <span>2018</span>
            <span class="mr-2">-</span>
            <span>All Right Reserved</span>
            <br>
            <span>Φροντιστήριο Πολύτροπο</span>
         </div>
      </div>
   </div>

   <?php include('_parts/scripts.php'); ?>
</body>

</html>