<?php
include('init.php');
?>
<!DOCTYPE html>
<html lang="el">
<head>   
   <?php include('_parts/header.php'); ?>
   <title>Μαθητολόγιο Πολύτροπο :: Error 401 - Unauthorized</title> 
</head>
<body>
   <div class="wrapper">
      <div class="abs-center abs-fixed wd-xl">
         <!-- START card-->
         <div class="text-center mb-4">
            <div class="mb-3">
               <em class="fas fa-ban fa-6x text-danger"></em>
            </div>
            <div class="text-lg mb-0">401</div>
            <div class="text-lg mb-2">- Unauthorized -</div>
            <p class="lead m-1">You have no permission to see content</p>
            <p><b>Bye</b></p>
         </div>
         <div class="p-3 text-center">
          <i class="far fa-copyright"></i> Φροντιστήριο Πολύτροπο</span>
       </div>
    </div>
 </div>
  <?php include('_parts/scripts.php'); ?>
</body>

</html>