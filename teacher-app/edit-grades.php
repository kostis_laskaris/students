<?php
session_start();
include("../init.php");

if (!isLoggedIn()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

// $allGrades = Grade::getAllGrades();

$lessonDropdown = Lesson::getLessonsAsDropdown('lesson');
$usersDropdown = User::getUsersAsDropdown('searchUser');
$studentsDropdown = Student::getAllStudentsAsDropdown('searchStudent');

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup('');

?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Επεξεργασία Βαθμολογιών</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed" id="theAnimo">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div>
                        Επεξεργασία Βαθμολογιών
                        <div class="d-flex mt-2">
                            <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                            <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'  => '#',
                        'title' => 'Επεξεργασία Βαθμολογιών',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <?php renderCustomMessages(); ?>
                <div class="row mt-2" id="animoSort">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <div class="card-title">
                                    <h4 class="mb-0 py-2">Πίνακας Βαθμολογιών</h4>
                                </div>
                                <div class="d-flex align-items-center">
                                    <a class="btn btn-info mr-2 text-bold" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                        <i class="fas fa-align-justify mr-2"></i>Προβολή Όλων
                                    </a>
                                    <a class="btn btn-purple mr-2 text-bold" href="#!" data-toggle="modal" data-target="#filterGradesModal" title="Φίλτρα Αναζήτησης">
                                        <i class="fas fa-filter mr-2"></i>Φίλτρα
                                    </a>
                                    <a class="btn btn-warning mr-2 text-bold" style="display:none;" href="#!" id="removeFilterGradesModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
                                        <i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
                                    </a>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover w-100" id="edit_grades_tbl">
                                        <thead>
                                            <tr>
                                                <th>Ημερομηνία Εξέτασης</th>
                                                <th>Ονοματεπώνυμο Μαθητή</th>
                                                <th>Μάθημα</th>
                                                <th>Τμήμα</th>
                                                <th>Είδος Εξέτασης</th>
                                                <th>Παρουσία</th>
                                                <th>Βαθμός</th>
                                                <th style="max-width: 130px; min-width: 130px; width: 130px;">Επιλογές</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script>
        var currentUserId = <?php echo $_SESSION['user_id']; ?>;
    </script>
    <script src=<?php initHelper::getWebTeacherPath("/js/edit-grades.js"); ?>></script>
    <?php
    //filter modal
    $modal = new Modal([
        'modalId' => 'filterGradesModal',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-purple',
        'modalHeader' => '<h4 id="filter-student-modal-title" class="modal-title"><i class="fas fa-filter mr-2"></i>Φίλτρα Αναζήτησης Βαθμολογίας</h4>',
        'modalBody' => '
		<div class="row mt-2">
            <div class="col-lg-4 mt-2">
                <label for="searchStudent">Όνομα Μαθητή</label>
                ' . $studentsDropdown . '
            </div>
			<div class="col-lg-4 mt-2 px-1">
                <label for="">Είδος Εξέτασης</label>
                <select class="custom-select custom-select-md" name="searchExam_kind" id="searchExam_kind">
                    <option value="">Επιλογή</option>
                    <option value="TEST">TEST</option>
                    <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                    <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                </select>
            </div>
			<div class="col-lg-4 mt-2 pl-1">
				<label for="">Παρουσία</label>
				<select class="custom-select custom-select-md" name="searchAbsence" id="searchAbsence">
					<option value="">Επιλογή</option>
					<option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
					<option value="ΑΠΩΝ">ΑΠΩΝ</option>
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-4 mt-2">
				<label for="">Βαθμός</label>
				<div class="d-flex pb-1">
					<input class="form-control w-45 mr-1" type="text" placeholder="Τελεστής" id="searchTelestis" name="searchTelestis" maxlength="2">
					<input class="form-control w-55" type="number" min="0" max="100" id="searchGrade" name="searchGrade" placeholder="Βαθμός">
				</div>
				<span class="small text-bold text-warning" id="accepted-telestes">* Αποδεκτές Τιμές: >, <, =, >=, <=, !=</span>
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Από</label>
				<input type="date" class="form-control" id="searchDate_from" name="searchDate_from">
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Έως</label>
				<input type="date" class="form-control" id="searchDate_to" name="searchDate_to">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-12 mt-2">
                <label for="">Μαθήματα</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary inputBtn addLessonsBtn" title="Επιλογή μαθημάτων" data-target="#lessonsModal" data-toggle="modal" type="button">
                            <i class="fas fa-plus"></i>
                        </button>
                        <button class="btn btn-danger inputBtn removeLessonsBtn" type="button" title="Αποεπιλογή όλων">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <input class="form-control" type="text" placeholder="Μαθήματα.." id="searchLessons" name="searchLessons" readonly>
					<input type="hidden" name="searchLessonIds" id="searchLessonIds">
                </div>
            </div>
		</div>
	',
        'modalFooter' => '<button class="btn btn-primary" id="initSearchBtn">Search</button><button class="btn btn-warning" id="clearSearchBtn">Clear Filters</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    //edit grade modal
    $modal = new Modal([
        'modalId' => 'editGradeModal',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-info',
        'modalHeader' => '<h4 class="modal-title" id="myModalLabelLarge4"><em class="fas fa-user-edit float-left fa-150x mr-2"></em>Επεξεργασία Βαθμολογίας</h4>',
        'modalBody' => '
        <form id="editGradeForm">
	<div class="col-md-12">
	<div class="modal-contents" id="animoSpin">
    <div class="row">
	<input type="hidden" id="editGradeId" name="editGradeId">
	<div class="col-lg-6 mb-3">
	<label for="lesson">Μάθημα <span class="reqField">(*)</span></label>
	' . $lessonDropdown . '
    </div>
	<div class="col-lg-6 mb-3">
	<label for="exam_date">Ημερομηνία Εξέτασης <span class="reqField">(*)</span></label>
	<input class="form-control" type="date" id="exam_date" name="exam_date" placeholder="-- No Value --">
	</div>
	</div>
    <div class="row">
	<div class="col-lg-4 mb-3">
    <label for="email">Είδος Εξέτασης <span class="reqField">(*)</span></label>
	<select class="custom-select custom-select-md" name="exam_kind" id="exam_kind">
        <option value="TEST">TEST</option>
        <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
        <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
    </select>
    </div>
	<div class="col-lg-4 mb-3">
    <label for="absence">Παρουσία <span class="reqField">(*)</span></label>
    <select class="custom-select custom-select-md" name="absence" id="absence">
        <option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
        <option value="ΑΠΩΝ">ΑΠΩΝ</option>
    </select>
	</div>
	<div class="col-lg-4 mb-3">
	<label for="grade">Βαθμός <span class="reqField">(*)</span></label>
	<input class="form-control" type="number" id="grade" name="grade" min="0" max="100" placeholder="Βαθμός..">
	</div>
    </div>
    <div class="row">
    <div class="col-lg-12 mb-3">
        <label for="comments">Σχόλια</label>
        <textarea class="form-control" name="comments" id="comments" rows="5" maxlength="300" placeholder="Σχόλια.."></textarea>
        <span class="d-block text-right small pt-1" id="letter-count"></span>
    </div
    </div>
	</div>
	</div>
    </div>
    </form>
	',
        'modalFooter' => '<button class="btn btn-success btnw-3" id="saveEditGradeBtn" type="button">Save</button>',
        'animoId' => 'editUserModalAnimo'
    ]);
    echo $modal->createModal();
    //create lesson modal
    $modal = new Modal([
        'modalId' => 'lessonsModal',
        'modalStyle' => 'z-index: 1060',
        'modalSize' => 'modal-xl',
        'modalBgHeaderColor' => 'bg-primary',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-search float-left fa-150x mr-2"></em>Αναζήτηση με κριτήριο μαθήματα</h4>',
        'modalBody' => '
		<input type="hidden" id="lessonSelected" name="lessonSelected">
		' . $lessonBoxGroup . '
		',
        'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    //delete student modal
	$modal = new Modal([
		'modalId' => 'deleteGradeModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Βαθμολογίας</h4>',
		'modalBody' => '
	    <input type="hidden" id="gradeDeleteId" name="gradeDeleteId">
	    <p class="py-3 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε τη βαθμολογία;</p>
	    ',
		'modalFooter' => '<button class="btn btn-success btnw-3" type="button" id="saveDeleteGradeBtn">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
    ?>
</body>

</html>