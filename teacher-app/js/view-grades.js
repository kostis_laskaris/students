$(document).ready(function () {

    var viewGradesJs = {
        $elements: {
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                lessons: $('#searchLessons'),
                lessonIds: $('#searchLessonIds'),
                studentId: $('#searchStudent'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
                searchLoader: $('#searchingLoader'),
                lessonModal: $('#lessonsModal'),
                removeLessonsBtn: $('.removeLessonsBtn'),
            },
            modals: {
                gradeLessonsPicker: {
                    theModal: $('#lessonsModal'),
                    saveLessons: $('#saveLessons'),
                },
                searchModal: {
                    theModal: $('#filterGradesModal'),
                },
            },
            viewGradesCard: $('#viewGradesCard'),
            viewGradesTbl: $('#view_grades_tbl'),
            assignmentsBox: $('#assignmentsBox'),
            alertBox: $('#alertBox'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
            url: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
            statsEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php'],
        },
        resetSearchFields: function () {
            Helpers.emptyFields(viewGradesJs.$elements.searchElements);
            unselectAll();
        },
        getEndPoint: function () {
            return viewGradesJs.$elements.url.join('/');
        },
        getSearchEndPoint: function () {
            return viewGradesJs.$elements.searchEndpointUrl.join('/');
        },
        getStatsEndPoint: function () {
            return viewGradesJs.$elements.statsEndpointUrl.join('/');
        },
        init: function () {
            viewGradesJs.initViewGradesTable();
            viewGradesJs.bindSaveLessons();
            viewGradesJs.bindRemoveLessons();
            viewGradesJs.bindSearchBtn();
            viewGradesJs.bindResetSearchBtn();
        },
        bindSaveLessons: function () {
            viewGradesJs.$elements.modals.gradeLessonsPicker.saveLessons.on('click', function () {
                var lessons = '';
                var ids = '';
                $('#assignmentsBox').find(':input').each(function () {
                    if ($(this).prop("checked")) {
                        lessons += $(this).attr("name") + ", ";
                        ids += $(this).attr("id") + ",";
                    }
                });
                viewGradesJs.$elements.searchElements.lessons.val(lessons.substring(0, lessons.length - 2));
                viewGradesJs.$elements.searchElements.lessonIds.val(ids.substring(0, ids.length - 1));
                viewGradesJs.$elements.modals.gradeLessonsPicker.theModal.modal('hide');
            });
        },
        bindRemoveLessons: function () {
            viewGradesJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
                Helpers.emptyField(viewGradesJs.$elements.searchElements.lessons);
                Helpers.emptyField(viewGradesJs.$elements.searchElements.lessonIds);
                unselectAll();
            });
        },
        bindSearchBtn: function () {
            viewGradesJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!viewGradesJs.isTelestisAccepted(viewGradesJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterGradesModal').show();
                var searchData = {
                    user_id: currentUserId,
                    exam_kind: viewGradesJs.$elements.searchElements.examKind.val(),
                    absence: viewGradesJs.$elements.searchElements.absence.val(),
                    grade: viewGradesJs.$elements.searchElements.telestis.val() + ":" + viewGradesJs.$elements.searchElements.grade.val(),
                    date_from: viewGradesJs.$elements.searchElements.examDateFrom.val(),
                    date_to: viewGradesJs.$elements.searchElements.examDateTo.val(),
                    lesson_id: viewGradesJs.$elements.searchElements.lessonIds.val(),
                    student_id: viewGradesJs.$elements.searchElements.studentId.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                        console.log(jqXHR);
                    },
                });
            });
        },
        bindResetSearchBtn: function () {
            $('#removeFilterGradesModal').on("click", function () {
                $('#removeFilterGradesModal').hide();
                viewGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        userId: currentUserId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewGradesJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            viewGradesJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterGradesModal').hide();
                viewGradesJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewGradesJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        userId: currentUserId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSort');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSort');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewGradesJs.renderGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        initViewGradesTable: function () {
            jQuery.ajax({
                url: viewGradesJs.getEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data:{
                    fireAction: 'grades_by_user_id',
                    userId: currentUserId,
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSort');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSort');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewGradesJs.renderGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        viewGradesJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        viewGradesJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderGradesTableData: function (the_data) {
            destroyDatatable('view_grades_tbl');
            if (the_data == 1) {
                viewGradesJs.$elements.viewGradesTbl.find('tbody').remove();
            } else {
                var cntExam = 0;
                var cntTest = 0;
                var cntErgasia = 0;
                var cntParon = 0;
                var cntApon = 0;
                var tbody = '';
                $.each(the_data, function (key, grade) {
                    grade.absence == "ΠΑΡΩΝ" ? cntParon++ : cntApon++;
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                        cntExam++;
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                        cntErgasia++;
                    } else {
                        cntTest++;
                    }
                    tbody += `
                <tr>
                    <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                    <td>${grade.fullStudent.fullName}</td>
                    <td>${grade.fullLesson.name}</td>
                    <td>${grade.fullTmima.name}</td>
                    <td><span class="badge ${badgeColor}">${grade.exam_kind}</span></td>
                    <td><span class="badge ${(grade.absence == "ΠΑΡΩΝ") ? 'badge-success' : 'badge-danger'}">${grade.absence}</span></td>
                    <td><b>${grade.grade}</b></td>
                    <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                    <td class="text-center">
                        <a title="Πλήρης Προβολή: ${grade.fullStudent.fullName}" href="view-student?sid=${Helpers.utf8_to_b64(grade.fullStudent.id)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
                            <em class="fas fa-external-link-alt fa-fw"></em>
                        </a>
                    </td>
                </tr>
                `;
                });
                viewGradesJs.$elements.viewGradesTbl.find('tbody').empty().append(tbody);
                var tfooter = `
                <div class="d-flex justify-content-between py-1">
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-purple">ΔΙΑΓΩΝΙΣΜΑ</span> : ${cntExam}
                        </div>
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-primary">TEST</span> : ${cntTest}
                        </div>
                        <div>
                            <span class="badge badge-green">ΕΡΓΑΣΙΑ</span> : ${cntErgasia}
                        </div>                    
                    </div>
                    <div class="d-flex text-bold">
                        <div class=" pr-2 mr-2">
                            <span class="badge badge-success">ΠΑΡΩΝ</span> : ${cntParon}
                        </div>
                        <div>
                            <span class="badge badge-danger">AΠΩΝ</span> : ${cntApon}
                        </div>
                    </div>
                </div>
                `;
                viewGradesJs.$elements.viewGradesCard.find('div.card-footer').empty().append(tfooter);
            }
            viewGradesJs.initViewGradeDatatable();
        },
        initViewGradeDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [0, 'desc'],
                    id: 'view_grades_tbl',
                    scrollx: true,
                    scrolly: "70vh",
                    downloadTitle: "Πίνακας Βαθμολογιών Πολύτροπο",
                    buttons: true,
                }

            );
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || viewGradesJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    }

    viewGradesJs.init();
});
function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}