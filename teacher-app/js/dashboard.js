$(document).ready(function () {
    var dashboardJs = {
        $elements: {
            updateChartBtn: $('#updateChartBtn'),
            lessonDropdown: $('#userLessons'),
            examKindDropdown: $('#examKind'),
            userLessonsForTableDorpdown: $('#userLessonsForTable'),
            gradesPerStudentAndLessonTable: $('#gradesPerStudentAndLessonTable'),
            latestErgasiesTable: $('#latestErgasiesTable'),
            latestExamsTable: $('#latestExamsTable'),
            latestTestsTable: $('#latestTestsTable'),
            gradeEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            gradeStatsEndpointUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php'],
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            dashboardJs.$elements.alertBox.removeClass();
            dashboardJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            dashboardJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            dashboardJs.$elements.alertBox.find('#alertMessage').html(message);
            dashboardJs.$elements.alertBox.show();
        },
        getGradeEndPoint: function () {
            return dashboardJs.$elements.gradeEndpointUrl.join("/");
        },
        getGradeStatsEndPoint: function () {
            return dashboardJs.$elements.gradeStatsEndpointUrl.join("/");
        },
        init: function () {
            dashboardJs.initLatestGradesTables();
            dashboardJs.initStudentStatsTable();
            dashboardJs.initGradesAvgPerMonthChart(true);
            dashboardJs.$elements.userLessonsForTableDorpdown.prop("selectedIndex", 1).change();
        },
        initLatestGradesTables: function () {
            jQuery.ajax({
                url: dashboardJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'last_n_grades_by_user_and_exam_kind',
                    gradeCount: 7
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('latestExamsAnimo');
                    addStandardWhirlAnime('latestTestsAnimo');
                    addStandardWhirlAnime('latestErgasiesAnimo');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('latestExamsAnimo');
                    removeStandardWhirlAnime('latestTestsAnimo');
                    removeStandardWhirlAnime('latestErgasiesAnimo');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        dashboardJs.renderGradeTablesData({
                            theData: response.data.exams,
                            theHtmlTableElement: dashboardJs.$elements.latestExamsTable
                        });
                        dashboardJs.renderGradeTablesData({
                            theData: response.data.test,
                            theHtmlTableElement: dashboardJs.$elements.latestTestsTable
                        });
                        dashboardJs.renderGradeTablesData({
                            theData: response.data.ergasia,
                            theHtmlTableElement: dashboardJs.$elements.latestErgasiesTable
                        });
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    if (jqXHR.responseJSON) {
                        dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        dashboardJs.manageAlertBox('danger', errorThrown);
                    }
                    console.log(errorThrown);
                    console.log(jqXHR);
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        initStudentStatsTable: function () {
            dashboardJs.$elements.userLessonsForTableDorpdown.on("change", function () {
                jQuery.ajax({
                    url: dashboardJs.getGradeStatsEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    data: {
                        fireAction: 'grade_stats_per_student_by_current_user_id',
                        lesson_id: $(this).val(),
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('gradesPerStudentAndLessonAnimo');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('gradesPerStudentAndLessonAnimo');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            destroyDatatable('gradesPerStudentAndLessonTable');
                            if (response.data == 1) {
                                dashboardJs.$elements.gradesPerStudentAndLessonTable.find('tbody').remove();
                            } else {
                                var item = '';
                                $.each(response.data, function (key, stat) {
                                    item += `
                                    <tr>        
                                    <td>
                                        <div class="${stat.avgGrade < 50 ? 'text-danger' : 'text-green'}">
                                        ${stat.avgGrade < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                                        </div>
                                    </td>
                                    <td class="d-none">${stat.lessonName}</td>
                                    <td>${stat.studentLastname}</td>
                                    <td>${stat.studentFirstname}</td>
                                    <td class="text-center">${stat.countGrade}</td>
                                    <td class="${stat.avgGrade > 50 ? 'text-green' : 'text-danger'} text-bold text-center">${stat.avgGrade == null?"0":stat.avgGrade}%</td>
                                    <td class="text-center">${stat.countAbsence}</td>
                                    <td class="text-center">${stat.maxGrade}%</td>
                                    <td class="text-center">${stat.minGrade == null?"0":stat.minGrade}%</td>
                                    <td class="text-center">
                                        <a title="Πλήρης Προβολή: ${stat.studentLastname + " " + stat.studentFirstname}" href="view-student?sid=${Helpers.utf8_to_b64(stat.studentId)}" class="btn btn-sm btn-green mr-1 command-edit text-white">
                                            <em class="fas fa-external-link-alt fa-fw"></em>
                                        </a>
                                    </td>
                                    </tr>
                                    `;
                                });
                                dashboardJs.$elements.gradesPerStudentAndLessonTable.find('tbody').empty().append(item);
                            }
                            dashboardJs.initStudentStatsDatatable();
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            dashboardJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
        },
        renderGradeTablesData: function (data) {
            if (data.theData == 1) {
                data.theHtmlTableElement.empty().append('<td class="text-center text-danger py-3">Δεν βρέθηκαν βαθμολογίες.</td>');
            } else {
                var item = '';
                $.each(data.theData, function (key, grade) {
                    item += `
                    <tr>
                    <td class="py-2 pr-0 align-middle">
                        <span class="fa-stack">
                            <em class="icon-star fa-stack-2x text-purple"></em>
                        </span>
                    </td>
                    <td class="py-2 pl-1">
                        <div class="text-bold">
                            <a href="view-student?sid=${Helpers.utf8_to_b64(grade.fullStudent.id)}">
                                ${grade.fullStudent.fullName}
                            </a>
                        </div>
                        <div>
                            <small class="mr-1 pr-1 br">${Helpers.dateOnlyConverter(grade.exam_date)}</small>
                            <small>${grade.fullLesson.name}</small>
                        </div>
                    </td>
                    <td class="py-2 text-center align-middle text-right text-bold">
                        <div class="${parseInt(grade.grade) > 50 ? 'text-green' : 'text-danger'}">
                            ${grade.grade}%
                        </div>
                        <div class="mt-1 badge ${grade.absence == 'ΠΑΡΩΝ' ? 'badge-success' : 'badge-danger'}">
                            ${grade.absence}
                        </div>
                    </td>
                    </tr>
                    `;
                });
                data.theHtmlTableElement.find('tbody').empty().append(item);
            }
        },
        initGradesAvgPerMonthChart: function (init = false) {
            dashboardJs.$elements.updateChartBtn.on("click", function () {
                jQuery.ajax({
                    url: dashboardJs.getGradeStatsEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    data: {
                        fireAction: 'grade_avg_per_month_per_user_and_lesson_and_kind',
                        exam_kind: dashboardJs.$elements.examKindDropdown.val(),
                        lesson_id: dashboardJs.$elements.lessonDropdown.val(),
                    },
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('gradeAvgPerMonth');
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('gradeAvgPerMonth');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            if (response.data == 1) {
                                $('#gradesAvgPerMonthChart').empty().append(`
                                    <h5 class="text-danger text-center">Δε βρέθηκαν δεδομένα.</h5>
                                `);
                                return false;
                            }
                            var labels = [];
                            var data = [];
                            $.each(response.data, function (key, stat) {
                                labels.push(Helpers.dateOnlyConverter(stat.exam_date));
                                data.push(parseFloat(stat.gradeAvg));
                            });
                            var lineData = {
                                labels: labels,
                                datasets: [{
                                    data: data,
                                    backgroundColor: 'rgba(114,102,186,0.2)',
                                    borderColor: 'rgba(114,102,186,1)',
                                    pointBorderColor: '#fff',
                                    pointRadius: 5,
                                    pointBackgroundColor: '#443d71',
                                }]
                            };

                            var lineOptions = {
                                legend: {
                                    display: false
                                },
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            min: 0,
                                            max: 100,
                                            stepSize: 20,
                                        }
                                    }]
                                }
                            };
                            $('#gradesAvgPerMonthChart').empty().append(`<canvas style="height: 350px;"></canvas>`);
                            var piectx = document.getElementById('gradesAvgPerMonthChart').getElementsByTagName('canvas')[0].getContext('2d');
                            var hBarChart = new Chart(piectx, {
                                data: lineData,
                                type: 'line',
                                options: lineOptions
                            });
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        if (jqXHR.responseJSON) {
                            dashboardJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                        } else {
                            dashboardJs.manageAlertBox('danger', errorThrown);
                        }
                        console.log(errorThrown);
                        console.log(jqXHR);
                        setTimeout(function () {
                            $(window).scrollTop(0);
                        }, 500);
                    },
                });
            });
            if (init == true) {
                dashboardJs.$elements.lessonDropdown.prop("selectedIndex", 1).change();
                dashboardJs.$elements.updateChartBtn.click();
            }
        },
        initStudentStatsDatatable: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [2, 'asc'],
                    columnDefs: [{
                        targets: 0,
                        orderable: false
                    }],
                    id: 'gradesPerStudentAndLessonTable',
                    scrollx: true,
                    scrolly: "50vh",
                    downloadTitle: "Πίνακας Βαθμολογιών Μαθητών ανά Μάθημα",
                    buttons: true,
                }
            );
        },
    }
    dashboardJs.init();
});