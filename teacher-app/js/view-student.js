$(document).ready(function () {

    var viewStudentJs = {
        $elements: {
            studentStatsCard: {
                lessonNameField: $('#lessonNameField'),
                latestDateField: $('#latestDateField')
            },
            searchElements: {
                teacher: $('#searchUser'),
                absence: $('#searchAbsence'),
                examKind: $('#searchExam_kind'),
                telestis: $('#searchTelestis'),
                grade: $('#searchGrade'),
                lessons: $('#searchLessons'),
                lessonIds: $('#searchLessonIds'),
                examDateFrom: $('#searchDate_from'),
                examDateTo: $('#searchDate_to'),
                resetBtn: $('#resetBtn'),
                initSearchBtn: $('#initSearchBtn'),
                clearSearchBtn: $('#clearSearchBtn'),
                searchLoader: $('#searchingLoader'),
                lessonModal: $('#lessonModal'),
                removeLessonsBtn: $('.removeLessonsBtn'),
            },
            modals: {
                studentlessonsPicker: {
                    theModal: $('#lessonsModal'),
                    saveLessons: $('#saveLessons'),
                },
                searchModal: {
                    theModal: $('#filterStudentGradesModal'),
                },
            },
            lessonStatsCard: $('#lessonStatsCard'),
            teacherLessonDropdown: $('#teacherLessonId'),
            studentName: $('#studentName'),
            studentClass: $('#studentClass'),
            studentTmima: $('#studentTmima'),
            gradeStatsTables: null,
            statsContainer: $('#statsContainer'),
            gradesTable: $('#student_grades_tbl'),
            studentGradesCard: $('#studentGradesCard'),
            studentId: null,
            assignmentsBox: $('#assignmentsBox'),
            alertBox: $('#alertBox'),
            acceptedTelestes: ['>', '<', '>=', '<=', '=', '!='],
            studentsUrl: ['..', 'php_actions', 'Controllers', 'StudentController.php'],
            gradesUrl: ['..', 'php_actions', 'Controllers', 'GradeController.php'],
            url: ["..", "php_actions", "get_actions", "get_grade_search.php"],
            searchEndpointUrl: ['..', 'php_actions', 'Controllers', 'SearchGradeController.php'],
            statsUrl: ['..', 'php_actions', 'Controllers', 'GradeStatsController.php']
        },
        resetSearchFields: function () {
            Helpers.emptyFields(viewStudentJs.$elements.searchElements);
            unselectAll();
        },
        manageAlertBox: function (mode, message) {
            let errorHeader = 'success';
            if (mode == "danger") {
                errorHeader = 'error';
            }
            viewStudentJs.$elements.alertBox.removeClass();
            viewStudentJs.$elements.alertBox.addClass('alert-' + mode + ' bg-' + mode + '-dark alert alert-dismissible fade show');
            viewStudentJs.$elements.alertBox.find('#alertBoxHeader').text(errorHeader.toUpperCase());
            viewStudentJs.$elements.alertBox.find('#alertMessage').html(message);
            viewStudentJs.$elements.alertBox.show();
        },
        init: function () {
            viewStudentJs.$elements.studentId = Helpers.b64_to_utf8(Helpers.urlParam('sid'));
            viewStudentJs.initStudentData();
            viewStudentJs.renderStudentGradesData();
            viewStudentJs.bindSaveLessons();
            viewStudentJs.bindRemoveLessons();
            viewStudentJs.bindSearchBtn();
            viewStudentJs.bindResetSearchBtn();
            viewStudentJs.bindShowStatsAction();
            viewStudentJs.$elements.teacherLessonDropdown.prop("selectedIndex", 1).change();
        },
        getGradeEndPoint: function () {
            return this.$elements.gradesUrl.join('/');
        },
        getStudentEndPoint: function () {
            return this.$elements.studentsUrl.join('/');
        },
        getEndPoint: function () {
            return this.$elements.url.join('/');
        },
        getSearchEndPoint: function () {
            return viewStudentJs.$elements.searchEndpointUrl.join('/');
        },
        getGradeStatsEndPoint: function () {
            return this.$elements.statsUrl.join('/');
        },
        initStudentData: function () {
            jQuery.ajax({
                url: viewStudentJs.getStudentEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_SINGLE_DATA",
                    "Method": "GET",
                },
                data: {
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: "student_by_id"
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSortStudentGrades');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSortStudentGrades');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewStudentJs.$elements.studentName.text(response.data.fullName);
                        viewStudentJs.$elements.studentClass.text(response.data.class.list_entry);
                        viewStudentJs.$elements.studentTmima.text(response.data.tmima.name);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    if (jqXHR.responseJSON) {
                        editStudentJs.manageAlertBox('danger', jqXHR.responseJSON.errorMessage);
                    } else {
                        editStudentJs.manageAlertBox('danger', errorThrown);
                    }
                    setTimeout(function () {
                        $(window).scrollTop(0);
                    }, 500);
                },
            });
        },
        renderStudentGradesData: function () {
            jQuery.ajax({
                url: viewStudentJs.getGradeEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                data: {
                    fireAction: 'grades_by_user_id_and_student_id',
                    userId: currentUserId,
                    studentId: viewStudentJs.$elements.studentId,
                },
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    addStandardWhirlAnime('animoSortStudentGrades');
                },
                complete: function (data) {
                    removeStandardWhirlAnime('animoSortStudentGrades');
                },
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        viewStudentJs.renderStudentGradesTableData(response.data);
                    }
                },
                error: function (jqXHR, error, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR);
                    var tbody = `
                    <tr>
                        <td class="text-center text-danger text-bold" style="font-size: 120%" colspan="8">Συνέβη Σφάλμα:</td>
                    </tr>
                    <tr>
                        <td class="text-center text-bold" style="font-size: 120%" colspan="8">${errorThrown}</td>
                    </tr>`;
                    viewStudentJs.$elements.gradesTable.find('tbody').empty().append(tbody);
                },
            });
        },
        bindSaveLessons: function () {
            viewStudentJs.$elements.modals.studentlessonsPicker.saveLessons.on('click', function () {
                var lessons = '';
                var ids = '';
                $('#assignmentsBox').find(':input').each(function () {
                    if ($(this).prop("checked")) {
                        lessons += $(this).attr("name") + ", ";
                        ids += $(this).attr("id") + ",";
                    }
                });
                viewStudentJs.$elements.searchElements.lessons.val(lessons.substring(0, lessons.length - 2));
                viewStudentJs.$elements.searchElements.lessonIds.val(ids.substring(0, ids.length - 1));
                viewStudentJs.$elements.modals.studentlessonsPicker.theModal.modal('hide');
            });
        },
        bindRemoveLessons: function () {
            viewStudentJs.$elements.searchElements.removeLessonsBtn.on('click', function () {
                Helpers.emptyField(viewStudentJs.$elements.searchElements.lessons);
                Helpers.emptyField(viewStudentJs.$elements.searchElements.lessonIds);
                unselectAll();
            });
        },
        bindResetSearchBtn: function () {
            $('#removeFilterStudentGradesModal').on("click", function () {
                $('#removeFilterStudentGradesModal').hide();
                viewStudentJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                        userId: currentUserId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
            viewStudentJs.$elements.searchElements.clearSearchBtn.on("click", function () {
                $('#removeFilterStudentGradesModal').hide();
                viewStudentJs.resetSearchFields();
                var searchData = {
                    reset: true
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                        userId: currentUserId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.searchElements.lessons.prop('readonly', true);
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        bindSearchBtn: function () {
            viewStudentJs.$elements.searchElements.initSearchBtn.on("click", function () {
                if (!viewStudentJs.isTelestisAccepted(viewStudentJs.$elements.searchElements.telestis.val())) {
                    alert('Μη αποδεκτός τελεστής.');
                    return false;
                }
                $('#removeFilterStudentGradesModal').show();
                var searchData = {
                    user_id: currentUserId,
                    exam_kind: viewStudentJs.$elements.searchElements.examKind.val(),
                    absence: viewStudentJs.$elements.searchElements.absence.val(),
                    grade: viewStudentJs.$elements.searchElements.telestis.val() + ":" + viewStudentJs.$elements.searchElements.grade.val(),
                    date_from: viewStudentJs.$elements.searchElements.examDateFrom.val(),
                    date_to: viewStudentJs.$elements.searchElements.examDateTo.val(),
                    lesson_id: viewStudentJs.$elements.searchElements.lessonIds.val(),
                };
                var saveBtn = $(this);
                var btnDiv = saveBtn.parent();
                jQuery.ajax({
                    url: viewStudentJs.getSearchEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        attribs: searchData,
                        studentId: viewStudentJs.$elements.studentId,
                    },
                    cache: false,
                    beforeSend: function () {
                        addStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').hide();
                        btnDiv.append(`<div style='float:right; font-weight: 600' class='ml-2 processingDiv'>Processing, please wait</div> <div style='float:right' class='ball-pulse'><div></div><div></div><div></div></div>`);
                        btnDiv.parent().find('input').prop('readonly', true);
                        btnDiv.parent().find('select').prop('disabled', true);
                    },
                    complete: function (data) {
                        removeStandardWhirlAnime('animoSortStudentGrades');
                        btnDiv.find('button').show();
                        btnDiv.find('.ball-pulse').hide();
                        btnDiv.find('.processingDiv').hide();
                        btnDiv.parent().find('input').prop('readonly', false);
                        btnDiv.parent().find('select').prop('disabled', false);
                        viewStudentJs.$elements.modals.searchModal.theModal.modal('hide');
                    },
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStudentGradesTableData(response.data);
                        } else {
                            alert(response.errorMessage);
                            console.log(response);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        alert('Η αναζήτηση απέτυχε. Error code: ' + error);
                        console.log(errorThrown);
                    },
                });
            });
        },
        bindShowStatsAction: function () {
            viewStudentJs.$elements.teacherLessonDropdown.on("change", function () {
                var lesson_id = $(this).val();
                jQuery.ajax({
                    url: viewStudentJs.getGradeStatsEndPoint(),
                    type: 'GET',
                    headers: {
                        "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                        "Action": "GET_CUSTOM_DATA",
                        "Method": "GET",
                    },
                    dataType: 'json',
                    data: {
                        studentId: viewStudentJs.$elements.studentId,
                        lessonId: lesson_id,
                        fireAction: 'get_student_stats_by_lesson_id_for_current_user',
                    },
                    cache: false,
                    success: function (response, textStatus, jQxhr) {
                        if (response.code === 200) {
                            viewStudentJs.renderStatsCardsData(response.data);
                        }
                    },
                    error: function (jqXHR, error, errorThrown) {
                        console.log(errorThrown);
                        console.log(jqXHR);
                    },
                });
                viewStudentJs.initFlotChard(lesson_id);
            });
        },
        renderStatsCardsData: function (theData) {
            if (theData === 1) {
                return false;
            }
            var grade20 = (theData.gradeAvg * 20 / 100).toFixed(1);
            viewStudentJs.$elements.studentStatsCard.lessonNameField.text(theData.lessonName);
            viewStudentJs.$elements.studentStatsCard.latestDateField.text(Helpers.dateOnlyConverter(theData.latestExamDate));
            var cardBody = `
				<p class="display-4 text-center ${theData.gradeAvg < 50 ? 'text-danger' : 'text-green'}">
				    ${theData.gradeAvg}% <span style="font-size: 80%">(${grade20.replace(".", ",")})</span>
				</p>
				<div>
				    <table class="table lesson_stats" data-lessonid="${theData.lesson_id}">
					    <tbody>
						    <tr>
							    <td><strong>Αριθμός Βαθμολογιών</strong></td>
								<td>${theData.gradeCount}</td>
							</tr>
							<tr>
							    <td><strong>Μέγιστη</strong></td>
								<td>${theData.maxGrade}%</td>
							</tr>
							<tr>
							    <td><strong>Ελάχιστη</strong></td>
								<td>${theData.minGrade}%</td>
							</tr>
							<tr>
							    <td><strong>Μέσος Όρος Test</strong></td>
								<td>${theData.testAvg ? Math.round(theData.testAvg) + "%" : "---"}</td>
							</tr>
							<tr>
							    <td><strong>Μέσος Όρος Διαγωνισμάτων</strong></td>
								<td>${theData.examAvg ? Math.round(theData.examAvg) + "%" : "---"}</td>
							</tr>
							<tr>
								<td><strong>Μέσος Όρος Εργασιών</strong></td>
								<td>${theData.ergasiaAvg ? Math.round(theData.ergasiaAvg) + "%" : "---"}</td>
							</tr>
							<tr>
							    <td><strong>Απουσίες</strong></td>
								<td>${theData.absence}</td>
							</tr>
						</tbody>
					</table>
				</div>
            `;
            viewStudentJs.$elements.lessonStatsCard.find('div.card-body').empty().append(cardBody);
        },
        initFlotChard: function (lessonId) {
            var lessonIds = [];
            lessonIds.push(lessonId);
            // viewStudentJs.$elements.gradeStatsTables.each(function (key, table) {
            //     lessonIds.push($(this).data('lessonid'));
            // });
            jQuery.ajax({
                url: viewStudentJs.getGradeStatsEndPoint(),
                type: 'GET',
                headers: {
                    "Csrftoken": $('meta[name=csrf_token]').attr("content"),
                    "Action": "GET_CUSTOM_DATA",
                    "Method": "GET",
                },
                dataType: 'json',
                data: {
                    lessonIds: lessonIds,
                    studentId: viewStudentJs.$elements.studentId,
                    fireAction: 'grades_by_student_per_lesson_for_chart',
                },
                cache: false,
                success: function (response, textStatus, jQxhr) {
                    if (response.code === 200) {
                        $.each(response.data, function (lesson_id, lesson) {
                            if (lesson == 1) {
                                $('#lessonGradeLineChart').empty().append(`
                                <div class="text-danger mt-5 text-bold text-center">
                                    <i class="fas fa-exclamation-triangle fa-3x mb-3"></i>
                                    <p>Δεν βρέθηκαν βαθμολογίες, ή ο μαθητής δεν παρεβρέθηκε σε καμία εξέταση</p>
                                </div>
                                `);
                                return false;
                            }
                            var labels = [];
                            var grades = [];
                            $.each(lesson, function (key, data) {
                                labels.push(Helpers.dateConverterWithoutDay(data[0]));
                                grades.push(data[1]);
                            });
                            var lineData = {
                                labels: labels,
                                datasets: [{
                                    backgroundColor: 'rgba(114,102,186,0.2)',
                                    borderColor: 'rgba(114,102,186,1)',
                                    pointBorderColor: '#fff',
                                    data: grades,
                                    pointRadius: 5,
                                    pointBackgroundColor: '#443d71',
                                }]
                            };

                            var lineOptions = {
                                legend: {
                                    display: false
                                },
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            min: 0,
                                            max: 100,
                                            stepSize: 20,
                                        }
                                    }]
                                }
                            };
                            $('#lessonGradeLineChart').empty().append(`<canvas></canvas>`);
                            var linectx = document.getElementById('lessonGradeLineChart').getElementsByTagName('canvas')[0].getContext('2d');
                            var lineChart = new Chart(linectx, {
                                data: lineData,
                                type: 'line',
                                options: lineOptions
                            });
                        });
                    }
                }
            });
        },
        initDataTables: function () {
            initDatatable(
                {
                    pages: [10, 20, 50, 100, -1],
                    order: [2, 'desc'],
                    id: 'student_grades_tbl',
                    scrollx: true,
                    columnDefs: [{
                        targets: 0,
                        orderable: false
                    }],
                    scrolly: "60vh",
                    downloadTitle: "Πίνακας Μαθητών Πολύτροπο",
                    buttons: true,
                },
            );
        },
        renderStudentGradesTableData: function (the_data) {
            destroyDatatable('student_grades_tbl');
            var tbody = '';
            var cntExam = 0;
            var cntTest = 0;
            var cntErgasia = 0;
            var cntParon = 0;
            var cntApon = 0;
            if (the_data == 1) {
                viewStudentJs.$elements.gradesTable.find('tbody').remove();
            } else {
                $.each(the_data, function (key, grade) {
                    grade.absence == "ΠΑΡΩΝ" ? cntParon++ : cntApon++;
                    var badgeColor = 'badge-primary';
                    if (grade.exam_kind == "ΔΙΑΓΩΝΙΣΜΑ") {
                        badgeColor = 'badge-purple';
                        cntExam++;
                    } else if (grade.exam_kind == "ΕΡΓΑΣΙΑ") {
                        badgeColor = 'badge-green';
                        cntErgasia++;
                    } else {
                        cntTest++;
                    }
                    tbody += `
                    <tr>             
                        <td>
                            <div class="${grade.grade < 50 ? 'text-danger' : 'text-green'}">
                            ${grade.grade < 50 ? '<i class="fas fa-arrow-down"></i>' : '<i class="fas fa-arrow-up"></i>'}
                            </div>
                        </td>
                        <td>${grade.id}</td>
                        <td data-sort="${grade.exam_date}">${Helpers.dateOnlyConverter(grade.exam_date)}</td>
                        <td>
                            <span class="badge ${badgeColor}">${grade.exam_kind}</span>
                        </td>
                        <td>
                            <span class="badge ${grade.absence == "ΠΑΡΩΝ" ? 'badge-success' : 'badge-danger'}">${grade.absence}</span>
                        </td>
                        <td>${grade.fullLesson.name}</td>
                        <td><strong>${grade.grade}</strong></td>
                        <td data-sort="${grade.created_at}">${Helpers.dateConverter(grade.created_at)}</td>
                        <td>${!Helpers.isEmpty(grade.updated_at) ? grade.updated_at : "Καμία"}</td>
                    </tr>									
                    `
                });
            }
            viewStudentJs.$elements.gradesTable.find('tbody').empty().append(tbody);
            var tfooter = `
            <div class="d-flex justify-content-between py-1">
                <div class="d-flex text-bold">
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-purple">ΔΙΑΓΩΝΙΣΜΑ</span> : ${cntExam}
                    </div>
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-primary">TEST</span> : ${cntTest}
                    </div>
                    <div>
                        <span class="badge badge-green">ΕΡΓΑΣΙΑ</span> : ${cntErgasia}
                    </div>                    
                </div>
                <div class="d-flex text-bold">
                    <div class=" pr-2 mr-2">
                        <span class="badge badge-success">ΠΑΡΩΝ</span> : ${cntParon}
                    </div>
                    <div>
                        <span class="badge badge-danger">AΠΩΝ</span> : ${cntApon}
                    </div>
                </div>
            </div>
            `;
            viewStudentJs.$elements.studentGradesCard.find('div.card-footer').empty().append(tfooter);
            viewStudentJs.initDataTables();
        },
        isTelestisAccepted: function (telestis) {
            if (Helpers.isEmpty(telestis) || viewStudentJs.$elements.acceptedTelestes.includes(telestis)) {
                return true;
            }
            return false;
        },
    }
    viewStudentJs.init();
});

function selectCheckbox(args) {
    unselectAll();
    let dataArr = args.split(',');
    for (var i = 0; i < dataArr.length; i++) {
        document.getElementById(dataArr[i]).checked = true;
    }
}

function unselectAll() {
    $('#assignmentsBox').find(':input').each(function () {
        $(this).prop("checked", false);
    })
}