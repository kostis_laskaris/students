<?php
session_start();
include("../init.php");

if (!isLoggedIn()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$usersDropdown = User::getUsersAsDropdown('searchUser');
$shortcutBtnsGrp = Shortcut::getShortcutsAsButtonGroup();

$checkbox = new CheckboxGroup;
$checkbox->group_input_name = '';
$checkbox->cols = 4;
$checkbox->input_id_prefix = '';
$checkbox->container_id = 'assignmentsBox';
$lessonBoxGroup = $checkbox->createLessonWithShortcutsGroup($shortcutBtnsGrp);
?>
<!DOCTYPE html>
<html lang="el">

<head>
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Προβολή Μαθητή</title>
	<?php initHelper::getDatatablesCss(); ?>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container" id="theAnimo">
			<!-- Page content-->
			<div class="content-wrapper">
				<div class="content-heading">
					<div class="d-flex align-items-center">
						<div class="mr-3">
							<img src="../assets/img/user/student-avatar.png" width="60">
						</div>
						<div>
							<span id="studentName"></span>
							<div class="d-flex mt-2">
								<div class="pr-3 mr-3 br" style="font-size: 60%" id="studentClass"></div>
								<div class="pr-3 mr-3 br" style="font-size: 60%" id="studentTmima"></div>
								<div class="pr-3 mr-3 br" style="font-size: 60%"><?php echo $_SESSION['periodos_mathimaton']; ?></div>
								<div style="font-size: 60%"><?php echo $_SESSION['curr_school_year']; ?></div>
							</div>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'  => '#',
						'title' => 'Προβολή Μαθητή',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row" id="statsContainer">
					<div class="col-xl-5">
						<div class="card b shadow-sm" id="lessonStatsCard">
							<div class="card-header d-flex justify-content-between align-items-center py-2 border-bottom">
								<div>
									<h4 class="m-0" id="lessonNameField"></h4>
									<small class="text-muted">
										<i class="far fa-calendar-check"></i>
										<strong>Τελευταία Βαθμολογία: </strong>
										<span id="latestDateField"></span>
									</small>
								</div>
								<div>
									<?php echo User::getCurrentUserLessonsThatGradedByStudentIdAsDropdown('teacherLessonId', urldecode(base64_decode($_GET['sid']))); ?>
								</div>
							</div>
							<div class="card-body" style="height: 400px; max-height: 400px;">
							</div>
						</div>
					</div>

					<div class="col-xl-7">
						<div class="card b shadow-sm" id="lessonStatsCard">
							<div class="card-header d-flex justify-content-between align-items-center py-2 border-bottom">
								<div>
									<h4 class="m-0">
										Γρααφική Αναπαράσταση
									</h4>
									<small class="text-muted">Πορεία Μαθητή</small>
								</div>
								<div>
									<i class="fas fa-chart-line fa-2x"></i>
								</div>
							</div>
							<div class="card-body" style="height: 400px; max-height: 400px;" id="lessonGradeLineChart">
							</div>
						</div>
					</div>
				</div>
				<div class="row " id="animoSortStudentGrades">
					<div class="col-md-12">
						<div class="card card-default" id="studentGradesCard">
							<div class="card-header d-flex justify-content-between align-items-center">
								<div class="card-title">
									<h4 class="mb-0 py-2">Πίνακας Βαθμολογιών</h4>
								</div>
								<div class="d-flex align-items-center">
									<a class="btn btn-info mr-2 text-bold" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
										<i class="fas fa-align-justify mr-2"></i>Προβολή Όλων
									</a>
									<a class="btn btn-purple mr-2 text-bold" href="#!" data-toggle="modal" data-target="#filterStudentGradesModal" title="Φίλτρα Αναζήτησης">
										<i class="fas fa-filter mr-2"></i>Φίλτρα
									</a>
									<a class="btn btn-warning mr-2 text-bold" style="display:none;" href="#!" id="removeFilterStudentGradesModal" title="Εκκαθάριση Φίλτρων Αναζήτησης">
										<i class="fas fa-eraser mr-2"></i>Εκκαθάριση Φίλτρων
									</a>
								</div>
							</div>
							<div class="card-body border-top">
								<table class="table table-striped table-hover w-100" id="student_grades_tbl">
									<thead>
										<tr>
											<th></th>
											<th>ID</th>
											<th>Ημερομηνία Εξέτασης</th>
											<th>Είδος Εξέτασης</th>
											<th>Παρουσία</th>
											<th>Μάθημα</th>
											<th>Βαθμός</th>
											<th>Καταχωρήθηκε</th>
											<th>Τροποποίηθηκε</th>
										</tr>
									</thead>
									<tbody id="gradesTbody">
									</tbody>
								</table>
							</div>
							<div class="card-footer">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<?php initHelper::getDatatablesJs(); ?>
	<script src=<?php initHelper::getWebPath("/assets/vendor/chart.js/dist/Chart.js"); ?>></script>
	<script>
		var currentUserId = <?php echo $_SESSION['user_id']; ?>;
	</script>
	<script src=<?php initHelper::getWebTeacherPath("/js/view-student.js"); ?>></script>
	<?php
	//filter modal
	$modal = new Modal([
		'modalId' => 'filterStudentGradesModal',
		'modalSize' => 'modal-lg',
		'modalBgHeaderColor' => 'bg-purple',
		'modalHeader' => '<h4 id="filter-student-modal-title" class="modal-title"><i class="fas fa-filter mr-2"></i>Φίλτρα Αναζήτησης Βαθμολογίας</h4>',
		'modalBody' => '
		<div class="row mt-2">
			<div class="col-lg-4 mt-2">
				<label for="searchUser">Όνομα Καθηγητή</label>
				' . $usersDropdown . '
			</div>
			<div class="col-lg-4 mt-2">
                <label for="">Είδος Εξέτασης</label>
                <select class="custom-select custom-select-md" name="searchExam_kind" id="searchExam_kind">
                    <option value="">Επιλογή</option>
                    <option value="TEST">TEST</option>
                    <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                    <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                </select>
            </div>
			<div class="col-lg-4 mt-2">
				<label for="">Παρουσία</label>
				<select class="custom-select custom-select-md" name="searchAbsence" id="searchAbsence">
					<option value="">Επιλογή</option>
					<option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
					<option value="ΑΠΩΝ">ΑΠΩΝ</option>
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-4 mt-2">
				<label for="">Βαθμός</label>
				<div class="d-flex pb-1">
					<input class="form-control w-45 mr-1" type="text" placeholder="Τελεστής" id="searchTelestis" name="searchTelestis" maxlength="2">
					<input class="form-control w-55" type="number" min="0" max="100" id="searchGrade" name="searchGrade" placeholder="Βαθμός">
				</div>
				<span class="small text-bold text-warning" id="accepted-telestes">* Αποδεκτές Τιμές: >, <, =, >=, <=, !=</span>
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Από</label>
				<input type="date" class="form-control" id="searchDate_from" name="searchDate_from">
			</div>
			<div class="col-lg-4 mt-2">
				<label for="">Ημ. Εξέτασης Έως</label>
				<input type="date" class="form-control" id="searchDate_to" name="searchDate_to">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-lg-12 mt-2">
                <label for="">Μαθήματα</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary inputBtn addLessonsBtn" title="Επιλογή μαθημάτων" data-target="#lessonsModal" data-toggle="modal" type="button">
                            <i class="fas fa-plus"></i>
                        </button>
                        <button class="btn btn-danger inputBtn removeLessonsBtn" type="button" title="Αποεπιλογή όλων">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <input class="form-control" type="text" placeholder="Μαθήματα.." id="searchLessons" name="searchLessons" readonly>
					<input type="hidden" name="searchLessonIds" id="searchLessonIds">
                </div>
            </div>
		</div>
	',
		'modalFooter' => '<button class="btn btn-primary" id="initSearchBtn">Search</button><button class="btn btn-warning" id="clearSearchBtn">Clear Filters</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//delete student modal
	$modal = new Modal([
		'modalId' => 'deleteStudentModal',
		'modalSize' => '',
		'modalBgHeaderColor' => 'bg-danger',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-trash float-left fa-150x mr-2"></em>Διαγραφή Μαθητή</h4>',
		'modalBody' => '
		<p class="py-3 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να διαγράψετε τον/την μαθητή/τρια <span class="text-danger" id="fillStudentItem"></span> ;</p>
		',
		'modalFooter' => '<button class="btn btn-success btnw-3" id="dleteStudentBtn" type="button">Submit</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	//create lessons modal
	$modal = new Modal([
		'modalId' => 'lessonsModal',
		'modalSize' => 'modal-xl',
		'modalBgHeaderColor' => 'bg-primary',
		'modalHeader' => '<h4 class="modal-title"><em class="fas fa-book-open float-left fa-150x mr-2"></em>Προσθήκη Μαθημάτων</h4>',
		'modalBody' => '<input type="hidden" id="lessonSelected" name="lessonSelected">' . $lessonBoxGroup,
		'modalFooter' => '<button class="btn btn-success btnw-3" id="saveLessons" type="button">Save</button>',
		'animoId' => ''
	]);
	echo $modal->createModal();
	?>
</body>

</html>