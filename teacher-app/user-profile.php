<?php
session_start();
include("../init.php");

if (!isLoggedIn() || isAdmin()) {
	redirectTo(WEB_ROOT . '/logout.php');
	exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
	redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

?>
<!DOCTYPE html>
<html>

<head lang="el">
	<?php include('_parts/head.php'); ?>
	<title>Μαθητολόγιο Πολύτροπο :: Επεξεργασία Προφίλ</title>
	<?php initHelper::getAnimationCss(); ?>
	<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
	<meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
	<div class="wrapper">
		<?php include('_parts/header.php'); ?>
		<?php include('_parts/sidebar.php'); ?>
		<section class="section-container">
			<div class="content-wrapper">
				<div class="content-heading">
					<div>
						Επεξεργασία Προφίλ
						<div class="d-flex mt-2">
							<small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
							<small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
						</div>
					</div>
					<!------ BREACRUMBS ------->
					<?php
					echo getBreadcrumbs([
						'link'  => '#',
						'title' => 'Προφίλ Χρήστη',
					]);
					?>
					<!------ BREACRUMBS END ------->
				</div>
				<?php renderCustomMessages(); ?>
				<div class="row">
					<div class="col-xl-12">
						<div class="card card-default">
							<div class="card-header d-flex align-items-center">
								<div class="d-flex justify-content-center col">
									<h4 class="m-0 text-center">Προφίλ Χρήστη: <?php echo $_SESSION['username']; ?></h4>
								</div>
							</div>
							<div class="card-body">
								<form method="post" action="../php_actions/update_actions/update_user_profile.php" id="userProfileForm">
									<div class="row py-4 justify-content-center">
										<div class="col-12 col-sm-10">
											<input style="display:none;" type="text" name="userId" value="<?php echo $user->id; ?>">
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="firstname">Όνομα <span class="reqField">(*)</span></label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="firstname" name="firstname" type="text" placeholder="Κανένα Όνομα.." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="lastname">Επίθετο <span class="reqField">(*)</span></label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="lastname" name="lastname" type="text" placeholder="Κανένα Επίθετο.." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="email">Email <span class="reqField">(*)</span></label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="email" name="email" type="email" required placeholder="Κανένα Email..">
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="phone">Τηλέφωνο</label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="phone" name="phone" maxlength="10" type="text" placeholder="Κανένα Τηλέφωνο">
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="mobile">Κινητό Τηλέφωνο</label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="mobile" name="mobile" maxlength="10" type="text" placeholder="Κανένα Κινητό Τηλέφωνο..">
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="address">Διεύθυνση</label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="address" name="address" type="text" placeholder="Καμία Διεύθυνση..">
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="zip">Τ.Κ.</label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="zip" name="zip" maxlength="5" placeholder="Κανένας Τ.Κ.."></input>
												</div>
											</div>
											<div class="form-group row">
												<label class="text-bold col-xl-2 col-md-3 col-4 col-form-label text-right" for="city">Περιοχή</label>
												<div class="col-xl-10 col-md-9 col-8">
													<input class="form-control" id="city" name="city" type="text" placeholder="Καμία Περιοχή..">
												</div>
											</div>
											<p class="text-center">
												<i class="fas fa-exclamation-circle mr-1 text-primary fa-120x"></i>Τα πεδία με <span class="reqField">(*)</span> είναι υποχρεωτικά
											</p>
											<div class="text-right mt-4">
												<button class="btn btn-success btnw-4 mr-2" id="saveProfileBtn" data-id="<?php echo $user->id; ?>" type="button">
													<i class="far fa-save mr-1"></i>Save
												</button>
												<button class="btn btn-warning btnw-4" type="button" name="clearForm" id="clearForm" onclick="clearFormFields('userProfileForm');">
													<i class="fas fa-eraser mr-1"></i>Clear All
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include('_parts/footer.php'); ?>
	</div>
	<?php include('_parts/scripts.php'); ?>
	<script src=<?php initHelper::getWebTeacherPath("/js/user-profile.js"); ?>></script>
</body>

</html>