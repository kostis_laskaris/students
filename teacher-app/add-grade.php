<?php
session_start();
include("../init.php");

if (!isLoggedIn()) {
    redirectTo(WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();

$gradeStudents = Student::getStudentsForGrade($_SESSION['user_id'], true);
$allGrades = Grade::getLastNGradesByLoggedUserId(10);

?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο :: Προσθήκη Βαθμολογίας</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed" id="theAnimo">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div>
                        Βαθμολογίες
                        <div class="d-flex mt-2">
                            <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                            <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs([
                        'link'    => '#',
                        'title' => 'Προσθήκη Βαθμολογίας',
                    ]);
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <?php renderCustomMessages(); ?>
                <div class="row" id="addGradeAnimo">
                    <div class="col-lg-12">
                        <form class="form-horizontal mt-2" method="post" action="" id="addGradeForm">
                            <div class="card card-default mb-3 shadow-sm" id="addStudentCard">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <div class="card-title">
                                        <h4 class="mb-0">Προσθήκη Νέας Βαθμολογίας</h4>
                                        <small>Τα πεδία με <span class="reqField">(*)</span> είναι υποχρεωτικά</small>
                                    </div>
                                    <a class="fa-150x text-info pointer-cursored" href="#" data-toggle="modal" data-target="#helpManual" title="Βοήθεια">
                                        <em class="far fa-question-circle"></em>
                                    </a>
                                </div>
                                <div class="card-body border-top">
                                    <div class="col-lg-12 pl-0 pr-0">
                                        <!----- 1st ROW ----->
                                        <div class="form-row">
                                            <div class="col-lg-3 mb-3">
                                                <label for="student_name">Μαθητής <span class="reqField">(*)</span></label>
                                                <?php
                                                if ($gradeStudents === 1) {
                                                ?>
                                                    <p class="text-danger">Δεν βρέθηκαν μαθητές. Δεν μπορείτε να βαθμολογήσετε.</p>
                                                <?php
                                                } else {
                                                    echo $gradeStudents;
                                                }
                                                ?>
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="class">Τάξη <span class="reqField">(*)</span></label>
                                                <input class="form-control" id="class" name="class" type="text" placeholder="Τάξη.." required readonly>
                                                <input class="form-control" id="class_id" name="class_id" type="hidden">
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="tmima">Τμήμα <span class="reqField">(*)</span></label>
                                                <input class="form-control" id="tmima" name="tmima" type="text" placeholder="Τμήμα.." required readonly>
                                                <input class="form-control" id="tmima_id" name="tmima_id" type="hidden">
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="lesson">Μάθημα <span class="reqField">(*)</span></label>
                                                <select class="custom-select custom-select-md" name="lesson" id="lesson" required disabled>
                                                </select>
                                            </div>
                                        </div>

                                        <!----- 2nd Row ----->
                                        <div class="form-row" style="margin-top: 15px;">
                                            <div class="col-lg-3 mb-3">
                                                <label for="absence">Παρουσία <span class="reqField">(*)</span></label>
                                                <select class="custom-select custom-select-md" name="absence" id="absence" required disabled>
                                                    <option value="ΠΑΡΩΝ">ΠΑΡΩΝ</option>
                                                    <option value="ΑΠΩΝ">ΑΠΩΝ</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="grade">Βαθμός <span class="reqField">(*)</span></label>
                                                <div class="input-group">
                                                    <input class="form-control" type="number" id="grade" name="grade" min="0" max="100" placeholder="Βαθμός.." require readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text bg-secondary-dark text-bold">%</span>
                                                        <span class="d-none input-group-text bg-secondary-dark text-bold" id="onTwenty" style="width: 83px;">/20</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="exam_kind">Είδος Εξέτασης<span class="reqField">(*)</span></label>
                                                <select class="custom-select custom-select-md" name="exam_kind" id="exam_kind" required disabled>
                                                    <option value="TEST">TEST</option>
                                                    <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                                                    <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3 mb-3">
                                                <label for="exam_date">Ημερομηνία Εξέτασης <span class="reqField">(*)</span></label>
                                                <input class="form-control" id="exam_date" name="exam_date" type="date" value="<?php echo date('Y-m-d'); ?>" required readonly>
                                            </div>
                                        </div>

                                        <!----- 3rd Row ----->
                                        <div class="form-row justify-content-center" style="margin-top: 15px;">
                                            <div class="col-lg-7 mb-3">
                                                <label for="comments">Σχόλια</label>
                                                <textarea class="form-control" name="comments" id="comments" rows="5" placeholder="Σχόλια.." readonly></textarea>
                                                <span class="d-block text-right small pt-1" id="letter-count"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-primary btnw-4" name="submit_form" id="submitBtn" data-toggle="modal" data-target="#submitSaveGradeModal" type="button">Αποθήκευση</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row mt-3" id="latestGradesAnimo">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <div class="card-title">
                                    <h4 class="mb-0 py-2">Τελευταίες Βαθμολογίες</h4>
                                </div>
                                <div class="d-flex align-items-center">
                                    <a class="btn btn-green mr-2 text-bold" href="<?php echo initHelper::getWebTeacherPath('/edit-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                        <i class="fas fa-edit mr-2"></i>Επεξεργασία Βαθμολογιών
                                    </a>
                                    <a class="btn btn-info text-bold" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>" title="Προβολή όλων των βαθμολογιών">
                                        <i class="fas fa-align-justify mr-2"></i>Προβολή Όλων
                                    </a>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <table class="table table-striped table-hover w-100" id="latestGradesTbl">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Ημερομηνία Εξέτασης</th>
                                            <th>Ονοματεπώνυμο Μαθητή</th>
                                            <th>Μάθημα</th>
                                            <th>Τμήμα</th>
                                            <th>Είδος Εξέτασης</th>
                                            <th>Παρουσία</th>
                                            <th>Βαθμός</th>
                                            <th>Καταχωρήθηκε</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script src=<?php initHelper::getWebTeacherPath("/js/add-grade.js"); ?>></script>
    <?php
    $modal = new Modal([
        'modalId' => 'helpManual',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => '',
        'modalHeader' => '<h4 class="modal-title"><em class="far fa-question-circle float-left fa-150x mr-2 text-info"></em>Πληροφορίες</h4>',
        'modalBody' => '
        <ul style="margin-bottom: 0px;">
            <li class="mb-2">Όλα τα πεδία με το σύμβολο <span class="reqField">(*)</span> είναι υποχρεωτικά. Δεν μπορείτε να καταχωρήσετε βαθμολογία χωρίς να τα έχετε συμπληρώσει</li>
            <li class="mb-2">Καταχωρείτε το βαθμό στα 100 (%). Σύμβολα δεν γίνονται δεκτά στο πεδίο του βαθμού</li>
            <li>Ο βαθμός παίρνει τιμές από 0 έως 100</li>
            <li>Για να ολοκληρώσετε τη βαθμολόγηση πατάτε το κουμπί <button class="btn btn-primary btn-sm ml-1">Αποθήκευση</button></li>
        </ul>
        ',
        'modalFooter' => '',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    // info modal
    $modal = new Modal([
        'modalId' => 'submitSaveGradeModal',
        'modalSize' => '',
        'modalBgHeaderColor' => 'bg-success-dark',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-plus float-left fa-150x mr-2"></em>Προσθήκη Βαθμολογίας</h4>',
        'modalBody' => '
		<p class="py-2 mb-0 text-center" style="font-size: 110%; font-weight: 500;">Είστε σίγουρος οτι θέλετε να αποθηκεύσετε την βαθμολογία;</p>
		',
        'modalFooter' => '<button class="btn btn-success btnw-3" id="submitSaveGrade" type="button">Save</button>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>