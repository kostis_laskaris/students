<?php
$current_page = basename($_SERVER['PHP_SELF']);
?>
<aside class="aside-container">
    <div class="aside-inner">
        <nav class="sidebar" data-sidebar-anyclick-close="">
            <ul class="sidebar-nav">
                <li class="has-user-block">
                    <div class="item user-block">
                        <div class="user-block-picture">
                            <div class="user-block-status">
                                <img class="img-thumbnail rounded-circle" src="../assets/img/user/man-avatar.png" alt="Avatar" width="60" height="60">
                                <div class="circle bg-success circle-lg"></div>
                            </div>
                        </div>
                        <div class="user-block-info">
                            <span class="user-block-name"><?php echo "Hello, " . $_SESSION['username']; ?></span>
                            <span class="user-block-role"><?php echo $user->permission; ?></span>
                        </div>
                    </div>
                </li>
                <li class="nav-heading ">
                    <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                </li>
                <li class="<?php if ($current_page == 'dashboard.php') echo 'active'; ?>">
                    <a href="<?php echo initHelper::getWebTeacherPath('/dashboard.php'); ?>" title="Dashboard">
                        <em class="icon-speedometer"></em>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?php if ($current_page == 'add-grade.php' || $current_page == 'edit-grades.php' || $current_page == 'view-grades.php') echo 'active'; ?>">
                    <a href="#gradeMenu" title="Διαχείριση Βαθμολογιών" data-toggle="collapse">
                        <em class="far fa-star"></em>
                        <span>Διαχείριση Βαθμολογιών</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="gradeMenu">
                        <li class="sidebar-subnav-header">Διαχείριση Βαθμολογιών</li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebTeacherPath('/add-grade.php'); ?>" title="Νέα Βαθμολογία">
                                <span>Νέα Βαθμολογία</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebTeacherPath('/edit-grades.php'); ?>" title="Επεξεργασία Βαθμολογιών">
                                <span>Επεξεργασία Βαθμών</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>" title="Προβολή Βαθμολογιών">
                                <span>Προβολή Βαθμών</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>