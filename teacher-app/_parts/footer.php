
<button onclick="topFunction()" id="toTopBtn" class="bg-warning" title="Go to top">
	<i class="fas fa-chevron-up fa-150x"></i>
</button>

<footer class="footer-container">         
	<span class="float-left"><i class="far fa-copyright"></i> <?php echo date('Y'); ?> Φροντιστήριο Πολύτροπο</span>
	<span class="float-right">Developed by <span style="color: #00ce68;">Laskaris Konstantinos</span></span>
</footer>