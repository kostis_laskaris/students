<header class="topnavbar-wrapper">
    <nav class="navbar topnavbar">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo initHelper::getWebTeacherPath('/dashboard.php'); ?>">
                <div class="brand-logo">
                    <img class="img-fluid" src="../assets/img/politropo/MATHITOLOGIO-05.png" alt="App Logo">
                </div>
                <div class="brand-logo-collapsed">
                    <img class="img-fluid" src="../assets/img/politropo/MATHITOLOGIO-SMALL.png" alt="App Logo">
                </div>
            </a>
        </div>
        <ul class="navbar-nav mr-auto flex-row">
            <li class="nav-item">
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed">
                    <em class="fas fa-bars"></em>
                </a>
                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                <a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true">
                    <em class="fas fa-bars"></em>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav flex-row">
            <li class="nav-item d-none d-md-block" title="Πίσω">
                <a class="nav-link" href="javascript:history.go(-1)">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </li>
            <li class="nav-item d-none d-md-block" title="Μπροστά">
                <a class="nav-link" href="javascript:history.go(+1)">
                    <i class="fas fa-arrow-right"></i>
                </a>
            </li>
            <li class="nav-item dropdown dropdown-list d-none d-md-block">
                <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-toggle="dropdown">
                    <em class="icon-people mr-1"></em>
                    <?php echo $_SESSION['username']; ?>
                </a>
                <!-- START Dropdown menu-->
                <div class="dropdown-menu dropdown-menu-right animated flipInX">
                    <div class="dropdown-item">
                        <!-- START list group-->
                        <div class="list-group">
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fa fa-user fa-2x text-info"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0"><?php echo $_SESSION['username']; ?></p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['permission']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="far fa-clock fa-2x text-warning"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0">Ώρα Εισόδου</p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['login_time']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fas fa-database fa-2x text-success"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0">Χρήση Βάσης</p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['curr_school_year'] . " | " . $_SESSION['periodos_mathimaton']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- user profile -->
                            <div class="list-group-item list-group-item-action">
                                <span class="d-flex align-items-center">
                                    <a href="<?php echo initHelper::getWebTeacherPath('/user-profile.php'); ?>" class="w-100">
                                        <button class="btn btn-primary w-100" type="button" style="letter-spacing: 0.5px;">
                                            <em class="fas fa-user-circle fa-120x mr-1"></em>
                                            Το Προφίλ μου
                                        </button>
                                    </a>
                                </span>
                            </div>
                            <!-- last list item-->
                            <div class="list-group-item list-group-item-action">
                                <span class="d-flex align-items-center">
                                    <a href="<?php echo initHelper::getWebPath('/logout.php'); ?>" class="w-100">
                                        <button class="btn btn-danger w-100" type="button" style="letter-spacing: 0.5px;">
                                            <em class="fas fa-sign-out-alt mr-1"></em>
                                            Αποσύνδεση
                                        </button>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <!-- END list group-->
                    </div>
                </div>
                <!-- END Dropdown menu-->
            </li>
            <!-- Button used to show user details. Only visible on mobiles-->
            <li class="nav-item dropdown dropdown-list d-md-none">
                <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-toggle="dropdown">
                    <em class="icon-people mr-1"></em>
                </a>
                <!-- START Dropdown menu-->
                <div class="dropdown-menu dropdown-menu-right animated flipInX">
                    <div class="dropdown-item">
                        <!-- START list group-->
                        <div class="list-group">
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fa fa-user fa-2x text-info"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0"><?php echo $_SESSION['username']; ?></p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['permission']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="far fa-clock fa-2x text-warning"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0">Ώρα Εισόδου</p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['login_time']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- list item-->
                            <div class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fas fa-calendar-alt fa-2x text-success"></em>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0">Σχολικό Έτος</p>
                                        <p class="m-0 text-muted text-sm"><?php echo $_SESSION['curr_school_year']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- last list item-->
                            <!-- user profile -->
                            <div class="list-group-item list-group-item-action">
                                <span class="d-flex align-items-center">
                                    <a href="user-profile.php" class="w-100">
                                        <button class="btn btn-primary w-100" type="button" style="letter-spacing: 0.5px;">
                                            <em class="fas fa-user-circle fa-120x mr-1"></em>
                                            User Profile
                                        </button>
                                    </a>
                                </span>
                            </div>
                            <!-- last list item-->
                            <div class="list-group-item list-group-item-action">
                                <span class="d-flex align-items-center">
                                    <a href="../logout.php?username=<?php echo $_SESSION['username']; ?>" class="w-100">
                                        <button class="btn btn-danger w-100" type="button" style="letter-spacing: 0.5px;">
                                            <em class="fas fa-sign-out-alt mr-1"></em>
                                            Αποσύνδεση
                                        </button>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <!-- END list group-->
                    </div>
                </div>
                <!-- END Dropdown menu-->
            </li>
            <!-- Fullscreen (only desktops)-->
            <li class="nav-item d-none d-md-block" id="fullscrn">
                <a class="nav-link" href="#">
                    <em class="fa fa-expand"></em>
                </a>
            </li>
            <li class="nav-item d-none d-md-block" id="exitFullScrn" style="display: none !important;">
                <a class="nav-link" href="#">
                    <em class="fa fa-compress"></em>
                </a>
            </li>
            <li class="nav-item" title="Αποσύνδεση">
                <a class="nav-link" href="<?php echo initHelper::getWebPath('/logout.php'); ?>">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
</header>