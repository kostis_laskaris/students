<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" type="image/x-icon" href=<?php initHelper::getWebPath("/assets/img/favicon.ico"); ?>>
<!-- FONT AWESOME-->
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-brands.css"); ?>>
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-regular.css"); ?>>
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-solid.css"); ?>>
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/vendor/@fortawesome/fontawesome-free-webfonts/css/fontawesome.css"); ?>>
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/vendor/simple-line-icons/css/simple-line-icons.css"); ?>>
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/bootstrap.css"); ?> id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/app.css"); ?> id="maincss">