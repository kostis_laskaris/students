<?php
session_start();
include("../init.php");

if (!isLoggedIn()) {
    (WEB_ROOT . '/logout.php');
    exit;
}
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    redirectTo(WEB_ROOT . '/logout.php');
}
$_SESSION['LAST_ACTIVITY'] = time();
$usersDropdown = User::getUsersAsDropdown('searchUser');
$userLessonsDropdown = User::getCurrentUserLessonsAsDropdown('userLessons');
$userLessonsDropdownForTable = User::getCurrentUserLessonsAsDropdown('userLessonsForTable');
?>
<!DOCTYPE html>
<html lang="el">

<head>
    <?php include('_parts/head.php'); ?>
    <title>Μαθητολόγιο Πολύτροπο</title>
    <?php initHelper::getDatatablesCss(); ?>
    <?php initHelper::getAnimationCss(); ?>
    <link rel="stylesheet" href=<?php initHelper::getWebPath("/assets/css/theme-e.css"); ?> id="maincss2">
    <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token']; ?>">
</head>

<body class="layout-fixed">
    <div class="wrapper">
        <?php include('_parts/header.php'); ?>
        <?php include('_parts/sidebar.php'); ?>
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div class="d-flex align-items-center">
                        <div>
                            Dashboard
                            <div class="d-flex mt-2">
                                <small class="mr-2 br pr-2"><i class="fas fa-calendar-alt mr-2"></i><?php echo $_SESSION['curr_school_year']; ?></small>
                                <small><i class="far fa-calendar-alt mr-2"></i><?php echo $_SESSION['periodos_mathimaton']; ?></small>
                            </div>
                        </div>
                    </div>
                    <!------ BREACRUMBS ------->
                    <?php
                    echo getBreadcrumbs();
                    ?>
                    <!------ BREACRUMBS END ------->
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-green-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="fas fa-user-graduate"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-green rounded-right">
                                <div class="h2 mt-0"><?php echo Student::getStudentsCountOfCurrentUser(); ?></div>
                                <div class="text-uppercase">ΜΑΘΗΤΕΣ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="fas fa-chalkboard"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-primary rounded-right">
                                <div class="h2 mt-0"><?php echo Tmima::getTmimataCountOfCurrentUser(); ?></div>
                                <div class="text-uppercase">ΤΜΗΜΑΤΑ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                                <em class="fa-3x"><i class="far fa-star"></i></em>
                            </div>
                            <div class="col-8 py-3 bg-purple rounded-right">
                                <div class="h2 mt-0"><?php echo Grade::getGradesCountOfCurrentUser(); ?></div>
                                <div class="text-uppercase">ΒΑΘΜΟΛΟΓΙΕΣ</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-green justify-content-center rounded-left">
                                <div class="text-center">
                                    <div class="text-sm" data-now="" data-format="MMMM"></div>
                                    <br>
                                    <div class="h2 mt-0" data-now="" data-format="D"></div>
                                </div>
                            </div>
                            <div class="col-8 py-3 rounded-right">
                                <div class="text-uppercase" data-now="" data-format="dddd"></div>
                                <br>
                                <div class="h2 mt-0" data-now="" data-format="HH:mm"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-9">
                        <!-- Chart -->
                        <div class="row" id="gradeAvgPerMonth">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-12">
                                                <h5 class="mb-0 py-2">Μέσος Όρος / Είδος Εξέτασης</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-12 d-flex align-items-center justify-content-end">
                                                <div class="mr-2 d-flex align-items-center">
                                                    <span class="text-bold mr-2">Είδος:</span>
                                                    <select name="examKind" id="examKind" class="custom-select custom-select-md">
                                                        <option value="ΔΙΑΓΩΝΙΣΜΑ">ΔΙΑΓΩΝΙΣΜΑ</option>
                                                        <option value="TEST">TEST</option>
                                                        <option value="ΕΡΓΑΣΙΑ">ΕΡΓΑΣΙΑ</option>
                                                    </select>
                                                </div>
                                                <div class="mr-2 d-flex align-items-center">
                                                    <span class="text-bold mr-2">Μάθημα: </span>
                                                    <?php echo $userLessonsDropdown; ?>
                                                </div>
                                                <button class="btn btn-primary" id="updateChartBtn">Προβολή</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body bt" id="gradesAvgPerMonthChart">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="gradesPerStudentAndLessonAnimo">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <div class="card-title">
                                            <h5 class="mb-0 py-2">Στατιστικά / Μαθητή</h5>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <span class="text-bold mr-2">Μάθημα: </span>
                                            <?php echo $userLessonsDropdownForTable; ?>
                                        </div>
                                    </div>
                                    <div class="card-body bt">
                                        <table class="table table-striped w-100" id="gradesPerStudentAndLessonTable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="d-none">Μάθημα</th>
                                                    <th>Επίθετο</th>
                                                    <th>Όνομα</th>
                                                    <th class="text-center">Αριθμός Βαθμολογιών</th>
                                                    <th class="text-center">Μέσος Όρος</th>
                                                    <th class="text-center">Απουσίες</th>
                                                    <th class="text-center">Μεγαλύτερος</th>
                                                    <th class="text-center">Μικρότερος</th>
                                                    <th>Προβολή</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Right Sidebar -->
                    <div class="col-lg-3">
                        <div class="card card-default" id="latestExamsAnimo">
                            <div class="card-header">
                                <div class="d-flex py-2">
                                    <div class="card-title">
                                        <h5 class="mb-0">Τελευταία Διαγωνίσματα</h5>
                                    </div>
                                    <em class="ml-auto text-muted icon-list fa-150x"></em>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table w-100" id="latestExamsTable">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="card-footer bt-3 clearfix btn-block d-flex border-top" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>">
                                <span>Προβολή όλων</span>
                                <span class="ml-auto">
                                    <em class="fa fa-chevron-circle-right"></em>
                                </span>
                            </a>
                        </div>

                        <div class="card card-default" id="latestTestsAnimo">
                            <div class="card-header">
                                <div class="d-flex py-2">
                                    <div class="card-title">
                                        <h5 class="mb-0">Τελευταία Test</h5>
                                    </div>
                                    <em class="ml-auto text-muted icon-list fa-150x"></em>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table w-100" id="latestTestsTable">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="card-footer bt-3 clearfix btn-block d-flex border-top" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>">
                                <span>Προβολή όλων</span>
                                <span class="ml-auto">
                                    <em class="fa fa-chevron-circle-right"></em>
                                </span>
                            </a>
                        </div>

                        <div class="card card-default" id="latestErgasiesAnimo">
                            <div class="card-header">
                                <div class="d-flex py-2">
                                    <div class="card-title">
                                        <h5 class="mb-0">Τελευταίες Εργασίες</h5>
                                    </div>
                                    <em class="ml-auto text-muted icon-list fa-150x"></em>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table w-100" id="latestErgasiesTable">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="card-footer bt-3 clearfix btn-block d-flex border-top" href="<?php echo initHelper::getWebTeacherPath('/view-grades.php'); ?>">
                                <span>Προβολή όλων</span>
                                <span class="ml-auto">
                                    <em class="fa fa-chevron-circle-right"></em>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('_parts/footer.php'); ?>
    </div>
    <?php include('_parts/scripts.php'); ?>
    <?php initHelper::getDatatablesJs(); ?>
    <script src=<?php initHelper::getWebPath("/assets/vendor/moment/min/moment-with-locales.js"); ?>></script>
    <script src=<?php initHelper::getWebPath("/assets/vendor/easy-pie-chart/dist/jquery.easypiechart.js"); ?>></script>
    <script src=<?php initHelper::getWebPath("/assets/vendor/chart.js/dist/Chart.js"); ?>></script>
    <script src=<?php initHelper::getWebTeacherPath("/js/dashboard.js"); ?>></script>
    <?php
    //line chart modal
    $modal = new Modal([
        'modalId' => 'userGradesPerMonthChar',
        'modalSize' => 'modal-lg',
        'modalBgHeaderColor' => 'bg-purple',
        'modalHeader' => '<h4 class="modal-title"><em class="fas fa-chart-line float-left fa-150x mr-2"></em>Γράφημα Βαθμολογιών Καθηγητών / Μήνα</h4>',
        'modalBody' => '
        <h4 class="text-center mb-4" id="teacherNameSpan"></h4>
		<div class="w-100"  id="userGradesPerMonthLineChart">		
		</div>
		',
        'modalFooter' => '<div class="mr-auto" id="finalAverage">Συνολικό Μέσο Όρο:</div>',
        'animoId' => ''
    ]);
    echo $modal->createModal();
    ?>
</body>

</html>