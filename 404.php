<?php
include('init.php');
?>
<!DOCTYPE html>
<html lang="el">
<head>   
   <?php include('_parts/header.php'); ?>
   <title>Μαθητολόγιο Πολύτροπο :: Error 404 - Page Not Found</title> 
</head>
<body>
   <div class="wrapper">
      <div class="abs-center abs-fixed wd-xl">
         <!-- START card-->
         <div class="text-center mb-4">
            <div class="mb-3">
               <em class="fas fa-unlink fa-6x text-danger"></em>
            </div>
            <div class="text-lg mb-0">404</div>
            <div class="text-lg mb-2">- Page Not Found -</div>
            <p class="lead m-1">The page that you are looking for does not exist</p>
            <p><b>Please try another</b></p>
         </div>
         <div class="p-3 text-center">
          <i class="far fa-copyright"></i> Φροντιστήριο Πολύτροπο</span>
       </div>
    </div>
 </div>
  <?php include('_parts/scripts.php'); ?>
</body>

</html>