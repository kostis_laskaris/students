<?php
$objectName = filter_input(INPUT_GET, 'objectName');
$fileName = filter_input(INPUT_GET, 'fileName');
if (isset($objectName) && isset($fileName)) {
    $data = '$(document).ready(function () {
        ';
    $data .= 'var ' . $objectName . ' = {
        $elements: {
            endpointUrl: [],
        },';
    $data .= 'getEndPoint: function () {
        return '.$objectName.'.$elements.endpointUrl.join("/"); 
    },';
    $data .= 'init: function () {},';
    $data .= '} 
    ' . $objectName . '.init();
    });';

$headerFile = fopen($fileName . '.js', "w") or die("Unable to open file!");
fwrite($headerFile, $data);
fclose($headerFile);
}else{
    echo 'No parameters!';
}
