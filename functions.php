<?php

function redirectTo($path)
{
	if (headers_sent()) {
		die('<script type="text/javascript">window.location=\'' . $path . '\';</script‌​>');
	} else {
		header('Location: ' . $path);
		die();
	}
}

function isAdmin()
{
	if (isset($_SESSION['permission']) && $_SESSION['permission'] === "Administrator") {
		return true;
	} else {
		return false;
	}
}

function isTeacher()
{
	if (isset($_SESSION['permission']) && $_SESSION['permission'] === "Teacher") {
		return true;
	} else {
		return false;
	}
}

function isLoggedIn()
{
	$condition = isset($_SESSION['username']) && !empty($_SESSION['username']) && isset($_SESSION['permission']) && !empty($_SESSION['permission']);
	if ($condition) {
		return true;
	} else {
		return false;
	}
}

function setCsrfToken()
{
	if (!isset($_SESSION['csrf_token'])) {
		$_SESSION['csrf_token'] = base64_encode(hash('md5', openssl_random_pseudo_bytes(32)));
	}
}

function validateCsrfToken($requestedCsrf)
{
	if (isset($_SESSION['csrf_token']) && $_SESSION['csrf_token'] === sanitizeField($requestedCsrf)) {
		return true;
	} else {
		return false;
	}
}

function generateAuthToken($length = 50)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ^()*?=+';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	$_SESSION['authToken'] = $randomString;
}

function validateAuthToken($authToken)
{
	if (isset($_SESSION['authToken']) && $_SESSION['authToken'] === sanitizeField($authToken)) {
		return true;
	} else {
		return false;
	}
}

function dateDifference($from, $to, $scope)
{
	if ($scope == 'd')
		return number_format(($from - $to) / 60 / 60 / 24, 0, ',', '');
}

function getGreekDatetime($date)
{
	if ($date) {
		$engDay = DateTime::createFromFormat('Y-m-d H:i:s', $date)->format('w');
		$day = '';
		if ($engDay == 0)
			$day = "Κυρ";
		elseif ($engDay == 1)
			$day = "Δευ";
		elseif ($engDay == 2)
			$day = "Τρι";
		elseif ($engDay == 3)
			$day = "Τετ";
		elseif ($engDay == 4)
			$day = "Πεμ";
		elseif ($engDay == 5)
			$day = "Παρ";
		else
			$day = "Σαβ";
		return $day . ', ' . DateTime::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
	} else {
		return '';
	}
}

function getGreekDate($date)
{
	if ($date) {
		$engDay = DateTime::createFromFormat('Y-m-d', $date)->format('w');
		$day = '';
		if ($engDay == 0)
			$day = "Κυρ";
		elseif ($engDay == 1)
			$day = "Δευ";
		elseif ($engDay == 2)
			$day = "Τρι";
		elseif ($engDay == 3)
			$day = "Τετ";
		elseif ($engDay == 4)
			$day = "Πεμ";
		elseif ($engDay == 5)
			$day = "Παρ";
		else
			$day = "Σαβ";
		return $day . ', ' . DateTime::createFromFormat('Y-m-d', $date)->format('d/m/Y');
	} else {
		return '';
	}
}

function destroySession()
{
	session_start();
	$_SESSION = array();
	session_unset();
	session_destroy();
}

function infoMessages()
{
	if (isset($_SESSION['did'])) {
		echo $_SESSION['did'];
		unset($_SESSION['did']);
	}
	if (isset($_SESSION['exists'])) {
		echo $_SESSION['exists'];
		unset($_SESSION['exists']);
	}
}

function renderCustomMessages()
{
	echo '
		<div class="alert alert-dismissible fade show" role="alert" id="alertBox" style="display:none;">
		<button class="close" type="button" aria-label="Close">
		<span onclick="this.parentNode.parentNode.style.display= \'none\'">×</span>
		</button>
		<h4 id="alertBoxHeader"></h4><span id="alertMessage"></span></div>
	';
}

function createInfoMessage($kind, $message)
{
	$message_header = '';
	if ($kind == 'success') {
		$message_header = 'Success';
	} else {
		$message_header = 'Error';
	}
	$box =
		'<div class="alert alert-' . $kind . ' bg-' . $kind . '-dark alert-dismissible fade show" role="alert">
	<button class="close" type="button" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">×</span>
	</button>
	<h4>' . $message_header . ': </h4>' . $message . '</div>';
	return $box;
}

function sanitizeField($field)
{
	return htmlspecialchars(strip_tags($field));
}

function getLoginSchoolYears()
{
	$start = 2018;
	$end = 0;
	$now = date('Y');
	$school_year = '<select class="custom-select custom-select-md cursor-pointer" id="school_year" name="school_year" style="height: 35px;"><option value="">Σχολικό Έτος </option>';
	while (intval($now) >= $end) {
		$end = $start + 1;
		$part_start = substr($start, -2);
		$part_end = substr($end, -2);
		$school_year .= '<option value="' . $part_start . '_' . $part_end . ':' . $start . '-' . $end . '">' . $start . '-' . $end . '</option>';
		$start++;
	}
	$school_year .= '</select>';
	return $school_year;
}

function getSchoolYears($choosed = true, $fieldName = "sx_etos")
{
	$start = 2019;
	$end = 0;
	$now = date('Y');
	$selected = '';
	$school_year = '<select class="custom-select custom-select-md" id="' . $fieldName . '" name="' . $fieldName . '" style="height: 35px;">';
	$school_year .= '<option value="">Επιλογή</option>';
	while (intval($now) >= $end) {
		if ($end == intval($now) && $choosed)
			$selected = "selected";
		else
			$selected = '';
		$end = $start + 1;
		$school_year .= '<option ' . $selected . ' value="' . $start . '-' . $end . '">' . $start . '-' . $end . '</option>';
		$start++;
	}
	$school_year .= '</select>';
	return $school_year;
}

function getSchoolYearsForResubStudents($fieldName = "sx_etos")
{
	$start = 2019;
	$end = 0;
	$start_short = 19;
	$end_short = 0;
	$now = date('Y');
	$school_year = '<select class="custom-select custom-select-md" id="' . $fieldName . '" name="' . $fieldName . '" style="height: 35px;">';
	$school_year .= '<option value="">Επιλογή</option>';
	while (intval($now) >= $end) {
		$end = $start + 1;
		$end_short = $start_short + 1;
		$school_year .= '<option value="' . $start_short . '_' . $end_short . '_ther">Θερινή ' . $start . '-' . $end . '</option>';
		$school_year .= '<option value="' . $start_short . '_' . $end_short . '_xeim">Χειμερινή ' . $start . '-' . $end . '</option>';
		$start++;
		$start_short++;
	}
	$school_year .= '</select>';
	return $school_year;
}

function getPeriodosMathimaton($fieldName = "periodos")
{
	$output = '<select name="' . $fieldName . '" id="' . $fieldName . '" class="custom-select custom-select-md cursor-pointer">';
	$output .= '<option value="">Περίοδος Μαθημάτων</option><option value="ther">Θερινή</option><option value="xeim">Χειμερινή</option>';
	$output .= '</select>';
	return $output;
}

function helperLogFile($txt, $filename)
{
	$myfile = fopen($filename . ".txt", "w") or die("Unable to open file!");
	fwrite($myfile, $txt);
	fclose($myfile);
}

function helperPrint($data, $mode)
{
	echo '<pre>';
	if ($mode == "var_dump") {
		var_dump($data);
	} else {
		print_r($data);
	}
	echo '</pre>';
}

function getBreadcrumbs(...$paths)
{
	if (empty($paths)) {
		if (isAdmin()) {
			return '<ol class="breadcrumb ml-auto"><li class="breadcrumb-item"><a href="' . initHelper::getTheWebAdminPath('/dashboard.php') . '">Dashboard</a></li></ol>';
		} elseif (isTeacher()) {
			return '<ol class="breadcrumb ml-auto"><li class="breadcrumb-item"><a href="' . initHelper::getTheWebTeacherPath('/dashboard.php') . '">Dashboard</a></li></ol>';
		} else {
			redirectTo(WEB_ROOT . '/logout.php');
		}
	}
	$breadcrumb = '';
	if (isAdmin()) {
		$breadcrumb = '<ol class="breadcrumb ml-auto"><li class="breadcrumb-item"><a href="' . initHelper::getTheWebAdminPath('/dashboard.php') . '">Dashboard</a></li>';
	} elseif (isTeacher()) {
		$breadcrumb = '<ol class="breadcrumb ml-auto"><li class="breadcrumb-item"><a href="' . initHelper::getTheWebTeacherPath('/dashboard.php') . '">Dashboard</a></li>';
	} else {
		redirectTo(WEB_ROOT . '/logout.php');
	}
	foreach ($paths as $path) {
		$breadcrumb .= '<li class="breadcrumb-item active"><a href="' . $path['link'] . '">' . $path['title'] . '</a></li>';
	}
	$breadcrumb .= '</ol>';
	return $breadcrumb;
}

class AjaxController
{

	public $code = 200;
	public $errorMessage = '';
	public $data = '';

	function __construct($code = 200, $data = '', $errorMessage = '')
	{
		$this->code = $code;
		$this->data = $data;
		$this->errorMessage = $errorMessage;
	}

	static function isAjax()
	{
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return true;
		}
		return false;
	}

	static function spitResponse(AjaxController $response)
	{
		header("Content-Type: application/json");
		header("Expires: on, 01 Jan 1970 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		if (!self::isAjax()) {
			http_response_code(401);
			ob_clean();
			echo json_encode('Unauthorized Access.', JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
		} else {
			http_response_code($response->code);
			ob_clean();
			echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
		}
		exit;
	}
}

class initHelper
{
	static function getWebPath(String $path)
	{
		echo WEB_ROOT . $path;
	}

	static function getTheWebPath(String $path)
	{
		return WEB_ROOT . $path;
	}

	static function getFilePath(String $path)
	{
		echo PROJECT_ROOT . $path;
	}

	static function getTheWebAdminPath(String $path)
	{
		return WEB_ADMIN_ROOT . $path;
	}

	static function getWebAdminPath(String $path)
	{
		echo WEB_ADMIN_ROOT . $path;
	}

	static function getTheWebTeacherPath(String $path)
	{
		return WEB_TEACHER_ROOT . $path;
	}

	static function getWebTeacherPath(String $path)
	{
		echo WEB_TEACHER_ROOT . $path;
	}

	static function getDatatablesCss()
	{
		echo '<link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css") . '>
        <link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css") . '>
        <link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css") . '>';
	}

	static function getAnimationCss()
	{
		echo '<link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/animate.css/animate.css") . '>
        <link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/whirl/dist/whirl.css") . '>
        <link rel="stylesheet" href=' . self::getTheWebPath("/assets/vendor/loaders.css/loaders.css") . '>';
	}

	static function getDatatablesJs()
	{
		echo '<script src=' . self::getTheWebPath("/assets/vendor/datatables.net/js/jquery.dataTables.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons/js/dataTables.buttons.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons/js/buttons.colVis.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons/js/buttons.flash.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons/js/buttons.html5.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/datatables.net-buttons/js/buttons.print.js") . '></script>
        <script src=' . self::getTheWebPath("/assets/vendor/jszip/dist/jszip.js") . '></script>';
	}
}
