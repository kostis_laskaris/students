function editUser(id){
    //Adding spin
    addStandardWhirlAnime('animoSpin2');
    $.get( 
        "../php_actions/get_actions/get_user_for_edit.php", 
        { 
            userId: id, 
            csrf_token: $('#csrf_token_edit_user').val()
        } 
        ).done(function( data ) {
           var result = JSON.parse(data);
           document.getElementById('myModalLabelLarge3').innerHTML = "<em class='fas fa-user-edit float-left fa-150x mr-2'></em>Επεξεργασία Χρήστη με ID: " + result.id;
           document.getElementById('firstname').value = result.firstname;
           document.getElementById('lastname').value = result.lastname;
           document.getElementById('email').value = result.email;
           document.getElementById('username').value = result.username;
           document.getElementById('permission').value = result.permission;      
           document.getElementById('status').value = result.status; 
           document.getElementById('userId').value = result.id;
            //Removing spin
            removeStandardWhirlAnime('animoSpin2');               
    });
}