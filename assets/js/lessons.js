function clearLessonsFields(actionId){	
	if(actionId == 0){
		var test = document.getElementsByClassName('lesson-entry');
		for(var i = 0; i < test.length; i++){
			test[i].checked = false;
		}
	}else{
		var test = document.getElementsByClassName('lesson-search-entry');
		for(var i = 0; i < test.length; i++){
			test[i].checked = false;
		}		
	}
}

function editLesson(id){
    //Adding spin
    addStandardWhirlAnime('animoSpin3');
    $.get( 
    	"../php_actions/get_actions/get_lesson_for_edit.php", 
    	{ 
    		lessonId: id, 
    		csrf_token: $('#csrf_token_edit_lesson').val()
    	} 
    	).done(function( data ) {
    		var result = JSON.parse(data);
    		document.getElementById('updateLessonName').value = result.name;      
    		document.getElementById('updateLessonDesc').value = result.description;
    		document.getElementById('lessonEditId').value = result.id;
            //Removing spin
            removeStandardWhirlAnime('animoSpin3');               
        });
}

    function selectLessons(op, actionId){
    	clearLessonsFields(actionId);
    	if(actionId == 0){
    		if(op == 0){
    			document.getElementById("αρχ").checked = true;
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("ιστ").checked = true;
    			document.getElementById("κοιν").checked = true;
    		}else if(op == 1){
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("φυσ").checked = true;
    			document.getElementById("χημ").checked = true;
    			document.getElementById("μαθ οπ").checked = true;
    		}else if(op == 2){
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("φυσ").checked = true;
    			document.getElementById("χημ").checked = true;
    			document.getElementById("βιο γπ").checked = true;
    		}else if(op == 3){
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("μαθ οπ").checked = true;
    			document.getElementById("αεππ").checked = true;
    			document.getElementById("αοθ").checked = true;
    		}else if(op == 4){
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("μαθ γπ").checked = true;
    		}else if(op == 5){
    			document.getElementById("ελλ").checked = true;
    			document.getElementById("μαθ γπ").checked = true;
    			document.getElementById("φυσ").checked = true;
    			document.getElementById("χημ").checked = true;
    		}else if(op == 6){
    			document.getElementById("μαθ γπ").checked = true;
    			document.getElementById("φυσ").checked = true;
    			document.getElementById("χημ").checked = true;
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("αρχ").checked = true;						
    		}else{
    			document.getElementById("εκθ").checked = true;
    			document.getElementById("μαθ γπ").checked = true;
    		}
    	}else{
    		if(op == 0){
    			document.getElementById("αρχSearch").checked = true;
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("ιστSearch").checked = true;
    			document.getElementById("κοινSearch").checked = true;
    		}else if(op == 1){
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("φυσSearch").checked = true;
    			document.getElementById("χημSearch").checked = true;
    			document.getElementById("μαθSearch").checked = true;
    		}else if(op == 2){
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("φυσSearch").checked = true;
    			document.getElementById("χημSearch").checked = true;
    			document.getElementById("βιο γ.πSearch").checked = true;
    		}else if(op == 3){
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("μαθSearch").checked = true;
    			document.getElementById("αεππSearch").checked = true;
    			document.getElementById("αοθSearch").checked = true;
    		}else if(op == 4){
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("μαθSearch").checked = true;
    		}else if(op == 5){
    			document.getElementById("ελλSearch").checked = true;
    			document.getElementById("μαθSearch").checked = true;
    			document.getElementById("φυσSearch").checked = true;
    			document.getElementById("χημSearch").checked = true;
    		}else if(op == 6){
    			document.getElementById("μαθSearch").checked = true;
    			document.getElementById("φυσSearch").checked = true;
    			document.getElementById("χημSearch").checked = true;
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("αρχSearch").checked = true;						
    		}else{
    			document.getElementById("εκθSearch").checked = true;
    			document.getElementById("μαθSearch").checked = true;
    		}
    	}
    }

    function setValueToModal(fieldId){
    	if(document.getElementById('teacherId'))
    		document.getElementById('teacherId').value = fieldId;
    	clearLessonsFields(0);
    	var lessons = document.getElementById(fieldId).value;
    	var lessonsArr = lessons.split(",");
    	for(var i = 0; i < lessonsArr.length; i++){
    		var val = lessonsArr[i].trim();
    		$("input[value='" + val + "']").prop('checked', true);
    	}
    }

    function saveValuesToField(fieldId, actionId){
    	var values = "";	
    	console.log(fieldId);
    	if(actionId == 0){
    		var test = document.getElementsByClassName('lesson-entry');
    		for(var i = 0; i < test.length; i++){
    			if(test[i].checked == true)
    				values += test[i].value + ", ";	
    		}		
    	}else{
    		var test = document.getElementsByClassName('lesson-search-entry');
    		for(var i = 0; i < test.length; i++){
    			if(test[i].checked == true)
    				values += test[i].value + ", ";	
    		}		
    	}
    	values = values.substring(0, values.length - 2);
    	document.getElementById(fieldId).value = values;
    	document.getElementById('closeBtn').click();
    	document.getElementById('closeBtnSearch').click();
    }