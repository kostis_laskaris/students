function editGrade(id, teacher_id){
    //Adding spin
    addStandardWhirlAnime('animoSpin2');
    var xhttp = new XMLHttpRequest();
    var url = "../php_actions/get_grade_for_edit.php?gradeId=" + id + "&teacherId=" + teacher_id;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            var result = JSON.parse(this.responseText);            
            document.getElementById('myModalLabelLarge3').innerHTML = "Επεξεργασία Βαθμολογίας με ID: " + result.grade_id;
            document.getElementById('editLesson').value = result.lesson_id;
            document.getElementById('editExam_date').value = result.exam_date;
            document.getElementById('editExam_kind').value = result.exam_kind;
            document.getElementById('editAbsence').value = result.absence;
            document.getElementById('editGrade').value = result.grade;
            document.getElementById('editComments').value = result.comments;           
            document.getElementById('gradeId').value = result.grade_id;
            if(result.absence == "ΑΠΩΝ"){
                document.getElementById('editGrade').value = "";
                document.getElementById('editGrade').readOnly = true;
            }else{
                document.getElementById('editGrade').readOnly = false;                
            }
            //Removing spin
            removeStandardWhirlAnime('animoSpin2');
        }               
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}