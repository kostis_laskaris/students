//GLOBALS
var i = 0;
var kinds = ['ΔΙΑΓΩΝΙΣΜΑ', 'TEST', 'ΕΡΓΑΣΙΑ'];
//Refresh student data
window.getUpdates = function (id, studentId) {
    var lessonPicked = $("#lessonSearch option:selected").text();
    if (i === 3) {
        i = 0;
    }
    //do nothing if no lesson picked on search
    if ($('#lessonSearch').val() == null || $('#lessonSearch').val() == '')
        return;
    //if lesson picked do ajax call
    $.ajax({
        url: "../php_actions/get_student_stats_for_admin?examKind=" + kinds[i] + "&lessonId=" + id + "&sId=" + studentId,
        async: true,
        dataType: 'json',
        success: function (data){
            console.log(data);
            console.log(i);
            if (i < 3) {
                if (i == 0) {
                    badges = document.getElementsByClassName('lesson-badge');
                    for(var j = 0; j < badges.length; j++){
                        badges[j].innerText = lessonPicked;
                    }
                    if(data.avg){
                        document.getElementById('examHeaderAvg').innerHTML = parseFloat(data.avg).toFixed(2) + "%";
                        document.getElementById('exam-bar').style.width = parseFloat(data.avg).toFixed(2) + "%";
                        if (parseFloat(data.avg) >= 50)
                            document.getElementById('examAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-up text-success ml-1' title='Πάνω από τη βάση'></i>";
                        else
                            document.getElementById('examAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-down text-danger ml-1' title='Κάτω από τη βάση'></i>";
                        document.getElementById('examAvg20').innerHTML = (parseFloat(data.avg) * 20 / 100).toFixed(2);
                        document.getElementById('examMaxGrade').innerHTML = parseFloat(data.max);
                        document.getElementById('examMinGrade').innerHTML = parseFloat(data.min);
                        document.getElementById('examParon').innerHTML = parseFloat(data.paron);
                        document.getElementById('examApon').innerHTML = parseFloat(data.apon);
                        document.getElementById('examCount').innerHTML = parseFloat(data.countAll);
                    }else{
                        document.getElementById('examHeaderAvg').innerHTML = "-";
                        document.getElementById('examAvg100').innerHTML = "-";
                        document.getElementById('exam-bar').style.width = "0%";
                        document.getElementById('examAvg20').innerHTML = "-";
                        document.getElementById('examMaxGrade').innerHTML = "-";
                        document.getElementById('examMinGrade').innerHTML = "-";
                        document.getElementById('examParon').innerHTML = "-";
                        document.getElementById('examApon').innerHTML = "-";
                        document.getElementById('examCount').innerHTML = "-";
                        // return;
                    }                   
                }else if(i == 1){
                    if(data.avg){
                        document.getElementById('testHeaderAvg').innerHTML = parseFloat(data.avg).toFixed(2) + "%";
                        document.getElementById('test-bar').style.width = parseFloat(data.avg).toFixed(2) + "%";
                        if (parseFloat(data.avg) >= 50)
                            document.getElementById('testAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-up text-success ml-1' title='Πάνω από τη βάση'></i>";
                        else
                            document.getElementById('testAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-down text-danger ml-1' title='Κάτω από τη βάση'></i>";
                        document.getElementById('testAvg20').innerHTML = (parseFloat(data.avg) * 20 / 100)
                        .toFixed(2);
                        document.getElementById('testMaxGrade').innerHTML = parseFloat(data.max);
                        document.getElementById('testMinGrade').innerHTML = parseFloat(data.min);
                        document.getElementById('testParon').innerHTML = parseFloat(data.paron);
                        document.getElementById('testApon').innerHTML = parseFloat(data.apon);
                        document.getElementById('testCount').innerHTML = parseFloat(data.countAll);
                    }else{
                        document.getElementById('testHeaderAvg').innerHTML = "-";
                        document.getElementById('testAvg100').innerHTML = "-";
                        document.getElementById('test-bar').style.width = "0%";
                        document.getElementById('testAvg20').innerHTML = "-";
                        document.getElementById('testMaxGrade').innerHTML = "-";
                        document.getElementById('testMinGrade').innerHTML = "-";
                        document.getElementById('testParon').innerHTML = "-";
                        document.getElementById('testApon').innerHTML = "-";
                        document.getElementById('testCount').innerHTML = "-";
                        // return;
                    }
                    
                }else if(i==2){
                    if(data.avg){
                        document.getElementById('ergasiaHeaderAvg').innerHTML = parseFloat(data.avg).toFixed(2) + "%";
                        document.getElementById('ergasia-bar').style.width = parseFloat(data.avg).toFixed(2) + "%";
                        if (parseFloat(data.avg) >= 50)
                            document.getElementById('ergasiaAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-up text-success ml-1' title='Πάνω από τη βάση'></i>";
                        else
                            document.getElementById('ergasiaAvg100').innerHTML = parseFloat(data.avg).toFixed(2) +
                        "% <i class='fas fa-arrow-down text-danger ml-1' title='Κάτω από τη βάση'></i>";
                        document.getElementById('ergasiaAvg20').innerHTML = (parseFloat(data.avg) * 20 / 100)
                        .toFixed(2);
                        document.getElementById('ergasiaMaxGrade').innerHTML = parseFloat(data.max);
                        document.getElementById('ergasiaMinGrade').innerHTML = parseFloat(data.min);
                        document.getElementById('ergasiaParon').innerHTML = parseFloat(data.paron);
                        document.getElementById('ergasiaApon').innerHTML = parseFloat(data.apon);
                        document.getElementById('ergasiaCount').innerHTML = parseFloat(data.countAll);
                    }else{
                        document.getElementById('ergasiaHeaderAvg').innerHTML = "-";
                        document.getElementById('ergasiaAvg100').innerHTML = "-";
                        document.getElementById('ergasia-bar').style.width = "0%";
                        document.getElementById('ergasiaAvg20').innerHTML = "-";
                        document.getElementById('ergasiaMaxGrade').innerHTML = "-";
                        document.getElementById('ergasiaMinGrade').innerHTML = "-";
                        document.getElementById('ergasiaParon').innerHTML = "-";
                        document.getElementById('ergasiaApon').innerHTML = "-";
                        document.getElementById('ergasiaCount').innerHTML = "-";
                        // return;
                    }
                    document.getElementById('tblAvg').innerHTML = "Μέσος όρος βαθμολογιών: <strong>" + parseFloat(data.totalAvg).toFixed(2) + "%</strong> ("+lessonPicked+")";
                    i = 0;
                    return;
                    // i++;
                }
                i++;
                getUpdates(id, studentId);     
            }else{
            }       
        }
    });
}