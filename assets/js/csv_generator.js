
function csvExport(csrf, filename) {
	addStandardWhirlAnime('theAnimo');
	$.get
		(
			"../php_actions/get_actions/get_all_students.php",
			{
				csrf_token: csrf
			}
		).done(function (item) {
			var response = JSON.parse(item);
			var rows = [];
			rows.push(`ID;Επίθετο;Όνομα;Email;Σταθερό Τηλέφωνο;Κινητό Τηλέφωνο;Διεύθυνση;Περιοχή;Τ.Κ;Ημερομηνία Εγγραφής;Όνομα Μητέρας;Όνομα Πατέρα;Τηλέφωνο Μητέρας;Τηλέφωνο Πατέρα;Email Κηδεμόνα;Όνομα Κηδεμόνα;Τηλέφωνο Κηδεμόνα;Δίδακτρα;Τμήμα;Σχολικό Έτος;Περίοδος Μαθημάτων;Τάξη;Μαθήματα;Ενημερώθηκε Από;Δημιουργήθηκε;Τροποποίηθηκε`);
			var today = new Date();
			var final_filename = filename + '_' + today.getDate() + (today.getMonth() + 1) + today.getFullYear() + '_' + today.getHours() + today.getMinutes() + today.getSeconds() + '.csv';
			for (let student of response) {
				rows.push(
					`${student.id};${student.lastname};${student.firstname};${student.email};${student.phone};${student.mobile};${student.address};${student.perioxi};${student.zip};${Helpers.dateOnlyConverter(student.date_eggrafis)};${student.mname};${student.faname};${student.mphone};${student.fphone};${student.email_kidemona};${student.kid_name};${student.kid_phone};${student.fee};${student.tmima};${student.sx_etos};${student.periodos};${student.class};${getStudentLessonsForView(student.lessons)['names']};${student.informed_by};${Helpers.dateConverter(student.created_at)};${Helpers.dateConverter(student.updated_at)}`
				);
			}
			downloadCSV(rows.join("\n"), final_filename);
			removeStandardWhirlAnime('theAnimo');
		});
}

function csvGradesExport(csrf, filename) {
	addStandardWhirlAnime('theAnimo');
	$.get
		(
			"../php_actions/get_actions/get_all_user_grades.php",
			{
				csrf_token: csrf
			}
		).done(function (item) {
			console.log(item);
			var response = JSON.parse(item);
			console.log(response);
			var rows = [];
			rows.push(`ID;Επίθετο;Όνομα;Τάξη;Τμήμα;Μάθημα;Είδος Εξέτασης;Παρουσία;Βαθμός /100;Βαθμός /20;Ημερομηνια Εξέτασης;Ημερομηνία Καταχώρησης;Ημερομηνία Τροποποίησης;Σχόλια`);
			var today = new Date();
			var final_filename = filename + '_' + today.getDate() + (today.getMonth() + 1) + today.getFullYear() + '_' + today.getHours() + today.getMinutes() + today.getSeconds() + '.csv';
			for (let grade of response['allGrades']) {
				let grade20 = (grade.grade*20)/100;
				rows.push(
					`${grade.id};${grade.fullStudent.lastname};${grade.fullStudent.firstname};${grade.class};${grade.fullTmima.name};${grade.fullLesson.name};${grade.exam_kind};${grade.absence};${grade.grade};${grade20};${Helpers.dateOnlyConverter(grade.exam_date)};${Helpers.dateConverter(grade.created_at)};${Helpers.dateConverter(grade.updated_at)};${grade.comments}`
				);
			}
			rows.push(`\n\n`);
			rows.push(`Επίθετο;Όνομα;Μέσος Όρος /100;Μέσος Όρος /20; Χαμηλότερος /100;Χαμηλότερος /20;Υψηλότερος /100;Υψηλότερος /20;Πλήθος Παρών;Πλήθος Απών`);
			for (let stat of response['gradesStats']) {
				fixedAvg = Number(stat.gradeAverage).toFixed(2);
				let avg20 = ((fixedAvg*20)/100).toFixed(1);
				let min20 = ((stat.gradeMin*20)/100).toFixed(1);
				let max20 = ((stat.gradeMax*20)/100).toFixed(1);
				rows.push(
					`${stat.student.lastname};${stat.student.firstname};${fixedAvg};${avg20};${stat.gradeMin};${min20};${stat.gradeMax};${max20};${stat.gradeCount};${stat.absence}`
				);
			}
			downloadCSV(rows.join("\n"), final_filename);
			removeStandardWhirlAnime('theAnimo');
		});
}

function downloadCSV(csv, filename) {
	var csvFile;
	var downloadLink;
	csvFile = new Blob([csv], { type: "text/csv" });
	downloadLink = document.createElement("a");
	downloadLink.download = filename;
	downloadLink.href = window.URL.createObjectURL(csvFile);
	downloadLink.style.display = "none";
	document.body.appendChild(downloadLink);
	downloadLink.click();
}

function exportTableToCSV(table_id, filename) {
	var csv = [];
	var today = new Date();
	var filename = filename + '_' + today.getDate() + (today.getMonth() + 1) + today.getFullYear() + '_' + today.getHours() + today.getMinutes() + today.getSeconds() + '.csv';
	var tables = document.getElementById(table_id);
	var rows = tables.querySelectorAll("tr");
	if (rows.length > 1) {
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].style.display !== 'none') {
				var row = [], cols = rows[i].querySelectorAll("td, th");
				for (var j = 0; j < cols.length; j++)
					row.push(cols[j].innerText);
				csv.push(row.join(";"));
			}
		}
		downloadCSV(csv.join("\n"), filename);
	} else {
		alert('Δεν βρέθηκε πληροφορία. Ο πίνακας είναι άδειος');
	}
}