function formValidation(formId, submitBtnId, confModalId){
	var inputs = document.getElementById(formId).getElementsByTagName("input");
	var selects = document.getElementById(formId).getElementsByTagName("select");
	var required = getRequiredFields(inputs, selects);
	var passedCnt = 0;
	var checkedCnt = 0;
	if(validateRequired(required)){
		for(var i = 0; i < inputs.length; i++){
			if(inputs[i].hasAttribute("def")){
				var definitions = inputs[i].getAttribute("def").split(",");	//1st maxlen, 2nd minlen, 3rd type
				checkedCnt++;
				if(validateDefinitions(definitions, inputs[i])){
					passedCnt++;
				}
			}else
				continue;		
		}
		if(passedCnt == checkedCnt){
			document.getElementById(submitBtnId).setAttribute("data-dismiss", "modal");
			document.getElementById(submitBtnId).setAttribute("data-target", "#"+confModalId);
			document.getElementById(submitBtnId).setAttribute("data-toggle", "modal");
			document.getElementById(submitBtnId).click();
		}else{	
			document.getElementById(submitBtnId).removeAttribute("data-dismiss", "modal");		
		 	document.getElementById(submitBtnId).removeAttribute("data-target");
			document.getElementById(submitBtnId).removeAttribute("data-toggle");
		}

	}else{
		document.getElementById('toTopBtn').click();
		return;
	}

}

function validateRequired(requiredList){
	var passed = true;
	for(var i = 0; i < requiredList.length; i++){
		if(isBlank(requiredList[i])){
			blankError(requiredList[i]);
			passed = false;
		}else{
			removeError(requiredList[i]);
		}
	}
	return passed;
}

function validateDefinitions(defs, input){
	var elemValue = input.value;
	var maxLength = defs[0];
	var minLength = defs[1];
	var regKind = defs[2];
	if(elemValue.length > parseInt(maxLength) || elemValue.length < parseInt(minLength) && !elemValue == ""){
		if(parseInt(maxLength) == parseInt(minLength))
			message = '<i class="fas fa-exclamation-triangle mr-1"></i> Δέχετε ακριβως ' + maxLength + ' χαρακτήρες';
		else
			message = '<i class="fas fa-exclamation-triangle mr-1"></i> Από ' + minLength + ' έως ' + maxLength + ' χαρακτήρες επιτρέπονται.';
		printError(message, input);
		return false;
	}else if(regKind == 'letters' && (!/^[A-Za-zΆ-ωΑ-ώ ()]*$/.test(elemValue)) ){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Μόνο γράμματα, κενά και παρενθέσεις επιτρέπονται.'
		printError(message, input);
		return false;
	}else if(regKind == 'nums' && ( !/^\d*$/.test(elemValue)) ){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Μόνο αριθμοί επιτρέπονται.'
		printError(message, input);
		return false;
	}else if(regKind == 'email' && (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(elemValue)) && !elemValue == "" ){		
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Το email δεν έχει σωστή μορφή.';
		printError(message, input);
		return false;
	}else if(regKind == 'kid_phone' && (!/^\d{10}(, ?\d{10})*$/.test(elemValue)) && !elemValue == ""){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Σωστή μορφή (xxxxxxxxxx ή xxxxxxxxxx, xxxxxxxxxx)';
		printError(message, input);
		return false;
	}else if(regKind == 'alphanum' && (!/^[A-Za-zΆ-ωΑ-ώ0-9]*( [A-Za-zΆ-ωΑ-ώ0-9]+)*$/.test(elemValue)) ){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Μόνο αλφαριθμητικά και κενά επιτρέπονται.';
		printError(message, input);
		return false;
	}else if(regKind == 'nums-limits' && ( ( !/^\d*$/.test(elemValue)) || (parseInt(elemValue) > parseInt(maxLength)) || (parseInt(elemValue) < parseInt(minLength)))){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Μόνο ακέραιοι αριθμοί μεταξύ ' + minLength + ' και ' + maxLength + ' επιτρέπονται';
		printError(message, input);
		return false;
	}else if(regKind == 'password' && (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(elemValue)) ){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Απαιτούμενα Κωδικού:<ul><li> Τουλάχιστον 8 λατινικούς χαρακτήρες</li><li>'+
		 'Ένα κεφαλαίο γράμμα</li> <li>Ένα πεζό γράμμα</li> <li>Έναν αριθμό</li> <li>Ένα σύμβολο από τα !@#$%^&*</li></ul>';
		printError(message, input);
		return false;
	}else if(regKind.includes('repassword') &&  (document.getElementById(regKind.split("_")[1]).value !== elemValue) ){
		message = '<i class="fas fa-exclamation-triangle mr-1"></i> Οι κωδικοί δεν είναι ίδιοι.';
		printError(message, input);
		return false;
	}else{
		removeError(input);
		return true;
	}
}

function getRequiredFields(inputs, selects){
	var requiredElems = new Array();
	for(var i = 0; i < inputs.length; i++){
		if(inputs[i].hasAttribute('required')){
			requiredElems.push(inputs[i]);
		}
	}
	for(var i = 0; i < selects.length; i++){
		if(selects[i].hasAttribute('required')){
			requiredElems.push(selects[i]);
		}
	}
	return requiredElems;
}

function isBlank(elem){
	if(elem.value == '')
		return true;
	else
		return false;
}

function blankError(elem){
	elem.parentNode.getElementsByClassName('invalid-feedback')[0].innerHTML ='<i class="fas fa-exclamation-triangle mr-1"></i> Υποχρεωτικό Πεδίο';
	elem.classList.add("is-invalid");
}

function removeError(elem){
	elem.parentNode.getElementsByClassName('invalid-feedback')[0].innerHTML = '';
	elem.classList.remove("is-invalid");
}

function printError(message, elem){
	elem.parentNode.getElementsByClassName('invalid-feedback')[0].innerHTML = message;
	elem.classList.add("is-invalid");
}