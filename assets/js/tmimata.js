function clearTmimataFields(){	
	var test = document.getElementsByClassName('tmimata-entry');
	for(var i = 0; i < test.length; i++){
		test[i].checked = false;		
	}
}

function setValueToTmimataModal(fieldId){
	if(document.getElementById('tTeacherId'))
		document.getElementById('tTeacherId').value = fieldId;
	clearTmimataFields();
	var tmimata = document.getElementById(fieldId).value;
	var tmimataArr = tmimata.split(",");
	for(var i = 0; i < tmimataArr.length; i++){
		var val = tmimataArr[i].trim();
		$("input[value='" + val + "']").prop('checked', true);
	}
}

function saveValuesToTmimataField(fieldId){
	var values = "";	
	var test = document.getElementsByClassName('tmimata-entry');
	for(var i = 0; i < test.length; i++){
		if(test[i].checked == true)
			values += test[i].value + ", ";	
	}	
	console.log(fieldId);
	values = values.substring(0, values.length - 2);
	document.getElementById(fieldId).value = values;
	document.getElementById('closeBtn2').click();
}