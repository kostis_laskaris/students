var StudentGrade = /** @class */ (function () {
    function StudentGrade() {
    }
    StudentGrade.prototype.getGrades = function () {
        var xhttp = new XMLHttpRequest();
        var url = "../php_actions/test_grades_rest?studentId=" + this.id;
        var student = new StudentGrade();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var result = JSON.parse(this.responseText);
                student.cntGrade = 0;
                student.maxGrade = 0;
                student.minGrade = 101;
                student.present = 0;
                student.notPresent = 0;
                var sum = 0;
                for (var i = 0; i < result.length; i++) {
                    student.cntGrade++;
                    sum += parseInt(result[i].grade);
                    if (result[i].grade > student.maxGrade) {
                        student.maxGrade = result[i].grade;
                        student.maxGradeLesson = result[i].lessons_name;
                    }
                    if (result[i].grade < student.minGrade) {
                        student.minGrade = result[i].grade;
                        student.minGradeLesson = result[i].lessons_name;
                    }
                    if (result[i].absence == "ΠΑΡΩΝ") {
                        student.present++;
                    }
                    else {
                        student.notPresent++;
                    }
                }
                student.avgGrade_100 = sum / student.cntGrade;
                student.avgGrade_20 = student.avgGrade_100 * 20 / 100;
                console.log(student);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send();
        return student;
    };
    StudentGrade.prototype.getGradesByLesson = function () {
        console.log(this.minGrade);
        console.log(this.minGradeLesson);
    };
    return StudentGrade;
}());
