function editClass(id){
    //Adding spin
    addStandardWhirlAnime('animoSpin2');
    var xhttp = new XMLHttpRequest();
    var url = "../php_actions/get_class_for_edit.php?tmimaId=" + id;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            var result = JSON.parse(this.responseText);
            document.getElementById('myModalLabel1').innerHTML = "<em class='fas fa-chalkboard float-left fa-150x mr-2'></em>Επεξεργασία Τάξης με ID: " + result.id;
            document.getElementById('editTmima').value = result.name;
            document.getElementById('editTaksi').value = result.schoolClass;
            document.getElementById('comments').value = result.comments;   
            document.getElementById('classId').value = result.id;      
            
            //Removing spin
    		removeStandardWhirlAnime('animoSpin2');
        }               
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function getClassStudents(id){
	//Adding spin
    addStandardWhirlAnime('animoShowSpin');
	var xhttp = new XMLHttpRequest();
    var url = "../php_actions/get_class_students_action.php?tmimaId=" + id;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
        	var result = JSON.parse(this.responseText);
            var container = document.getElementById('classStudentContainer');
        	if(result == null){        		
            	document.getElementById('myModalLabel4').innerHTML = "Κενό Τμήμα";      		
	            container.innerHTML = "<p class='text-center'>Δεν υπάρχουν καταχωρημένοι μαθητές στο τμήμα.</p>";
    			removeStandardWhirlAnime('animoShowSpin');
	       		return;
        	} 
            document.getElementById('myModalLabel4').innerHTML = "<em class='fas fa-users float-left fa-150x mr-2'></em>Προβολή Μαθητών Τμήματος: <em>" + result[0].tmima + "</em>";
		    while (container.firstChild) {
		        container.removeChild(container.firstChild);
		    } 

		    for(var i = 0; i < result.length; i++){
	            var idspan = document.createElement("span");
	            idspan.setAttribute('class', 'fa-stack');
	            idspan.style.color = '#3b87e8';
	            idspan.style.fontSize = '130%';
	            idspan.innerHTML = '<i class="fas fa-user-graduate"></i>';
	            // id div
	            var idDiv = document.createElement("div");
	            idDiv.setAttribute('class', 'align-self-start mr-2');
	            idDiv.appendChild(idspan);
	            // p student name
	            var pName = document.createElement("p");
	            pName.setAttribute('class', 'mb-0');
	            pName.innerHTML = result[i].firstname + " " + result[i].lastname;
	            //p parents name
	            var date = document.createElement("p");
	            date.setAttribute('class', 'm-0');
	            date.style.fontStyle = 'italic';
	            var small = document.createElement('small');
	            small.innerHTML = Helpers.dateConverter(result[i].date_eggrafis) + " (" + Helpers.dateDiffInDays(new Date(), new Date(result[i].date_eggrafis)) + " ημέρες πριν)";
	            date.appendChild(small);
	            //name Div
	            var namesDiv = document.createElement("div");
	            namesDiv.setAttribute('class', 'media-body text-truncate');
	            namesDiv.appendChild(pName);
	            namesDiv.appendChild(date);
	            //select classes
	            var tmimaInput = document.createElement('input');
	            tmimaInput.setAttribute('type','text');            
	            tmimaInput.setAttribute('class', 'form-control');
	            tmimaInput.setAttribute('readonly', 'true');
	            tmimaInput.value = result[i].tmima;
	            //select classes div
	            var tmimaDiv = document.createElement("div");
	            tmimaDiv.setAttribute('class', 'ml-auto mr-2');
	            tmimaDiv.appendChild(tmimaInput);
	            //select taksi
	            var classInput = document.createElement('input');     
	            classInput.setAttribute('type','text');                   
	            classInput.setAttribute('class', 'form-control');
	            classInput.setAttribute('readonly', 'true');
	            classInput.value =result[i].class;
	            //select taksi div
	            var taksiDiv = document.createElement("div");
	            taksiDiv.setAttribute('class', 'mr-auto');
	            taksiDiv.appendChild(classInput);
	            // media div
	            var mediaDiv = document.createElement("div");
	            mediaDiv.setAttribute('class', 'media');
	            mediaDiv.appendChild(idDiv);
	            mediaDiv.appendChild(namesDiv);
	            mediaDiv.appendChild(tmimaDiv);
	            mediaDiv.appendChild(taksiDiv);

	            //create list div
	            var listDiv = document.createElement("div");
	            listDiv.setAttribute('class', 'list-group-item');
	            listDiv.appendChild(mediaDiv);
	            container.appendChild(listDiv);
         	} 
	        //Removing spin
    		removeStandardWhirlAnime('animoShowSpin');        	
        }	                        
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}