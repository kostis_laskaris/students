    window.onload = function(){
        datavars1 = new Array();
        labelVals1 = ["Γ Λυκειου", "Β Λυκείου", "Α Λυκείου", "Γ ΕΠΑΛ", "Β ΕΠΑΛ", "Α ΕΠΑΛ", "Απόφοιτοι", "Γ Γυμνασίου", "Β Γυμνασίου", "Α Γυνασίου"];
        datavars2 = new Array();
        labelVals2 = ["Λύκειο", "ΕΠΑΛ", "Απόφοιτοι", "Γυμνάσιο"];
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                datavars1.push(response[0].gluk); datavars1.push(response[0].bluk); datavars1.push(response[0].aluk); datavars1.push(response[0].gepal);
                datavars1.push(response[0].bepal); datavars1.push(response[0].aepal); datavars1.push(response[0].apof); datavars1.push(response[0].ggumn); 
                datavars1.push(response[0].bgumn); datavars1.push(response[0].agumn);

                datavars2.push(parseInt(response[0].gluk) + parseInt(response[0].bluk) + parseInt(response[0].aluk));
                datavars2.push(parseInt(response[0].gepal) + parseInt(response[0].bepal) + parseInt(response[0].aepal));
                datavars2.push(parseInt(response[0].apof));
                datavars2.push(parseInt(response[0].ggumn) + parseInt(response[0].bgumn) + parseInt(response[0].agumn));

                var pieData = {
                labels: labelVals1,
                datasets: [{
                    data: datavars1,
                    backgroundColor: ["rgba(128, 0, 0, 0.5)","rgba(170, 110, 40, 0.5)","rgba(128, 128, 0, 0.5)","rgba(0, 128, 128, 0.5)","rgba(230, 25, 75, 0.5)","rgba(245, 130, 48, 0.5)",
                        "rgba(255, 225, 25, 0.5)","rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)","rgba(0, 130, 200, 0.5)","rgba(145, 30, 180, 0.5)","rgba(240, 50, 230, 0.5)",
                        "rgba(128, 128, 128, 0.5)","rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
                    borderColor:["rgba(128, 0, 0, 1)","rgba(170, 110, 40, 1)","rgba(128, 128, 0, 1)","rgba(0, 128, 128, 1)","rgba(230, 25, 75, 1)","rgba(245, 130, 48, 1)",
                        "rgba(255, 225, 25, 1)","rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)","rgba(0, 130, 200, 1)","rgba(145, 30, 180, 1)","rgba(240, 50, 230, 1)",
                        "rgba(128, 128, 128, 1)","rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
                }]
                };
                var pieData2 = {
                    labels: labelVals2,
                    datasets: [{
                        data: datavars2,
                        backgroundColor: ["rgba(128, 0, 0, 0.5)","rgba(170, 110, 40, 0.5)","rgba(128, 128, 0, 0.5)","rgba(0, 128, 128, 0.5)","rgba(230, 25, 75, 0.5)","rgba(245, 130, 48, 0.5)",
                            "rgba(255, 225, 25, 0.5)","rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)","rgba(0, 130, 200, 0.5)","rgba(145, 30, 180, 0.5)","rgba(240, 50, 230, 0.5)",
                            "rgba(128, 128, 128, 0.5)","rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
                        borderColor:["rgba(128, 0, 0, 1)","rgba(170, 110, 40, 1)","rgba(128, 128, 0, 1)","rgba(0, 128, 128, 1)","rgba(230, 25, 75, 1)","rgba(245, 130, 48, 1)",
                            "rgba(255, 225, 25, 1)","rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)","rgba(0, 130, 200, 1)","rgba(145, 30, 180, 1)","rgba(240, 50, 230, 1)",
                            "rgba(128, 128, 128, 1)","rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
                    }]
                };

                var pieOptions = {
                    legend: {
                        display: false
                    }
                };
                var piectx = document.getElementById('chartjs-piechart').getContext('2d');
                var piectx2 = document.getElementById('chartjs-piechart2').getContext('2d');
                var pieChart = new Chart(piectx, {
                    data: pieData,
                    type: 'pie',
                    options: pieOptions
                });
                var pieChart2 = new Chart(piectx2, {
                    data: pieData2,
                    type: 'pie',
                    options: pieOptions
                });

                //CREATE STUDENTS TABLE
                var sum = 0;
                for(var i = 0; i < datavars2.length; i++){
                    sum += datavars2[i];
                }
                var ths = ["Τάξη", "Αριθμός Μαθητών", "Ποσοστό"];
                var tbl = document.getElementById("studentTable");
                var thead = document.createElement("thead");
                var tr = document.createElement("tr");
                for(var i = 0; i < ths.length; i++){
                    var th = document.createElement("th");
                    th.innerHTML = (ths[i]);
                    tr.appendChild(th);   
                }           
                ths = null;                
                thead.appendChild(tr);
                tbl.appendChild(thead);
                tbody = document.createElement("tbody");
                for(var i = 0; i < datavars1.length; i++){
                    //class
                    var tr = document.createElement("tr"); 
                    var td = document.createElement("td");
                    td.innerHTML = labelVals1[i];
                    tr.appendChild(td);  
                    //students num
                    var td = document.createElement("td");
                    td.innerHTML = datavars1[i];
                    tr.appendChild(td);    
                    //percentage
                    var td = document.createElement("td");
                    td.innerHTML = Math.round(datavars1[i]*100 / sum) + " %";
                    tr.appendChild(td);
                    tbody.appendChild(tr);              
                }          
                tbl.appendChild(tbody);
                $('#studentTable').DataTable({
                  "scrollCollapse": true,
                  "pagingType": "full_numbers",
                  'ordering': true,
                  'info': true,
                  "lengthMenu": [[5, -1], [5, "All"]],
                  responsive: true,
                  oLanguage: {
                      sSearch: 'Search all columns:',
                      sLengthMenu: '_MENU_ records per page',
                      info: 'Showing page _PAGE_ of _PAGES_',
                      zeroRecords: 'Nothing found - sorry',
                      infoEmpty: 'No records available',
                      infoFiltered: '(filtered from _MAX_ total records)',
                      oPaginate: {
                          sNext: '<em class="fa fa-caret-right"></em>',
                          sPrevious: '<em class="fa fa-caret-left"></em>'
                      }
                  },
                  dom: 'lfrtip'                            
              });                
            }
        };
        xhttp.open("POST", "../php_actions/get_actions/getStudentsPerClass.php", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send();
    }