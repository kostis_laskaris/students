window.onload = function(){
    datavars1 = new Array();
    labelVals1 = ["Μαρία Καμαράτου", "Ρίτα Ρούσα", "Γιάννα Βορδώνη", "Ηλίας Λάσκαρης", "Θανάσης Αρβανίτης", "Γιάννης Ρηγανάς", "Πατσουράκου Γιώτα", "Δημήτρης Σταυρίδης"];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            datavars1.push(response.kamar); datavars1.push(response.rita); datavars1.push(response.vord); datavars1.push(response.las);
            datavars1.push(response.arv);datavars1.push(response.rig); datavars1.push(response.pats); datavars1.push(response.stav);
            var pieData = {
                labels: labelVals1,
                datasets: [{
                    data: datavars1,
                    backgroundColor: ["rgba(128, 0, 0, 0.5)","rgba(170, 110, 40, 0.5)","rgba(128, 128, 0, 0.5)","rgba(0, 128, 128, 0.5)","rgba(230, 25, 75, 0.5)","rgba(245, 130, 48, 0.5)",
                    "rgba(255, 225, 25, 0.5)","rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)","rgba(0, 130, 200, 0.5)","rgba(145, 30, 180, 0.5)","rgba(240, 50, 230, 0.5)",
                    "rgba(128, 128, 128, 0.5)","rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
                    borderColor:["rgba(128, 0, 0, 1)","rgba(170, 110, 40, 1)","rgba(128, 128, 0, 1)","rgba(0, 128, 128, 1)","rgba(230, 25, 75, 1)","rgba(245, 130, 48, 1)",
                    "rgba(255, 225, 25, 1)","rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)","rgba(0, 130, 200, 1)","rgba(145, 30, 180, 1)","rgba(240, 50, 230, 1)",
                    "rgba(128, 128, 128, 1)","rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"]
                }]
            };
            var pieOptions = { 
                responsive: true,
                maintainAspectRatio: false,
                legend: {                   
                    display: false
                }
            };
            var piectx = document.getElementById('grades-chart').getContext('2d');
            var pieChart = new Chart(piectx, {
                data: pieData,
                type: 'pie',
                options: pieOptions
            });       
        }
    };
    xhttp.open("POST", "php_actions/getGradesPerTeacher.php", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();


    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            datavars = new Array();  
            labels = new Array();          
            var response = JSON.parse(this.responseText);
            for(var i = 0; i < response.length; i++){
                labels.push(response[i].lesson);
                datavars.push(response[i].avg.toFixed(2));
            }
            var barData = {
                labels: labels,
                datasets: [{
                    borderWidth: 1,
                    backgroundColor: ["rgba(128, 0, 0, 0.5)","rgba(170, 110, 40, 0.5)","rgba(128, 128, 0, 0.5)","rgba(0, 128, 128, 0.5)","rgba(230, 25, 75, 0.5)","rgba(245, 130, 48, 0.5)",
                    "rgba(255, 225, 25, 0.5)","rgba(210, 245, 60, 0.5)", "rgba(60, 180, 75, 0.5)","rgba(0, 130, 200, 0.5)","rgba(145, 30, 180, 0.5)","rgba(240, 50, 230, 0.5)",
                    "rgba(128, 128, 128, 0.5)","rgba(255, 215, 180, 0.5)", "rgba(170, 255, 195, 0.5)", "rgba(230, 190, 255, 0.5)"],
                    borderColor:["rgba(128, 0, 0, 1)","rgba(170, 110, 40, 1)","rgba(128, 128, 0, 1)","rgba(0, 128, 128, 1)","rgba(230, 25, 75, 1)","rgba(245, 130, 48, 1)",
                    "rgba(255, 225, 25, 1)","rgba(210, 245, 60, 1)", "rgba(60, 180, 75, 1)","rgba(0, 130, 200, 1)","rgba(145, 30, 180, 1)","rgba(240, 50, 230, 1)",
                    "rgba(128, 128, 128, 1)","rgba(255, 215, 180, 1)", "rgba(170, 255, 195, 1)", "rgba(230, 190, 255, 1)"],
                    data: datavars
                }]
            };

            var barOptions = {
                legend: {
                    display: false
                }
            };
            var barctx = document.getElementById('avg-grades-barchart').getContext('2d');
            var barChart = new Chart(barctx, {
                data: barData,
                type: 'bar',
                options: barOptions
            });

        }
    };
    xhttp2.open("POST", "php_actions/getAvgPerLesson.php", true);
    xhttp2.setRequestHeader("Content-type", "application/json");
    xhttp2.send();
}