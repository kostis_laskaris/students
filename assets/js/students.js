function showStudent(id){
    //Adding spin
    addStandardWhirlAnime('animoSpin');
    $.get( 
        "../php_actions/get_actions/get_student_for_view.php", 
        { 
            studentId: id, 
            csrf_token: $('#csrf_token').val() 
        } 
    ).done(function( data ) {
        var result = JSON.parse(data); 
        var email = result.email;  
            var emailKid = result.email_kidemona; 
            var addressRedirect = '';
            if(result.address =='' && result.tk =='')
                addressRedirect = '';
            else if(result.address !='' && result.tk =='')
                addressRedirect = result.address;
            else if(result.address =='' && result.tk !='')
                addressRedirect = result.tk;
            else
                addressRedirect = result.address + ", " + result.tk;
            document.getElementById('myModalLabelLarge2').innerHTML = result.lastname + " " + result.firstname + " (ID: " + result.id + ")";
            document.getElementById('phone').value = result.phone;
            document.getElementById('mobile').value = result.mobile;
            document.getElementById('email').value = email;
            configFormInputBtn('mailBtn', 0, email);
            document.getElementById('address').value = result.address;
            configFormInputBtn('addressBtn', 1, addressRedirect);
            document.getElementById('tk').value = result.tk;
            document.getElementById('perioxi').value = result.perioxi;
            document.getElementById('subDate').value = Helpers.dateConverter(result.date_eggrafis);
            document.getElementById('mname').value = result.mname;
            document.getElementById('fname').value = result.faname;
            document.getElementById('mphone').value = result.mphone;
            document.getElementById('fphone').value = result.fphone;
            document.getElementById('kidName').value = result.kid_name;
            document.getElementById('kidPhone').value = result.kid_phone;
            document.getElementById('kidEmail').value = result.email_kidemona;
            configFormInputBtn('mailBtn2', 0, emailKid);
            document.getElementById('fee').value = result.fee;
            document.getElementById('tmima').value = result.tmima;
            document.getElementById('lessons').value = result.lessons;
            document.getElementById('period').value = result.periodos;
            document.getElementById('schoolYear').value = result.sx_etos;
            document.getElementById('taksi').value = result.class;

            //Removing spin
            removeStandardWhirlAnime('animoSpin');
    });
}

function editStudent(id){
    //Adding spin
    addStandardWhirlAnime('animoSpin2');
    var xhttp = new XMLHttpRequest();
    var url = "../php_actions/get_student_for_edit.php?studentId=" + id;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            var result = JSON.parse(this.responseText);            
            document.getElementById('myModalLabelLarge3').innerHTML = "<em class='fas fa-user-edit float-left fa-150x mr-2'></em>" + "Επεξεργασία Μαθητή με ID: " + result.id;
            document.getElementById('firstname').value = result.firstname;
            document.getElementById('lastname').value = result.lastname;
            document.getElementById('editPhone').value = result.phone;
            document.getElementById('editMobile').value = result.mobile;
            document.getElementById('editEmail').value = result.email;
            document.getElementById('editAddress').value = result.address;
            document.getElementById('editTk').value = result.tk;
            document.getElementById('editPerioxi').value = result.perioxi;
            document.getElementById('editSubDate').value = dateFixer(result.date_eggrafis, "-");
            document.getElementById('editMname').value = result.mname;
            document.getElementById('editFname').value = result.faname;
            document.getElementById('editMphone').value = result.mphone;
            document.getElementById('editFphone').value = result.fphone;
            document.getElementById('editKidName').value = result.kid_name;
            document.getElementById('editKidPhone').value = result.kid_phone;
            document.getElementById('editKidEmail').value = result.email_kidemona;
            document.getElementById('editFee').value = result.fee;
            document.getElementById('editTmima').value = result.tmima;
            document.getElementById('editPeriod').value = result.periodos;
            document.getElementById('editSchoolYear').value = result.sx_etos;
            document.getElementById('editTaksi').value = result.class;
            document.getElementById('studentId').value = result.id;

            //put ids and lessons values
            var tmp = result.lessons.split(",");
            var ids = '';
            var lessons = '';
            for(var i = 0; i < tmp.length; i++){
                var tmp2 = tmp[i].split("_");
                for(var j = 0; j < tmp2.length; j++){
                    if(j/2!=0)
                        ids += tmp2[j]+",";
                    else
                        lessons += tmp2[j]+",";
                }
            }
            ids = ids.substring(0, ids.length - 1);
            lessons = lessons.substring(0, lessons.length - 1);
            document.getElementById('editLessons').value = lessons;
            
            //Removing spin
            removeStandardWhirlAnime('animoSpin2');
        }               
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function getStudentClassAndTmima(id){
    var xhttp = new XMLHttpRequest();
    var url = "../php_actions/get_class_and_tmima_of_student.php?studentId=" + id;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            var result = JSON.parse(this.responseText)
            document.getElementById('class').value = result.class;
            document.getElementById('tmima').value = result.tmima; 
            $("#lesson").empty();   //clear lessons list
            for(var i = 0; i < result.lessons.length; i++){
                var x = document.getElementById("lesson");
                var option = document.createElement("option");
                option.text = result.lessons[i].name;
                option.value = result.lessons[i].lesson_id;
                x.add(option); 
            }
            document.getElementById('lesson').disabled = false;
        }          
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function getStudentsPerSchoolYear(schoolYear, selectedText){
    addStandardWhirlAnime('addAnimate');
    var xhttp = new XMLHttpRequest();
    if(selectedText == "Επιλογή"){
        document.getElementById('addAnimate').classList.remove("whirl");
        document.getElementById('addAnimate').classList.remove("standard");      
        $('#studentsResubTbl').dataTable().fnClearTable(); 
        $('#studentsResubTbl').dataTable().fnDestroy();        
        $('#studentsResubTbl').empty();
        document.getElementById('tblContainer').style.display = 'none';
        document.getElementById('saveResub').style.display = 'none';
        return;
    }
    var url = "php_actions/getStudentsFromSchoolYear.php?tbl=" + schoolYear + "&schoolYear=" + selectedText;
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            var result = JSON.parse(this.responseText);
            var thead = document.createElement("thead");
            var ths = ["SID", "Όνομα", "Επίθετο", "Όνομα Πατέρα", "Όνομα Μητέρας", "Επιλογή"];            
            var tr = document.createElement("tr");
            for(var i = 0; i < ths.length; i++){
                var th = document.createElement("th");
                th.innerHTML = ths[i];
                tr.appendChild(th);
            }
            thead.appendChild(tr);
            var tbody = document.createElement("tbody");
            for(var i = 0; i< result.length; i++){
                var tr = document.createElement("tr");
                tr.setAttribute('id', result[i].id);
                var td = document.createElement("td");  //id
                td.innerHTML = result[i].id;
                tr.appendChild(td);
                var td = document.createElement("td");  //firstname
                td.innerHTML = result[i].firstname;
                tr.appendChild(td);
                var td = document.createElement("td");  //lastname
                td.innerHTML = result[i].lastname;
                tr.appendChild(td);
                var td = document.createElement("td");  //faname
                td.innerHTML = result[i].faname;
                tr.appendChild(td);
                var td = document.createElement("td");  //mname
                td.innerHTML = result[i].mname;
                tr.appendChild(td);
                var td = document.createElement("td");  //checkbox
                td.appendChild(createCheckTd(result[i].id));
                tr.appendChild(td);
                tbody.appendChild(tr);
            }
            document.getElementById('studentsResubTbl').appendChild(thead);
            document.getElementById('studentsResubTbl').appendChild(tbody);
            document.getElementById('tblContainer').style.display = 'block';
            var table = $('#studentsResubTbl').DataTable({
                "pagingType": "full_numbers",
                "scrollCollapse": true,
                "lengthMenu": [[-1], ["All"]],
                responsive: true,
                oLanguage: {
                    sSearch: 'Αναζήτηση παντού:',
                    sLengthMenu: '_MENU_ στοιχεία ανά σελίδα',
                    info: 'Προβολή σελίδα _PAGE_ από _PAGES_',
                    zeroRecords: 'Nothing found - sorry',
                    infoEmpty: 'Καμία εγγραφή - Ο πίνακας είναι άδειος',
                    infoFiltered: '(Φιλτράρισμα από _MAX_ συνολικές εγγραφές)'                   
                },
                dom: 'fltip'
            });    
            jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');        
            removeStandardWhirlAnime('addAnimate');
            document.getElementById('saveResub').style.display = 'block';
        }               
    };
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function createCheckTd(id){
    var input = document.createElement("input");
    input.setAttribute('type', 'checkbox');
    input.addEventListener( 'change', function() {
        if(this.checked) {
            document.getElementById(id).classList.add('bg-primary-light');
        } else {
            document.getElementById(id).classList.remove('bg-primary-light');
        }
    });
    var span = document.createElement("span");
    span.setAttribute('class', 'fa fa-check');
    var label = document.createElement("label");
    label.appendChild(input);
    label.appendChild(span);
    var div = document.createElement("div");
    div.setAttribute('class', 'checkbox c-checkbox');
    div.appendChild(label);
    return div;
}

function doReSub(json, db, s_year){
    //document.getElementById('animoSpin2').classList.add("whirl");
    //document.getElementById('animoSpin2').classList.add("standard");
    $.ajax({
        type:"POST",
        data:{data:json, tbl_name: db, school_year: s_year},
        url:"php_actions/resub.php",
        success: function(result){
            var result = JSON.parse(result);
            document.getElementById('closeResubModal').click();
            if(result.code == 0){
                document.getElementById('errorPopup').setAttribute("data-message", result.msg);
                document.getElementById('errorPopup').click();
            }else if(result.code == 1){
                document.getElementById('warningPopup').setAttribute("data-message", result.msg);               
                document.getElementById('warningPopup').click();
            }else{
                document.getElementById('successPopup').setAttribute("data-message", result.msg);         
                document.getElementById('successPopup').click();
            }
            var checks = document.getElementById('studentsResubTbl').getElementsByTagName('input');
            for(var i = 0; i < checks.length; i++){
                checks[i].checked = false;
                checks[i].parentNode.parentNode.parentNode.parentNode.classList.remove('bg-primary-light');
            }
        }
    });    
}

function dateFixer(dateString, seperator){
    var tempDate = dateString.split(seperator);
    if(parseInt(tempDate[1]) < 10 && tempDate[1].length <2)
        tempDate[1] = "0" + tempDate[1];
    if(parseInt(tempDate[2]) < 10 && tempDate[1].length <2)
        tempDate[2] = "0" + tempDate[2];
    return tempDate[0] + "-" + tempDate[1] + "-" + tempDate[2];
}

function googleRedirection(location){
	var link = "https://www.google.com/maps?q=" + location;
	window.open(link, '_blank');
}

function openMailClient(mail){
	var link = "mailto:" + mail;
	window.open(link, '_parent');
}	

function configFormInputBtn(btnId, kind, data){
	if(kind == 0){		//0 = Email
     if(data == '' || data == null){
      document.getElementById(btnId).disabled = true;
      document.getElementById(btnId).style.cursor = 'not-allowed';
      document.getElementById(btnId).setAttribute('title', 'No Data');
  }else{
      document.getElementById(btnId).disabled = false;
      document.getElementById(btnId).style.cursor = 'pointer';
      document.getElementById(btnId).setAttribute('onclick', 'openMailClient("'+data+'")');
      document.getElementById(btnId).setAttribute('title', 'Αποστολή Email');
  }
    }else{		//1 = address
    	if(data == '' || data == null){
    		document.getElementById(btnId).disabled = true;
    		document.getElementById(btnId).style.cursor = 'not-allowed';
    		document.getElementById(btnId).setAttribute('title', 'No Data');
    	}else{
    		document.getElementById(btnId).disabled = false;
    		document.getElementById(btnId).style.cursor = 'pointer';
    		document.getElementById(btnId).setAttribute('onclick', 'googleRedirection("'+data+'")');
    		document.getElementById(btnId).setAttribute('title', 'Δείτε το στο χάρτη');
    	}
    }
}